# Docker image

## Build image
before building a new image remove any existing container
> docker stop mandatewebdynamic
> docker rm mandatewebdynamic

Then run
> docker build -t mandate-web-dynamic:dev .

## Run container

>docker run -p 444:3000 -d --name mandatewebdynamic mandate-web-dynamic:dev

Now you can access the mandate-web-dynamic at https://localhost
You have to ignore the security warning.

##Tag
You can change the tag (dev) in the above commands to a more logical one