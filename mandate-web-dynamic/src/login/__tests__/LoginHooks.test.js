// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import useLogin from "../LoginHooks";
import React from "react";
import { act } from "react-dom/test-utils";
import ReactDOM from "react-dom";

let result;

function TestComponent(props) {
  result = useLogin(props);
  return null;
}

let containerTest;

beforeEach(() => {
  containerTest = document.createElement("div");
  document.body.appendChild(containerTest);
});

afterEach(() => {
  document.body.removeChild(containerTest);
  containerTest = null;
});

describe("<LoginHooks />", () => {
  test("Should have proper initial values", () => {
    act(() => {
      ReactDOM.render(
        <TestComponent
          authenticate={jest.fn()}
          setAuthInProgress={jest.fn()}
        />,
        containerTest
      );
    });

    expect(result.proofSocketResponse).toEqual({
      state: "uninitiated",
      value: "",
    });
    expect(result.showQRCode).toBeFalsy();
  });

  test("Should handle show qr code modal", () => {
    act(() => {
      ReactDOM.render(
        <TestComponent
          authenticate={jest.fn()}
          setAuthInProgress={jest.fn()}
        />,
        containerTest
      );
      result.handleShowQrCodeModal();
    });
    expect(result.showQRCode).toBeTruthy();
  });
});
