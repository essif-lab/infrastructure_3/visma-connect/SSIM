// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import AuthCallback from "../../AuthCallback";
import axios from "axios";
import { act } from "react-dom/test-utils";
import ReactDOM from "react-dom";

jest.mock("axios", () => ({
  ...jest.requireActual("axios"),
  post: jest.fn(),
}));

let result;
let authenticateCalled = false;
const props = {
  authenticate: () => (authenticateCalled = true),
};

function TestComponent() {
  result = AuthCallback(props);
  return null;
}

let containerTest;

beforeEach(() => {
  containerTest = document.createElement("div");
  document.body.appendChild(containerTest);
});

afterEach(() => {
  document.body.removeChild(containerTest);
  containerTest = null;
});

describe("<AuthCallback />", () => {
  test("Should fetch token", async () => {
    authenticateCalled = false;
    const data = {
      data: {
        access_token: "someToken",
      },
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(data));

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
    });
    await expect(authenticateCalled).toBeTruthy();
  });

  test("Should fetch token with empty response", async () => {
    authenticateCalled = false;
    const data = {
      data: null,
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(data));

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
    });
    await expect(authenticateCalled).toBeFalsy();
  });

  test("Should fetch token with empty response", async () => {
    authenticateCalled = false;
    const data = {
      data: {
        otherValue: "value",
      },
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(data));

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
    });
    await expect(window.sessionStorage["access_token"]).toBe("undefined");
  });

  test("Should fetch token with error", async () => {
    axios.post.mockImplementationOnce(() => Promise.reject(new Error("error")));

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
    });
  });
});
