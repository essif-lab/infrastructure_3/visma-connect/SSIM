// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useEffect } from "react";
import "./LoginScreen.scss";
import qs from "query-string";
import AdaptiveSpinnerComponent from "common/AdaptiveSpinnerComponent";
import SnakeSpinner from "../styles/nc4/img/spinner-snake-blue-light.svg";
import VismaLogo from "../styles/nc4/img/visma-logo.svg";
import axios from "axios";

const AuthCallback = ({ authenticate }) => {
  useEffect(() => {
    fetchAccessToken();
  }, []);

  const fetchAccessToken = () => {
    const tokenUrl = "https://connect.identity.stagaws.visma.com/connect/token";
    const currentQueryParams = qs.parse(window.location.search);
    const authCode = currentQueryParams["code"];
    const codeVerifier = sessionStorage["code_verifier"];
    const authRedirectUri = process.env.REACT_APP_LOGIN_REDIRECT_URI;

    const queryParams = new URLSearchParams({
      client_id: "ssi-platform",
      grant_type: "authorization_code",
      redirect_uri: authRedirectUri,
      code: authCode,
      code_verifier: codeVerifier,
    }).toString();
    const config = {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    };

    axios
      .post(tokenUrl, queryParams, config)
      .then((result) => {
        if (result.data) {
          sessionStorage.setItem("access_token", result.data["access_token"]);
          if (sessionStorage["access_token"] !== "undefined") {
            authenticate();
          }
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };

  return (
    <div className="login-wrapper">
      <div className="login-box">
        <div className="login-header">
          <div className="logo">Logo placeholder</div>
          <h1>SSI Platform</h1>
        </div>
        <div className="d-flex flex-column text-center">
          <AdaptiveSpinnerComponent
            id="loading"
            spinner={SnakeSpinner}
            divisionMultiplier={2}
            fullWidth={true}
          />
        </div>
        <div className="login-footer">
          <img src={VismaLogo} alt="Visma Logo" />
        </div>
      </div>
    </div>
  );
};

export default AuthCallback;
