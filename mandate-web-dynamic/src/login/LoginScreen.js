// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useState } from "react";
import "./LoginScreen.scss";
import VismaLogo from "../styles/nc4/img/visma-logo.svg";
import useLogin from "./LoginHooks";
import MandateVerificationQRResult from "verifier/MandateVerificationQRResult";
import pkceChallenge from "pkce-challenge";

const LoginScreen = ({ authenticate, setAuthInProgress }) => {
  const {
    handleShowQrCodeModal,
    handleSubmit,
    proofSocketResponse,
    showQRCode,
    showEmailInput,
    setShowEmailInput,
  } = useLogin(authenticate);

  const handleLogin = () => {
    setAuthInProgress(true);
    const pkceDetails = pkceChallenge();
    sessionStorage.setItem("code_verifier", pkceDetails.code_verifier);
    const authRedirectUri = process.env.REACT_APP_LOGIN_REDIRECT_URI;
    const clientId = process.env.REACT_APP_CLIENT_ID;
    const queryParams = new URLSearchParams({
      client_id: clientId,
      response_type: "code",
      scope: "openid email profile",
      redirect_uri: authRedirectUri,
      code_challenge: pkceDetails.code_challenge,
      code_challenge_method: "S256",
    }).toString();
    const authUrl = `https://connect.identity.stagaws.visma.com/connect/authorize?${queryParams}`;
    window.location.assign(authUrl);
  };

  const showEmailInputField = () => {
    setShowEmailInput(true);
  };

  return (
    <div className="login-wrapper">
      {showQRCode ? (
        <MandateVerificationQRResult
          proofSocketResponse={proofSocketResponse}
          show={showQRCode}
          onHide={handleShowQrCodeModal}
          onFinish={authenticate}
        />
      ) : (
        <div className="login-box">
          <div className="login-header">
            <div className="logo">Logo placeholder</div>
            <h1>SSI Platform</h1>
          </div>
          {showEmailInput ? (
            <div>
              <div>Enter e-mail</div>
              <input
                className="login-email w-100"
                type="email"
                value="test@test.com"
                disabled
              />
              <button
                type="button"
                className="btn btn-primary btn-lg font-20"
                onClick={handleSubmit}
                id="login_continue_with_qr_button"
              >
                Continue
              </button>
            </div>
          ) : (
            <div className="d-flex flex-column align-items-center">
              <button
                type="button"
                className="btn btn-primary btn-lg font-20"
                onClick={handleLogin}
                id="login_button"
              >
                Log in
              </button>
              <button
                type="button"
                className="btn btn-primary btn-lg font-20"
                onClick={showEmailInputField}
                id="login_with_qr_button"
              >
                Log in with wallet app
              </button>
            </div>
          )}

          <div className="login-footer">
            <img src={VismaLogo} alt="Visma Logo" />
          </div>
        </div>
      )}
    </div>
  );
};

export default LoginScreen;
