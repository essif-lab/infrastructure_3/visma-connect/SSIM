// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { useEffect, useState } from "react";
import SockJS from "sockjs-client";
import Stomp from "stompjs";

const useLogin = (authenticate) => {
  const schemaName = "mandate-login-schema:1.0";
  const API_DOMAIN = process.env.REACT_APP_DOMAIN || "unknown";
  const requestJson = [{ credential: "email" }, { credential: "role" }];

  const [showQRCode, setShowQRCode] = useState(false);
  const [stompClient] = useState(Stomp.over(new SockJS(API_DOMAIN + "/ws")));
  const [proofSocketResponse, setProofSocketResponse] = useState({
    state: "uninitiated",
    value: "",
  });
  const [showEmailInput, setShowEmailInput] = useState(false);

  useEffect(() => {
    stompClient.connect({}, function (frame) {
      console.log("Connected: " + frame);

      stompClient.subscribe("/user/present-proof/response", function (message) {
        let messageBody = JSON.parse(message.body);
        if (messageBody && messageBody.state) {
          if (
            messageBody.state === "succeeded" &&
            messageBody.response !== ""
          ) {
            sessionStorage.setItem("access_token", messageBody.response);
            authenticate();
          } else {
            setProofSocketResponse((prevState) => ({
              ...prevState,
              state: messageBody.state,
            }));
            if (messageBody.state === "qrcode") {
              setProofSocketResponse((prevState) => ({
                ...prevState,
                value: atob(messageBody.response),
              }));
            }
            if (messageBody.state === "error") {
              setProofSocketResponse((prevState) => ({
                ...prevState,
                value: messageBody.response,
              }));
            }
          }
        }
      });
    });
    return () => {
      stompClient.disconnect();
    };
  }, [stompClient]);

  const handleSubmit = (event) => {
    if (event) {
      event.preventDefault();
      setProofSocketResponse((prevState) => ({
        ...prevState,
        state: "processing",
      }));

      console.log("JSON request vars >>>>>>", requestJson);
      handleShowQrCodeModal();

      const requestObject = JSON.stringify({
        schemaName: schemaName,
        jsonRequest: btoa(JSON.stringify(requestJson)),
        comment: "test@test.com", // TODO: get this from input field just before generating QR code
      });

      console.log("Login Request Json >>>>>>", requestObject);
      stompClient.send("/proof/request", {}, requestObject);
    }
  };

  const handleShowQrCodeModal = () => {
    setShowQRCode(!showQRCode);
    setShowEmailInput(false);
  };

  return {
    handleSubmit,
    handleShowQrCodeModal,
    showQRCode,
    proofSocketResponse,
    showEmailInput,
    setShowEmailInput,
  };
};
export default useLogin;
