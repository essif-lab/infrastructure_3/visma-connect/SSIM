// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow, mount } from "enzyme";
import { Route } from "react-router";
import App from "../App";
import Home from "../home/Home";
import MandateRequestForm from "../request/MandateRequestForm";
import MandateVerifierForm from "../verifier/MandateVerifierForm";
import LoginScreen from "login/LoginScreen";
import Header from "navbar/Header";
import AuthCallback from "login/AuthCallback";
import ProtectedRoute from "common/protectedRoute/ProtectedRoute";
import { act } from "react-dom/test-utils";

describe("User Logged in", () => {
  test("Should show main app container with class App", () => {
    const component = mount(<App />);
    component.find(LoginScreen).prop("authenticate").call();
  });

  test("Should handle logout", () => {
    window.sessionStorage.setItem("access_token", "someToken");
    const component = mount(<App />);
    const header = component.find(Header);
    expect(header.exists()).toBeTruthy();
    act(() => {
      header.prop("handleLogout").call();
    });
    component.update();
    expect(component.find(Header).exists()).toBeFalsy();
    window.sessionStorage.clear();
  });
});

let routeMap = {};

describe("Routing using array of routers", () => {
  let component;
  test("Should be Login component for /login route", () => {
    component = shallow(<App />);
    expect(
      component
        .find(Route)
        .filter({ path: "/login" })
        .renderProp("render")()
        .find(LoginScreen)
    ).toBeDefined();
  });

  test("Should be AuthCallback component for /oauthcallback route", () => {
    component = shallow(<App />);
    expect(
      component
        .find(Route)
        .filter({ path: "/oauthcallback" })
        .renderProp("render")()
        .find(AuthCallback)
    ).toBeDefined();
  });

  beforeAll(() => {
    component = shallow(<App />);
    routeMap = component.find(ProtectedRoute).reduce((routeMap, route) => {
      const routeProperties = route.props();
      routeMap[routeProperties.path] = routeProperties.component;
      return routeMap;
    }, {});
  });

  test("Should show Home screen for / router", () => {
    expect(routeMap["/"]).toBeDefined();
    expect(routeMap["/"]).toBe(Home);
  });

  test("Should show Home component for /home router", () => {
    expect(routeMap["/home"]).toBeDefined();
    expect(routeMap["/home"]).toBe(Home);
  });

  test("Should show MandateRequestForm component for /mandateRequest router", () => {
    expect(routeMap["/mandateRequest"]).toBeDefined();
    expect(routeMap["/mandateRequest"]).toBe(MandateRequestForm);
  });

  test("Should show MandateVerifierForm component for /mandateVerifier router", () => {
    expect(routeMap["/mandateVerifier"]).toBeDefined();
    expect(routeMap["/mandateVerifier"]).toBe(MandateVerifierForm);
  });

  test("Should NOT show any component for undefined router", () => {
    expect(routeMap["/abc"]).not.toBeDefined();
  });
});
