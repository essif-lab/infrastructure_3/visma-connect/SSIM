// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import ReactDOM from "react-dom";
import App from "../App";
import React from "react";
import "../index";

jest.mock("react-dom", () => ({
  render: jest.fn(),
}));

describe("Index", () => {
  test("Should render page without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<App />, div);
    global.document.getElementById = (id) => id === "root" && div;
    expect(ReactDOM.render).toHaveBeenCalledWith(<App />, div);
  });
});
