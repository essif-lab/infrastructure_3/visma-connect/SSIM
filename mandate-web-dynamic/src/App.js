// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./home/Home";
import MandateRequestForm from "./request/MandateRequestForm";
import MandatetVerifierForm from "./verifier/MandateVerifierForm";
import MandateRevocationForm from "./revocation/MandateRevocationForm";
import MandateSchemasForm from "./schemas/MandateSchemasForm";
import LoginScreen from "./login/LoginScreen";
import Header from "./navbar/Header";
import AuthCallback from "./login/AuthCallback";
import ProtectedRoute from "./common/protectedRoute/ProtectedRoute";

/**
 *
 * @return {JSX.Element}
 * @constructor
 */

function App() {
  const [isAuth, setIsAuth] = useState(
    Boolean(window.sessionStorage.access_token)
  );
  const [authInProgress, setAuthInProgress] = useState(false);

  const handleLogout = () => {
    setIsAuth(false);
    sessionStorage.clear();
  };

  const authenticate = () => {
    setAuthInProgress(false);
    window.location.href = process.env.REACT_APP_URI;
  };

  return (
    <div className="App">
      <Router>
        {isAuth && !authInProgress && <Header handleLogout={handleLogout} />}
        <Switch>
          <Route
            exact
            path="/login"
            render={(props) => (
              <LoginScreen
                {...props}
                authenticate={authenticate}
                setAuthInProgress={setAuthInProgress}
              />
            )}
          />

          <Route
            exact
            path="/oauthcallback"
            render={(props) => (
              <AuthCallback {...props} authenticate={authenticate} />
            )}
          />
          <ProtectedRoute exact path="/" component={Home} isAuth={isAuth} />
          <ProtectedRoute exact path="/home" component={Home} isAuth={isAuth} />
          <ProtectedRoute
            path="/mandateRequest"
            component={MandateRequestForm}
            isAuth={isAuth}
          />
          <ProtectedRoute
            path="/mandateVerifier"
            component={MandatetVerifierForm}
            isAuth={isAuth}
          />
          <ProtectedRoute
            path="/mandateRevocation"
            component={MandateRevocationForm}
            isAuth={isAuth}
          />
          <ProtectedRoute
            path="/mandateSchemas"
            component={MandateSchemasForm}
            isAuth={isAuth}
          />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
