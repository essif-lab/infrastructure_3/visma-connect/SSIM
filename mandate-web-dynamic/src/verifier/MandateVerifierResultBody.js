// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React from "react";
import AdaptiveQRComponent from "../common/AdaptiveQRComponent";
import AdaptiveSpinnerComponent from "../common/AdaptiveSpinnerComponent";
import SnakeSpinner from "../styles/nc4/img/spinner-snake-blue-light.svg";
import DottedSpinner from "../styles/nc4/img/spinner-dotted-blue-light.svg";

/**
 * @author Kevin Kerkhoven
 * @description Result code snippet which will be refreshed once a result has been returned from the websocket
 * @Return {JSX.Element}
 */
const MandateVerifierResultBody = ({ proofSocketResponse }) => {
  return (
    <React.Fragment>
      <div className="row justify-content-center">
        {proofSocketResponse === null || proofSocketResponse === undefined ? (
          <div className="col text-center">
            <div
              id="alert-error-unexpected"
              className="alert alert-danger"
              role="alert"
            >
              <strong>Error!</strong> Unexpected error occurred. Please
              regenerate the verification request and try again.
            </div>
          </div>
        ) : proofSocketResponse.state === "error" ? (
          <div className="col text-center">
            <div id="alert-error" className="alert alert-danger" role="alert">
              <strong>Error!</strong>
              {" " + proofSocketResponse.value}
            </div>
          </div>
        ) : proofSocketResponse.state === "qrcode" ? (
          <div className="col text-center">
            <AdaptiveQRComponent
              qrValue={proofSocketResponse.value}
              id="qrcode"
            />
          </div>
        ) : proofSocketResponse.state === "loading" ? (
          <div className="col text-center">
            <AdaptiveSpinnerComponent
              id="loading"
              spinner={SnakeSpinner}
              divisionMultiplier={4}
            />
          </div>
        ) : proofSocketResponse.state === "succeeded" ? (
          <div className="col text-center">
            <div
              id="alert-received"
              className="alert alert-success"
              role="alert"
            >
              <strong>Success!</strong> The user's credentials meet your proof
              request!
            </div>
          </div>
        ) : proofSocketResponse.state === "failed" ? (
          <div className="col text-center">
            <div id="alert-failed" className="alert alert-danger" role="alert">
              <strong>Failed!</strong> The user's credentials did not meet your
              proof request!
            </div>
          </div>
        ) : proofSocketResponse.state === "timed-out" ? (
          <div className="col text-center">
            <div
              id="alert-timed-out"
              className="alert alert-warning"
              role="alert"
            >
              The request timed out. Please regenerate the verification request
              and try again.
            </div>
          </div>
        ) : proofSocketResponse.state === "policy-failed" ? (
          <div className="col text-center">
            <div
              id="alert-policy-failed"
              className="alert alert-danger"
              role="alert"
            >
              <strong>Failed!</strong> The user's credentials did not meet your
              schema's attached policy!
            </div>
          </div>
        ) : (
          //This state can happen if the QR code is not loaded yet
          <div className="col text-center">
            <AdaptiveSpinnerComponent
              id="other"
              spinner={DottedSpinner}
              divisionMultiplier={4}
            />
          </div>
        )}
      </div>
    </React.Fragment>
  );
};

export default MandateVerifierResultBody;
