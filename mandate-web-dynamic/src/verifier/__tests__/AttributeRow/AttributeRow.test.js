// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow, mount } from "enzyme";
import AttributeRow from "../../AttributeRow";
import React from "react";
import { act } from "react-dom/test-utils";

describe("<AttributeRow /> with Signing Basic schema", () => {
  let handlePredicateChangeCalled = false;
  let handleConditionChangeCalled = false;

  const props = {
    inputs: {
      attributes: {
        "spending-limit": {
          predicateValue: ">",
          predicateTitle: "Greater than",
          condition: 10,
          disabled: false,
        },
      },
    },
    attribute: {
      name: "spending-limit",
      title: "Spending limit",
      type: "number",
      predicate: {
        name: "spending-limit",
        type: "number",
        condition: [
          {
            title: "Greater than",
            value: ">",
          },
        ],
      },
    },
    handleConditionChange: () => (handleConditionChangeCalled = true),
    handleCheckboxChange: jest.fn(),
    handlePredicateChange: () => (handlePredicateChangeCalled = true),
    conditionError: {},
  };
  const container = shallow(<AttributeRow {...props} />);

  test("Should handle predicate select with existing predicate", () => {
    const event = {
      target: {
        id: "dropdown-basic-button-mandate-signing-basic",
        innerHTML: "Signing Basic Schema",
      },
      preventDefault: jest.fn(),
    };
    container
      .find("a[id='dropdown-pred-spending-limit-button-greater-than']")
      .simulate("click", event);
    expect(
      container.find("div[className='w-100 dropdown ']").exists()
    ).toBeTruthy();
    expect(handlePredicateChangeCalled).toBeTruthy();
  });

  test("Should handle predicate select with None predicate", () => {
    const event = {
      target: {
        id: "dropdown-basic-button-mandate-signing-basic",
        innerHTML: "None",
      },
      preventDefault: jest.fn(),
    };
    container
      .find("a[id='dropdown-pred-spending-limit-button-none']")
      .simulate("click", event);
    expect(
      container.find("div[className='w-100 dropdown ']").exists()
    ).toBeTruthy();
  });

  test("Should handle condition change", () => {
    const event = {
      target: {
        value: "100",
      },
    };
    container.find("#form-condition").simulate("change", event);
    expect(handleConditionChangeCalled).toBeTruthy();
  });
});

describe("<AttributeRow /> with Signing Basic schema - no conditions", () => {
  const props = {
    inputs: {
      attributes: {
        "spending-limit": {
          predicateValue: ">",
          predicateTitle: "Greater than",
          condition: 10,
          disabled: false,
        },
      },
    },
    attribute: {
      name: "spending-limit",
      title: "Spending limit",
      type: "number",
      predicate: {
        name: "spending-limit",
        type: "number",
        condition: [],
      },
    },
    handleConditionChange: jest.fn(),
    handleCheckboxChange: jest.fn(),
    handlePredicateChange: jest.fn(),
    conditionError: {},
  };
  const container = shallow(<AttributeRow {...props} />);

  test("Should have only None in condition dropdown", () => {
    const dropdownButtons = container
      .find("ul[id='dropdown-pred-menu-list']")
      .children();
    expect(dropdownButtons.length).toBe(1);
    expect(dropdownButtons.at(0).text()).toBe("None");
  });
});

describe("<AttributeRow /> with useEffect", () => {
  const props = {
    inputs: {
      attributes: {
        "spending-limit": {
          predicateValue: ">",
          predicateTitle: "Greater than",
          condition: 10,
          disabled: false,
        },
      },
    },
    attribute: {
      name: "spending-limit",
      title: "Spending limit",
      type: "number",
      predicate: {
        name: "spending-limit",
        type: "number",
        condition: [
          {
            title: "Greater than",
            value: ">",
          },
        ],
      },
    },
    handleConditionChange: jest.fn(),
    handleCheckboxChange: jest.fn(),
    handlePredicateChange: jest.fn(),
    conditionError: {},
  };

  test("Should handle mousedown listener", () => {
    jest
      .spyOn(document, "addEventListener")
      .mockImplementation(() => jest.fn());
    jest
      .spyOn(document, "removeEventListener")
      .mockImplementation(() => jest.fn());
    const container = mount(<AttributeRow {...props} />);
    expect(document.addEventListener).toHaveBeenLastCalledWith(
      "mousedown",
      expect.any(Function)
    );
    act(() => {
      container.unmount();
    });
    expect(document.removeEventListener).toHaveBeenLastCalledWith(
      "mousedown",
      expect.any(Function)
    );
  });

  test("Should handle click outside when dropdown ref does not contain target", async () => {
    const map = {};
    document.addEventListener = jest.fn((event, callback) => {
      map[event] = callback;
    });
    const wrapper = mount(<AttributeRow {...props} />);
    const mockedEvent = { stopPropagation: () => {}, preventDefault: () => {} };
    act(() => {
      wrapper.find("input[id='spending-limit']").simulate("change", {
        target: { checked: true, name: "spending-limit" },
      });
    });
    wrapper.update();
    act(() => {
      wrapper
        .find("button[id='dropdown-pred-button']")
        .simulate("click", mockedEvent);
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='w-100 dropdown open']").exists()
    ).toBeTruthy();
    act(() => {
      map.mousedown({ target: null });
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='w-100 dropdown ']").exists()
    ).toBeTruthy();
  });

  test("Should handle click outside when dropdown ref contains target", async () => {
    const map = {};
    document.addEventListener = jest.fn((event, callback) => {
      map[event] = callback;
    });
    const wrapper = mount(<AttributeRow {...props} />);
    const mockedEvent = {
      AttributeRow: () => {},
      preventDefault: () => {},
    };
    act(() => {
      wrapper.find("input[id='spending-limit']").simulate("change", {
        target: { checked: true, name: "spending-limit" },
      });
    });
    wrapper.update();
    act(() => {
      wrapper
        .find("button[id='dropdown-pred-button']")
        .simulate("click", mockedEvent);
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='w-100 dropdown open']").exists()
    ).toBeTruthy();
    act(() => {
      map.mousedown({
        target: wrapper.find("button[id='dropdown-pred-button']").getDOMNode(),
      });
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='w-100 dropdown open']").exists()
    ).toBeTruthy();
  });
});

describe("<AttributeRow /> with empty inputs", () => {
  const props = {
    inputs: {},
    attribute: {
      name: "spending-limit",
      title: "Spending limit",
      type: "number",
      predicate: {
        name: "spending-limit",
        type: "number",
        condition: [
          {
            title: "Greater than",
            value: ">",
          },
        ],
      },
    },
    handleConditionChange: jest.fn(),
    handleCheckboxChange: jest.fn(),
    handlePredicateChange: jest.fn(),
    conditionError: {},
  };
  const container = shallow(<AttributeRow {...props} />);

  test("Should have disabled condition field", () => {
    expect(
      container.find("input[id='form-condition']").prop("disabled")
    ).toBeTruthy();
  });
});

describe("<AttributeRow /> with Signing Basic schema - no conditions", () => {
  const props = {
    inputs: {
      attributes: {
        "spending-limit": {
          predicateValue: ">",
          predicateTitle: "Greater than",
          condition: undefined,
          disabled: false,
        },
      },
    },
    attribute: {
      name: "spending-limit",
      title: "Spending limit",
      type: "number",
      predicate: {
        name: "spending-limit",
        type: "number",
        condition: [],
      },
    },
    handleConditionChange: jest.fn(),
    handleCheckboxChange: jest.fn(),
    handlePredicateChange: jest.fn(),
    conditionError: {},
  };
  const container = shallow(<AttributeRow {...props} />);

  test("Should have disabled condition field", () => {
    expect(container.find("input[id='form-condition']").prop("value")).toBe("");
  });
});
