// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import AttributeRow from "../../AttributeRow";
import React from "react";

jest.mock("../../MandateVerifierFormHooks", () => {
  const handleSubmit = jest.fn();
  const handleInputChange = jest.fn();
  const handleConditionChange = jest.fn();
  const handleShowQrModal = jest.fn();
  const inputs = { condition: 123 };
  const showQRCode = false;
  const schemaName = "name";
  const handleCheckboxChange = jest.fn();
  const handlePredicateChange = jest.fn();
  const proofSocketResponse = { state: "uninitiated", value: "" };
  const showNoAttributesSelected = true;
  const conditionError = {};

  return () => ({
    handleSubmit,
    handleInputChange,
    handleConditionChange,
    handleShowQrModal,
    inputs,
    showQRCode,
    schemaName,
    handleCheckboxChange,
    handlePredicateChange,
    proofSocketResponse,
    showNoAttributesSelected,
    conditionError,
  });
});

describe("<AttributeRow - Inputs /> with Signing Basic schema", () => {
  const props = {
    inputs: {
      attributes: {
        "spending-limit": {
          predicateValue: ">",
          predicateTitle: "Greater than",
          condition: 10,
          disabled: false,
        },
      },
    },
    attribute: {
      name: "spending-limit",
      title: "Spending limit",
      type: "number",
      predicate: {
        name: "spending-limit",
        type: "number",
        condition: [
          {
            title: "Greater than",
            value: ">",
          },
        ],
      },
    },
    handleConditionChange: jest.fn(),
    handleCheckboxChange: jest.fn(),
    handlePredicateChange: jest.fn(),
    conditionError: {},
  };
  const container = shallow(<AttributeRow {...props} />);

  test("Should render condition input field filled", () => {
    const inputWrapper = container.find("input[id='form-condition']");
    expect(inputWrapper.exists()).toBeTruthy();
    expect(inputWrapper.prop("value")).toBe(10);
  });
});
