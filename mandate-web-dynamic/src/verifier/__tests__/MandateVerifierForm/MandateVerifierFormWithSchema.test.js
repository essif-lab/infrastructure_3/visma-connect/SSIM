// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow, mount } from "enzyme";
import MandateVerifierForm from "../../MandateVerifierForm";
import React from "react";
import { act } from "react-dom/test-utils";

const mockHistoryPush = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

jest.mock("axios", () => ({
  ...jest.requireActual("axios"),
  get: jest.fn(),
}));

jest.mock("../../../common/SchemaService", () => {
  const schemas = [
    {
      schemaId: "abcdef:2:mandate-kvk-schema:1.0",
      title: "KVK Schema",
    },
    {
      schemaId: "abcdef:2:mandate-signing-basic:1.0",
      title: "Signing Basic Schema",
    },
    { schemaId: "abcdef:2:fenex-schema:1.0", title: "Fenex Schema" },
  ];

  return () => ({
    schemas,
  });
});

jest.mock("../../../common/hooks/FetchSchemaById", () => {
  const schema = {
    schemaId: "BNrRkkQZsTXBx1RZZzjkFS:2:mandate-signing-basic:1.0",
    name: "mandate-signing-basic",
    title: "Signing Basic Schema",
    version: "1.0",
    attributes: [
      {
        name: "spending-limit",
        title: "Spending limit",
        type: "number",
        predicate: {
          name: "spending-limit",
          type: "number",
          condition: [
            {
              title: "Greater than",
              value: ">",
            },
          ],
        },
      },
      { name: "authorization", title: "Authorization", type: "text" },
    ],
    predicates: [
      {
        name: "spending-limit",
        type: "number",
        condition: [
          {
            title: "Greater than",
            value: ">",
          },
        ],
      },
    ],
  };

  const setSchema = jest.fn();

  return () => ({
    schema,
    setSchema,
  });
});

describe("<MandateVerifierForm - with Schema />", () => {
  let container;
  beforeEach(() => {
    container = shallow(<MandateVerifierForm />);
  });

  test("Should have heading with proper value", () => {
    const headingWrapper = container.find("h1[className='headings']");
    expect(headingWrapper.exists()).toBeTruthy();
    expect(headingWrapper.render().text()).toBe(
      "Create a proof request to verify credentials"
    );
  });

  test("Should change from selected form to schema selection screen", () => {
    const container = mount(<MandateVerifierForm />);
    const verifierFormWrapper = container.find("VerifierFormTemplate");

    act(() => {
      verifierFormWrapper.props().schemaSelectionCB();
    });
    container.update();
    expect(container.find("VerifierFormTemplate").exists()).toBeTruthy();
  });
});
