// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import renderer from "react-test-renderer";
import { shallow, mount } from "enzyme";
import MandateVerifierForm from "../../MandateVerifierForm";
import React from "react";
import { act } from "react-dom/test-utils";

const mockHistoryPush = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

jest.mock("axios", () => ({
  ...jest.requireActual("axios"),
  get: jest.fn(),
}));

jest.mock("../../../common/SchemaService", () => {
  const schemas = [
    {
      schemaId: "abcdef:2:mandate-kvk-schema:1.0",
      title: "KVK Schema",
    },
    {
      schemaId: "abcdef:2:mandate-signing-basic:1.0",
      title: "Signing Basic Schema",
    },
    { schemaId: "abcdef:2:fenex-schema:1.0", title: "Fenex Schema" },
  ];

  return () => ({
    schemas,
  });
});

jest.mock("../../../common/hooks/FetchSchemaById", () => {
  const schema = null;
  const setSchema = jest.fn();

  return () => ({
    schema,
    setSchema,
  });
});

describe("<MandateVerifierForm /> without hooks", () => {
  let container;
  beforeEach(() => {
    container = shallow(<MandateVerifierForm />);
  });

  test("Should match old snapshot", () => {
    const mandateVerifierFormTree = renderer
      .create(<MandateVerifierForm />)
      .toJSON();
    expect(mandateVerifierFormTree).toMatchSnapshot();
  });

  test("Should have heading with proper value", () => {
    const headingWrapper = container.find("h1[className='headings']");
    expect(headingWrapper.exists()).toBeTruthy();
    expect(headingWrapper.render().text()).toBe(
      "Create a proof request to verify credentials"
    );
  });

  test("Should have description with proper value", () => {
    const descriptionWrapper = container.find("h4[className='description']");
    expect(descriptionWrapper.exists()).toBeTruthy();
    expect(descriptionWrapper.render().text()).toBe(
      "Please select a schema from dropdown menu below and fill out a form in order to verify credentials."
    );
  });

  test("Should have dropdown button with proper props", () => {
    const dropdownWrapper = container.find(
      "button[id='dropdown-basic-button']"
    );
    expect(dropdownWrapper.exists()).toBeTruthy();
    expect(dropdownWrapper.prop("onClick")).toBeDefined();
    expect(dropdownWrapper.prop("className")).toBe(
      "btn btn-default dropdown-toggle btn-lg"
    );
    expect(dropdownWrapper.prop("data-toggle")).toBe("dropdown");
    expect(dropdownWrapper.prop("aria-haspopup")).toBe("true");
    expect(dropdownWrapper.prop("aria-expanded")).toBe("false");
    expect(dropdownWrapper.text()).toBe("Select a Mandate schema...");
  });

  test("Should render back button", () => {
    const backButtonWrapper = container.find("button[id='button-back']");
    expect(backButtonWrapper.exists()).toBeTruthy();
    expect(backButtonWrapper.prop("type")).toBe("button");
    expect(backButtonWrapper.prop("style")).toEqual({ width: "100%" });
    expect(backButtonWrapper.prop("onClick")).toBeDefined();
    expect(backButtonWrapper.prop("className")).toBe(
      "btn btn-default btn-lg btn-block"
    );
    expect(backButtonWrapper.text()).toBe("Back");
  });
});

describe("<MandateVerifierForm /> with hooks", () => {
  let container;
  beforeEach(() => {
    container = shallow(<MandateVerifierForm />);
  });

  test("Should trigger handleBack function", () => {
    const mockedEvent = { target: {}, preventDefault: () => {} };
    container.find("button[id='button-back']").simulate("click", mockedEvent);
    expect(mockHistoryPush).toHaveBeenCalledWith("/home");
  });

  test("Should open dropdown menu", () => {
    const mockedEvent = { stopPropagation: () => {}, preventDefault: () => {} };
    act(() => {
      container
        .find("button[id='dropdown-basic-button']")
        .simulate("click", mockedEvent);
    });
    const dropdownWrapper = container.find("div[className='dropdown open']");
    expect(dropdownWrapper.exists()).toBeTruthy();
  });

  test("Should call handleSelect", () => {
    const container = mount(<MandateVerifierForm />);
    const event = {
      target: {
        id: "abcdef:2:mandate-kvk-schema:1.0",
        innerText: "KVK Schema",
      },
      preventDefault: jest.fn(),
    };

    const button = container.find("a[id='abcdef:2:mandate-kvk-schema:1.0']");
    expect(button.exists()).toBeTruthy();
    expect(button.prop("onClick")).toBeDefined();
    button.simulate("click", event);
  });
});

describe("<MandateVerifierForm /> with useEffect", () => {
  test("Should handle mousedown listener", () => {
    jest
      .spyOn(document, "addEventListener")
      .mockImplementation(() => jest.fn());
    jest
      .spyOn(document, "removeEventListener")
      .mockImplementation(() => jest.fn());
    const container = mount(<MandateVerifierForm />);
    expect(document.addEventListener).toHaveBeenLastCalledWith(
      "mousedown",
      expect.any(Function)
    );
    act(() => {
      container.unmount();
    });
    expect(document.removeEventListener).toHaveBeenLastCalledWith(
      "mousedown",
      expect.any(Function)
    );
  });

  test("Should handle click outside when dropdown ref does not contain target", async () => {
    const map = {};
    document.addEventListener = jest.fn((event, callback) => {
      map[event] = callback;
    });
    const wrapper = mount(<MandateVerifierForm />);
    const mockedEvent = { stopPropagation: () => {}, preventDefault: () => {} };
    act(() => {
      wrapper
        .find("button[id='dropdown-basic-button']")
        .simulate("click", mockedEvent);
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
    act(() => {
      map.mousedown({ target: null });
    });
    wrapper.update();
    expect(wrapper.find("div[className='dropdown ']").exists()).toBeTruthy();
  });

  test("Should handle click outside when dropdown ref contains target", async () => {
    const map = {};
    document.addEventListener = jest.fn((event, callback) => {
      map[event] = callback;
    });
    const wrapper = mount(<MandateVerifierForm />);
    const mockedEvent = {
      MandateVerifierForm: () => {},
      preventDefault: () => {},
    };
    act(() => {
      wrapper
        .find("button[id='dropdown-basic-button']")
        .simulate("click", mockedEvent);
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
    act(() => {
      map.mousedown({
        target: wrapper.find("button[id='dropdown-basic-button']").getDOMNode(),
      });
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
  });
});
