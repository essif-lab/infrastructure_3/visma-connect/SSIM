// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import useVerifyingForm from "../MandateVerifierFormHooks";
import React from "react";
import { act } from "react-dom/test-utils";
import ReactDOM from "react-dom";

let result;

function TestComponent(props) {
  result = useVerifyingForm(props);
  return null;
}

let containerTest;

beforeEach(() => {
  containerTest = document.createElement("div");
  document.body.appendChild(containerTest);
});

afterEach(() => {
  document.body.removeChild(containerTest);
  containerTest = null;
});

describe("<MandateVerifierFormHooks />", () => {
  test("Should have proper initial values with KVK schema", () => {
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-kvk-schema:1.0" />,
        containerTest
      );
    });

    expect(result.schemaName).toBe("mandate-kvk-schema:1.0");
    expect(result.proofSocketResponse).toEqual({
      state: "uninitiated",
      value: "",
    });
  });

  test("Should have proper initial values with Signing Basic schema", () => {
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
    });

    expect(result.schemaName).toBe("mandate-signing-basic:1.0");
    expect(result.proofSocketResponse).toEqual({
      state: "uninitiated",
      value: "",
    });
  });

  test("Should handle checkbox change when checked and name is spending-limit", () => {
    const event = {
      persist: () => {},
      target: {
        checked: true,
        name: "spending-limit",
      },
    };
    const inputs = {};
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleCheckboxChange(event, inputs);
    });
    expect(result.inputs.attributes["spending-limit"].disabled).toBe(false);
  });

  test("Should handle checkbox change when NOT checked and name is spending-limit", () => {
    const event = {
      persist: () => {},
      target: {
        checked: false,
        name: "spending-limit",
      },
    };
    const inputs = {
      attributes: {
        "spending-limit": {
          disabled: false,
          predicateTitle: "pred-title",
          condition: 0,
        },
      },
    };
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleCheckboxChange(event, inputs);
    });
    expect(result.inputs.attributes["spending-limit"].disabled).toBe(true);
  });

  test("Should handle valid input change", () => {
    const event = {
      persist: () => {},
      target: {
        value: 1,
        name: "condition",
      },
    };
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleInputChange(event);
    });
    expect(result.inputs).toEqual({ condition: 1 });
  });

  test("Should handle invalid input change", () => {
    const event = {
      persist: () => {},
      target: {
        value: -1,
        name: "condition",
      },
    };
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleInputChange(event);
    });
    expect(result.inputs).toEqual({ condition: -1 });
  });

  test("Should handle valid condition change", () => {
    const event = {
      persist: () => {},
      target: {
        value: 1,
      },
    };
    const inputs = {
      attributes: {
        "attribute-name": {
          condition: 0,
        },
      },
    };
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleConditionChange(event, "attribute-name", inputs);
    });
    expect(result.inputs.attributes["attribute-name"].condition).toBe(1);
  });

  test("Should handle invalid condition change", () => {
    const event = {
      persist: () => {},
      target: {
        value: -1,
      },
    };
    const inputs = {
      attributes: {
        "attribute-name": {
          condition: 0,
        },
      },
    };
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleConditionChange(event, "attribute-name", inputs);
    });
    expect(result.inputs.attributes["attribute-name"].condition).toBe(0);
  });

  test("Should handle show qr code modal", () => {
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleShowQrCodeModal();
    });
    expect(result.showQRCode).toBeTruthy();
  });

  test("Should handle Greater or equal predicate change", () => {
    const inputs = {
      attributes: {
        "attribute-name": {
          predicateTitle: "",
          predicateValue: "",
          condition: 10,
        },
      },
    };
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handlePredicateChange(
        "Greater than or equal",
        "greater-than-or-equal",
        "attribute-name",
        inputs
      );
    });
    expect(result.inputs).toEqual({
      attributes: {
        "attribute-name": {
          condition: 10,
          predicateTitle: "Greater than or equal",
          predicateValue: ">=",
        },
      },
    });
  });

  test("Should handle Greater predicate change", () => {
    const inputs = {
      attributes: {
        "attribute-name": {
          predicateTitle: "",
          predicateValue: "",
          condition: 10,
        },
      },
    };
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handlePredicateChange(
        "Greater than",
        "greater-than",
        "attribute-name",
        inputs
      );
    });
    expect(result.inputs).toEqual({
      attributes: {
        "attribute-name": {
          condition: 10,
          predicateTitle: "Greater than",
          predicateValue: ">",
        },
      },
    });
  });

  test("Should handle Less predicate change", () => {
    const inputs = {
      attributes: {
        "attribute-name": {
          predicateTitle: "",
          predicateValue: "",
          condition: 10,
        },
      },
    };
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handlePredicateChange(
        "Less than",
        "less-than",
        "attribute-name",
        inputs
      );
    });
    expect(result.inputs).toEqual({
      attributes: {
        "attribute-name": {
          condition: 10,
          predicateTitle: "Less than",
          predicateValue: "<",
        },
      },
    });
  });

  test("Should handle Less or equal predicate change", () => {
    const inputs = {
      attributes: {
        "attribute-name": {
          predicateTitle: "",
          predicateValue: "",
          condition: 10,
        },
      },
    };
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handlePredicateChange(
        "Less than or equal",
        "less-than-or-equal",
        "attribute-name",
        inputs
      );
    });
    expect(result.inputs).toEqual({
      attributes: {
        "attribute-name": {
          condition: 10,
          predicateTitle: "Less than or equal",
          predicateValue: "<=",
        },
      },
    });
  });

  test("Should handle None predicate change", () => {
    const inputs = {
      attributes: {
        "attribute-name": {
          predicateTitle: "",
          predicateValue: "",
          condition: 0,
        },
      },
    };
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handlePredicateChange("None", "none", "attribute-name", inputs);
    });
    expect(result.inputs).toEqual({
      attributes: {
        "attribute-name": {
          condition: null,
          predicateTitle: "None",
          predicateValue: "",
        },
      },
    });
  });
});
