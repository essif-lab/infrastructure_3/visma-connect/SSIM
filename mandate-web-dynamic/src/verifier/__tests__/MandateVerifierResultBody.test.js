// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import MandateVerifierResultBody from "../MandateVerifierResultBody";

describe("<MandateVerifierResultBody />", () => {
  test("Should match old snapshot", () => {
    const mandateVerifierResultBodyTree = renderer
      .create(<MandateVerifierResultBody />)
      .toJSON();
    expect(mandateVerifierResultBodyTree).toMatchSnapshot();
  });

  test("Should render error Alert", () => {
    const proofSocketResponse = null;
    const mandateVerifierResultBody = (
      <MandateVerifierResultBody proofSocketResponse={proofSocketResponse} />
    );

    const mandateVerifierResultBodyWrapper = shallow(mandateVerifierResultBody);
    const alertWrapper = mandateVerifierResultBodyWrapper.find(
      "div[id='alert-error-unexpected']"
    );
    expect(alertWrapper.exists()).toBeTruthy();
    expect(alertWrapper.prop("className")).toBe("alert alert-danger");
    expect(alertWrapper.prop("role")).toBe("alert");
    expect(alertWrapper.render().text()).toBe(
      "Error! Unexpected error occurred. Please regenerate the verification request and try again."
    );
  });

  test("Should render error Alert", () => {
    const proofSocketResponse = undefined;
    const mandateVerifierResultBody = (
      <MandateVerifierResultBody proofSocketResponse={proofSocketResponse} />
    );

    const mandateVerifierResultBodyWrapper = shallow(mandateVerifierResultBody);
    const alertWrapper = mandateVerifierResultBodyWrapper.find(
      "div[id='alert-error-unexpected']"
    );
    expect(alertWrapper.exists()).toBeTruthy();
    expect(alertWrapper.prop("className")).toBe("alert alert-danger");
    expect(alertWrapper.prop("role")).toBe("alert");
    expect(alertWrapper.render().text()).toBe(
      "Error! Unexpected error occurred. Please regenerate the verification request and try again."
    );
  });

  test("Should render error Alert", () => {
    const proofSocketResponse = {
      state: "error",
      value: "error message",
    };
    const mandateVerifierResultBody = (
      <MandateVerifierResultBody proofSocketResponse={proofSocketResponse} />
    );

    const mandateVerifierResultBodyWrapper = shallow(mandateVerifierResultBody);
    const alertWrapper = mandateVerifierResultBodyWrapper.find(
      "div[id='alert-error']"
    );
    expect(alertWrapper.exists()).toBeTruthy();
    expect(alertWrapper.prop("className")).toBe("alert alert-danger");
    expect(alertWrapper.prop("role")).toBe("alert");
    expect(alertWrapper.render().text()).toBe("Error! error message");
  });

  test("Should render QR code", () => {
    const proofSocketResponse = {
      state: "qrcode",
      value: "someQrCode",
    };
    const mandateVerifierResultBody = (
      <MandateVerifierResultBody proofSocketResponse={proofSocketResponse} />
    );

    const mandateVerifierResultBodyWrapper = shallow(mandateVerifierResultBody);
    const qrCodeWrapper = mandateVerifierResultBodyWrapper.find(
      "AdaptiveQRComponent[id='qrcode']"
    );
    expect(qrCodeWrapper.exists()).toBeTruthy();
    expect(qrCodeWrapper.prop("qrValue")).toBe("someQrCode");
  });

  test("Should render loading spinner", () => {
    const proofSocketResponse = {
      state: "loading",
    };
    const mandateVerifierResultBody = (
      <MandateVerifierResultBody proofSocketResponse={proofSocketResponse} />
    );

    const mandateVerifierResultBodyWrapper = shallow(mandateVerifierResultBody);
    const loadingSpinnerWrapper = mandateVerifierResultBodyWrapper.find(
      "AdaptiveSpinnerComponent[id='loading']"
    );
    expect(loadingSpinnerWrapper.exists()).toBeTruthy();
    expect(loadingSpinnerWrapper.prop("spinner")).toBe(
      "spinner-snake-blue-light.svg"
    );
  });

  test("Should render succeeded Alert", () => {
    const proofSocketResponse = {
      state: "succeeded",
    };
    const mandateVerifierResultBody = (
      <MandateVerifierResultBody proofSocketResponse={proofSocketResponse} />
    );

    const mandateVerifierResultBodyWrapper = shallow(mandateVerifierResultBody);
    const alertWrapper = mandateVerifierResultBodyWrapper.find(
      "div[id='alert-received']"
    );
    expect(alertWrapper.exists()).toBeTruthy();
    expect(alertWrapper.prop("className")).toBe("alert alert-success");
    expect(alertWrapper.prop("role")).toBe("alert");
    expect(alertWrapper.render().text()).toBe(
      "Success! The user's credentials meet your proof request!"
    );
  });

  test("Should render failed Alert", () => {
    const proofSocketResponse = {
      state: "failed",
    };
    const mandateVerifierResultBody = (
      <MandateVerifierResultBody proofSocketResponse={proofSocketResponse} />
    );

    const mandateVerifierResultBodyWrapper = shallow(mandateVerifierResultBody);
    const alertWrapper = mandateVerifierResultBodyWrapper.find(
      "div[id='alert-failed']"
    );
    expect(alertWrapper.exists()).toBeTruthy();
    expect(alertWrapper.prop("className")).toBe("alert alert-danger");
    expect(alertWrapper.prop("role")).toBe("alert");
    expect(alertWrapper.render().text()).toBe(
      "Failed! The user's credentials did not meet your proof request!"
    );
  });

  test("Should render timed-out Alert", () => {
    const proofSocketResponse = {
      state: "timed-out",
    };
    const mandateVerifierResultBody = (
      <MandateVerifierResultBody proofSocketResponse={proofSocketResponse} />
    );

    const mandateVerifierResultBodyWrapper = shallow(mandateVerifierResultBody);
    const alertWrapper = mandateVerifierResultBodyWrapper.find(
      "div[id='alert-timed-out']"
    );
    expect(alertWrapper.exists()).toBeTruthy();
    expect(alertWrapper.prop("className")).toBe("alert alert-warning");
    expect(alertWrapper.prop("role")).toBe("alert");
    expect(alertWrapper.render().text()).toBe(
      "The request timed out. Please regenerate the verification request and try again."
    );
  });

  test("Should render error Alert when policy failed", () => {
    const proofSocketResponse = {
      state: "policy-failed",
    };
    const mandateVerifierResultBody = (
      <MandateVerifierResultBody proofSocketResponse={proofSocketResponse} />
    );

    const mandateVerifierResultBodyWrapper = shallow(mandateVerifierResultBody);
    const alertWrapper = mandateVerifierResultBodyWrapper.find(
      "div[id='alert-policy-failed']"
    );
    expect(alertWrapper.exists()).toBeTruthy();
    expect(alertWrapper.prop("className")).toBe("alert alert-danger");
    expect(alertWrapper.prop("role")).toBe("alert");
    expect(alertWrapper.render().text()).toBe(
      "Failed! The user's credentials did not meet your schema's attached policy!"
    );
  });

  test("Should render dotted spinner when state is other", () => {
    const proofSocketResponse = {
      state: "abc",
    };
    const mandateVerifierResultBody = (
      <MandateVerifierResultBody proofSocketResponse={proofSocketResponse} />
    );
    const mandateVerifierResultBodyWrapper = shallow(mandateVerifierResultBody);
    const loadingDottedSpinnerWrapper = mandateVerifierResultBodyWrapper.find(
      "AdaptiveSpinnerComponent[id='other']"
    );
    expect(loadingDottedSpinnerWrapper.exists()).toBeTruthy();
    expect(loadingDottedSpinnerWrapper.prop("spinner")).toBe(
      "spinner-dotted-blue-light.svg"
    );
  });

  test("Should render dotted spinner when state is null", () => {
    const proofSocketResponse = {
      state: null,
    };

    const mandateVerifierResultBodyWrapper = shallow(
      <MandateVerifierResultBody proofSocketResponse={proofSocketResponse} />
    );
    const loadingDottedSpinnerWrapper = mandateVerifierResultBodyWrapper.find(
      "AdaptiveSpinnerComponent[id='other']"
    );
    expect(loadingDottedSpinnerWrapper.exists()).toBeTruthy();
    expect(loadingDottedSpinnerWrapper.prop("spinner")).toBe(
      "spinner-dotted-blue-light.svg"
    );
  });

  test("Should render dotted spinner when state is undefined", () => {
    const proofSocketResponse = {
      state: undefined,
    };

    const mandateVerifierResultBodyWrapper = shallow(
      <MandateVerifierResultBody proofSocketResponse={proofSocketResponse} />
    );
    const loadingDottedSpinnerWrapper = mandateVerifierResultBodyWrapper.find(
      "AdaptiveSpinnerComponent[id='other']"
    );
    expect(loadingDottedSpinnerWrapper.exists()).toBeTruthy();
    expect(loadingDottedSpinnerWrapper.prop("spinner")).toBe(
      "spinner-dotted-blue-light.svg"
    );
  });
});
