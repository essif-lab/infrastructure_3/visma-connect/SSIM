// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import MandateVerificationQRResult from "../MandateVerificationQRResult";
import React from "react";

describe("<MandateVerificationQRResult /> context depending on props", () => {
  test("Should match old snapshot", () => {
    const mandateVerificationQRResultTree = renderer
      .create(<MandateVerificationQRResult />)
      .toJSON();
    expect(mandateVerificationQRResultTree).toMatchSnapshot();
  });

  test("Should have modal but not render it", () => {
    const props = {
      show: false,
    };
    const container = shallow(<MandateVerificationQRResult {...props} />);
    const modalWrapper = container.find("div[id='modal-qr-result']");
    expect(modalWrapper.exists()).toBeTruthy();
    expect(modalWrapper.prop("className")).toBe("modal ");
  });

  test("Should have modal and render it", () => {
    const props = {
      show: true,
    };
    const container = shallow(<MandateVerificationQRResult {...props} />);
    const modalWrapper = container.find("div[id='modal-qr-result']");
    expect(modalWrapper.exists()).toBeTruthy();
    expect(modalWrapper.prop("className")).toBe("modal in");
  });

  test("Should have MandateVerifierResultBody component", () => {
    const props = {
      proofSocketResponse: {
        status: "uninitiated",
      },
    };
    const container = shallow(<MandateVerificationQRResult {...props} />);
    const modalWrapper = container.find("MandateVerifierResultBody");
    expect(modalWrapper.exists()).toBeTruthy();
  });
});
describe("<MandateVerificationQRResult /> buttons", () => {
  let container;
  beforeEach(() => {
    container = shallow(
      <MandateVerificationQRResult
        onHide={() => jest.fn()}
        onFinish={() => jest.fn()}
      />
    );
  });

  test("Should render finish button", () => {
    const finishButtonWrapper = container.find("button[id='modal-btn-finish']");
    expect(finishButtonWrapper.exists()).toBeTruthy();
    expect(finishButtonWrapper.prop("type")).toBe("button");
    expect(finishButtonWrapper.prop("className")).toBe("btn btn-primary");
    expect(finishButtonWrapper.prop("onClick")).toBeDefined();
    expect(finishButtonWrapper.text()).toBe("Finish");
  });

  test("Should render cancel button", () => {
    const cancelButtonWrapper = container.find("button[id='modal-btn-cancel']");
    expect(cancelButtonWrapper.exists()).toBeTruthy();
    expect(cancelButtonWrapper.prop("type")).toBe("button");
    expect(cancelButtonWrapper.prop("onClick")).toBeDefined();
    expect(cancelButtonWrapper.prop("className")).toBe("btn btn-default");
    expect(cancelButtonWrapper.prop("data-toggle")).toBe("modal");
    expect(cancelButtonWrapper.prop("data-target")).toBe("#modal-qr-result");
    expect(cancelButtonWrapper.text()).toBe("Cancel");
  });
});
