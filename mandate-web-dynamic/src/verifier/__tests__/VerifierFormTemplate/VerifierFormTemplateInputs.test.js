// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import VerifierFormTemplate from "../../VerifierFormTemplate";
import React from "react";

jest.mock("../../MandateVerifierFormHooks", () => {
  const handleSubmit = jest.fn();
  const handleInputChange = jest.fn();
  const handleConditionChange = jest.fn();
  const handleShowQrModal = jest.fn();
  const inputs = { condition: 123 };
  const showQRCode = false;
  const schemaName = "name";
  const handleCheckboxChange = jest.fn();
  const handlePredicateChange = jest.fn();
  const proofSocketResponse = { state: "uninitiated", value: "" };
  const showNoAttributesSelected = true;

  return () => ({
    handleSubmit,
    handleInputChange,
    handleConditionChange,
    handleShowQrModal,
    inputs,
    showQRCode,
    schemaName,
    handleCheckboxChange,
    handlePredicateChange,
    proofSocketResponse,
    showNoAttributesSelected,
  });
});

describe("<VerifierFormTemplate /> with Signing Basic schema", () => {
  const props = {
    schemaSelectionCB: jest.fn(),
    schema: {
      schemaId: "BNrRkkQZsTXBx1RZZzjkFS:2:mandate-signing-basic:1.0",
      name: "mandate-signing-basic",
      title: "Signing Basic Schema",
      version: "1.0",
      attributes: [
        {
          name: "spending-limit",
          title: "Spending limit",
          type: "number",
          predicate: {
            name: "spending-limit",
            type: "number",
            condition: [
              {
                title: "Greater than",
                value: ">",
              },
            ],
          },
        },
        { name: "authorization", title: "Authorization", type: "text" },
      ],
    },
  };
  const container = shallow(<VerifierFormTemplate {...props} />);

  test("Should render error message", () => {
    const errorMsgWrapper = container.find("h4[className='has-error']");
    expect(errorMsgWrapper.exists()).toBeTruthy();
    expect(errorMsgWrapper.text()).toBe("No attributes selected");
  });
});
