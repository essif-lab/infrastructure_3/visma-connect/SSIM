// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow, mount } from "enzyme";
import VerifierFormTemplate from "../../VerifierFormTemplate";
import ActionButtons from "common/ActionButtons";
import React from "react";

const mockHistoryPush = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe("<VerifierFormTemplate /> with Signing Basic schema", () => {
  const props = {
    schemaSelectionCB: jest.fn(),
    schema: {
      schemaId: "BNrRkkQZsTXBx1RZZzjkFS:2:mandate-signing-basic:1.0",
      name: "mandate-signing-basic",
      title: "Signing Basic Schema",
      version: "1.0",
      attributes: [
        {
          name: "spending-limit",
          title: "Spending limit",
          type: "number",
          predicate: {
            name: "spending-limit",
            type: "number",
            condition: [
              {
                title: "Greater than",
                value: ">",
              },
            ],
          },
        },
        { name: "authorization", title: "Authorization", type: "text" },
      ],
    },
  };
  const container = shallow(<VerifierFormTemplate {...props} />);

  test("Should render schemaName label", () => {
    const labelWrapper = container.find("label").children().at(0);
    expect(labelWrapper.exists()).toBeTruthy();
    expect(labelWrapper.text()).toBe("Schema Name");
  });

  test("Should call handleBack function", () => {
    container.find(ActionButtons).prop("handleBack").call();
    expect(props.schemaSelectionCB).toHaveBeenCalled();
  });

  test("Should trigger handleBack function", () => {
    const container = shallow(<VerifierFormTemplate {...props} />);
    const mockedEvent = { target: {}, preventDefault: () => {} };
    container.find("MandateVerificationQRResult").prop("onFinish").call();
    expect(mockHistoryPush).toHaveBeenCalledWith("/home");
  });
});
