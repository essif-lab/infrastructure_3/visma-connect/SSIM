// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import useFetchSchemas from "../common/SchemaService";
import SchemaList from "../common/SchemaList";
import VerifierFormTemplate from "./VerifierFormTemplate";
import useFetchSchemaById from "../common/hooks/FetchSchemaById";

/**
 * @author Ragesh Shunmugam, Lukas Nakas
 * @description MandateRequestVerifierForm
 * @Return {JSX.Element}
 */
function MandateVerifierForm() {
  const [value, setValue] = useState("Select a Mandate schema...");
  const [dropdownMenuActive, setDropdownMenuActive] = useState(false);
  const dropdownRef = React.createRef();
  const history = useHistory();
  const onlyUserSchemas = true;

  const [schemaId, setSchemaId] = useState(null);
  const { schemas } = useFetchSchemas(onlyUserSchemas);
  const { schema, setSchema } = useFetchSchemaById(schemaId);

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  });

  const handleClickOutside = (e) => {
    if (dropdownRef.current && !dropdownRef.current.contains(e.target)) {
      setDropdownMenuActive(false);
    }
  };

  const dropdownMenuOpen = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setDropdownMenuActive(!dropdownMenuActive);
  };

  const handleSelect = (e) => {
    e.preventDefault();
    setValue(e.target.innerText);
    setSchemaId(e.target.id);
    setDropdownMenuActive(false);
  };

  const handleBack = () => {
    history.push("/home");
  };

  const showSchemaSelection = () => {
    setSchemaId(null);
    setSchema(null);
    setValue("Select a Mandate schema...");
  };

  return (
    <div className="request-body">
      <div className="container">
        <h1 className="headings">
          Create a proof request to verify credentials
        </h1>
        {schema === null ? (
          <div className="row rows justify-content-center">
            <div className="col columns text-center">
              <div className="row rows justify-content-center">
                <div className="col columns text-center">
                  <h4 className="description">
                    Please select a schema from dropdown menu below and fill out
                    a form in order to verify credentials.
                  </h4>
                  <div
                    className={`dropdown ${dropdownMenuActive ? "open" : ""}`}
                    ref={dropdownRef}
                  >
                    <button
                      type="button"
                      id="dropdown-basic-button"
                      className="btn btn-default dropdown-toggle btn-lg"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onClick={dropdownMenuOpen}
                    >
                      {value}
                    </button>
                    <SchemaList schemas={schemas} handleSelect={handleSelect} />
                  </div>
                </div>
              </div>
              <div className="row rows justify-content-center justify-content-xs-start">
                <div className="col-8 col-xs-1 columns text-xs-left">
                  <button
                    type="button"
                    className="btn btn-default btn-lg btn-block"
                    onClick={handleBack}
                    id="button-back"
                    style={{ width: "100%" }}
                  >
                    Back
                  </button>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <div className="row justify-content-center" key="1">
            <VerifierFormTemplate
              schema={schema}
              schemaName={schema.name + ":" + schema.version}
              schemaSelectionCB={showSchemaSelection}
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default MandateVerifierForm;
