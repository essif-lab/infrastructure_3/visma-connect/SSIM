// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useState, useEffect } from "react";

/**
 * @author Lukas Nakas
 * @description AttributeRow
 * @Return {JSX.Element}
 */
const AttributeRow = ({
  inputs,
  attribute,
  handleConditionChange,
  handleCheckboxChange,
  handlePredicateChange,
  conditionError,
}) => {
  const [dropdownMenuActive, setDropdownMenuActive] = useState(false);
  const dropdownRef = React.createRef();

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  });

  const handleClickOutside = (e) => {
    if (dropdownRef.current && !dropdownRef.current.contains(e.target)) {
      setDropdownMenuActive(false);
    }
  };

  const dropdownMenuOpen = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setDropdownMenuActive(!dropdownMenuActive);
  };

  const decorateName = (name) => {
    return name.split(" ").join("-").toLowerCase();
  };

  const handlePredicateSelect = (conditionTitle, attributeName) => {
    setDropdownMenuActive(false);
    handlePredicateChange(
      conditionTitle,
      decorateName(conditionTitle),
      attributeName,
      inputs
    );
  };

  const getCurrentPredicate = (attributeName) => {
    if (
      inputs.attributes &&
      inputs.attributes[attributeName] &&
      inputs.attributes[attributeName].predicateTitle
    ) {
      return inputs.attributes[attributeName].predicateTitle;
    } else {
      return "None";
    }
  };

  const isPredicateNone = (attributeName) => {
    if (
      inputs.attributes &&
      inputs.attributes[attributeName] &&
      inputs.attributes[attributeName].predicateTitle
    ) {
      return inputs.attributes[attributeName].predicateTitle === "None";
    } else {
      return false;
    }
  };

  const getConditionValue = (attributeName) => {
    if (
      inputs.attributes &&
      inputs.attributes[attributeName] &&
      inputs.attributes[attributeName].condition
    ) {
      return inputs.attributes[attributeName].condition;
    } else {
      return "";
    }
  };

  const isAttributeDisabled = (attributeName) => {
    if (inputs.attributes && inputs.attributes[attributeName]) {
      return inputs.attributes[attributeName].disabled;
    } else {
      return true;
    }
  };

  return (
    <>
      <div className="row" key={attribute.name}>
        <div className="col">
          <div className="checkbox">
            <input
              type="checkbox"
              className="form-control"
              name={attribute.name}
              onChange={(e) => handleCheckboxChange(e, inputs)}
              id={attribute.name}
            />
            <label htmlFor={attribute.name} className="control-label">
              {attribute.title}
            </label>
          </div>
        </div>
      </div>
      {attribute.predicate ? (
        <div className="row no-gutters">
          <div className="col-xs-3 col-md-2 text-xs-right offset-1 offset-xs-0">
            <label
              htmlFor={
                attribute.predicate.name + "form-spending-limit-predicate"
              }
              className="control-label"
            >
              Predicate
            </label>
          </div>
          <div className="col-xs-9 col-md-10">
            <div className="row no-gutters justify-content-xs-between justify-content-center">
              <div className="col-10 col-xs-8 col-sm-7 col-md-5 col-lg-5">
                <div
                  className={`w-100 dropdown ${
                    dropdownMenuActive ? "open" : ""
                  }`}
                  ref={dropdownRef}
                >
                  <button
                    type="button"
                    id="dropdown-pred-button"
                    name="predicate"
                    className="w-100 btn btn-default dropdown-toggle"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    onClick={dropdownMenuOpen}
                    disabled={isAttributeDisabled(attribute.name)}
                  >
                    {getCurrentPredicate(attribute.name)}
                  </button>
                  <ul
                    id="dropdown-pred-menu-list"
                    className="dropdown-menu"
                    role="menu"
                    aria-expanded="false"
                    aria-hidden="true"
                    aria-labelledby="dropdown-pred-button"
                  >
                    <li role="menuitem">
                      <a
                        id={`dropdown-pred-${attribute.predicate.name}-button-none`}
                        onClick={() =>
                          handlePredicateSelect("None", attribute.name)
                        }
                      >
                        None
                      </a>
                    </li>
                    {attribute.predicate.condition.length > 0 ? (
                      attribute.predicate.condition.map((cond) => (
                        <li role="menuitem" key={decorateName(cond.title)}>
                          <a
                            id={`dropdown-pred-${
                              attribute.predicate.name
                            }-button-${decorateName(cond.title)}`}
                            onClick={() =>
                              handlePredicateSelect(cond.title, attribute.name)
                            }
                          >
                            {cond.title}
                          </a>
                        </li>
                      ))
                    ) : (
                      <></>
                    )}
                  </ul>
                </div>
              </div>
              <div className="col-10 col-xs-3 col-sm-4 col-md-6 col-lg-6">
                <input
                  type="number"
                  className="form-control"
                  name="condition"
                  onChange={(e) =>
                    handleConditionChange(e, attribute.name, inputs)
                  }
                  value={getConditionValue(attribute.name)}
                  id="form-condition"
                  size="9"
                  disabled={
                    isAttributeDisabled(attribute.name) ||
                    isPredicateNone(attribute.name)
                  }
                />
                {!isPredicateNone(attribute.name) &&
                  !isAttributeDisabled(attribute.name) &&
                  conditionError[attribute.name] && (
                    <div
                      id={`${attribute.name}-condtion-error`}
                      className="has-error"
                    >
                      Please add condition.
                    </div>
                  )}
              </div>
            </div>
          </div>
        </div>
      ) : (
        <></>
      )}
    </>
  );
};

export default AttributeRow;
