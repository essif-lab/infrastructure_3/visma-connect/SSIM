// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React from "react";
import useVerifyingForm from "./MandateVerifierFormHooks";
import MandateVerificationQRResult from "./MandateVerificationQRResult";
import ActionButtons from "../common/ActionButtons";
import AttributeRow from "./AttributeRow";
import { useHistory } from "react-router-dom";

/**
 * @author Lukas Nakas
 * @description VerifierFormTemplate
 * @Return {JSX.Element}
 */
function VerifierFormTemplate(props) {
  const { schema } = props;
  const {
    isStompConnected,
    inputs,
    handleInputChange,
    handleConditionChange,
    handleShowQrCodeModal,
    handleSubmit,
    handleCheckboxChange,
    handlePredicateChange,
    proofSocketResponse,
    showQRCode,
    showNoAttributesSelected,
    conditionError,
  } = useVerifyingForm(props);

  const history = useHistory();

  const handleBack = () => {
    props.schemaSelectionCB();
  };

  const handleFinish = () => {
    history.push("/home");
  };

  return (
    <div className="col-lg-8">
      <MandateVerificationQRResult
        proofSocketResponse={proofSocketResponse}
        show={showQRCode}
        onHide={handleShowQrCodeModal}
        onFinish={handleFinish}
      />
      <form className="form-horizontal" onSubmit={handleSubmit}>
        <div className="form-group">
          <label
            htmlFor="form-schema"
            className="col-sm-3 col-md-2 control-label"
          >
            Schema Name
          </label>
          <div className="col-sm-9 col-md-10">
            <input
              type="text"
              className="form-control"
              id="form-schema"
              readOnly
              value={schema.title}
              onChange={handleInputChange}
              name="schema"
            />
          </div>
        </div>
        <div className="form-group no-gutters">
          <div className="col-sm-3 col-md-2 required form-group">
            <label
              htmlFor="form-attributes"
              className="col-sm-3 col-md-2 control-label"
            >
              Attributes
            </label>
          </div>
          <div className="col-sm-9 col-md-10" id="form-attributes">
            {schema.attributes.map((attribute) => (
              <AttributeRow
                key={attribute.name}
                inputs={inputs}
                attribute={attribute}
                handleConditionChange={handleConditionChange}
                handleCheckboxChange={handleCheckboxChange}
                handlePredicateChange={handlePredicateChange}
                conditionError={conditionError}
              />
            ))}
            {showNoAttributesSelected && (
              <h4 className="has-error">No attributes selected</h4>
            )}
          </div>
        </div>
        <div className="form-group">
          <label
            htmlFor="form-comment"
            className="col-sm-3 col-md-2 control-label"
          >
            Comment
          </label>
          <div className="col-sm-9 col-md-10">
            <input
              type="text"
              className="form-control"
              name="comment"
              onChange={handleInputChange}
              value={inputs.comment}
              id="form-comment"
            />
          </div>
        </div>
        <div className="form-group offset-sm-3 offset-md-2 no-gutters">
          <ActionButtons handleBack={handleBack} disabled={!isStompConnected} />
        </div>
      </form>
    </div>
  );
}

export default VerifierFormTemplate;
