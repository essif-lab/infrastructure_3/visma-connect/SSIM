// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { useEffect, useState, useCallback } from "react";
import SockJS from "sockjs-client";
import Stomp from "stompjs";

const useVerifyingForm = (props) => {
  const { schemaName } = props;
  const API_DOMAIN = process.env.REACT_APP_DOMAIN || "unknown";

  const [inputs, setInputs] = useState({});
  const [requestJson, setRequestJson] = useState([]);
  const [proofSocketResponse, setProofSocketResponse] = useState({
    state: "uninitiated",
    value: "",
  });
  const [showQRCode, setShowQRCode] = useState(false);
  const [showNoAttributesSelected, setShowNoAttributesSelected] =
    useState(false);
  const [conditionError, setConditionError] = useState({});
  const [stompClient] = useState(Stomp.over(new SockJS(API_DOMAIN + "/ws")));

  const [, updateState] = useState();
  const forceUpdate = useCallback(() => updateState({}), []);

  const [isStompConnected, setStompConnected] = useState(false);

  useEffect(() => {
    const headers = {
      Authorization: sessionStorage["access_token"],
    };
    stompClient.connect(headers, function (frame) {
      console.log("Connected: " + frame);
      setStompConnected(true);

      stompClient.subscribe("/user/present-proof/response", function (message) {
        let messageBody = JSON.parse(message.body);
        if (messageBody && messageBody.state) {
          setProofSocketResponse((prevState) => ({
            ...prevState,
            state: messageBody.state,
          }));
          if (messageBody.state === "qrcode") {
            setProofSocketResponse((prevState) => ({
              ...prevState,
              value: atob(messageBody.response),
            }));
          }
          if (messageBody.state === "error") {
            setProofSocketResponse((prevState) => ({
              ...prevState,
              value: messageBody.response,
            }));
          }
        }
      });
    });
    return () => {
      setStompConnected(false);
      stompClient.disconnect();
    };
  }, [stompClient]);

  const allConditionsValid = () => {
    setConditionError({});
    const requestJsonWithErrors = requestJson.map((attr) => {
      if (
        !!attr.predicate &&
        (attr.condition === undefined ||
          attr.condition === 0 ||
          attr.condition === null)
      ) {
        setConditionError((prevErrors) => ({
          ...prevErrors,
          [attr.credential]: attr.credential,
        }));
        return {
          ...attr,
          hasError: true,
        };
      }
      if (!!attr.predicate && !!attr.condition) {
        return {
          ...attr,
          hasError: false,
        };
      }
      return {
        ...attr,
        hasError: false,
      };
    });
    return requestJsonWithErrors.every((x) => x.hasError === false);
  };

  const handleSubmit = (event) => {
    if (event) {
      event.preventDefault();
      if (requestJson.length > 0 && !allConditionsValid()) {
        return false;
      }
      if (requestJson.length > 0) {
        setShowNoAttributesSelected(false);
        setProofSocketResponse((prevState) => ({
          ...prevState,
          state: "processing",
        }));

        console.log("JSON request vars >>>>>>", requestJson);
        handleShowQrCodeModal();

        const requestObject = JSON.stringify({
          schemaName: schemaName,
          comment: inputs.comment === undefined ? "" : inputs.comment,
          jsonRequest: btoa(JSON.stringify(requestJson)),
        });

        console.log("Verifier Request Json >>>>>>", requestObject);

        stompClient.send("/proof/request", {}, requestObject);
      } else {
        setShowNoAttributesSelected(true);
      }
    }
  };

  const handleInputChange = (event) => {
    event.persist();
    setInputs((inputs) => ({
      ...inputs,
      [event.target.name]: event.target.value,
    }));
  };

  const handleConditionChange = (event, attributeName, inputs) => {
    let conditionValue;
    const conditionInput = parseInt(event.target.value);
    if (!isNaN(conditionInput) && conditionInput >= 0) {
      conditionValue = parseInt(event.target.value);
    } else {
      conditionValue = 0;
    }

    const newInputs = inputs;
    newInputs.attributes[attributeName].condition = conditionValue;
    setInputs(newInputs);

    setRequestJson(
      requestJson.filter((item) => item.credential !== attributeName)
    );
    setRequestJson((requestJson) => [
      ...requestJson,
      {
        credential: attributeName,
        predicate: inputs.attributes[attributeName].predicateValue,
        condition: conditionValue,
      },
    ]);
  };

  const handleCheckboxChange = (event, inputs) => {
    event.persist();
    if (event.target.checked) {
      const newInputs = inputs;
      if (!("attributes" in newInputs)) {
        newInputs.attributes = {};
      }
      if (!(event.target.name in newInputs.attributes)) {
        newInputs.attributes[event.target.name] = {};
      }
      newInputs.attributes[event.target.name].disabled = false;
      newInputs.attributes[event.target.name].predicateTitle = "None";
      newInputs.attributes[event.target.name].condition = undefined;
      setInputs(newInputs);
      setRequestJson((requestJson) => [
        ...requestJson,
        { credential: event.target.name },
      ]);
    } else {
      const newInputs = inputs;
      newInputs.attributes[event.target.name].disabled = true;
      newInputs.attributes[event.target.name].predicateTitle = "None";
      newInputs.attributes[event.target.name].condition = undefined;
      setInputs(newInputs);
      const index = requestJson.findIndex(
        (obj) => obj.credential === event.target.name
      );
      if (index !== -1) {
        requestJson.splice(index, 1);
        setRequestJson(requestJson);
      }
    }
    forceUpdate();
  };

  const handleShowQrCodeModal = () => {
    setShowQRCode(!showQRCode);
  };

  const handlePredicateChange = (
    conditionTitle,
    conditionId,
    attributeName,
    inputs
  ) => {
    const newInputs = inputs;
    newInputs.attributes[attributeName].predicateTitle = conditionTitle;

    if (conditionId === "greater-than-or-equal") {
      updatePredicate(attributeName, ">=", newInputs);
    }
    if (conditionId === "greater-than") {
      updatePredicate(attributeName, ">", newInputs);
    }
    if (conditionId === "less-than") {
      updatePredicate(attributeName, "<", newInputs);
    }
    if (conditionId === "less-than-or-equal") {
      updatePredicate(attributeName, "<=", newInputs);
    }
    if (conditionId === "none") {
      updatePredicate(attributeName, "", newInputs);
    }
    setInputs(newInputs);
  };

  const updatePredicate = (attribute, predicate, inputsToUpdate) => {
    setRequestJson(requestJson.filter((item) => item.credential !== attribute));

    inputsToUpdate.attributes[attribute].predicateValue = predicate;
    if (predicate !== "") {
      setRequestJson((requestJson) => [
        ...requestJson,
        {
          credential: attribute,
          predicate: predicate,
          condition: inputsToUpdate.attributes[attribute].condition,
        },
      ]);
    } else {
      inputsToUpdate.attributes[attribute].condition = null;
      setRequestJson((requestJson) => [
        ...requestJson,
        {
          credential: attribute,
        },
      ]);
    }
  };

  return {
    isStompConnected,
    handleSubmit,
    handleInputChange,
    handleConditionChange,
    handleShowQrCodeModal,
    inputs,
    showQRCode,
    schemaName,
    handleCheckboxChange,
    handlePredicateChange,
    proofSocketResponse,
    showNoAttributesSelected,
    conditionError,
  };
};
export default useVerifyingForm;
