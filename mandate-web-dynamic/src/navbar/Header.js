// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { Link } from "react-router-dom";
import { ReactComponent as Logo } from "../images/visma-logo-white.svg";
import { NavLink } from "react-router-dom";

/**
 * @author Lukas Nakas
 * @description Header component with logo and navigation bar
 * @Return {JSX.Element}
 */
const Header = ({ handleLogout }) => {
  return (
    <header className="navbar navbar-default d-flex justify-content-between align-items-center">
      <div className="navbar-header">
        <div className="navbar-brand">
          <Link to="/home" title="Visma Mandate Service">
            <Logo title="Visma Mandate Service" />
          </Link>
        </div>
      </div>
      <NavLink to="/">
        <button
          className="btn btn-primary"
          id="logout_button"
          onClick={handleLogout}
        >
          Log Out
        </button>
      </NavLink>
    </header>
  );
};

export default Header;
