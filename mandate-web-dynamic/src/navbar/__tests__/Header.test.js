// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import Header from "../Header";

describe("<Header />", () => {
  const headerWrapper = shallow(<Header />);

  test("Should have Link on logo to return to home page", () => {
    const linkWrapper = headerWrapper.find("Link");
    expect(linkWrapper.exists()).toBeTruthy();
    expect(linkWrapper.prop("to")).toBe("/home");
    expect(linkWrapper.prop("title")).toBe("Visma Mandate Service");
  });

  test("Should render Logo", () => {
    const logoWrapper = headerWrapper.find("Link").childAt(0);
    expect(logoWrapper.exists()).toBeTruthy();
    expect(logoWrapper.prop("title")).toBe("Visma Mandate Service");
    expect(logoWrapper.type().render().type).toBe("svg");
    expect(logoWrapper.type().render().props.children).toBe(
      "visma-logo-white.svg"
    );
  });
});
