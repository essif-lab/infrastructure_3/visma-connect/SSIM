// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import ProtectedRoute from "./ProtectedRoute";
import { Route } from "react-router";
import { Redirect } from "react-router";

describe("<ProtectedRoute>", () => {
  it("Should pass props to Route component", () => {
    const { wrapper } = setup({ path: "/scarif" });
    expect(wrapper.find(Route).prop("path")).toBe("/scarif");
  });

  it("Should redirect unauthenticated users to login", () => {
    const { wrapper } = setup();
    const ComponentToRender = wrapper.prop("render");
    const redirectWrapper = shallow(<ComponentToRender location="/scarif" />);

    expect(redirectWrapper.find(Redirect).props()).toEqual({
      to: {
        pathname: "/login",
        state: { from: "/scarif" },
      },
    });
  });

  it("Should display passed component to authenticated users", () => {
    const { wrapper, props } = setup({ isAuth: true });
    const ComponentToRender = wrapper.prop("render");
    const componentWrapper = shallow(<ComponentToRender location="/scarif" />);
    expect(componentWrapper.find(props.component).props()).toEqual({
      location: "/scarif",
    });
  });
});

function setup(customProps) {
  const props = {
    component: () => <h1>Test Component</h1>,
    isAuth: false,
    ...customProps,
  };

  const wrapper = shallow(<ProtectedRoute {...props} />);

  return {
    wrapper,
    props,
  };
}
