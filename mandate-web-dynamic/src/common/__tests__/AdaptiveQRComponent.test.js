// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import AdaptiveQRComponent from "../AdaptiveQRComponent";
import React from "react";

jest.mock("react-resize-detector", () => ({
  ...jest.requireActual("react-resize-detector"),
  useResizeDetector: () => {
    const width = 100;
    const height = 100;
    const ref = null;

    return {
      width,
      height,
      ref,
    };
  },
}));

describe("<AdaptiveQRComponent />", () => {
  test("Should match old snapshot", () => {
    const adaptiveQRComponentTree = renderer
      .create(<AdaptiveQRComponent qrValue="someQrValue" />)
      .toJSON();
    expect(adaptiveQRComponentTree).toMatchSnapshot();
  });

  test("Should have QRCode component", () => {
    const container = shallow(<AdaptiveQRComponent qrValue="someQrValue" />);
    const qrCodeWrapper = container.find("QRCode[id='qrcode']");
    expect(qrCodeWrapper.exists()).toBeTruthy();
    expect(qrCodeWrapper.prop("size")).toBe(100);
    expect(qrCodeWrapper.prop("value")).toBe("someQrValue");
  });
});
