// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import ActionButtons from "../ActionButtons";

describe("<ActionButtons />", () => {
  const component = shallow(<ActionButtons handleBack={jest.fn()} />);

  test("Should render send request button", () => {
    const sendRequestButtonWrapper = component.find(
      "button[id='button-send-request']"
    );
    expect(sendRequestButtonWrapper.exists()).toBeTruthy();
    expect(sendRequestButtonWrapper.prop("type")).toBe("submit");
    expect(sendRequestButtonWrapper.prop("className")).toBe("btn btn-primary");
    expect(sendRequestButtonWrapper.text()).toBe("Send Request");
  });

  test("Should render back button", () => {
    const backButtonWrapper = component.find("button[id='button-back']");
    expect(backButtonWrapper.exists()).toBeTruthy();
    expect(backButtonWrapper.prop("type")).toBe("button");
    expect(backButtonWrapper.prop("onClick")).toBeDefined();
    expect(backButtonWrapper.prop("className")).toBe("btn btn-default");
    expect(backButtonWrapper.text()).toBe("Back");
  });

  test("Should trigger handleBack function", () => {
    const backBtn = component.find("#button-back");
    expect(backBtn.prop("onClick")).toBeDefined();

    const mockedEvent = { target: {}, preventDefault: () => {} };
    backBtn.simulate("click", mockedEvent);
  });
});
