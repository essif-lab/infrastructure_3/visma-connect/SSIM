// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import SchemaList from "../SchemaList";
import React from "react";

describe("<SchemaList /> without hooks", () => {
  test("Should match old snapshot", () => {
    const schemaListTree = renderer
      .create(<SchemaList schemas={[]} handleSelect={() => {}} />)
      .toJSON();
    expect(schemaListTree).toMatchSnapshot();
  });

  test("Should have proper dropdown buttons", () => {
    const props = {
      schemas: [
        {
          schemaId: "abcdef:2:mandate-kvk-schema:1.0",
          name: "mandate-kvk-schema",
          title: "KVK Schema",
        },
        {
          schemaId: "abcdef:2:mandate-signing-basic:1.0",
          name: "mandate-signing-basic",
          title: "Signing Basic Schema",
        },
        {
          schemaId: "abcdef:2:fenex-schema:1.0",
          name: "fenex-schema",
          title: "Fenex Schema",
        },
      ],
      handleSelect: () => {},
    };
    const container = shallow(<SchemaList {...props} />);
    const dropdownButtons = container
      .find("ul[id='dropdown-menu-list']")
      .children();
    expect(dropdownButtons.at(0).prop("role")).toBe("menuitem");

    expect(dropdownButtons.at(0).childAt(0).text()).toBe("KVK Schema");
    expect(dropdownButtons.at(0).childAt(0).prop("onClick")).toBeDefined();
    expect(dropdownButtons.at(1).prop("role")).toBe("menuitem");
    expect(dropdownButtons.at(1).childAt(0).text()).toBe(
      "Signing Basic Schema"
    );
    expect(dropdownButtons.at(1).childAt(0).prop("onClick")).toBeDefined();
  });

  test("Should have no schemas found button", () => {
    const props = {
      schemas: [],
    };
    const container = shallow(<SchemaList {...props} />);
    const dropdownButtons = container
      .find("ul[id='dropdown-menu-list']")
      .children();
    expect(dropdownButtons.at(0).prop("role")).toBe("menuitem");
    expect(dropdownButtons.at(0).childAt(0).text()).toBe("No schemas found");
    expect(dropdownButtons.at(0).childAt(0).prop("onClick")).not.toBeDefined();
  });
});
