// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import useFetchSchemas from "../SchemaService";
import axios from "axios";
import { act } from "react-dom/test-utils";
import ReactDOM from "react-dom";

jest.mock("axios", () => ({
  ...jest.requireActual("axios"),
  get: jest.fn(),
}));

let result;

function TestComponent() {
  result = useFetchSchemas();
  return null;
}

let containerTest;

beforeEach(() => {
  containerTest = document.createElement("div");
  document.body.appendChild(containerTest);
});

afterEach(() => {
  document.body.removeChild(containerTest);
  containerTest = null;
});

describe("<SchemaService />", () => {
  test("Should fetch schemas with correct response", async () => {
    const data = {
      data: [
        {
          schemaId: "abcdef:2:mandate-signing-basic:1.0",
          name: "mandate-signing-basic",
          title: "Signing Basic Schema",
        },
        {
          schemaId: "abcdef:2:mandate-kvk-schema:1.0",
          name: "mandate-kvk-schema",
          title: "KVK Schema",
        },
      ],
    };
    axios.get.mockImplementationOnce(() => Promise.resolve(data));

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
    });
    await expect(result.schemas).toEqual([
      {
        schemaId: "abcdef:2:mandate-signing-basic:1.0",
        name: "mandate-signing-basic",
        title: "Signing Basic Schema",
      },
      {
        schemaId: "abcdef:2:mandate-kvk-schema:1.0",
        name: "mandate-kvk-schema",
        title: "KVK Schema",
      },
    ]);
  });

  test("Should fetch schemas with correct response", async () => {
    const data = {
      data: null,
    };
    axios.get.mockImplementationOnce(() => Promise.resolve(data));

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
    });
    await expect(result.schemas).toEqual([]);
  });

  test("Should fetch schemas with error", async () => {
    axios.get.mockImplementationOnce(() => Promise.reject(new Error("error")));

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
    });
  });
});
