// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React from "react";
import { shallow } from "enzyme";
import IconWithPopover from "../IconWithPopover";

const handleOutsideClick = jest.fn();
const mockHandleClick = jest.fn();

describe("<SimplePopover />", () => {
  const container = shallow(
    <IconWithPopover
      onOutsideClick={handleOutsideClick}
      handleClick={mockHandleClick}
    />
  );
  const questionmarkIcon = container.find('div[data-test="questionmark-icon"]');

  test("Should render question mark icon", () => {
    expect(questionmarkIcon.exists()).toBeTruthy();
    expect(questionmarkIcon.prop("onClick")).toBeDefined();
  });

  test("Should triger question mark icon onClick event", () => {
    questionmarkIcon.simulate("click", mockHandleClick);
  });

  test("Show message if icon cliked", () => {
    const component = shallow(
      <IconWithPopover
        onOutsideClick={handleOutsideClick}
        handleClick={mockHandleClick}
        showPopover={true}
      />
    );
    const popover = component.find('div[data-test="popover-with-message"]');
    expect(popover.exists()).toBeTruthy();
  });
});
