// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { useResizeDetector } from "react-resize-detector";

/**
 * @author Lukas Nakas
 * @description Resizeable spinner component (depends on parent div width)
 * @Return {JSX.Element}
 */
const AdaptiveSpinnerComponent = ({
  spinner,
  divisionMultiplier,
  fullWidth,
}) => {
  const { width, ref } = useResizeDetector();

  return (
    <div ref={ref} className={`${fullWidth ? "w-100" : ""}`}>
      <div className="col">
        <object
          type="image/svg+xml"
          data={spinner}
          width={width ? width / divisionMultiplier : undefined}
          height={width ? width / divisionMultiplier : undefined}
        />
      </div>
    </div>
  );
};

export default AdaptiveSpinnerComponent;
