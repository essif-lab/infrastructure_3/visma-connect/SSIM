// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
const SchemaList = ({ schemas, handleSelect, isSsicomms }) => {
  return (
    <ul
      id="dropdown-menu-list"
      className="dropdown-menu"
      role="menu"
      aria-expanded="false"
      aria-hidden="true"
      aria-labelledby="dropdown-basic-button"
    >
      {schemas.length ? (
        schemas.map((schema) => {
          return isSsicomms && !schema.ssicommsEnabled ? null : (
            <li role="menuitem" key={schema.schemaId}>
              <a href="#" id={schema.schemaId} onClick={handleSelect}>
                {schema.title}
              </a>
            </li>
          );
        })
      ) : (
        <li role="menuitem" key="nodata">
          <a href="#" id={"dropdown-basic-button-nodata"}>
            No schemas found
          </a>
        </li>
      )}
    </ul>
  );
};

export default SchemaList;
