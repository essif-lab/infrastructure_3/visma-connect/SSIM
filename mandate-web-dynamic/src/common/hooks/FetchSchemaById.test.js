// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import axios from "axios";
import ReactDOM from "react-dom";
import { act } from "react-dom/test-utils";
import useFetchSchemaById from "./FetchSchemaById";

jest.mock("axios", () => ({
  ...jest.requireActual("axios"),
  get: jest.fn(),
}));

let result;
const id = "BNrRkkQZsTXBx1RZZzjkFS:2:mandate-kvk-schema:1.0";

function TestComponent() {
  result = useFetchSchemaById(id);
  return null;
}

let containerTest;

beforeEach(() => {
  containerTest = document.createElement("div");
  document.body.appendChild(containerTest);
});

afterEach(() => {
  document.body.removeChild(containerTest);
  containerTest = null;
});

describe("<FetchSchemaById />", () => {
  test("Should fetch schema with correct response", async () => {
    const data = {
      data: {
        schemaId: "BNrRkkQZsTXBx1RZZzjkFS:2:mandate-signing-basic:1.0",
        name: "mandate-signing-basic",
        title: "Signing Basic Schema",
        version: "1.0",
        attributes: [
          {
            name: "spending-limit",
            title: "Spending limit",
            type: "number",
          },
          { name: "authorization", title: "Authorization", type: "text" },
        ],
        predicates: [
          {
            name: "spending-limit",
            type: "number",
            condition: [
              {
                title: "Greater than",
                value: ">",
              },
            ],
          },
        ],
      },
    };
    axios.get.mockImplementationOnce(() => Promise.resolve(data));
    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
    });

    await expect(result.schema).toEqual({
      schemaId: "BNrRkkQZsTXBx1RZZzjkFS:2:mandate-signing-basic:1.0",
      name: "mandate-signing-basic",
      title: "Signing Basic Schema",
      version: "1.0",
      attributes: [
        {
          name: "spending-limit",
          title: "Spending limit",
          type: "number",
          predicate: {
            name: "spending-limit",
            type: "number",
            condition: [
              {
                title: "Greater than",
                value: ">",
              },
            ],
          },
        },
        { name: "authorization", title: "Authorization", type: "text" },
      ],
      predicates: [
        {
          name: "spending-limit",
          type: "number",
          condition: [
            {
              title: "Greater than",
              value: ">",
            },
          ],
        },
      ],
    });
  });

  test("Should fetch schema with null response", async () => {
    const data = {
      data: null,
    };
    axios.get.mockImplementationOnce(() => Promise.resolve(data));

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
    });
    await expect(result.schema).toEqual(null);
  });

  test("Should fetch schemas with error", async () => {
    axios.get.mockImplementationOnce(() => Promise.reject(new Error("error")));

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
    });

    await expect(result.schema).toEqual(null);
  });
});
