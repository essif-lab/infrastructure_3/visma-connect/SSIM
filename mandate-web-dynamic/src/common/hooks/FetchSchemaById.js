// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { useState, useEffect } from "react";
import axios from "axios";

const useFetchSchemaById = (schemaId) => {
  const API_DOMAIN = process.env.REACT_APP_DOMAIN || "unknown";
  const url = `${API_DOMAIN}/schemas/${schemaId}`;

  const [schema, setSchema] = useState(null);

  const fetchSchema = () => {
    const headers = {
      Authorization: sessionStorage["access_token"],
    };
    axios
      .get(url, { headers: headers })
      .then((res) => {
        if (res.data !== null) {
          res.data.attributes.forEach((attribute) => {
            res.data.predicates.forEach((predicate) => {
              if (attribute.name === predicate.name) {
                attribute.predicate = predicate;
              }
            });
          });
          setSchema(res.data);
        }
      })
      .catch((error) => {
        // Error occurred in our service
        // todo: some error handling?
        console.error(
          "Something went wrong when gathering schema by ID from the database: ",
          error
        );
      });
  };

  useEffect(() => {
    if (schemaId !== null) {
      fetchSchema();
    }
  }, [schemaId]);

  return { schema, setSchema };
};

export default useFetchSchemaById;
