// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { useState, useEffect } from "react";
import axios from "axios";

/**
 * @author Lukas Nakas
 * @description Service to fetch all schema IDs from ledger
 * @Return {JSX.Element}
 */
const useFetchSchemas = (onlyUserSchemas) => {
  const API_DOMAIN = process.env.REACT_APP_DOMAIN || "unknown";
  const url = `${API_DOMAIN}/schemas`;

  const [schemas, setSchemas] = useState([]);

  useEffect(() => {
    fetchSchemas();
  }, []);

  const fetchSchemas = () => {
    const headers = {
      Authorization: sessionStorage["access_token"],
    };
    const params = {
      onlyUserSchemas: onlyUserSchemas,
    };
    axios
      .get(url, { headers: headers, params: params })
      .then((res) => {
        if (res.data !== null) {
          setSchemas(res.data);
        }
      })
      .catch((error) => {
        // Error occurred in our service
        // todo: some error handling?
        console.error(
          "Something went wrong when gathering schemas from the ledger: ",
          error
        );
      });
  };

  return {
    schemas,
  };
};
export default useFetchSchemas;
