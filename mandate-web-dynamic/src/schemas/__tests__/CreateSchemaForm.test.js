// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow, mount } from "enzyme";
import { fireEvent, render, screen } from "@testing-library/react";
import CreateSchemaForm from "../CreateSchemaForm";

jest.mock("../MandateSchemaCreationFormHooks", () => {
  const inputs = {
    title: "title",
    name: "name",
    version: "1.0",
    ssicommsEnabled: false,
  };
  const handleInputChange = jest.fn();
  const handleCheckboxChange = jest.fn();
  const handleSubmit = jest.fn();
  const setInputs = jest.fn();
  const errorMessage = "";
  const createSuccessful = false;
  const loading = false;
  const readOnlyAttr = [
    {
      title: "SIPID Authorized representative",
      name: "sipid-authorized-representative",
      type: "text",
      hasPredicates: false,
    },
    {
      title: "SIPID Mandate provider",
      name: "sipid-mandate-provider",
      type: "text",
      hasPredicates: false,
    },
  ];

  return () => ({
    handleSubmit,
    handleInputChange,
    handleCheckboxChange,
    inputs,
    setInputs,
    createSuccessful,
    errorMessage,
    loading,
    readOnlyAttr,
  });
});

describe("<CreateSchemaForm />", () => {
  let functionCalled = false;
  const schemaCreationFormWrapper = shallow(
    <CreateSchemaForm showActionSelection={() => (functionCalled = true)} />
  );

  test("Should have modals", () => {
    expect(
      schemaCreationFormWrapper.find("AttributesModal").exists()
    ).toBeTruthy();
    expect(
      schemaCreationFormWrapper.find("PredicatesModal").exists()
    ).toBeTruthy();
  });

  test("Should have tables", () => {
    expect(
      schemaCreationFormWrapper.find("AttributesTable").exists()
    ).toBeTruthy();
    expect(
      schemaCreationFormWrapper.find("PredicatesTable").exists()
    ).toBeTruthy();
  });

  test("Should trigger showActionSelection CB", () => {
    expect(functionCalled).toBeFalsy();
    schemaCreationFormWrapper.find("ActionButtons").prop("handleBack").call();
    expect(functionCalled).toBeTruthy();
  });

  test("Should trigger onHide for AttributesModal", () => {
    const schemaCreationFormWrapper = mount(
      <CreateSchemaForm showActionSelection={jest.fn()} />
    );
    schemaCreationFormWrapper.find("AttributesModal").prop("onHide").call();
    schemaCreationFormWrapper.update();
    expect(
      schemaCreationFormWrapper
        .find("#modal-manage-attributes")
        .prop("className")
    ).toBe("modal ");
  });

  test("Should trigger onHide for PredicatesModal", () => {
    const schemaCreationFormWrapper = mount(
      <CreateSchemaForm showActionSelection={jest.fn()} />
    );
    schemaCreationFormWrapper.find("PredicatesModal").prop("onHide").call();
    schemaCreationFormWrapper.update();
    expect(
      schemaCreationFormWrapper
        .find("#modal-manage-predicates")
        .prop("className")
    ).toBe("modal ");
  });

  test("Should be possible to select policy checkbox", () => {
    render(<CreateSchemaForm showActionSelection={jest.fn()} />);
    const checkbox = screen.getByTestId("policy-checkbox");
    expect(checkbox.checked).toEqual(false);
    fireEvent.click(checkbox);
    expect(checkbox.checked).toEqual(true);
  });

  test("Should be possible to select SSIComms checkbox", () => {
    render(<CreateSchemaForm showActionSelection={jest.fn()} />);
    const checkbox = screen.getByTestId("ssicomms-checkbox");
    expect(checkbox.checked).toEqual(false);

    fireEvent.click(checkbox);
    expect(checkbox.checked).toEqual(true);
    console.log("debug", screen.debug());
  });
});
