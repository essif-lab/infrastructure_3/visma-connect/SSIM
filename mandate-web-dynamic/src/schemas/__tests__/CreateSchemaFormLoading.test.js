// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import AdaptiveSpinnerComponent from "../../common/AdaptiveSpinnerComponent";
import SchemaCreationForm from "../CreateSchemaForm";

jest.mock("../MandateSchemaCreationFormHooks", () => {
  const inputs = { title: "title", name: "name", version: "1.0" };
  const handleInputChange = jest.fn();
  const handleSubmit = jest.fn();
  const setInputs = jest.fn();
  const errorMessage = "";
  const createSuccessful = false;
  const loading = true;

  return () => ({
    handleSubmit,
    handleInputChange,
    inputs,
    setInputs,
    createSuccessful,
    errorMessage,
    loading,
  });
});

describe("<CreateSchemaForm />", () => {
  const schemaCreationFormWrapper = shallow(
    <SchemaCreationForm showActionSelection={jest.fn()} />
  );

  test("Should be loading spinner before success or error state", () => {
    expect(
      schemaCreationFormWrapper.find(AdaptiveSpinnerComponent).exists()
    ).toBe(true);
  });
});
