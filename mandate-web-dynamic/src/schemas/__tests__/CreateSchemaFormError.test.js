// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import { fireEvent, render, screen } from "@testing-library/react";
import CreateSchemaForm from "../CreateSchemaForm";

jest.mock("../MandateSchemaCreationFormHooks", () => {
  const inputs = { title: "title", name: "name", version: "1.0" };
  const handleInputChange = jest.fn();
  const handleSubmit = jest.fn();
  const setInputs = jest.fn();
  const errorMessage = "Unable to create new schema";
  const createSuccessful = false;
  const loading = false;
  const policyError = "Box is empty";

  return () => ({
    handleSubmit,
    handleInputChange,
    inputs,
    setInputs,
    createSuccessful,
    errorMessage,
    loading,
    policyError,
  });
});

describe("<CreateSchemaForm />", () => {
  test("Should have error message", () => {
    const schemaCreationFormWrapper = shallow(
      <CreateSchemaForm showActionSelection={jest.fn()} />
    );
    expect(
      schemaCreationFormWrapper.find('div[data-test="error-msg"]').text()
    ).toBe("Error! Unable to create new schema");
  });

  test("Should have error message if user is trying to validate emtpy text box", () => {
    render(<CreateSchemaForm showActionSelection={jest.fn()} />);
    fireEvent.click(screen.getByTestId("policy-checkbox"));
    const btn = screen.getByTestId("validate-policy-btn");
    fireEvent.click(btn);

    expect(screen.getByText("Box is empty")).toBeInTheDocument();
  });
});
