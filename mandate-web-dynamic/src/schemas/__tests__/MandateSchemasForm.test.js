// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow, mount } from "enzyme";
import MandateSchemasForm from "../MandateSchemasForm";
import React from "react";
import { act } from "react-dom/test-utils";

const mockHistoryPush = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe("<MandateSchemasForm /> without hooks", () => {
  let container;
  beforeEach(() => {
    container = shallow(<MandateSchemasForm />);
  });

  test("Should have dropdown button with proper props", () => {
    const dropdownWrapper = container.find(
      "button[id='dropdown-basic-button']"
    );
    expect(dropdownWrapper.exists()).toBeTruthy();
    expect(dropdownWrapper.prop("onClick")).toBeDefined();
  });

  test("Should render back button", () => {
    const backButtonWrapper = container.find("button[id='button-back']");
    expect(backButtonWrapper.exists()).toBeTruthy();
    expect(backButtonWrapper.prop("onClick")).toBeDefined();
  });
});

describe("<MandateSchemasForm /> with hooks", () => {
  let container;
  beforeEach(() => {
    container = shallow(<MandateSchemasForm />);
  });

  test("Should trigger handleBack function", () => {
    const mockedEvent = { target: {}, preventDefault: () => {} };
    container.find("button[id='button-back']").simulate("click", mockedEvent);
    expect(mockHistoryPush).toHaveBeenCalledWith("/home");
  });

  test("Should open dropdown menu", () => {
    const mockedEvent = { stopPropagation: () => {}, preventDefault: () => {} };
    act(() => {
      container
        .find("button[id='dropdown-basic-button']")
        .simulate("click", mockedEvent);
    });
    const dropdownWrapper = container.find("div[className='dropdown open']");
    expect(dropdownWrapper.exists()).toBeTruthy();
  });

  test("Should call handleSelect", () => {
    const container = mount(<MandateSchemasForm />);
    const event = {
      target: {
        id: "create",
        innerText: "Create Schema",
      },
      preventDefault: jest.fn(),
    };

    const button = container.find("a[id='create']");
    expect(button.exists()).toBeTruthy();
    expect(button.prop("onClick")).toBeDefined();
    button.simulate("click", event);
  });

  test("Should trigger showActionSelection CB", () => {
    const container = mount(<MandateSchemasForm />);
    const event = {
      target: {
        id: "create",
        innerText: "Create Schema",
      },
      preventDefault: jest.fn(),
    };

    container.find("a[id='create']").simulate("click", event);
    expect(container.find("CreateSchemaForm").exists()).toBeTruthy();
    container.find("CreateSchemaForm").prop("showActionSelection").call();
    container.update();
    expect(container.find("CreateSchemaForm").exists()).toBeFalsy();
  });
});

describe("<MandateSchemasForm /> with useEffect", () => {
  test("Should handle mousedown listener", () => {
    jest
      .spyOn(document, "addEventListener")
      .mockImplementation(() => jest.fn());
    jest
      .spyOn(document, "removeEventListener")
      .mockImplementation(() => jest.fn());
    const container = mount(<MandateSchemasForm />);
    expect(document.addEventListener).toHaveBeenLastCalledWith(
      "mousedown",
      expect.any(Function)
    );
    act(() => {
      container.unmount();
    });
    expect(document.removeEventListener).toHaveBeenLastCalledWith(
      "mousedown",
      expect.any(Function)
    );
  });

  test("Should handle click outside when dropdown ref does not contain target", async () => {
    const map = {};
    document.addEventListener = jest.fn((event, callback) => {
      map[event] = callback;
    });
    const wrapper = mount(<MandateSchemasForm />);
    const mockedEvent = { stopPropagation: () => {}, preventDefault: () => {} };
    act(() => {
      wrapper
        .find("button[id='dropdown-basic-button']")
        .simulate("click", mockedEvent);
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
    act(() => {
      map.mousedown({ target: null });
    });
    wrapper.update();
    expect(wrapper.find("div[className='dropdown ']").exists()).toBeTruthy();
  });

  test("Should handle click outside when dropdown ref contains target", async () => {
    const map = {};
    document.addEventListener = jest.fn((event, callback) => {
      map[event] = callback;
    });
    const wrapper = mount(<MandateSchemasForm />);
    const mockedEvent = {
      MandateSchemasForm: () => {},
      preventDefault: () => {},
    };
    act(() => {
      wrapper
        .find("button[id='dropdown-basic-button']")
        .simulate("click", mockedEvent);
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
    act(() => {
      map.mousedown({
        target: wrapper.find("button[id='dropdown-basic-button']").getDOMNode(),
      });
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
  });
});
