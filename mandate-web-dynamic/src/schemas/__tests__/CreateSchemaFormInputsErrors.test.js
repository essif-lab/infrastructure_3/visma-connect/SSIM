// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { mount } from "enzyme";
import InputFieldError from "../component/InputFieldError";
import SchemaCreationForm from "../CreateSchemaForm";

jest.mock("../MandateSchemaCreationFormHooks", () => {
  const inputs = { title: "", name: "", version: "", attributes: [] };
  const handleInputChange = jest.fn();
  const handleSubmit = jest.fn();
  const setInputs = jest.fn();
  const errorMessage = "";
  const createSuccessful = false;
  const loading = false;
  const inputError = {
    title: "Title is required.",
    version: "Version is required.",
    attributes: "Attributes is required.",
    predicates: "Conditions required for predicates: somePredName.",
  };

  return () => ({
    handleSubmit,
    handleInputChange,
    inputs,
    setInputs,
    createSuccessful,
    errorMessage,
    loading,
    inputError,
  });
});

describe("<CreateSchemaForm />", () => {
  const wrapper = mount(<SchemaCreationForm showActionSelection={jest.fn()} />);

  test("Should have error messages", () => {
    expect(wrapper.find(InputFieldError).at(0).prop("error")).toBe(
      "Title is required."
    );
    expect(wrapper.find(InputFieldError).at(1).prop("error")).toBe(
      "Version is required."
    );
    expect(wrapper.find(InputFieldError).at(2).prop("error")).toBe(
      "Attributes is required."
    );
    expect(wrapper.find(InputFieldError).at(3).prop("error")).toBe(
      "Conditions required for predicates: somePredName."
    );
  });
});
