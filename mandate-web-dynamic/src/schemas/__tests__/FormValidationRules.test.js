// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { validate } from "../FormValidationRules";

let result = {};

describe("FormValidationRules", () => {
  test("Should validate fields when nothing is passed", () => {
    const testProps = {
      title: "",
      version: "",
      attributes: "",
      predicates: "",
    };
    result = validate(testProps);

    expect(result).toStrictEqual({
      title: "Title is required.",
      version: "Version is required.",
      attributes: "Attributes is required.",
    });
  });

  test("Should validate fields when version is incorrect", () => {
    const testProps = {
      title: "",
      version: "1",
      attributes: "",
      predicates: "",
    };
    result = validate(testProps);

    expect(result).toStrictEqual({
      title: "Title is required.",
      version: "Please write version with the decimal number (0.1, 1.0 ...).",
      attributes: "Attributes is required.",
    });
  });

  test("Should validate fields when predicates are incorrect", () => {
    const testProps = {
      title: "Title",
      version: "0.1",
      attributes: [{ name: "atr", title: "atr", type: "number" }],
      predicates: [
        {
          name: "pred",
          type: "number",
          condition: [],
        },
      ],
    };
    result = validate(testProps);

    expect(result).toStrictEqual({
      predicates: "Conditions required for predicates: pred.",
    });
  });

  test("Should validate fields when all fields are correct", () => {
    const testProps = {
      title: "Title",
      version: "0.1",
      attributes: [{ name: "atr", title: "atr", type: "number" }],
      predicates: [
        {
          name: "pred",
          type: "number",
          condition: [{ title: "cond", value: ">" }],
        },
      ],
    };
    result = validate(testProps);

    expect(result).toStrictEqual({});
  });
});
