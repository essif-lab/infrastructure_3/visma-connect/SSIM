// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import SchemaCreationForm from "../CreateSchemaForm";
import SuccessMsgWrapper from "../component/SuccessMsgWrapper";

jest.mock("../MandateSchemaCreationFormHooks", () => {
  const inputs = { title: "title", name: "name", version: "1.0" };
  const handleInputChange = jest.fn();
  const handleSubmit = jest.fn();
  const setInputs = jest.fn();
  const errorMessage = "";
  const createSuccessful = true;
  const loading = false;

  return () => ({
    handleSubmit,
    handleInputChange,
    inputs,
    setInputs,
    createSuccessful,
    errorMessage,
    loading,
  });
});

describe("<CreateSchemaForm />", () => {
  let functionCalled = false;
  const schemaCreationFormWrapper = shallow(
    <SchemaCreationForm showActionSelection={() => (functionCalled = true)} />
  );

  test("Should have success message", () => {
    expect(
      schemaCreationFormWrapper.find(SuccessMsgWrapper).exists()
    ).toBeTruthy();
  });

  test("Should trigger back button", () => {
    expect(functionCalled).toBeFalsy();
    schemaCreationFormWrapper
      .find("SuccessMsgWrapper")
      .prop("handleBack")
      .call();
    expect(functionCalled).toBeTruthy();
  });
});
