// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import useSchemaCreationForm from "../MandateSchemaCreationFormHooks";
import axios from "axios";
import { act } from "react-dom/test-utils";
import ReactDOM from "react-dom";

jest.mock("axios", () => ({
  ...jest.requireActual("axios"),
  post: jest.fn(),
}));

let result;

function TestComponent(props) {
  result = useSchemaCreationForm(props);
  return null;
}

let containerTest;

beforeEach(() => {
  containerTest = document.createElement("div");
  document.body.appendChild(containerTest);
});

afterEach(() => {
  document.body.removeChild(containerTest);
  containerTest = null;
});

describe("<MandateSchemaCreationFormHooks />", () => {
  test("Should have proper initial values", () => {
    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
    });

    expect(result.createSuccessful).toBeFalsy();
    expect(result.errorMessage).toBe("");
    expect(result.inputs).toEqual({
      name: "Enter schema title..",
      ssicommsEnabled: false,
    });
  });

  test("Should be possible to add text in policy text box", () => {
    const policyEvent = {
      target: {
        value: "Text value",
      },
    };

    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handlePolicyChange(policyEvent);
    });

    expect(result.textValue).toBe("Text value");
    expect(result.isPolicyValid).toBe(false);
  });

  test("Should be possible to call policy validation method with empty box and get error", () => {
    const policyEvent = {
      target: {
        value: "",
      },
    };

    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handlePolicyChange(policyEvent);
      result.handleValidation();
    });
    expect(result.policyError).toBe("Text box is empty");
  });

  test("Should be possible to call policy validation method and get response error", async () => {
    const policyEvent = {
      target: {
        value: "Some text",
      },
    };

    axios.post.mockImplementationOnce(() => Promise.reject(new Error("Error")));

    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handlePolicyChange(policyEvent);
    });

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleValidation();
    });

    expect(result.policyError).toBe("Something wrong. Error.");
  });

  test("Should be possible to call policy validation method and get response", async () => {
    const policyEvent = {
      target: {
        value: "Some text",
      },
    };

    const data = {
      data: { valid: true },
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(data));

    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handlePolicyChange(policyEvent);
    });

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleValidation();
    });

    expect(result.isPolicyValid).toBe(true);
  });

  test("Should be possible to call policy validation method and get response that validation is invalid", async () => {
    const policyEvent = {
      target: {
        value: "Some text",
      },
    };

    const data = {
      data: { valid: false },
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(data));

    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handlePolicyChange(policyEvent);
    });

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleValidation();
    });

    expect(result.isPolicyValid).toBe(false);
  });

  test("Should show error messages if user try to submit empty form", async () => {
    const event = {
      preventDefault: () => {},
    };
    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleSubmit(event);
    });

    expect(result.inputError.title).toBe("Title is required.");
    expect(result.inputError.version).toBe("Version is required.");
    expect(result.inputError.attributes).toBe("Attributes is required.");
  });

  test("Should successfully submit form if all required fields are filled", async () => {
    const event = {
      preventDefault: () => {},
    };
    const titleEvent = {
      persist: () => {},
      target: {
        value: "Value Title",
        name: "title",
      },
    };
    const versionEvent = {
      persist: () => {},
      target: {
        value: "0.1",
        name: "version",
      },
    };
    const attributesEvent = {
      persist: () => {},
      target: {
        value: [{ name: "attr", title: "attr", type: "number" }],
        name: "attributes",
      },
    };
    const data = {
      data: { title: "Test", status: "ok" },
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(data));
    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleInputChange(titleEvent);
      result.handleInputChange(versionEvent);
      result.handleInputChange(attributesEvent);
    });

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleSubmit(event);
    });
    await expect(result.createSuccessful).toBeTruthy();
  });

  test("Should successfully submit form with required fields and attached policy", async () => {
    const event = {
      preventDefault: () => {},
    };
    const titleEvent = {
      persist: () => {},
      target: {
        value: "Value Title",
        name: "title",
      },
    };
    const versionEvent = {
      persist: () => {},
      target: {
        value: "0.1",
        name: "version",
      },
    };
    const attributesEvent = {
      persist: () => {},
      target: {
        value: [{ name: "attr", title: "attr", type: "number" }],
        name: "attributes",
      },
    };
    const policyEvent = {
      target: {
        value: "some policy",
      },
    };

    const policyResponse = {
      data: { valid: true },
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(policyResponse));

    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handlePolicyChange(policyEvent);
    });

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleValidation();
    });

    expect(result.isPolicyValid).toBe(true);

    const data = {
      data: { title: "Test", status: "ok" },
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(data));
    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleInputChange(titleEvent);
      result.handleInputChange(versionEvent);
      result.handleInputChange(attributesEvent);
      result.handlePolicyChange(policyEvent);
    });

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleSubmit(event);
    });
    await expect(result.createSuccessful).toBeTruthy();
  });

  test("Should not handle submit with no event", () => {
    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleSubmit(null);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle submit with null response", async () => {
    const event = {
      preventDefault: () => {},
    };
    const titleEvent = {
      persist: () => {},
      target: {
        value: "Value Title",
        name: "title",
      },
    };
    const versionEvent = {
      persist: () => {},
      target: {
        value: "0.1",
        name: "version",
      },
    };
    const attributesEvent = {
      persist: () => {},
      target: {
        value: [{ name: "attr", title: "attr", type: "number" }],
        name: "attributes",
      },
    };

    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleInputChange(titleEvent);
      result.handleInputChange(versionEvent);
      result.handleInputChange(attributesEvent);
    });
    const data = {
      data: null,
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(data));

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleSubmit(event);
    });
    await expect(result.errorMessage).toBe("Unable to create new schema");
  });

  test("Should handle submit with error", async () => {
    const event = {
      preventDefault: () => {},
    };
    const titleEvent = {
      persist: () => {},
      target: {
        value: "Value Title",
        name: "title",
      },
    };
    const versionEvent = {
      persist: () => {},
      target: {
        value: "0.1",
        name: "version",
      },
    };
    const attributesEvent = {
      persist: () => {},
      target: {
        value: [{ name: "attr", title: "attr", type: "number" }],
        name: "attributes",
      },
    };

    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleInputChange(titleEvent);
      result.handleInputChange(versionEvent);
      result.handleInputChange(attributesEvent);
    });
    axios.post.mockImplementationOnce(() =>
      Promise.reject(new Error("Unable to create new schema"))
    );

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleSubmit(event);
    });

    await expect(result.errorMessage).toBe("Unable to create new schema");
  });

  test("Should handle input change without title", () => {
    const event = {
      persist: () => {},
      target: {
        value: "valueName",
        name: "name",
      },
    };
    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleInputChange(event);
    });
    expect(result.inputs).toEqual({
      name: "valueName",
      ssicommsEnabled: false,
    });
  });

  test("Should handle input change with title", () => {
    const event = {
      persist: () => {},
      target: {
        value: "Value Title",
        name: "title",
      },
    };
    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleInputChange(event);
    });
    expect(result.inputs).toEqual({
      title: "Value Title",
      ssicommsEnabled: false,
      name: "value-title",
    });
  });

  test("Should handle input change with empty title", () => {
    const event = {
      persist: () => {},
      target: {
        value: "",
        name: "title",
      },
    };
    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleInputChange(event);
    });
    expect(result.inputs).toEqual({
      title: "",
      ssicommsEnabled: false,
      name: "",
    });
  });

  test("Should handle input change with undefined title", () => {
    const event = {
      persist: () => {},
      target: {
        value: undefined,
        name: "title",
      },
    };
    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleInputChange(event);
    });
    expect(result.inputs).toEqual({
      title: undefined,
      ssicommsEnabled: false,
      name: "Enter schema title..",
    });
  });

  test("Should handle error when user try to submit form with name that already exist", async () => {
    const event = {
      preventDefault: () => {},
    };
    const titleEvent = {
      persist: () => {},
      target: {
        value: "Value Title",
        name: "title",
      },
    };
    const versionEvent = {
      persist: () => {},
      target: {
        value: "0.1",
        name: "version",
      },
    };
    const attributesEvent = {
      persist: () => {},
      target: {
        value: [{ name: "attr", title: "attr", type: "number" }],
        name: "attributes",
      },
    };
    const data = {
      data: { title: "Test", status: "already_exists" },
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(data));
    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleInputChange(titleEvent);
      result.handleInputChange(versionEvent);
      result.handleInputChange(attributesEvent);
    });

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleSubmit(event);
    });

    await expect(result.errorMessage).toBeTruthy();
  });
});
