// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
/**
 * @author Lukas Nakas
 * @description TypeList
 * @Return {JSX.Element}
 */
const TypeList = ({ types, handleSelect }) => {
  return (
    <ul
      id="dropdown-menu-list"
      className="dropdown-menu"
      role="menu"
      aria-expanded="false"
      aria-hidden="true"
      aria-labelledby="dropdown-basic-button"
    >
      {types.length ? (
        types.map((type) => (
          <li role="menuitem" key={type.id}>
            <a href="#" id={type.id} onClick={() => handleSelect(type)}>
              {type.title}
            </a>
          </li>
        ))
      ) : (
        <li role="menuitem" key="nodata">
          <a href="#" id={"dropdown-basic-button-nodata"}>
            No types found
          </a>
        </li>
      )}
    </ul>
  );
};

export default TypeList;
