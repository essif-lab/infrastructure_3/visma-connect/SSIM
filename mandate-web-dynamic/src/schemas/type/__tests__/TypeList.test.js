// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import TypeList from "../TypeList";

describe("<TypeList />", () => {
  test("Should render types", () => {
    const props = {
      handleSelect: jest.fn(),
      types: [
        {
          id: "1",
          title: "Number",
        },
      ],
    };
    const typeListWrapper = shallow(<TypeList {...props} />);
    expect(typeListWrapper.find("li").length).toBe(1);
    expect(typeListWrapper.find("li").at(0).text()).toBe("Number");
  });

  test("Should render no data", () => {
    const props = {
      handleSelect: jest.fn(),
      types: [],
    };
    const typeListWrapper = shallow(<TypeList {...props} />);
    expect(typeListWrapper.find("#dropdown-basic-button-nodata").text()).toBe(
      "No types found"
    );
  });

  test("Should render types", () => {
    let functionCalled = false;
    const props = {
      handleSelect: () => {
        functionCalled = true;
      },
      types: [
        {
          id: "1",
          title: "Number",
        },
      ],
    };
    const typeListWrapper = shallow(<TypeList {...props} />);
    expect(functionCalled).toBeFalsy();
    typeListWrapper.find("li").at(0).childAt(0).simulate("click", {});
    expect(functionCalled).toBeTruthy();
  });
});
