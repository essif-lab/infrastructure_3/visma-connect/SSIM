// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useState } from "react";
import useSchemaCreationForm from "./MandateSchemaCreationFormHooks";
import ActionButtons from "common/ActionButtons";
import AttributesModal from "./modal/attributesModal/AttributesModal";
import AttributesTable from "./component/AttributesTable";
import PredicatesModal from "./modal/predicatesModal/PredicatesModal";
import PredicatesTable from "./component/PredicatesTable";
import SuccessMsgWrapper from "./component/SuccessMsgWrapper";
import AdaptiveSpinnerComponent from "../common/AdaptiveSpinnerComponent";
import spinner from "../styles/nc4/img/spinner-snake-blue-light.svg";
import InputFieldError from "./component/InputFieldError";
import PolicyTextBox from "./component/PolicyTextBox";

/**
 * @author Lukas Nakas
 * @description CreateSchemaForm
 * @Return {JSX.Element}
 */
const CreateSchemaForm = ({ showActionSelection }) => {
  const {
    handleSubmit,
    handleInputChange,
    handleCheckboxChange,
    inputs,
    setInputs,
    createSuccessful,
    errorMessage,
    loading,
    inputError,
    textValue,
    isPolicyValid,
    policyError,
    handlePolicyChange,
    handleValidation,
    readOnlyAttr,
  } = useSchemaCreationForm();

  const [selectedAttribute, setSelectedAttribute] = useState({});
  const [selectedPredicate, setSelectedPredicate] = useState({});
  const [showAttributesModal, setShowAttributesModal] = useState(false);
  const [showConditionsModal, setShowConditionsModal] = useState(false);
  const [action, setAction] = useState("");

  const [isPolicyNeeded, setIsPolicyNeeded] = useState(false);
  const [isSsicomms, setIsSsicomms] = useState(false);

  const handleBack = () => {
    showActionSelection();
  };

  return (
    <div className="col-lg-8">
      <AttributesModal
        show={showAttributesModal}
        onHide={() => setShowAttributesModal(false)}
        action={action}
        selectedAttribute={selectedAttribute}
        setSelectedAttribute={setSelectedAttribute}
        inputs={inputs}
      />
      <PredicatesModal
        show={showConditionsModal}
        onHide={() => setShowConditionsModal(false)}
        selectedPredicate={selectedPredicate}
        setSelectedPredicate={setSelectedPredicate}
        inputs={inputs}
        setInputs={setInputs}
      />
      {loading && (
        <div
          className="col text-center"
          style={{ width: "50%", margin: "auto" }}
        >
          <AdaptiveSpinnerComponent spinner={spinner} divisionMultiplier={4} />
        </div>
      )}
      {!errorMessage && createSuccessful ? (
        <SuccessMsgWrapper handleBack={handleBack} schemaTitle={inputs.title} />
      ) : (
        <>
          {errorMessage && (
            <div
              className="alert alert-danger"
              role="alert"
              data-test="error-msg"
            >
              <strong>Error!</strong> {errorMessage}
            </div>
          )}

          <form className="form-horizontal" onSubmit={handleSubmit} noValidate>
            <div className="form-group required">
              <label
                htmlFor="form-title"
                className="col-sm-3 col-md-2 control-label"
              >
                Schema Title
              </label>
              <div className="col-sm-9 col-md-10">
                <input
                  type="text"
                  className="form-control"
                  id="form-title"
                  required
                  value={inputs.title || ""}
                  onChange={handleInputChange}
                  name="title"
                />
                {inputError && inputError["title"] && (
                  <InputFieldError error={inputError["title"]} />
                )}
              </div>
            </div>
            <div className="form-group required">
              <label
                htmlFor="form-name"
                className="col-sm-3 col-md-2 control-label"
              >
                Schema Name
              </label>
              <div className="col-sm-9 col-md-10">
                <input
                  type="text"
                  className="form-control"
                  id="form-name"
                  readOnly
                  value={inputs.name}
                  name="name"
                />
              </div>
            </div>
            <div className="form-group required">
              <label
                htmlFor="form-version"
                className="col-sm-3 col-md-2 control-label"
              >
                Version
              </label>
              <div className="col-sm-9 col-md-10">
                <input
                  type="number"
                  className="form-control"
                  name="version"
                  onChange={handleInputChange}
                  value={inputs.version || ""}
                  required
                  id="form-version"
                />
                {inputError && inputError["version"] && (
                  <InputFieldError error={inputError["version"]} />
                )}
              </div>
            </div>

            <div className="form-group">
              <div
                className="col-sm-3 col-md-2 control-label"
                style={{ padding: "6px 6px 6px 0" }}
              >
                SSIComms
              </div>
              <div className="col-sm-9 col-md-10" style={{ padding: "0" }}>
                <div className="checkbox">
                  <input
                    type="checkbox"
                    className="form-control"
                    name="ssicommsEnabled"
                    onChange={(event) => {
                      setIsSsicomms(!isSsicomms);
                      handleCheckboxChange(event);
                    }}
                    id="ssicomms-checkbox"
                    data-testid="ssicomms-checkbox"
                    checked={isSsicomms}
                  />
                  <label
                    htmlFor="ssicomms-checkbox"
                    className="control-label"
                  ></label>
                </div>
              </div>
            </div>

            {isSsicomms && (
              <div className="form-group">
                <div
                  className="col-sm-3 col-md-2 control-label"
                  style={{ padding: "6px 6px 6px 0" }}
                >
                  Read-only attributes
                </div>
                <div className="col-sm-9 col-md-10" style={{ padding: "0" }}>
                  <table
                    className="table table-active table-responsive"
                    style={{ marginBottom: "18px" }}
                    id="ssicomms-read-only-table"
                  >
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th className="small-table-cell">Type</th>
                        <th>Has predicates</th>
                      </tr>
                    </thead>
                    <tbody>
                      {readOnlyAttr.map((attr) => (
                        <tr key={attr.name} id={attr.name}>
                          <th>{attr.title}</th>
                          <th className="small-table-cell">{attr.type}</th>
                          <th>{attr.hasPredicates ? "Yes" : "No"}</th>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </div>
            )}

            <AttributesTable
              setAction={setAction}
              setShowAttributesModal={setShowAttributesModal}
              selectedAttribute={selectedAttribute}
              setSelectedAttribute={setSelectedAttribute}
              inputs={inputs}
              inputError={inputError || ""}
            />
            <PredicatesTable
              setShowConditionsModal={setShowConditionsModal}
              selectedPredicate={selectedPredicate}
              setSelectedPredicate={setSelectedPredicate}
              inputs={inputs}
              inputError={inputError || ""}
            />

            <div className="form-group">
              <div
                className="col-sm-3 col-md-2 control-label"
                style={{ padding: "6px 6px 6px 0" }}
              >
                Policy
              </div>
              <div className="col-sm-9 col-md-10" style={{ padding: "0" }}>
                <div className="checkbox">
                  <input
                    type="checkbox"
                    className="form-control"
                    name="policy"
                    onChange={() => setIsPolicyNeeded(!isPolicyNeeded)}
                    id="form-has-policy"
                    data-testid="policy-checkbox"
                    checked={isPolicyNeeded}
                  />
                  <label
                    htmlFor="form-has-policy"
                    className="control-label"
                  ></label>
                </div>
              </div>
            </div>
            {isPolicyNeeded && (
              <PolicyTextBox
                value={textValue}
                handlePolicyChange={handlePolicyChange}
                handleValidation={handleValidation}
                isPolicyValid={isPolicyValid}
                error={policyError}
              />
            )}
            <div className="form-group offset-sm-3 offset-md-2 no-gutters">
              <ActionButtons
                handleBack={handleBack}
                disabled={isPolicyNeeded && !isPolicyValid}
              />
            </div>
          </form>
        </>
      )}
    </div>
  );
};

export default CreateSchemaForm;
