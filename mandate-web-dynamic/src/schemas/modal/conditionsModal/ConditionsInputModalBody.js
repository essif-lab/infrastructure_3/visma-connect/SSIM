// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React from "react";

const conditions = [
  {
    id: "greater-than",
    title: "Greater than",
    value: ">",
  },
  {
    id: "greater-than-or-equal",
    title: "Greater than or equal",
    value: ">=",
  },
  {
    id: "less-than",
    title: "Less than",
    value: "<",
  },
  {
    id: "less-than-or-equal",
    title: "Less than or equal",
    value: "<=",
  },
];

const ConditionsInputModalBody = ({ condition, setCondition }) => {
  const setConditionValue = (conditions, value) => {
    return conditions
      .filter((cond) => (cond.title === value ? cond.value : ""))
      .map((cond) => cond.value)[0];
  };

  const handleChange = (event) => {
    setCondition({
      title: event.target.value,
      value: setConditionValue(conditions, event.target.value),
    });
  };

  return (
    <form className="form-horizontal">
      <div className="form-group required">
        <label className="col-sm-3 col-md-2 control-label">Title</label>
        <div className="col-sm-9 col-md-10">
          {conditions.map((cond) => (
            <div key={cond.id} className="d-flex">
              <input
                type="radio"
                id={cond.id}
                name="title"
                value={cond.title}
                checked={condition.title === cond.title}
                onChange={handleChange}
              />
              <label htmlFor={cond.id}>{cond.title}</label>
            </div>
          ))}
        </div>
      </div>
      <div className="form-group required">
        <label
          htmlFor="form-cond-value"
          className="col-sm-3 col-md-2 control-label"
        >
          Value
        </label>
        <div className="col-sm-9 col-md-10">
          <input
            type="text"
            className="form-control"
            id="form-cond-value"
            required
            value={condition.value}
            readOnly
            name="value"
          />
        </div>
      </div>
    </form>
  );
};

export default ConditionsInputModalBody;
