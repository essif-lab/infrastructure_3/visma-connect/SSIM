// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
const useConditionActions = (props) => {
  const { inputs } = props;

  const handleAdd = (predicate, value) => {
    let predicateList = inputs["predicates"];
    for (let i = 0; i < predicateList.length; i++) {
      if (predicateList[i].name === predicate.name) {
        let conditionList = predicateList[i].condition;

        if (!conditionList.some((cond) => cond.title === value.title)) {
          conditionList.push(value);
          predicateList[i].condition = conditionList;
        }
      }
    }
  };

  const handleEdit = (predicate, oldName, value) => {
    let predicateList = inputs["predicates"];
    for (let i = 0; i < predicateList.length; i++) {
      if (predicateList[i].name === predicate.name) {
        let conditionList = predicateList[i].condition;
        const condIndex = conditionList.findIndex(
          (obj) => obj.title === oldName
        );

        if (!conditionList.some((cond) => cond.title === value.title)) {
          conditionList[condIndex] = value;
          predicateList[i].condition = conditionList;
        }
      }
    }
  };

  const handleRemove = (predicate, value) => {
    let predicateList = inputs["predicates"];
    for (let i = 0; i < predicateList.length; i++) {
      if (predicateList[i].name === predicate.name) {
        let conditionList = predicateList[i].condition;
        const condIndex = conditionList.findIndex(
          (obj) => obj.title === value.title
        );
        if (condIndex !== -1) {
          conditionList.splice(condIndex, 1);
          predicateList[i].condition = conditionList;
        }
      }
    }
  };

  return {
    handleAdd,
    handleEdit,
    handleRemove,
  };
};
export default useConditionActions;
