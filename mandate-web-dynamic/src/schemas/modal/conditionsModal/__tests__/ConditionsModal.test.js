// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import ConditionsModal from "../ConditionsModal";
import ConditionsInputModalBody from "../ConditionsInputModalBody";

let calledHandleAdd = false;
let calledHandleEdit = false;
let calledHandleRemove = false;

jest.mock("../ConditionActionHooks", () => {
  const handleAdd = () => (calledHandleAdd = true);
  const handleEdit = () => (calledHandleEdit = true);
  const handleRemove = () => (calledHandleRemove = true);

  return () => ({
    handleAdd,
    handleEdit,
    handleRemove,
  });
});

describe("<ConditionsModal /> modal show/don't show", () => {
  test("Should show modal", () => {
    const props = {
      onHide: jest.fn(),
      show: true,
      action: "add",
      selectedCondition: {},
      setSelectedCondition: jest.fn(),
      oldTitle: "",
      predicate: {},
      inputs: {},
    };
    const conditionsModalWrapper = shallow(<ConditionsModal {...props} />);

    expect(
      conditionsModalWrapper.find("#modal-manage-conditions").prop("className")
    ).toBe("modal in");
  });

  test("Should hide modal", () => {
    const props = {
      onHide: jest.fn(),
      show: false,
      action: "add",
      selectedCondition: {},
      setSelectedCondition: jest.fn(),
      oldTitle: "",
      predicate: {},
      inputs: {},
    };
    const conditionsModalWrapper = shallow(<ConditionsModal {...props} />);

    expect(
      conditionsModalWrapper.find("#modal-manage-conditions").prop("className")
    ).toBe("modal ");
  });
});

describe("<ConditionsModal /> action 'add'", () => {
  let calledOnHide = false;
  const props = {
    onHide: () => (calledOnHide = true),
    show: true,
    action: "add",
    selectedCondition: {},
    setSelectedCondition: jest.fn(),
    oldTitle: "",
    predicate: {},
    inputs: {},
  };
  const conditionsModalWrapper = shallow(<ConditionsModal {...props} />);

  test("Should render 'add' condition body", () => {
    expect(
      conditionsModalWrapper.find("ConditionsInputModalBody").exists()
    ).toBeTruthy();
  });

  test("Should render 'add another' checkbox", () => {
    expect(
      conditionsModalWrapper.find("#form-add-another-cond").exists()
    ).toBeTruthy();
  });

  test("Should handle add conditions without adding another", () => {
    expect(calledOnHide).toBeFalsy();
    expect(calledHandleAdd).toBeFalsy();
    conditionsModalWrapper.find("#modal-btn-manage-cond").simulate("click", {});
    expect(calledOnHide).toBeTruthy();
    expect(calledHandleAdd).toBeTruthy();
  });

  test("Should handle add conditions with adding another", () => {
    calledOnHide = false;
    expect(calledOnHide).toBeFalsy();
    conditionsModalWrapper
      .find("#form-add-another-cond")
      .simulate("change", { target: { checked: true }, persist: jest.fn() });
    conditionsModalWrapper.find("#modal-btn-manage-cond").simulate("click", {});
    expect(calledOnHide).toBeFalsy();
  });
});

describe("<ConditionsModal /> action 'edit'", () => {
  let calledOnHide = false;
  const props = {
    onHide: () => (calledOnHide = true),
    show: true,
    action: "edit",
    selectedCondition: {},
    setSelectedCondition: jest.fn(),
    oldTitle: "",
    predicate: {},
    inputs: {},
  };
  const conditionsModalWrapper = shallow(<ConditionsModal {...props} />);

  test("Should render 'edit' condition body", () => {
    expect(
      conditionsModalWrapper.find("ConditionsInputModalBody").exists()
    ).toBeTruthy();
  });

  test("Should handle edit conditions", () => {
    expect(calledOnHide).toBeFalsy();
    expect(calledHandleEdit).toBeFalsy();
    conditionsModalWrapper.find("#modal-btn-manage-cond").simulate("click", {});
    expect(calledOnHide).toBeTruthy();
    expect(calledHandleEdit).toBeTruthy();
  });

  test("Should have selectedCondition values if selectedCondition not empty", () => {
    const props = {
      onHide: () => (calledOnHide = true),
      show: true,
      action: "edit",
      selectedCondition: { title: "Less than", value: "<" },
      setSelectedCondition: jest.fn(),
      oldTitle: "",
      predicate: {},
      inputs: {},
    };
    const conditionsModalWrapper = shallow(<ConditionsModal {...props} />);

    expect(
      conditionsModalWrapper.find(ConditionsInputModalBody).prop("condition")
    ).toEqual({ title: "Less than", value: "<" });
  });
});

describe("<ConditionsModal /> action 'remove'", () => {
  let calledOnHide = false;
  const props = {
    onHide: () => (calledOnHide = true),
    show: true,
    action: "remove",
    selectedCondition: { title: "Greater than", value: ">" },
    setSelectedCondition: jest.fn(),
    oldTitle: "",
    predicate: {},
    inputs: {},
  };
  const conditionsModalWrapper = shallow(<ConditionsModal {...props} />);

  test("Should render 'remove' condition body", () => {
    expect(
      conditionsModalWrapper
        .find("div[className='modal-body']")
        .childAt(0)
        .text()
    ).toBe("Are you sure you want to remove selected condition?");
  });

  test("Should handle remove conditions", () => {
    expect(calledOnHide).toBeFalsy();
    expect(calledHandleRemove).toBeFalsy();
    conditionsModalWrapper.find("#modal-btn-manage-cond").simulate("click", {});
    expect(calledOnHide).toBeTruthy();
    expect(calledHandleRemove).toBeTruthy();
  });
});
