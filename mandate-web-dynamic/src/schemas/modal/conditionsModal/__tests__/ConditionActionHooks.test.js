// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import useConditionActions from "../ConditionActionHooks";
import { act } from "react-dom/test-utils";
import ReactDOM from "react-dom";

let result;

function TestComponent(inputs) {
  result = useConditionActions(inputs);
  return null;
}

let containerTest;

beforeEach(() => {
  containerTest = document.createElement("div");
  document.body.appendChild(containerTest);
});

afterEach(() => {
  document.body.removeChild(containerTest);
  containerTest = null;
});

describe("<ConditionActionHooks /> - handle add", () => {
  test("Should handle add with empty condition list", () => {
    const inputs = {
      predicates: [
        {
          name: "predName",
          condition: [],
        },
      ],
    };
    const predicate = {
      name: "predName",
    };
    const value = {
      title: "Less than",
      value: "<",
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleAdd(predicate, value);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle add with non-existing predicate", () => {
    const inputs = {
      predicates: [
        {
          name: "predName",
          condition: [],
        },
      ],
    };
    const predicate = {
      name: "predNameInvalid",
    };
    const value = {
      title: "Less than",
      value: "<",
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleAdd(predicate, value);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle add with already present condition", () => {
    const inputs = {
      predicates: [
        {
          name: "predName",
          condition: [
            {
              title: "Less than",
              value: "<",
            },
          ],
        },
      ],
    };
    const predicate = {
      name: "predName",
    };
    const value = {
      title: "Less than",
      value: "<",
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleAdd(predicate, value);
    });
    expect(true).toBeTruthy();
  });
});

describe("<ConditionActionHooks /> - handle edit", () => {
  test("Should handle edit with existing predicate and condition", () => {
    const inputs = {
      predicates: [
        {
          name: "predName",
          condition: [
            {
              title: "Less than",
              value: "<",
            },
          ],
        },
      ],
    };
    const predicate = {
      name: "predName",
    };
    const value = {
      title: "Less than",
      value: "<",
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleEdit(predicate, "condTitle", value);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle edit with existing predicate and non-existing condition", () => {
    const inputs = {
      predicates: [
        {
          name: "predName",
          condition: [
            {
              title: "Greater than",
              value: ">",
            },
          ],
        },
      ],
    };
    const predicate = {
      name: "predName",
    };
    const value = {
      title: "Greater than or equal",
      value: ">=",
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleEdit(predicate, "Greater than", value);
    });

    expect(true).toBeTruthy();
  });

  test("Should handle edit with non-existing predicate", () => {
    const inputs = {
      predicates: [
        {
          name: "predName",
          condition: [
            {
              title: "Greater than",
              value: ">",
            },
          ],
        },
      ],
    };
    const predicate = {
      name: "predNameInvalid",
    };
    const value = {
      title: "Greater than",
      value: ">",
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleEdit(predicate, "condTitle", value);
    });
    expect(true).toBeTruthy();
  });
});

describe("<ConditionActionHooks /> - handle remove", () => {
  test("Should handle remove with existing predicate and condition", () => {
    const inputs = {
      predicates: [
        {
          name: "predName",
          condition: [
            {
              title: "Greater than",
              value: ">",
            },
          ],
        },
      ],
    };
    const predicate = {
      name: "predName",
    };
    const value = {
      title: "Greater than",
      value: ">",
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleRemove(predicate, value);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle remove with existing predicate and non-condition", () => {
    const inputs = {
      predicates: [
        {
          name: "predName",
          condition: [
            {
              title: "Greater than",
              value: ">",
            },
          ],
        },
      ],
    };
    const predicate = {
      name: "predName",
    };
    const value = {
      title: "Greater than or equal",
      value: ">=",
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleRemove(predicate, value);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle remove with non-existing predicate", () => {
    const inputs = {
      predicates: [
        {
          name: "predName",
          condition: [
            {
              title: "Greater than",
              value: ">",
            },
          ],
        },
      ],
    };
    const predicate = {
      name: "predNameInvalid",
    };
    const value = {
      title: "Greater than",
      value: ">",
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleRemove(predicate, value);
    });
    expect(true).toBeTruthy();
  });
});
