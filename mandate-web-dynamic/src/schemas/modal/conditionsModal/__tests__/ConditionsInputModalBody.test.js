// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import ConditionsInputModalBody from "../ConditionsInputModalBody";
import React from "react";

describe("<ConditionsInputModalBody /> with hooks", () => {
  const props = {
    condition: { title: "Greater than", value: ">" },
    setCondition: jest.fn(),
  };
  let container = shallow(<ConditionsInputModalBody {...props} />);

  test("Should handle checkbox changes", () => {
    const firstCheckbox = container.find("#greater-than");
    const secondCheckbox = container.find("#greater-than-or-equal");
    const thirdCheckbox = container.find("#less-than");
    const fourthCheckbox = container.find("#less-than-or-equal");

    expect(firstCheckbox.prop("checked")).toBe(true);
    expect(container.find("#form-cond-value").prop("value")).toEqual(">");

    secondCheckbox.simulate("change", {
      persist: jest.fn(),
      target: {
        value: "Less than",
      },
    });
    thirdCheckbox.simulate("change", {
      persist: jest.fn(),
      target: {
        value: "Less than or equal",
      },
    });
    fourthCheckbox.simulate("change", {
      persist: jest.fn(),
      target: {
        value: "Greater than",
      },
    });
    firstCheckbox.simulate("change", {
      persist: jest.fn(),
      target: {
        value: "Greater than or equal",
      },
    });
  });
});
