// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useState } from "react";
import ConditionsInputModalBody from "./ConditionsInputModalBody";
import useConditionActions from "./ConditionActionHooks";

/**
 * @author Lukas Nakas
 * @description Modal component for managing conditions for selected predicate
 * @Return {JSX.Element}
 */
const ConditionsModal = ({
  onHide,
  show,
  action,
  selectedCondition,
  setSelectedCondition,
  oldTitle,
  predicate,
  inputs,
}) => {
  const { handleAdd, handleEdit, handleRemove } = useConditionActions({
    inputs,
  });
  const [condition, setCondition] = useState({
    title: "Greater than",
    value: ">",
  });
  const [addAnother, setAddAnother] = useState(false);

  const convertToTitleCase = () => {
    return action.charAt(0).toUpperCase() + action.substr(1, action.length - 1);
  };

  const handleAddConditions = () => {
    if (!addAnother) {
      onHide();
    }
    handleAdd(predicate, condition);
    setCondition({ title: "Greater than", value: ">" });
  };

  const handleEditConditions = () => {
    onHide();
    handleEdit(predicate, oldTitle, selectedCondition);
    setSelectedCondition({});
  };

  const handleRemoveConditions = () => {
    onHide();
    handleRemove(predicate, selectedCondition);
    setSelectedCondition({});
  };

  const handleSubmit = () => {
    if (action === "add") {
      handleAddConditions();
    } else if (action === "edit") {
      handleEditConditions();
    } else {
      handleRemoveConditions();
    }
  };

  const handleCheckboxChange = (event) => {
    event.persist();
    setAddAnother(event.target.checked);
  };

  return (
    <div
      id="modal-manage-conditions"
      className={`modal ${show ? "in" : ""}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="modal-title"
      aria-hidden="false"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header text-center">
            <h4 id="modal-title" className="modal-title">
              {`${convertToTitleCase()} condition`}
            </h4>
          </div>
          <div className="modal-body">
            {action === "add" ? (
              <ConditionsInputModalBody
                condition={condition}
                setCondition={setCondition}
              />
            ) : action === "edit" ? (
              <ConditionsInputModalBody
                condition={
                  Object.keys(selectedCondition).length === 0
                    ? condition
                    : selectedCondition
                }
                setCondition={setSelectedCondition}
              />
            ) : (
              <div className="text-center">
                Are you sure you want to remove selected condition?
              </div>
            )}
          </div>
          <div className="modal-footer">
            <div className="row justify-content-between">
              <div className="col-12 col-xs-6 text-center text-xs-left order-last order-xs-first">
                <button
                  id="modal-btn-cancel"
                  type="button"
                  className="btn btn-default"
                  onClick={onHide}
                >
                  Cancel
                </button>
              </div>
              <div className="col-12 col-xs-6 text-center text-xs-right order-first order-xs-last">
                <button
                  id="modal-btn-manage-cond"
                  type="button"
                  className="btn btn-primary"
                  onClick={handleSubmit}
                >
                  {convertToTitleCase()}
                </button>
                {action === "add" && (
                  <div className="checkbox">
                    <input
                      type="checkbox"
                      className="form-control"
                      name="add-another"
                      onChange={handleCheckboxChange}
                      id="form-add-another-cond"
                    />
                    <label
                      htmlFor="form-add-another-cond"
                      className="control-label"
                    >
                      Add another
                    </label>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ConditionsModal;
