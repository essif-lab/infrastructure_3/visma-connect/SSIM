// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import PredicatesModal from "../PredicatesModal";
import { shallow, mount } from "enzyme";

describe("<PredicatesModal />", () => {
  let calledOnHide = false;
  const props = {
    onHide: () => (calledOnHide = true),
    show: false,
    selectedPredicate: {
      name: "selectedPredicate",
      condition: [
        {
          title: "Cond 1",
          value: "Val 1",
        },
      ],
    },
    setSelectedPredicate: jest.fn(),
    inputs: {},
    setInputs: jest.fn(),
  };

  test("Should show modal", () => {
    const props = {
      onHide: jest.fn(),
      show: true,
      selectedPredicate: { name: "selectedPredicate" },
      setSelectedPredicate: jest.fn(),
      inputs: {},
      setInputs: jest.fn(),
    };
    const predicatesModalWrapper = shallow(<PredicatesModal {...props} />);
    expect(
      predicatesModalWrapper.find("#modal-manage-predicates").prop("className")
    ).toBe("modal in");
  });

  test("Should hide modal", () => {
    const props = {
      onHide: jest.fn(),
      show: false,
      selectedPredicate: { name: "selectedPredicate" },
      setSelectedPredicate: jest.fn(),
      inputs: {},
      setInputs: jest.fn(),
    };
    const predicatesModalWrapper = shallow(<PredicatesModal {...props} />);
    expect(
      predicatesModalWrapper.find("#modal-manage-predicates").prop("className")
    ).toBe("modal ");
  });

  test("Should trigger onHide for ConditionsModal", () => {
    const predicatesModalWrapper = mount(<PredicatesModal {...props} />);
    predicatesModalWrapper.find("ConditionsModal").prop("onHide").call();
    predicatesModalWrapper.update();
    expect(
      predicatesModalWrapper.find("#modal-manage-predicates").prop("className")
    ).toBe("modal ");
  });

  test("Should handle action 'add'", () => {
    const predicatesModalWrapper = shallow(<PredicatesModal {...props} />);
    predicatesModalWrapper.find("tbody").childAt(0).simulate("click", {});
    predicatesModalWrapper.find("#button-add-cond").simulate("click", {});
    expect(
      predicatesModalWrapper.find("ConditionsModal").prop("show")
    ).toBeTruthy();
  });

  test("Should handle action 'edit'", () => {
    const predicatesModalWrapper = shallow(<PredicatesModal {...props} />);
    predicatesModalWrapper.find("tbody").childAt(0).simulate("click", {});
    predicatesModalWrapper.find("#button-edit-cond").simulate("click", {});
    expect(
      predicatesModalWrapper.find("ConditionsModal").prop("show")
    ).toBeTruthy();
  });

  test("Should handle action 'remove'", () => {
    const predicatesModalWrapper = shallow(<PredicatesModal {...props} />);
    predicatesModalWrapper.find("tbody").childAt(0).simulate("click", {});
    predicatesModalWrapper.find("#button-remove-cond").simulate("click", {});
    expect(
      predicatesModalWrapper.find("ConditionsModal").prop("show")
    ).toBeTruthy();
  });

  test("Should not handle action 'edit'", () => {
    const predicatesModalWrapper = shallow(<PredicatesModal {...props} />);
    predicatesModalWrapper.find("#button-edit-cond").simulate("click", {});
    expect(
      predicatesModalWrapper.find("ConditionsModal").prop("show")
    ).toBeFalsy();
  });

  test("Should not handle action 'remove'", () => {
    const predicatesModalWrapper = shallow(<PredicatesModal {...props} />);
    predicatesModalWrapper.find("#button-remove-cond").simulate("click", {});
    expect(
      predicatesModalWrapper.find("ConditionsModal").prop("show")
    ).toBeFalsy();
  });

  test("Should have no data", () => {
    const props = {
      onHide: jest.fn(),
      show: false,
      selectedPredicate: {
        name: "selectedPredicate",
        condition: [],
      },
      setSelectedPredicate: jest.fn(),
      inputs: {},
      setInputs: jest.fn(),
    };
    const predicatesModalWrapper = shallow(<PredicatesModal {...props} />);
    expect(
      predicatesModalWrapper.find("tbody").childAt(0).childAt(0).text()
    ).toBe("No data entered");
  });

  test("Should handle back", () => {
    const predicatesModalWrapper = shallow(<PredicatesModal {...props} />);
    expect(calledOnHide).toBeFalsy();
    predicatesModalWrapper.find("#modal-btn-back").simulate("click", {});
    expect(calledOnHide).toBeTruthy();
  });
});
