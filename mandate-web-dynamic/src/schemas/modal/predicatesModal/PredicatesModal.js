// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useState } from "react";
import ConditionsModal from "../conditionsModal/ConditionsModal";
import ActionError from "schemas/component/ActionError";

/**
 * @author Lukas Nakas
 * @description Modal component for managing predicates conditions
 * @Return {JSX.Element}
 */
const PredicatesModal = ({
  show,
  onHide,
  selectedPredicate,
  setSelectedPredicate,
  inputs,
  setInputs,
}) => {
  const [showConditionsModal, setShowConditionsModal] = useState(false);
  const [selectedCondition, setSelectedCondition] = useState({});
  const [action, setAction] = useState("");
  const [oldTitle, setOldTitle] = useState("");

  const [error, setError] = useState();

  return (
    <div
      id="modal-manage-predicates"
      className={`modal ${show ? "in" : ""}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="modal-title"
      aria-hidden="false"
    >
      <ConditionsModal
        show={showConditionsModal}
        onHide={() => setShowConditionsModal(false)}
        action={action}
        selectedCondition={selectedCondition}
        setSelectedCondition={setSelectedCondition}
        oldTitle={oldTitle}
        predicate={selectedPredicate}
        inputs={inputs}
        setInputs={setInputs}
      />
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header text-center">
            <h4 id="modal-title" className="modal-title">
              {`Selected predicate's [${selectedPredicate.name}] conditions`}
            </h4>
          </div>
          <div className="modal-body">
            <div className="row">
              <div className="col-1">
                <h3>Actions: </h3>
              </div>
              <div className="col-10 offset-1">
                <div className="row justify-content-between">
                  <div className="col-4">
                    <button
                      type="button"
                      className="btn btn-primary scalable-mid-width"
                      id="button-add-cond"
                      onClick={() => {
                        setAction("add");
                        setShowConditionsModal(true);
                      }}
                    >
                      Add
                    </button>
                  </div>
                  <div className="col-4">
                    <button
                      type="button"
                      className="btn btn-primary scalable-mid-width"
                      id="button-edit-cond"
                      onClick={() => {
                        if (Object.keys(selectedCondition).length) {
                          setAction("edit");
                          setOldTitle(selectedCondition.title);
                          setShowConditionsModal(true);
                          setError();
                        }
                        if (Object.keys(selectedCondition).length === 0) {
                          setError("Please select row you want to edit");
                        }
                      }}
                    >
                      Edit
                    </button>
                  </div>
                  <div className="col-4">
                    <button
                      type="button"
                      className="btn btn-primary scalable-mid-width"
                      id="button-remove-cond"
                      onClick={() => {
                        if (Object.keys(selectedCondition).length) {
                          setAction("remove");
                          setShowConditionsModal(true);
                          setError();
                        }
                        if (Object.keys(selectedCondition).length === 0) {
                          setError("Please select row you want to remove");
                        }
                      }}
                    >
                      Remove
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <ActionError error={error} selectedItem={selectedCondition} />
            <div className="row">
              <div className="col scrollable-container">
                <table className="table table-active table-responsive">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>Value</th>
                    </tr>
                  </thead>
                  <tbody>
                    {selectedPredicate.condition &&
                    selectedPredicate.condition.length ? (
                      selectedPredicate.condition.map((cond) => (
                        <tr
                          key={cond.title}
                          onClick={() => setSelectedCondition(cond)}
                          className={`${
                            selectedCondition.title === cond.title
                              ? "selected-item"
                              : ""
                          }`}
                        >
                          <td>{cond.title}</td>
                          <td>{cond.value}</td>
                        </tr>
                      ))
                    ) : (
                      <tr>
                        <td>No data entered</td>
                        <td />
                      </tr>
                    )}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div className="modal-footer">
            <div className="row justify-content-between">
              <div className="col-12 col-xs-6 text-center text-xs-left order-last order-xs-first">
                <button
                  id="modal-btn-back"
                  type="button"
                  className="btn btn-default"
                  onClick={() => {
                    setSelectedPredicate({});
                    onHide();
                  }}
                >
                  Back
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PredicatesModal;
