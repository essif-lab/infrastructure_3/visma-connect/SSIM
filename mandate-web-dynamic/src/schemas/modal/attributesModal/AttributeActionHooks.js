// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
const useAttributeActions = (props) => {
  const { inputs } = props;
  const handleAdd = (value) => {
    if (inputs["attributes"]) {
      let attributeList = inputs["attributes"];
      if (!attributeList.some((attr) => attr.name === value.name)) {
        attributeList.push(value);
        let predicate = {};
        if (value.hasPredicates) {
          predicate.name = value.name;
          predicate.type = value.type;
          predicate.condition = [];
        }
        let predicateList = inputs["predicates"];
        if (Object.keys(predicate).length) {
          predicateList.push(predicate);
        }
      }
    } else {
      inputs["attributes"] = [value];
      let predicate = {};
      if (value.hasPredicates) {
        predicate.name = value.name;
        predicate.type = value.type;
        predicate.condition = [];
      }
      inputs["predicates"] = Object.keys(predicate).length ? [predicate] : [];
    }
  };

  const handleEdit = (oldName, value) => {
    let attributeList = inputs["attributes"];
    let predicateList = inputs["predicates"];
    const objIndex = attributeList.findIndex((obj) => obj.name === oldName);

    if (!attributeList[objIndex]?.hasPredicates && value.hasPredicates) {
      let predicate = {
        name: value.name,
        type: value.type,
        condition: [],
      };

      predicateList.push(predicate);
    }

    if (attributeList[objIndex]?.hasPredicates && !value.hasPredicates) {
      const predIndex = predicateList.findIndex((obj) => obj.name === oldName);
      predicateList.splice(predIndex, 1);
    }

    if (attributeList[objIndex]?.hasPredicates && value.hasPredicates) {
      const predIndex = predicateList.findIndex((obj) => obj.name === oldName);
      predicateList[predIndex].name = value.name;
    }

    attributeList[objIndex] = value;
  };

  const handleRemove = (value) => {
    let predicateList = inputs["predicates"];
    const predIndex = predicateList?.findIndex(
      (obj) => obj.name === value.name
    );
    if (predIndex !== -1) {
      predicateList?.splice(predIndex, 1);
    }
    let attributeList = inputs["attributes"];
    const attrIndex = attributeList.findIndex((obj) => obj.name === value.name);
    if (attrIndex !== -1) {
      attributeList.splice(attrIndex, 1);
    }
  };

  return {
    handleAdd,
    handleEdit,
    handleRemove,
  };
};
export default useAttributeActions;
