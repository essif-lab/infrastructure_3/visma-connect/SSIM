// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useState, useEffect } from "react";
import AttributesInputModalBody from "./AttributesInputModalBody";
import useAttributeActions from "./AttributeActionHooks";
import { validate } from "./AttributeValidationRules";

/**
 * @author Lukas Nakas
 * @description Modal component for adding attributes for schema creation
 * @Return {JSX.Element}
 */
const AttributesModal = ({
  onHide,
  show,
  action,
  selectedAttribute,
  setSelectedAttribute,
  inputs,
}) => {
  const { handleAdd, handleEdit, handleRemove } = useAttributeActions({
    inputs,
  });

  const [attribute, setAttribute] = useState({
    title: "",
    name: "Enter attribute title...",
    type: "number",
    hasPredicates: false,
  });
  const [oldName, setOldName] = useState("");
  const [dropdownValue, setDropdownValue] = useState("Number");
  const [isChecked, setChecked] = useState(false);
  const [addAnother, setAddAnother] = useState(false);

  const [attributeError, setAttributeError] = useState({});

  const initialValidState = {
    addAction: false,
    editAction: false,
    removeAction: false,
  };
  const [isSubmitting, setIsSubmitting] = useState(initialValidState);
  const [valid, setValid] = useState(initialValidState);

  const formAttributeName = (title) => {
    return title
      ? title.split(" ").join("-").toLowerCase()
      : "Enter attribute title..";
  };

  const convertToTitleCase = () => {
    return action.charAt(0).toUpperCase() + action.substr(1, action.length - 1);
  };

  const handleAddAttributes = () => {
    if (!addAnother) {
      onHide();
    }
    let attr = attribute;
    attr.name = formAttributeName(attribute.title);
    setAttribute({
      title: "",
      name: "Enter attribute title...",
      type: "number",
      hasPredicates: false,
    });
    handleAdd(attr);
  };

  const handleEditAttributes = () => {
    onHide();
    let attr = selectedAttribute;
    attr.name = formAttributeName(selectedAttribute.title);
    handleEdit(oldName, attr);
    setSelectedAttribute({});
  };

  const handleRemoveAttributes = () => {
    onHide();
    let attr = selectedAttribute;
    attr.name = formAttributeName(selectedAttribute.title);
    handleRemove(attr);
    setSelectedAttribute({});
  };

  useEffect(() => {
    if (
      action === "add" &&
      Object.keys(attributeError).length === 0 &&
      isSubmitting.addAction
    ) {
      setValid({
        ...valid,
        addAction: true,
      });
    }
    if (
      action === "edit" &&
      Object.keys(attributeError).length === 0 &&
      isSubmitting.editAction
    ) {
      setValid({
        ...valid,
        editAction: true,
      });
    }
    if (action === "remove" && isSubmitting.removeAction) {
      setValid({
        ...valid,
        removeAction: true,
      });
    }
  }, [
    attributeError,
    isSubmitting.editAction,
    isSubmitting.addAction,
    isSubmitting.removeAction,
  ]);

  useEffect(() => {
    if (valid.addAction && action === "add") {
      setDropdownValue("Number");
      setChecked(false);
      handleAddAttributes();
    }
    if (valid.editAction && action === "edit") {
      setChecked(selectedAttribute.hasPredicates);
      handleEditAttributes();
    }
    if (valid.removeAction && action === "remove") {
      handleRemoveAttributes();
    }
    setValid(initialValidState);
    setIsSubmitting(initialValidState);
  }, [valid.editAction, valid.addAction, valid.removeAction]);

  const handleSubmit = () => {
    if (action === "add") {
      setAttributeError(validate(attribute));
      setIsSubmitting({
        ...isSubmitting,
        addAction: true,
      });
    }
    if (action === "edit") {
      setAttributeError(validate(selectedAttribute));
      setIsSubmitting({
        ...isSubmitting,
        editAction: true,
      });
    }
    if (action === "remove") {
      setIsSubmitting({
        ...isSubmitting,
        removeAction: true,
      });
    }
  };

  const handleCheckboxChange = (event) => {
    event.persist();
    setAddAnother(event.target.checked);
  };

  return (
    <div
      id="modal-manage-attributes"
      className={`modal ${show ? "in" : ""}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="modal-title"
      aria-hidden="false"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header text-center">
            <h4 id="modal-title" className="modal-title">
              {`${convertToTitleCase()} attribute`}
            </h4>
          </div>
          <div className="modal-body">
            {action === "add" ? (
              <AttributesInputModalBody
                attribute={attribute}
                setAttribute={setAttribute}
                formAttributeName={formAttributeName}
                setOldName={setOldName}
                dropdownValue={dropdownValue}
                setDropdownValue={setDropdownValue}
                isChecked={isChecked}
                setChecked={setChecked}
                attributeError={attributeError}
              />
            ) : action === "edit" ? (
              <AttributesInputModalBody
                attribute={selectedAttribute}
                setAttribute={setSelectedAttribute}
                formAttributeName={formAttributeName}
                setOldName={setOldName}
                dropdownValue={selectedAttribute.type}
                setDropdownValue={setDropdownValue}
                isChecked={selectedAttribute.hasPredicates || false}
                setChecked={setChecked}
                attributeError={attributeError}
              />
            ) : (
              <div className="text-center">
                Are you sure you want to remove selected attribute?
              </div>
            )}
          </div>
          <div className="modal-footer">
            <div className="row justify-content-between">
              <div className="col-12 col-xs-6 text-center text-xs-left order-last order-xs-first">
                <button
                  id="modal-btn-cancel"
                  type="button"
                  className="btn btn-default"
                  onClick={onHide}
                >
                  Cancel
                </button>
              </div>
              <div className="col-12 col-xs-6 text-center text-xs-right order-first order-xs-last">
                <button
                  id="modal-btn-manage-attr"
                  type="button"
                  className="btn btn-primary"
                  onClick={handleSubmit}
                >
                  {convertToTitleCase()}
                </button>
                {action === "add" && (
                  <div className="checkbox">
                    <input
                      type="checkbox"
                      className="form-control"
                      name="add-another"
                      onChange={handleCheckboxChange}
                      id="form-add-another-attr"
                    />
                    <label
                      htmlFor="form-add-another-attr"
                      className="control-label"
                    >
                      Add another
                    </label>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AttributesModal;
