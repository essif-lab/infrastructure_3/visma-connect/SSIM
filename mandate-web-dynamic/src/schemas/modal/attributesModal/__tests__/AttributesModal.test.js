// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow, mount } from "enzyme";
import InputFieldError from "../../../component/InputFieldError";

import AttributesModal from "../AttributesModal";

let calledHandleAdd = false;
let calledHandleEdit = false;
let calledHandleRemove = false;

jest.mock("../AttributeActionHooks", () => {
  const handleAdd = () => (calledHandleAdd = true);
  const handleEdit = () => (calledHandleEdit = true);
  const handleRemove = () => (calledHandleRemove = true);

  return () => ({
    handleAdd,
    handleEdit,
    handleRemove,
  });
});

describe("<AttributesModal /> modal show/don't show", () => {
  test("Should show modal", () => {
    const props = {
      onHide: jest.fn(),
      show: true,
      action: "add",
      selectedAttribute: {},
      setSelectedAttribute: jest.fn(),
      inputs: {},
      setInputs: jest.fn(),
    };
    const attributesModalWrapper = shallow(<AttributesModal {...props} />);

    expect(
      attributesModalWrapper.find("#modal-manage-attributes").prop("className")
    ).toBe("modal in");
  });

  test("Should hide modal", () => {
    const props = {
      onHide: jest.fn(),
      show: false,
      action: "add",
      selectedAttribute: {},
      setSelectedAttribute: jest.fn(),
      inputs: {},
      setInputs: jest.fn(),
    };
    const attributesModalWrapper = shallow(<AttributesModal {...props} />);

    expect(
      attributesModalWrapper.find("#modal-manage-attributes").prop("className")
    ).toBe("modal ");
  });
});

describe("<AttributesModal /> action 'add'", () => {
  const props = {
    onHide: jest.fn(),
    show: calledHandleAdd ? true : false,
    action: "add",
    selectedAttribute: {},
    setSelectedAttribute: jest.fn(),
    inputs: {},
    setInputs: jest.fn(),
  };
  const attributesModalWrapper = mount(<AttributesModal {...props} />);

  test("Should render 'add' attribute body", () => {
    expect(
      attributesModalWrapper.find("AttributesInputModalBody").exists()
    ).toBeTruthy();
  });

  test("Should render 'add another' checkbox", () => {
    expect(
      attributesModalWrapper.find("#form-add-another-attr").exists()
    ).toBeTruthy();
  });

  test("Should ...", () => {
    const titleEvent = {
      persist: () => {},
      target: {
        value: "",
        name: "title",
      },
    };
    attributesModalWrapper
      .find("#form-attr-title")
      .simulate("change", titleEvent);
    attributesModalWrapper.find("#modal-btn-manage-attr").simulate("click");
    expect(attributesModalWrapper.find(InputFieldError).exists()).toBe(true);
  });

  test("Should handle add attributes without adding another", () => {
    expect(calledHandleAdd).toBeFalsy();
    const titleEvent = {
      persist: () => {},
      target: {
        value: "Value Title",
        name: "title",
      },
    };
    attributesModalWrapper
      .find("#form-attr-title")
      .simulate("change", titleEvent);
    attributesModalWrapper.find("#modal-btn-manage-attr").simulate("click");
    expect(calledHandleAdd).toBeTruthy();
  });

  test("Should handle add attributes with adding another", () => {
    const titleEvent = {
      persist: () => {},
      target: {
        value: "Value Title",
        name: "title",
      },
    };
    attributesModalWrapper
      .find("#form-attr-title")
      .simulate("change", titleEvent);

    attributesModalWrapper
      .find("#form-add-another-attr")
      .simulate("change", { target: { checked: true }, persist: jest.fn() });
    attributesModalWrapper.find("#modal-btn-manage-attr").simulate("click", {});
  });
});

describe("<AttributesModal /> action 'edit'", () => {
  const props = {
    onHide: jest.fn(),
    show: true,
    action: "edit",
    selectedAttribute: {
      title: "atr",
      name: "atr",
      type: "text",
      hasPredicates: false,
    },
    setSelectedAttribute: jest.fn(),
    inputs: {
      attributes: [
        { title: "atr", name: "atr", type: "text", hasPredicates: false },
      ],
    },
    setInputs: jest.fn(),
  };
  const attributesModalWrapper = mount(<AttributesModal {...props} />);

  test("Should render 'edit' attribute body", () => {
    expect(
      attributesModalWrapper.find("AttributesInputModalBody").exists()
    ).toBeTruthy();
  });

  test("Should handle edit attributes", () => {
    expect(calledHandleEdit).toBeFalsy();
    const titleEvent = {
      persist: () => {},
      target: {
        value: "Value Title2",
        name: "title",
      },
    };
    attributesModalWrapper
      .find("#form-attr-title")
      .simulate("change", titleEvent);
    attributesModalWrapper.find("#modal-btn-manage-attr").simulate("click", {});
    attributesModalWrapper.update();
    expect(calledHandleEdit).toBeTruthy();
  });
});

describe("<AttributesModal /> action 'remove'", () => {
  const props = {
    onHide: jest.fn(),
    show: true,
    action: "remove",
    selectedAttribute: {
      title: "atr",
      name: "atr",
      type: "text",
      hasPredicates: false,
    },
    setSelectedAttribute: jest.fn(),
    inputs: {
      attributes: [
        { title: "atr", name: "atr", type: "text", hasPredicates: false },
      ],
    },
    setInputs: jest.fn(),
  };
  const attributesModalWrapper = mount(<AttributesModal {...props} />);

  test("Should render 'remove' attribute body", () => {
    expect(
      attributesModalWrapper
        .find("div[className='modal-body']")
        .childAt(0)
        .text()
    ).toBe("Are you sure you want to remove selected attribute?");
  });

  test("Should handle remove attributes", () => {
    expect(calledHandleRemove).toBeFalsy();
    attributesModalWrapper.find("#modal-btn-manage-attr").simulate("click", {});

    expect(calledHandleRemove).toBeTruthy();
  });
});
