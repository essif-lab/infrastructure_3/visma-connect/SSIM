// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import useAttributeActions from "../AttributeActionHooks";
import { act } from "react-dom/test-utils";
import ReactDOM from "react-dom";

let result;

function TestComponent(inputs) {
  result = useAttributeActions(inputs);
  return null;
}

let containerTest;

beforeEach(() => {
  containerTest = document.createElement("div");
  document.body.appendChild(containerTest);
});

afterEach(() => {
  document.body.removeChild(containerTest);
  containerTest = null;
});

describe("<AttributeActionHooks /> - handle add", () => {
  test("Should handle add with existing attribute list and has predicates", () => {
    const inputs = {
      attributes: [
        {
          name: "name",
          type: "text",
          hasPredicates: false,
        },
      ],
      predicates: [],
    };
    const value = {
      name: "name2",
      type: "text2",
      hasPredicates: true,
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleAdd(value);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle add with existing attribute list and does not have predicates", () => {
    const inputs = {
      attributes: [
        {
          name: "name",
          type: "text",
          hasPredicates: false,
        },
      ],
      predicates: [],
    };
    const value = {
      name: "name2",
      type: "text2",
      hasPredicates: false,
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleAdd(value);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle add with existing attribute list and same attribute", () => {
    const inputs = {
      attributes: [
        {
          name: "name",
          type: "text",
          hasPredicates: true,
        },
      ],
      predicates: [],
    };
    const value = {
      name: "name",
      type: "text",
      hasPredicates: true,
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleAdd(value);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle add without existing attribute list and has predicates", () => {
    const inputs = {};
    const value = {
      name: "name",
      type: "text",
      hasPredicates: true,
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleAdd(value);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle add without existing attribute list and does not have predicates", () => {
    const inputs = {};
    const value = {
      name: "name",
      type: "text",
      hasPredicates: false,
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleAdd(value);
    });
    expect(true).toBeTruthy();
  });
});

describe("<AttributeActionHooks /> - handle edit", () => {
  test("Should handle edit with adding new predicate", () => {
    const inputs = {
      attributes: [
        {
          name: "name",
          type: "text",
          hasPredicates: false,
        },
      ],
      predicates: [],
    };
    const value = {
      name: "name2",
      type: "text2",
      hasPredicates: true,
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleEdit("name", value);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle edit with removing existing predicate", () => {
    const inputs = {
      attributes: [
        {
          name: "name",
          type: "text",
          hasPredicates: true,
        },
      ],
      predicates: [
        {
          name: "name",
          type: "text",
          condition: [],
        },
      ],
    };
    const value = {
      name: "name2",
      type: "text2",
      hasPredicates: false,
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleEdit("name", value);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle edit with updating existing predicate", () => {
    const inputs = {
      attributes: [
        {
          name: "name",
          type: "text",
          hasPredicates: true,
        },
      ],
      predicates: [
        {
          name: "name",
          type: "text",
          condition: [],
        },
      ],
    };
    const value = {
      name: "name2",
      type: "text2",
      hasPredicates: true,
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleEdit("name", value);
    });
    expect(true).toBeTruthy();
  });
});

describe("<AttributeActionHooks /> - handle remove", () => {
  test("Should handle remove with existing attribute and predicate", () => {
    const inputs = {
      attributes: [
        {
          name: "name",
          type: "text",
          hasPredicates: true,
        },
      ],
      predicates: [
        {
          name: "name",
          type: "text",
          condition: [],
        },
      ],
    };
    const value = {
      name: "name",
      type: "text",
      hasPredicates: true,
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleRemove(value);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle remove with non-existing attribute and predicate", () => {
    const inputs = {
      attributes: [],
      predicates: [],
    };

    const value = {
      name: "name",
      type: "text",
      hasPredicates: true,
    };

    act(() => {
      ReactDOM.render(<TestComponent inputs={inputs} />, containerTest);
      result.handleRemove(value);
    });
    expect(true).toBeTruthy();
  });
});
