// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { validate } from "../AttributeValidationRules";

let result = {};

describe("AttributeValidationRules", () => {
  test("Should validate field when nothing is passed", () => {
    const testProps = {
      title: "",
    };
    result = validate(testProps);

    expect(result).toStrictEqual({
      title: "Title is required.",
    });
  });

  test("Should validate field when title input is filled", () => {
    const testProps = {
      title: "Title",
    };
    result = validate(testProps);

    expect(result).toStrictEqual({});
  });
});
