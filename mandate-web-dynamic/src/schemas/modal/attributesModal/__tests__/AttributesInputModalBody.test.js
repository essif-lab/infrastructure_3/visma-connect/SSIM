// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow, mount } from "enzyme";
import AttributesInputModalBody from "../AttributesInputModalBody";
import React from "react";
import { act } from "react-dom/test-utils";

describe("<AttributesInputModalBody /> with hooks", () => {
  let calledSetCheckedValue = false;
  const props = {
    attribute: { title: "" },
    setAttribute: jest.fn(),
    formAttributeName: jest.fn(),
    setOldName: jest.fn(),
    dropdownValue: "dropdownValue",
    setDropdownValue: jest.fn(),
    isChecked: false,
    setChecked: (checked) => (calledSetCheckedValue = checked),
    attributeError: "Title is required.",
  };
  let container = shallow(<AttributesInputModalBody {...props} />);

  test("Should open dropdown menu", () => {
    const mockedEvent = { stopPropagation: () => {}, preventDefault: () => {} };
    act(() => {
      container
        .find("button[id='dropdown-basic-button']")
        .simulate("click", mockedEvent);
    });
    const dropdownWrapper = container.find("div[className='dropdown open']");
    expect(dropdownWrapper.exists()).toBeTruthy();
  });

  test("Should call handleSelect", () => {
    const type = {
      title: "Number",
    };

    expect(
      container.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
    container.find("TypeList").invoke("handleSelect")(type);
    expect(container.find("div[className='dropdown ']").exists()).toBeTruthy();
  });

  test("Should handle Title input change", () => {
    const input = container.find("#form-attr-title");
    expect(input.props().value).toBe("");

    input.simulate("change", {
      persist: jest.fn(),
      target: { value: "Attr Title", name: "title" },
    });
  });

  test("Should handle Name input change", () => {
    const input = container.find("#form-attr-name");
    expect(input.props().value).toBe("Enter attribute title...");

    input.simulate("change", {
      persist: jest.fn(),
      target: { value: "Name" },
    });
  });

  test("Should handle checkbox change", () => {
    const checkbox = container.find("#form-has-predicates");

    expect(calledSetCheckedValue).toBeFalsy();
    checkbox.simulate("change", {
      persist: jest.fn(),
      target: { checked: true },
    });
    expect(calledSetCheckedValue).toBeTruthy();
  });
});

describe("<AttributesInputModalBody /> with useEffect", () => {
  const props = {
    attribute: { title: "Attr Title" },
    setAttribute: jest.fn(),
    formAttributeName: jest.fn(),
    setOldName: jest.fn(),
    dropdownValue: "dropdownValue",
    setDropdownValue: jest.fn(),
    isChecked: false,
    setChecked: jest.fn(),
    attributeError: "",
  };

  test("Should handle mousedown listener", () => {
    jest
      .spyOn(document, "addEventListener")
      .mockImplementation(() => jest.fn());
    jest
      .spyOn(document, "removeEventListener")
      .mockImplementation(() => jest.fn());
    const container = mount(<AttributesInputModalBody {...props} />);
    expect(document.addEventListener).toHaveBeenLastCalledWith(
      "mousedown",
      expect.any(Function)
    );
    act(() => {
      container.unmount();
    });
    expect(document.removeEventListener).toHaveBeenLastCalledWith(
      "mousedown",
      expect.any(Function)
    );
  });

  test("Should handle click outside when dropdown ref does not contain target", async () => {
    const map = {};
    document.addEventListener = jest.fn((event, callback) => {
      map[event] = callback;
    });
    const wrapper = mount(<AttributesInputModalBody {...props} />);
    const mockedEvent = { stopPropagation: () => {}, preventDefault: () => {} };
    act(() => {
      wrapper
        .find("button[id='dropdown-basic-button']")
        .simulate("click", mockedEvent);
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
    act(() => {
      map.mousedown({ target: null });
    });
    wrapper.update();
    expect(wrapper.find("div[className='dropdown ']").exists()).toBeTruthy();
  });

  test("Should handle click outside when dropdown ref contains target", async () => {
    const map = {};
    document.addEventListener = jest.fn((event, callback) => {
      map[event] = callback;
    });
    const wrapper = mount(<AttributesInputModalBody {...props} />);
    const mockedEvent = {
      AttributesInputModalBody: () => {},
      preventDefault: () => {},
    };
    act(() => {
      wrapper
        .find("button[id='dropdown-basic-button']")
        .simulate("click", mockedEvent);
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
    act(() => {
      map.mousedown({
        target: wrapper.find("button[id='dropdown-basic-button']").getDOMNode(),
      });
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
  });
});
