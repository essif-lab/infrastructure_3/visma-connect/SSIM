// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useState, useEffect } from "react";
import TypeList from "../../type/TypeList";
import { Types } from "../../type/Types";
import InputFieldError from "../../component/InputFieldError";

const AttributesInputModalBody = ({
  attribute,
  setAttribute,
  formAttributeName,
  setOldName,
  dropdownValue,
  setDropdownValue,
  isChecked,
  setChecked,
  attributeError,
}) => {
  const [dropdownMenuActive, setDropdownMenuActive] = useState(false);

  const dropdownRef = React.createRef();

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  });

  useEffect(() => {
    setOldName(attribute.name);
  });

  const handleClickOutside = (e) => {
    if (dropdownRef.current && !dropdownRef.current.contains(e.target)) {
      setDropdownMenuActive(false);
    }
  };

  const dropdownMenuOpen = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setDropdownMenuActive(!dropdownMenuActive);
  };

  const handleSelect = (type) => {
    setAttribute((attribute) => ({
      ...attribute,
      type: type.title.toLowerCase(),
    }));
    setDropdownValue(type.title);
    setDropdownMenuActive(false);
  };

  const handleInputChange = (event) => {
    event.persist();
    setAttribute((attribute) => ({
      ...attribute,
      [event.target.name]: event.target.value,
    }));
    setDropdownMenuActive(false);
  };

  const handleCheckboxChange = (event) => {
    event.persist();
    setChecked(event.target.checked);
    setAttribute((attribute) => ({
      ...attribute,
      hasPredicates: event.target.checked,
    }));
  };

  return (
    <form className="form-horizontal">
      <div className="form-group required">
        <label
          htmlFor="form-attr-title"
          className="col-sm-3 col-md-2 control-label"
        >
          Title
        </label>
        <div className="col-sm-9 col-md-10">
          <input
            type="text"
            className="form-control"
            id="form-attr-title"
            required
            value={attribute.title || ""}
            onChange={handleInputChange}
            name="title"
          />
          {attributeError && attributeError["title"] && (
            <InputFieldError error={attributeError["title"]} />
          )}
        </div>
      </div>
      <div className="form-group required">
        <label
          htmlFor="form-attr-name"
          className="col-sm-3 col-md-2 control-label"
        >
          Name
        </label>
        <div className="col-sm-9 col-md-10">
          <input
            type="text"
            className="form-control"
            id="form-attr-name"
            readOnly
            value={
              formAttributeName(attribute.title) || "Enter attribute title..."
            }
            name="name"
          />
        </div>
      </div>
      <div className="form-group required">
        <label htmlFor="form-type" className="col-sm-3 col-md-2 control-label">
          Type
        </label>
        <div
          className={`dropdown ${dropdownMenuActive ? "open" : ""}`}
          ref={dropdownRef}
          style={{ width: "40%" }}
        >
          <button
            type="button"
            id="dropdown-basic-button"
            className="btn btn-default dropdown-toggle btn-lg"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
            onClick={dropdownMenuOpen}
            style={{ width: "100%" }}
          >
            {dropdownValue}
          </button>
          <TypeList handleSelect={handleSelect} types={Types} />
        </div>
      </div>
      <div className="checkbox">
        <input
          type="checkbox"
          className="form-control"
          name="has-predicates"
          onChange={handleCheckboxChange}
          id="form-has-predicates"
          checked={isChecked}
        />
        <label htmlFor="form-has-predicates" className="control-label">
          Has predicates
        </label>
      </div>
    </form>
  );
};

export default AttributesInputModalBody;
