// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { useState, useEffect } from "react";
import axios from "axios";
import { validate } from "./FormValidationRules";

const useSchemaCreationForm = () => {
  const API_DOMAIN = process.env.REACT_APP_DOMAIN || "unknown";
  console.log("api-domain", API_DOMAIN);
  const url = `${API_DOMAIN}/schemas`;
  const policyValidationUrl = `${API_DOMAIN}/policies/validate`;

  const [inputs, setInputs] = useState({
    name: "Enter schema title..",
    ssicommsEnabled: false,
  });

  const [createSuccessful, setCreateSuccessful] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const [loading, setLoading] = useState(false);

  const [inputError, setInputError] = useState();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [validInputs, setValidInputs] = useState(false);

  const [isPolicyValid, setPolicyIsValid] = useState(false);
  const [textValue, setTextValue] = useState("");
  const [policyError, setPolicyError] = useState("");

  const readOnlyAttr = [
    {
      title: "SIPID Authorized representative",
      name: "sipid-authorized-representative",
      type: "text",
      hasPredicates: false,
    },
    {
      title: "SIPID Mandate provider",
      name: "sipid-mandate-provider",
      type: "text",
      hasPredicates: false,
    },
  ];

  const attrStructure = (array) => {
    return array?.map((x) => {
      return {
        name: x.name,
        title: x.title,
        type: x.type,
      };
    });
  };

  // prepare attributes structure for backend
  const removeUnnecessaryAttr = () => {
    if (!inputs.attributes) {
      return {
        ...inputs,
      };
    }

    return {
      ...inputs,
      policy: textValue,
      attributes:
        inputs.ssicommsEnabled && inputs.attributes.length
          ? [
              ...attrStructure(inputs.attributes),
              ...attrStructure(readOnlyAttr),
            ]
          : attrStructure(inputs.attributes),
    };
  };

  const inputsToPost = removeUnnecessaryAttr();

  useEffect(() => {
    if (inputError && Object.keys(inputError).length === 0 && isSubmitting) {
      setValidInputs(true);
    }
  }, [inputError, isSubmitting]);

  useEffect(() => {
    if (validInputs) {
      console.log("HandleSubmit Schema Creation >>>>", inputsToPost);
      setLoading(true);
      setErrorMessage("");
      const headers = {
        Authorization: sessionStorage["access_token"],
      };
      axios
        .post(url, inputsToPost, { headers: headers })
        .then((response) => {
          let data = response.data;

          if (data !== null && data.status === "ok") {
            console.log("Response Data--->", data);
            setLoading(false);
            setCreateSuccessful(true);
            setErrorMessage("");
          } else if (data !== null && data.status === "already_exists") {
            console.log("Response Data--->", data);
            setLoading(false);
            setCreateSuccessful(false);
            setErrorMessage(
              "Unable to create new schema. Schema with such name already exists. The combination of title and version should be unique."
            );
            setValidInputs(false);
          } else {
            // Error occurred by Mandate-API
            setLoading(false);
            setCreateSuccessful(false);
            setErrorMessage("Unable to create new schema");
            setValidInputs(false);
          }
        })
        .catch((error) => {
          // Error occurred in our service
          setLoading(false);
          setCreateSuccessful(false);
          setErrorMessage("Unable to create new schema");
          setValidInputs(false);
        });
    }
  }, [validInputs]);

  const handleSubmit = (event) => {
    if (event) {
      event.preventDefault();
      setInputError(validate(inputsToPost));
      setIsSubmitting(true);
    }
  };

  const formSchemaName = (title) => {
    return title === undefined
      ? "Enter schema title.."
      : title === ""
      ? ""
      : title.split(" ").join("-").toLowerCase();
  };

  const handleInputChange = (event) => {
    event.persist();
    if (event.target.name === "title") {
      setErrorMessage("");
      setInputs((inputs) => ({
        ...inputs,
        [event.target.name]: event.target.value,
        name: formSchemaName(event.target.value),
      }));
    } else {
      setInputs((inputs) => ({
        ...inputs,
        [event.target.name]: event.target.value,
      }));
    }
  };

  const handleCheckboxChange = (event) => {
    setInputs((inputs) => ({
      ...inputs,
      [event.target.name]: event.target.checked,
    }));
  };

  const handlePolicyChange = (e) => {
    setTextValue(e.target.value);
    setPolicyIsValid(false);
  };

  const handleValidation = async () => {
    if (!textValue) {
      setPolicyError("Text box is empty");
      return;
    }
    await axios
      .post(
        policyValidationUrl,
        { policy: textValue },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then((response) => {
        if (response.data.valid) {
          setPolicyError("");
          setPolicyIsValid(true);
        } else {
          setPolicyError("Validation result is invalid");
        }
      })
      .catch((err) => {
        setPolicyError(`Something wrong. ${err.message}.`);
      });
  };

  return {
    handleSubmit,
    handleInputChange,
    handleCheckboxChange,
    inputs,
    setInputs,
    createSuccessful,
    errorMessage,
    loading,
    inputError,
    textValue,
    isPolicyValid,
    policyError,
    handlePolicyChange,
    handleValidation,
    readOnlyAttr,
  };
};
export default useSchemaCreationForm;
