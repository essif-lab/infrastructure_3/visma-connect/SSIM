// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
export const validate = (values) => {
  let errors = {};

  function hasDecimal(num) {
    return (!!(Number(num) % 1) || num.includes(".")) && num.indexOf(".") !== 0;
  }

  if (!values["title"]) {
    errors["title"] = "Title is required.";
  }

  if (!values["version"]) {
    errors["version"] = "Version is required.";
  }
  if (values["version"] && !hasDecimal(values["version"])) {
    errors["version"] =
      "Please write version with the decimal number (0.1, 1.0 ...).";
  }

  if (
    values["attributes"]?.length === 0 ||
    values["attributes"] === undefined
  ) {
    errors["attributes"] = "Attributes is required.";
  }

  if (values["predicates"]?.length > 0) {
    const predicatesWithouthConditions = values["predicates"]
      .filter(
        (pred) => pred.condition?.length === 0 || pred.condition === undefined
      )
      .map((pred) => pred.name);

    if (predicatesWithouthConditions.length > 0) {
      const invalidPredicateNames = predicatesWithouthConditions.join(", ");
      errors[
        "predicates"
      ] = `Conditions required for predicates: ${invalidPredicateNames}.`;
    }
  }

  return errors;
};
