// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
/**
 * @author Lukas Nakas
 * @description ActionList
 * @Return {JSX.Element}
 */
const ActionList = ({ actions, handleSelect }) => {
  return (
    <ul
      id="dropdown-menu-list"
      className="dropdown-menu"
      role="menu"
      aria-expanded="false"
      aria-hidden="true"
      aria-labelledby="dropdown-basic-button"
    >
      {actions.length ? (
        actions.map((action) => (
          <li role="menuitem" key={action.id}>
            <a href="#" id={action.id} onClick={handleSelect}>
              {action.title}
            </a>
          </li>
        ))
      ) : (
        <li role="menuitem" key="nodata">
          <a href="#" id={"dropdown-basic-button-nodata"}>
            No actions found
          </a>
        </li>
      )}
    </ul>
  );
};

export default ActionList;
