// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React from "react";
import InputFieldError from "./InputFieldError";

const PolicyTextBox = ({
  value,
  handlePolicyChange,
  handleValidation,
  isPolicyValid,
  error,
}) => {
  return (
    <div className="form-group">
      <div className="col-sm-3 col-md-2 control-label" />
      <div className="col-sm-9 col-md-10" style={{ padding: "0" }}>
        <textarea
          className="form-control"
          rows="5"
          value={value}
          onChange={handlePolicyChange}
        ></textarea>
        {error && <InputFieldError error={error} dataTestId="policy-error" />}
      </div>
      <button
        type="button"
        className="btn btn-primary"
        onClick={handleValidation}
        id="validate-policy-btn"
        data-testid="validate-policy-btn"
        style={{ marginTop: "20px", marginLeft: "auto" }}
        disabled={isPolicyValid}
      >
        Validate Policy
      </button>
    </div>
  );
};

export default PolicyTextBox;
