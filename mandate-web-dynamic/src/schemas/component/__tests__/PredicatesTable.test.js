// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import PredicatesTable from "../PredicatesTable";

describe("<PredicatesTable />", () => {
  let calledSetShowConditionsModal = false;
  let calledSetSelectedPredicateValue = {};
  const props = {
    setShowConditionsModal: () => (calledSetShowConditionsModal = true),
    selectedPredicate: {
      someKey: "someValue",
    },
    setSelectedPredicate: (pred) => {
      calledSetSelectedPredicateValue = pred;
    },
    inputs: {
      predicates: [
        {
          name: "pred-name-1",
          type: "number",
          condition: [{ title: "Cond 1" }],
        },
        {
          name: "pred-name-2",
          type: "number",
          condition: [{ title: "Cond 2" }],
        },
      ],
    },
  };
  const predicatesTableWrapper = shallow(<PredicatesTable {...props} />);

  test("Should handle action 'edit'", () => {
    predicatesTableWrapper.find("#button-edit-pred").simulate("click", {});
    expect(calledSetShowConditionsModal).toBeTruthy();
  });

  test("Should render no data", () => {
    const props = {
      setShowConditionsModal: jest.fn(),
      selectedPredicate: {},
      setSelectedPredicate: jest.fn(),
      inputs: {},
    };
    const predicatesTableWrapper = shallow(<PredicatesTable {...props} />);
    expect(
      predicatesTableWrapper.find("tbody").find("tr").childAt(0).text()
    ).toBe("No data entered");
  });

  test("Should render predicate list", () => {
    const tableRow = predicatesTableWrapper.find("tbody").find("tr");
    expect(tableRow.length).toBe(2);

    const pred1 = tableRow.at(0);
    const pred2 = tableRow.at(1);

    expect(pred1.childAt(0).text()).toBe("pred-name-1");
    expect(pred1.childAt(1).text()).toBe("number");
    expect(pred1.childAt(2).childAt(0).text()).toBe("Cond 1");
    expect(pred2.childAt(0).text()).toBe("pred-name-2");
    expect(pred2.childAt(1).text()).toBe("number");
    expect(pred2.childAt(2).childAt(0).text()).toBe("Cond 2");
  });

  test("Should render list with selectable predicates ", () => {
    const tableRow = predicatesTableWrapper.find("tbody").find("tr");
    expect(tableRow.length).toBe(2);

    tableRow.at(0).simulate("click", {});

    expect(calledSetSelectedPredicateValue.name).toBe("pred-name-1");
    expect(calledSetSelectedPredicateValue.type).toBe("number");
    expect(calledSetSelectedPredicateValue.condition).toEqual([
      { title: "Cond 1" },
    ]);
  });

  test("Should selected predicate with proper className", () => {
    const props = {
      selectedPredicate: {
        name: "pred-name-1",
        type: "number",
        condition: [{ title: "Cond 1" }],
      },
      setSelectedPredicate: (pred) => {
        calledSetSelectedPredicateValue = pred;
      },
      inputs: {
        predicates: [
          {
            name: "pred-name-1",
            type: "number",
            condition: [{ title: "Cond 1" }],
          },
        ],
      },
    };
    const predicatesTableWrapper = shallow(<PredicatesTable {...props} />);

    expect(
      predicatesTableWrapper.find("tbody").find("tr").at(0).prop("className")
    ).toBe("selected-item");
  });
});

describe("<PredicatesTable /> without selectedPredicate", () => {
  let calledSetShowConditionsModal = false;
  const props = {
    setShowConditionsModal: jest.fn(),
    selectedPredicate: {},
    setSelectedPredicate: jest.fn(),
    inputs: {},
  };
  const predicatesTableWrapper = shallow(<PredicatesTable {...props} />);

  test("Should not trigger action 'edit'", () => {
    predicatesTableWrapper.find("#button-edit-pred").simulate("click", {});
    expect(calledSetShowConditionsModal).toBeFalsy();
  });
});
