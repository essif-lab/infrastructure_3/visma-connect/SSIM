// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import AttributesTable from "../AttributesTable";

describe("<AttributesTable />", () => {
  let calledSetActionValue = "";
  let calledSetShowAttributesModal = false;
  let calledSetSelectedAttributeValue = {};
  const props = {
    setAction: (type) => {
      calledSetActionValue = type;
    },
    setShowAttributesModal: () => (calledSetShowAttributesModal = true),
    selectedAttribute: {
      someKey: "someValue",
    },
    setSelectedAttribute: (attr) => {
      calledSetSelectedAttributeValue = attr;
    },
    inputs: {
      attributes: [
        {
          name: "attr-name-1",
          title: "Attr Name 1",
          type: "text",
          hasPredicates: false,
        },
        {
          name: "attr-name-2",
          title: "Attr Name 2",
          type: "text",
          hasPredicates: true,
        },
      ],
    },
  };
  const attributesTableWrapper = shallow(<AttributesTable {...props} />);

  test("Should handle action 'add'", () => {
    attributesTableWrapper.find("#button-add-attr").simulate("click", {});

    expect(calledSetActionValue).toBe("add");
    expect(calledSetShowAttributesModal).toBeTruthy();
  });

  test("Should handle action 'edit'", () => {
    attributesTableWrapper.find("#button-edit-attr").simulate("click", {});

    expect(calledSetActionValue).toBe("edit");
    expect(calledSetShowAttributesModal).toBeTruthy();
  });

  test("Should handle action 'remove'", () => {
    attributesTableWrapper.find("#button-remove-attr").simulate("click", {});

    expect(calledSetActionValue).toBe("remove");
    expect(calledSetShowAttributesModal).toBeTruthy();
  });

  test("Should render no data", () => {
    const props = {
      setAction: jest.fn(),
      setShowAttributesModal: jest.fn(),
      selectedAttribute: {},
      setSelectedAttribute: jest.fn(),
      inputs: {},
    };
    const attributesTableWrapper = shallow(<AttributesTable {...props} />);
    expect(
      attributesTableWrapper.find("tbody").find("tr").childAt(0).text()
    ).toBe("No data entered");
  });

  test("Should render attribute list", () => {
    const tableRow = attributesTableWrapper.find("tbody").find("tr");
    expect(tableRow.length).toBe(2);

    const attr1 = tableRow.at(0);
    const attr2 = tableRow.at(1);

    expect(attr1.childAt(0).text()).toBe("Attr Name 1");
    expect(attr1.childAt(1).text()).toBe("text");
    expect(attr1.childAt(2).text()).toBe("No");
    expect(attr2.childAt(0).text()).toBe("Attr Name 2");
    expect(attr2.childAt(1).text()).toBe("text");
    expect(attr2.childAt(2).text()).toBe("Yes");
  });

  test("Should render list with selectable attributes", () => {
    const tableRow = attributesTableWrapper.find("tbody").find("tr");
    expect(tableRow.length).toBe(2);

    tableRow.at(0).simulate("click", {});

    expect(calledSetSelectedAttributeValue.name).toBe("attr-name-1");
    expect(calledSetSelectedAttributeValue.title).toBe("Attr Name 1");
    expect(calledSetSelectedAttributeValue.type).toBe("text");
    expect(calledSetSelectedAttributeValue.hasPredicates).toBeFalsy();
  });

  test("Should selected attribute with proper className", () => {
    const props = {
      selectedAttribute: {
        name: "attr-name-1",
        title: "Attr Name 1",
        type: "text",
        hasPredicates: false,
      },
      setSelectedAttribute: (attr) => {
        calledSetSelectedAttributeValue = attr;
      },
      inputs: {
        attributes: [
          {
            name: "attr-name-1",
            title: "Attr Name 1",
            type: "text",
            hasPredicates: false,
          },
        ],
      },
    };
    const attributesTableWrapper = shallow(<AttributesTable {...props} />);

    expect(
      attributesTableWrapper.find("tbody").find("tr").at(0).prop("className")
    ).toBe("selected-item");
  });
});

describe("<AttributesTable /> without selectedAttribute", () => {
  let calledSetActionValue = "";
  let calledSetShowAttributesModal = false;
  const props = {
    setAction: jest.fn(),
    setShowAttributesModal: jest.fn(),
    selectedAttribute: {},
    setSelectedAttribute: jest.fn(),
    inputs: {},
  };
  const attributesTableWrapper = shallow(<AttributesTable {...props} />);

  test("Should not trigger action 'edit'", () => {
    attributesTableWrapper.find("#button-edit-attr").simulate("click", {});

    expect(calledSetActionValue).toBe("");
    expect(calledSetShowAttributesModal).toBeFalsy();
  });

  test("Should not trigger action 'remove'", () => {
    attributesTableWrapper.find("#button-remove-attr").simulate("click", {});

    expect(calledSetActionValue).toBe("");
    expect(calledSetShowAttributesModal).toBeFalsy();
  });
});
