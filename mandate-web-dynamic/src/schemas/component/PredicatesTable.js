// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { useState } from "react";
import ActionError from "./ActionError";
import InputFieldError from "./InputFieldError";

const PredicatesTable = ({
  setShowConditionsModal,
  selectedPredicate,
  setSelectedPredicate,
  inputs,
  inputError,
}) => {
  const [error, setError] = useState();

  return (
    <div className="form-group">
      <label
        htmlFor="form-predicates"
        className="col-sm-3 col-md-2 control-label"
      >
        Predicates
      </label>
      <div className="col-sm-9 col-md-10">
        <div className="row">
          <div className="col-1">
            <h3>Actions: </h3>
          </div>
          <div className="col-10 offset-1">
            <div className="row">
              <div className="col-6">
                <button
                  type="button"
                  className="btn btn-primary scalable-mid-width"
                  id="button-edit-pred"
                  onClick={() => {
                    if (Object.keys(selectedPredicate).length) {
                      setShowConditionsModal(true);
                      setError();
                    }
                    if (Object.keys(selectedPredicate).length === 0) {
                      setError(
                        "Please select row you want to manage conditions"
                      );
                    }
                  }}
                >
                  Manage conditions
                </button>
              </div>
            </div>
          </div>
        </div>
        <ActionError error={error} selectedItem={selectedPredicate} />
        <div className="row">
          <div className="col-12 scrollable-container">
            <table
              className="table table-active table-responsive"
              style={
                inputError && inputError["predicates"]
                  ? { marginBottom: "0" }
                  : { marginBottom: "18px" }
              }
            >
              <thead>
                <tr>
                  <th>Name</th>
                  <th className="small-table-cell">Type</th>
                  <th>Conditions</th>
                </tr>
              </thead>
              <tbody>
                {inputs.predicates?.length ? (
                  inputs.predicates.map((pred) => (
                    <tr
                      key={pred.name}
                      onClick={() => setSelectedPredicate(pred)}
                      className={`${
                        selectedPredicate.name === pred.name
                          ? "selected-item"
                          : ""
                      }`}
                    >
                      <td>{pred.name}</td>
                      <td>{pred.type}</td>
                      <td>
                        {pred.condition &&
                          pred.condition.map((cond) => (
                            <div key={cond.title}>{cond.title}</div>
                          ))}
                      </td>
                    </tr>
                  ))
                ) : (
                  <tr>
                    <td>No data entered</td>
                    <td />
                    <td />
                  </tr>
                )}
              </tbody>
            </table>
            {inputError && inputError["predicates"] && (
              <InputFieldError error={inputError["predicates"]} />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default PredicatesTable;
