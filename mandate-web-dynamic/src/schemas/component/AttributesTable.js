// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { useState } from "react";
import ActionError from "./ActionError";
import InputFieldError from "./InputFieldError";

const AttributesTable = ({
  setAction,
  setShowAttributesModal,
  selectedAttribute,
  setSelectedAttribute,
  inputs,
  inputError,
}) => {
  const [error, setError] = useState();

  const attrToShow = inputs.attributes?.filter(
    (x) =>
      x.name !== "sipid-authorized-representative" &&
      x.name !== "sipid-mandate-provider"
  );

  return (
    <div className="form-group required">
      <label
        htmlFor="form-attributes"
        className="col-sm-3 col-md-2 control-label"
      >
        Attributes
      </label>
      <div className="col-sm-9 col-md-10">
        <div className="row">
          <div className="col-1">
            <h3>Actions: </h3>
          </div>
          <div className="col-10 offset-1">
            <div className="row justify-content-between">
              <div className="col-3">
                <button
                  type="button"
                  className="btn btn-primary scalable-mid-width"
                  id="button-add-attr"
                  onClick={() => {
                    setAction("add");
                    setShowAttributesModal(true);
                  }}
                >
                  Add
                </button>
              </div>
              <div className="col-3">
                <button
                  type="button"
                  className="btn btn-primary scalable-mid-width"
                  id="button-edit-attr"
                  onClick={() => {
                    if (Object.keys(selectedAttribute).length) {
                      setAction("edit");
                      setShowAttributesModal(true);
                      setError();
                    }
                    if (Object.keys(selectedAttribute).length === 0) {
                      setError("Please select row you want to edit");
                    }
                  }}
                >
                  Edit
                </button>
              </div>
              <div className="col-3">
                <button
                  type="button"
                  className="btn btn-primary scalable-mid-width"
                  id="button-remove-attr"
                  onClick={() => {
                    if (Object.keys(selectedAttribute).length) {
                      setAction("remove");
                      setShowAttributesModal(true);
                    }
                    if (Object.keys(selectedAttribute).length === 0) {
                      setError("Please select row you want to delete");
                    }
                  }}
                >
                  Remove
                </button>
              </div>
            </div>
          </div>
        </div>
        <ActionError error={error} selectedItem={selectedAttribute} />
        <div className="row">
          <div className="col scrollable-container">
            <table
              className="table table-active table-responsive"
              style={
                inputError && inputError["attributes"]
                  ? { marginBottom: "0" }
                  : { marginBottom: "18px" }
              }
            >
              <thead>
                <tr>
                  <th>Title</th>
                  <th className="small-table-cell">Type</th>
                  <th>Has predicates</th>
                </tr>
              </thead>
              <tbody>
                {attrToShow?.length ? (
                  attrToShow.map((attr) => (
                    <tr
                      key={attr.name}
                      onClick={() => setSelectedAttribute(attr)}
                      className={`${
                        selectedAttribute.name === attr.name
                          ? "selected-item"
                          : ""
                      }`}
                    >
                      <td>{attr.title}</td>
                      <td>{attr.type}</td>
                      <td>{`${attr.hasPredicates ? "Yes" : "No"}`}</td>
                    </tr>
                  ))
                ) : (
                  <tr>
                    <td>No data entered</td>
                    <td />
                    <td />
                  </tr>
                )}
              </tbody>
            </table>
            {inputError && inputError["attributes"] && (
              <InputFieldError error={inputError["attributes"]} />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AttributesTable;
