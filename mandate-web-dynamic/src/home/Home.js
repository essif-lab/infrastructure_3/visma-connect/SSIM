// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import IconWithPopover from "../common/IconWithPopover";
import OutsideClickHandler from "react-outside-click-handler";

/**
 * @author Ragesh Shunmugam, Lukas Nakas
 * @description Home screen to choose from Owner/Verifier/Revocation
 * @Return {JSX.Element}
 */

function Home() {
  const history = useHistory();

  const handleClickOwner = () => {
    history.push("/mandateRequest");
  };

  const handleClickVerifier = () => {
    history.push("/mandateVerifier");
  };

  const handleClickRevocation = () => {
    history.push("/mandateRevocation");
  };

  const handleClickSchemas = () => {
    history.push("/mandateSchemas");
  };

  const [ownerPopover, setOwnerPopover] = useState(false);
  const [verifierPopover, setVerifierPopover] = useState(false);
  const [revocationPopover, setRevocationPopover] = useState(false);
  const [schemasPopover, setSchemasPopover] = useState(false);

  return (
    <div className="request-body">
      <div className="container">
        <div className="row rows justify-content-center align-items-center">
          <div className="columns col-xs-12 col-sm-6 text-center align-button-with-popover">
            <button
              className="btn btn-primary btn-lg btn-block font-20"
              id="button_select_owner"
              type="button"
              onClick={handleClickOwner}
            >
              Owner
            </button>
            <OutsideClickHandler onOutsideClick={() => setOwnerPopover(false)}>
              <IconWithPopover
                id="owner-details-icon"
                handleClick={() => setOwnerPopover(!ownerPopover)}
                showPopover={ownerPopover}
                content="Choose this action to issue credentials"
              />
            </OutsideClickHandler>
          </div>
          <div className="columns col-xs-12 col-sm-6 text-center align-button-with-popover">
            <button
              className="btn btn-primary btn-lg btn-block font-20"
              id="button_select_verifier"
              type="button"
              onClick={handleClickVerifier}
            >
              Verifier
            </button>
            <OutsideClickHandler
              onOutsideClick={() => setVerifierPopover(false)}
            >
              <IconWithPopover
                id="verifier-details-icon"
                handleClick={() => setVerifierPopover(!verifierPopover)}
                showPopover={verifierPopover}
                content="Choose this action to verify credentials"
              />
            </OutsideClickHandler>
          </div>
        </div>
        <div className="row rows justify-content-center align-items-center">
          <div className="columns col-xs-12 col-sm-6 text-center align-button-with-popover">
            <button
              className="btn btn-primary btn-lg btn-block font-20"
              id="button_select_revocation"
              type="button"
              onClick={handleClickRevocation}
            >
              Revocation
            </button>
            <OutsideClickHandler
              onOutsideClick={() => setRevocationPopover(false)}
            >
              <IconWithPopover
                id="revocation-details-icon"
                handleClick={() => setRevocationPopover(!revocationPopover)}
                showPopover={revocationPopover}
                content="Choose this action to revoke credentials"
              />
            </OutsideClickHandler>
          </div>
          <div className="columns col-xs-12 col-sm-6 text-center align-button-with-popover">
            <button
              className="btn btn-primary btn-lg btn-block font-20"
              id="button-select-schemas"
              type="button"
              onClick={handleClickSchemas}
            >
              Schemas
            </button>
            <OutsideClickHandler
              onOutsideClick={() => setSchemasPopover(false)}
            >
              <IconWithPopover
                id="schemas-details-icon"
                handleClick={() => setSchemasPopover(!schemasPopover)}
                showPopover={schemasPopover}
                content="Choose this action to manage schemas"
              />
            </OutsideClickHandler>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Home;
