// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React from "react";
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import Home from "../Home";
import OutsideClickHandler from "react-outside-click-handler";

const mockHistoryPush = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe("<Home />", () => {
  const container = shallow(<Home />);
  test("Should match old snapshot", () => {
    const homeTree = renderer.create(<Home />).toJSON();
    expect(homeTree).toMatchSnapshot();
  });

  test("Should render select-owner button", () => {
    const ownerButtonWrapper = container.find(
      "button[id='button_select_owner']"
    );
    expect(ownerButtonWrapper.exists()).toBeTruthy();
    expect(ownerButtonWrapper.prop("className")).toBe(
      "btn btn-primary btn-lg btn-block font-20"
    );
    expect(ownerButtonWrapper.prop("type")).toBe("button");
    expect(ownerButtonWrapper.text()).toBe("Owner");
  });

  test("Should render select-verifier button", () => {
    const verifierButtonWrapper = container.find(
      "button[id='button_select_verifier']"
    );
    expect(verifierButtonWrapper.exists()).toBeTruthy();
    expect(verifierButtonWrapper.prop("className")).toBe(
      "btn btn-primary btn-lg btn-block font-20"
    );
    expect(verifierButtonWrapper.prop("type")).toBe("button");
    expect(verifierButtonWrapper.prop("onClick")).toBeDefined();
    expect(verifierButtonWrapper.text()).toBe("Verifier");
  });

  test("Should render select-revocation button", () => {
    const revocationButtonWrapper = container.find(
      "button[id='button_select_revocation']"
    );
    expect(revocationButtonWrapper.exists()).toBeTruthy();
    expect(revocationButtonWrapper.prop("className")).toBe(
      "btn btn-primary btn-lg btn-block font-20"
    );
    expect(revocationButtonWrapper.prop("type")).toBe("button");
    expect(revocationButtonWrapper.prop("onClick")).toBeDefined();
    expect(revocationButtonWrapper.text()).toBe("Revocation");
  });

  test("Should render select-schemas button", () => {
    const revocationButtonWrapper = container.find(
      "button[id='button-select-schemas']"
    );
    expect(revocationButtonWrapper.exists()).toBeTruthy();
    expect(revocationButtonWrapper.prop("onClick")).toBeDefined();
    expect(revocationButtonWrapper.text()).toBe("Schemas");
  });

  test("Should trigger handleClickOwner function", () => {
    const mockedEvent = { target: {}, preventDefault: () => {} };
    container
      .find("button[id='button_select_owner']")
      .simulate("click", mockedEvent);
    expect(mockHistoryPush).toHaveBeenCalledWith("/mandateRequest");
  });

  test("Should trigger handleClickVerifier function", () => {
    const mockedEvent = { target: {}, preventDefault: () => {} };
    container
      .find("button[id='button_select_verifier']")
      .simulate("click", mockedEvent);
    expect(mockHistoryPush).toHaveBeenCalledWith("/mandateVerifier");
  });

  test("Should contain four question mark icons", () => {
    expect(container.find("#owner-details-icon").length).toEqual(1);
    expect(container.find("#verifier-details-icon").length).toEqual(1);
    expect(container.find("#revocation-details-icon").length).toEqual(1);
    expect(container.find("#schemas-details-icon").length).toEqual(1);
  });

  test("Owner question mark should be clickable", () => {
    const ownerDetailsIcon = container.find("#owner-details-icon");
    expect(ownerDetailsIcon.length).toEqual(1);

    expect(ownerDetailsIcon.prop("showPopover")).toBeFalsy();
    expect(ownerDetailsIcon.prop("handleClick")).toBeDefined();

    ownerDetailsIcon.prop("handleClick").call();
    container.update();

    const ownerDetailsIconAfter = container.find("#owner-details-icon");
    expect(ownerDetailsIconAfter.prop("showPopover")).toBeTruthy();
  });

  test("Verifier question mark should be clickable", () => {
    const verifierDetailsIcon = container.find("#verifier-details-icon");
    expect(verifierDetailsIcon.length).toEqual(1);
    expect(verifierDetailsIcon.prop("showPopover")).toBeFalsy();
    expect(verifierDetailsIcon.prop("handleClick")).toBeDefined();

    verifierDetailsIcon.prop("handleClick").call();
    container.update();

    const verifierDetailsIconAfter = container.find("#verifier-details-icon");
    expect(verifierDetailsIconAfter.prop("showPopover")).toBeTruthy();
  });

  test("Revocation question mark should be clickable", () => {
    const revocationDetailsIcon = container.find("#revocation-details-icon");
    expect(revocationDetailsIcon.length).toEqual(1);
    expect(revocationDetailsIcon.prop("showPopover")).toBeFalsy();
    expect(revocationDetailsIcon.prop("handleClick")).toBeDefined();

    revocationDetailsIcon.prop("handleClick").call();
    container.update();

    const revocationDetailsIconAfter = container.find(
      "#revocation-details-icon"
    );
    expect(revocationDetailsIconAfter.prop("showPopover")).toBeTruthy();
  });

  test("Schemas question mark should be clickable", () => {
    const schemasDetailsIcon = container.find("#schemas-details-icon");
    expect(schemasDetailsIcon.length).toEqual(1);
    expect(schemasDetailsIcon.prop("showPopover")).toBeFalsy();
    expect(schemasDetailsIcon.prop("handleClick")).toBeDefined();

    schemasDetailsIcon.prop("handleClick").call();
    container.update();

    const schemasDetailsIconAfter = container.find("#schemas-details-icon");
    expect(schemasDetailsIconAfter.prop("showPopover")).toBeTruthy();
  });

  test("Should contain four outside click components, trigger outside clicks", () => {
    const outsideClicker = container.find(OutsideClickHandler);
    expect(outsideClicker.length).toEqual(4);
    expect(outsideClicker.at(0).prop("onOutsideClick")).toBeDefined();
    expect(outsideClicker.at(1).prop("onOutsideClick")).toBeDefined();
    expect(outsideClicker.at(2).prop("onOutsideClick")).toBeDefined();
    expect(outsideClicker.at(3).prop("onOutsideClick")).toBeDefined();

    const ownerDetailsIcon = container.find("#owner-details-icon");
    expect(ownerDetailsIcon.prop("showPopover")).toBeTruthy();

    const verifierDetailsIcon = container.find("#verifier-details-icon");
    expect(verifierDetailsIcon.prop("showPopover")).toBeTruthy();

    const revocationDetailsIcon = container.find("#revocation-details-icon");
    expect(revocationDetailsIcon.prop("showPopover")).toBeTruthy();

    const schemasDetailsIcon = container.find("#schemas-details-icon");
    expect(schemasDetailsIcon.prop("showPopover")).toBeTruthy();

    outsideClicker.at(0).prop("onOutsideClick").call();
    outsideClicker.at(1).prop("onOutsideClick").call();
    outsideClicker.at(2).prop("onOutsideClick").call();
    outsideClicker.at(3).prop("onOutsideClick").call();
    container.update();

    const ownerDetailsIconAfter = container.find("#owner-details-icon");
    expect(ownerDetailsIconAfter.prop("showPopover")).toBeFalsy();

    const verifierDetailsIconAfter = container.find("#verifier-details-icon");
    expect(verifierDetailsIconAfter.prop("showPopover")).toBeFalsy();

    const revocationDetailsIconAfter = container.find(
      "#revocation-details-icon"
    );
    expect(revocationDetailsIconAfter.prop("showPopover")).toBeFalsy();

    const schemasDetailsIconAfter = container.find("#schemas-details-icon");
    expect(schemasDetailsIconAfter.prop("showPopover")).toBeFalsy();
  });

  test("Should trigger handleClickRevocation function", () => {
    const mockedEvent = { target: {}, preventDefault: () => {} };
    container
      .find("button[id='button_select_revocation']")
      .simulate("click", mockedEvent);
    expect(mockHistoryPush).toHaveBeenCalledWith("/mandateRevocation");
  });

  test("Should trigger handleClickSchemas function", () => {
    const mockedEvent = { target: {}, preventDefault: () => {} };
    container
      .find("button[id='button-select-schemas']")
      .simulate("click", mockedEvent);
    expect(mockHistoryPush).toHaveBeenCalledWith("/mandateSchemas");
  });
});
