// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { useState } from "react";
import axios from "axios";

const useSigningForm = (props) => {
  const { schemaName } = props;
  const API_DOMAIN = process.env.REACT_APP_DOMAIN || "unknown";
  console.log("api-domain", API_DOMAIN);
  const url = `${API_DOMAIN}/mandates`;

  const [qrValue, setQrValue] = useState("");
  const [ssiCommsResponse, setSsiCommsResponse] = useState("");
  const [showResultModal, setShowResultModal] = useState(false);
  const [inputs, setInputs] = useState({});
  const [errorMessage, setErrorMessage] = useState("");
  const handleSubmit = (event) => {
    if (event) {
      event.preventDefault();
      console.log("HandleSubmit Mandate Request>>>>", inputs);
      handleShowResultModal();
      // Note that the schemaName will be doubled encoded
      // here mandate-signing-basic:1.0 becomes  mandate-signing-basic%3A1.0
      // and than that is encoded by axios.
      const encodedSchemaName = encodeURIComponent(schemaName);
      console.log("encodeURIComponent() ", encodedSchemaName);
      //Call backend service to store mandate request data in MongoDB
      const params = {
        schemaName: encodedSchemaName,
        ssicommsEnabled: props.ssicommsEnabled,
      };
      const headers = {
        Authorization: sessionStorage["access_token"],
      };
      axios
        .post(url, inputs, {
          params: params,
          headers: headers,
        })
        .then((response) => {
          let data = response.data;
          if (null !== data) {
            console.log("Response Data--->", data);
            const decodedData = atob(data);
            if (props.ssicommsEnabled) {
              setSsiCommsResponse(`${url}/${decodedData}`);
            } else {
              setQrValue(decodedData);
            }
          } else {
            // Error occurred by Mandate-API
            setErrorMessage("Unable to create QR Code");
          }
        })
        .catch((error) => {
          // Error occurred in our service
          setErrorMessage("Unable to create QR Code");
        });
    }
  };
  const handleInputChange = (event) => {
    event.persist();
    setInputs((inputs) => ({
      ...inputs,
      [event.target.name]: event.target.value,
    }));
  };

  const handleDateChange = (name, value) => {
    setInputs((inputs) => ({
      ...inputs,
      [name]: value,
    }));
  };

  const handleShowResultModal = () => {
    setShowResultModal(!showResultModal);
  };

  return {
    handleSubmit,
    handleInputChange,
    handleDateChange,
    handleShowResultModal,
    inputs,
    showResultModal,
    qrValue,
    schemaName,
    errorMessage,
    ssiCommsResponse,
  };
};
export default useSigningForm;
