// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import MandateRequestResultBody from "../MandateRequestResultBody";
import React from "react";
import { act } from "react-dom/test-utils";

describe("<MandateRequestResultBody /> context depending on props", () => {
  test("Should match old snapshot", () => {
    const mandateRequestResultBodyTree = renderer
      .create(<MandateRequestResultBody />)
      .toJSON();
    expect(mandateRequestResultBodyTree).toMatchSnapshot();
  });

  test("Should have adaptive qr component when qrValue is present", () => {
    const props = {
      qrValue: "someQrValue",
    };
    const container = shallow(<MandateRequestResultBody {...props} />);
    const modalWrapper = container.find("AdaptiveQRComponent");
    expect(modalWrapper.exists()).toBeTruthy();
  });

  test("Should have invitation url when ssiCommsResponse is present", () => {
    const props = {
      ssiCommsResponse: "someUrl",
    };
    const container = shallow(<MandateRequestResultBody {...props} />);
    let invitationWrapper = container.find(
      "div[className='ssicomms-response']"
    );
    expect(invitationWrapper.exists()).toBeFalsy();
    act(() => {
      const mockedEvent = { target: {}, preventDefault: () => {} };
      container
        .find("button[id='button-show-url']")
        .simulate("click", mockedEvent);
    });
    invitationWrapper = container.find("div[className='ssicomms-response']");
    expect(invitationWrapper.exists()).toBeTruthy();
    expect(invitationWrapper.text()).toBe("Invitation URL: someUrl");
    expect(container.find("CopyToClipboard").exists()).toBeTruthy();
  });

  test("Should show message about copying url when it is copied to clipboard", () => {
    const props = {
      ssiCommsResponse: "someUrl",
    };
    const container = shallow(<MandateRequestResultBody {...props} />);
    act(() => {
      container.find("CopyToClipboard").prop("onCopy").call();
    });
    expect(container.find("p").text()).toBe(
      "Invitation URL copied to clipboard."
    );
  });

  test("Should render error Alert when errorMessage is present", () => {
    const props = {
      errorMessage: "someError",
    };
    const container = shallow(<MandateRequestResultBody {...props} />);
    const alertWrapper = container.find("div[className='alert alert-danger']");
    expect(alertWrapper.exists()).toBeTruthy();
    expect(alertWrapper.render().text()).toBe("Error! someError");
  });

  test("Should render Spinner when qrCode and errorMessage are absent", () => {
    const container = shallow(<MandateRequestResultBody />);
    const spinnerWrapper = container.find("AdaptiveSpinnerComponent");
    expect(spinnerWrapper.exists()).toBeTruthy();
  });
});

