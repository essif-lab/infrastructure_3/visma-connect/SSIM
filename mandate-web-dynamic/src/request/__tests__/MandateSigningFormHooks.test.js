// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import useSigningForm from "../MandateSigningFormHooks";
import axios from "axios";
import { act } from "react-dom/test-utils";
import ReactDOM from "react-dom";

jest.mock("axios", () => ({
  ...jest.requireActual("axios"),
  post: jest.fn(),
}));

let result;

function TestComponent(props) {
  result = useSigningForm(props);
  return null;
}

let containerTest;

beforeEach(() => {
  containerTest = document.createElement("div");
  document.body.appendChild(containerTest);
});

afterEach(() => {
  document.body.removeChild(containerTest);
  containerTest = null;
});

describe("<MandateSigningFormHooks />", () => {
  test("Should have proper initial values with KVK schema", () => {
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-kvk-schema:1.0" />,
        containerTest
      );
    });

    expect(result.schemaName).toBe("mandate-kvk-schema:1.0");
    expect(result.showQrCode).toBeFalsy();
    expect(result.qrValue).toBe("");
    expect(result.errorMessage).toBe("");
    expect(result.inputs).toEqual({});
  });

  test("Should have proper initial values with Signing Basic schema", () => {
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
    });

    expect(result.schemaName).toBe("mandate-signing-basic:1.0");
    expect(result.showQrCode).toBeFalsy();
    expect(result.qrValue).toBe("");
    expect(result.errorMessage).toBe("");
    expect(result.inputs).toEqual({});
  });

  test("Should not handle submit with no event", () => {
    const event = null;

    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleSubmit(event);
    });
    expect(true).toBeTruthy();
  });

  test("Should handle submit with correct response", async () => {
    const event = {
      preventDefault: () => {},
    };
    const qrValue = "someBase64";
    const encodedQrValue = "c29tZUJhc2U2NA==";
    const data = {
      data: encodedQrValue,
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(data));

    await act(async () => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleSubmit(event);
    });
    await expect(result.qrValue).toBe(qrValue);
  });

  test("Should handle submit with correct response for SSIComms mandate", async () => {
    const event = {
      preventDefault: () => {},
    };
    const ssiCommsMpId = "someMpId";
    const encodedMpId = "c29tZU1wSWQ=";
    const data = {
      data: encodedMpId,
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(data));

    await act(async () => {
      ReactDOM.render(
        <TestComponent
          schemaName="mandate-signing-basic:1.0"
          ssicommsEnabled={true}
        />,
        containerTest
      );
      result.handleSubmit(event);
    });
    await expect(result.ssiCommsResponse).toContain(ssiCommsMpId);
  });

  test("Should handle submit with null response", async () => {
    const event = {
      preventDefault: () => {},
    };
    const data = {
      data: null,
    };
    axios.post.mockImplementationOnce(() => Promise.resolve(data));

    await act(async () => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleSubmit(event);
    });
    await expect(result.errorMessage).toBe("Unable to create QR Code");
  });

  test("Should handle submit with error", async () => {
    const event = {
      preventDefault: () => {},
    };
    axios.post.mockImplementationOnce(() =>
      Promise.reject(new Error("Unable to create QR Code"))
    );

    await act(async () => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleSubmit(event);
    });
    await expect(result.errorMessage).toBe("Unable to create QR Code");
  });

  test("Should handle input change", () => {
    const event = {
      persist: () => {},
      target: {
        value: 1,
        name: "condition",
      },
    };
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleInputChange(event);
    });
    expect(result.inputs).toEqual({ condition: 1 });
  });

  test("Should handle show result modal", () => {
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="mandate-signing-basic:1.0" />,
        containerTest
      );
      result.handleShowResultModal();
    });
    expect(result.showResultModal).toBeTruthy();
  });

  test("Should handle date change", () => {
    act(() => {
      ReactDOM.render(
        <TestComponent schemaName="fenex-schema:1.0" />,
        containerTest
      );
      result.handleDateChange("name", "value");
    });
    expect(result.inputs).toEqual({ name: "value" });
  });
});
