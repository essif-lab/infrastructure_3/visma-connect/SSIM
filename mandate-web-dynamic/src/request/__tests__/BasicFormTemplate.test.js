// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { shallow } from "enzyme";
import BasicFormTemplate from "../BasicFormTemplate";
import ActionButtons from "common/ActionButtons";

describe("<BasicFormTemplate /> with KVK schema", () => {
  const props = {
    schemaSelectionCB: jest.fn(),
    data: {
      schemaId: "BNrRkkQZsTXBx1RZZzjkFS:2:mandate-kvk-schema:1.0",
      name: "mandate-kvk-schema",
      title: "KVK Schema",
      version: "1.0",
      attributes: [
        {
          name: "chamber-of-commerce-reference",
          title: "Chamber of commerce reference",
          type: "text",
        },
        { name: "director", title: "Director", type: "text" },
      ],
    },
  };
  const container = shallow(<BasicFormTemplate {...props} />);

  test("Should render schemaName label", () => {
    const labelWrapper = container.find("label").children().at(0);
    expect(labelWrapper.exists()).toBeTruthy();
    expect(labelWrapper.text()).toBe("Schema Name");
  });

  test("Should call handleBack function", () => {
    container.find(ActionButtons).prop("handleBack").call();
    expect(props.schemaSelectionCB).toHaveBeenCalled();
  });
});

describe("<BasicFormTemplate /> with fenex schema", () => {
  const props = {
    schemaSelectionCB: jest.fn(),
    data: {
      schemaId: "BNrRkkQZsTXBx1RZZzjkFS:2:fenex-schema:1.0",
      name: "fenex-schema",
      title: "Fenex Schema",
      version: "1.0",
      attributes: [
        {
          name: "represented-company-name",
          title: "Represented company name",
          type: "text",
        },
        {
          name: "represented-address",
          title: "Represented address",
          type: "text",
        },
        {
          name: "represented-postal-code",
          title: "Represented postal code",
          type: "text",
        },
        {
          name: "represented-place",
          title: "Represented place",
          type: "text",
        },
        {
          name: "represented-country",
          title: "Represented country",
          type: "text",
        },
        {
          name: "represented-chamber-of-commerce-registration-no",
          title: "Represented chamber of commerce registration No.",
          type: "text",
        },
        {
          name: "represented-vat-id-no",
          title: "Represented VAT ID No.",
          type: "text",
        },
        {
          name: "represented-eori-number",
          title: "Represented EORI No.",
          type: "text",
        },
        {
          name: "forwarder-company-name",
          title: "Forwarder company name",
          type: "text",
        },
        {
          name: "forwarder-address",
          title: "Forwarder address",
          type: "text",
        },
        {
          name: "forwarder-postal-code",
          title: "Forwarder postal code",
          type: "text",
        },
        {
          name: "forwarder-place",
          title: "Forwarder place",
          type: "text",
        },
        {
          name: "forwarder-country",
          title: "Forwarder country",
          type: "text",
        },
        {
          name: "valid-from",
          title: "Valid from",
          type: "date",
        },
        {
          name: "valid-until",
          title: "Valid until",
          type: "date",
        },
      ],
      predicates: [],
    },
  };
  const container = shallow(<BasicFormTemplate {...props} />);

  test("Should render schemaName label", () => {
    const labelWrapper = container.find("label").children().at(0);
    expect(labelWrapper.exists()).toBeTruthy();
    expect(labelWrapper.text()).toBe("Schema Name");
  });

  test("Should call handleBack function", () => {
    container.find(ActionButtons).prop("handleBack").call();
    expect(props.schemaSelectionCB).toHaveBeenCalled();
  });

  test("Should render valid-from datepicker", () => {
    const datepickerWrapper = container.find("r[id='form-valid-from']");
    expect(datepickerWrapper.exists()).toBeTruthy();
    expect(datepickerWrapper.prop("dateFormat")).toBe("yyyy-MM-dd");
    expect(datepickerWrapper.prop("name")).toBe("valid-from");
    expect(datepickerWrapper.prop("selected")).toBe(null);
    expect(datepickerWrapper.prop("required")).toBeTruthy();
    expect(datepickerWrapper.prop("autoComplete")).toBe("off");
    expect(datepickerWrapper.prop("showPopperArrow")).toBeFalsy();
    expect(datepickerWrapper.prop("selectsStart")).toBeTruthy();
    expect(datepickerWrapper.prop("isClearable")).toBeTruthy();
    expect(datepickerWrapper.prop("maxDate")).toBe(null);
    expect(datepickerWrapper.prop("onChange")).toBeDefined();
  });

  test("Should change valid-from value", () => {
    container
      .find("r[id='form-valid-from']")
      .simulate("change", new Date(1609495200000));
    const date = container
      .find("r[id='form-valid-from']")
      .prop("selected")
      .toISOString();
    expect(date).toBe("2021-01-01T10:00:00.000Z");
  });

  test("Should render valid-until label", () => {
    const labelWrapper = container.find("label").children().at(15);
    expect(labelWrapper.exists()).toBeTruthy();
    expect(labelWrapper.text()).toBe("Valid until");
  });

  test("Should render valid-until datepicker", () => {
    const datepickerWrapper = container.find("r[id='form-valid-until']");
    expect(datepickerWrapper.exists()).toBeTruthy();
    expect(datepickerWrapper.prop("dateFormat")).toBe("yyyy-MM-dd");
    expect(datepickerWrapper.prop("name")).toBe("valid-until");
    expect(datepickerWrapper.prop("selected")).toBe(null);
    expect(datepickerWrapper.prop("required")).toBeTruthy();
    expect(datepickerWrapper.prop("autoComplete")).toBe("off");
    expect(datepickerWrapper.prop("showPopperArrow")).toBeFalsy();
    expect(datepickerWrapper.prop("selectsEnd")).toBeTruthy();
    expect(datepickerWrapper.prop("isClearable")).toBeTruthy();
    expect(datepickerWrapper.prop("minDate")).toBe(null);
    expect(datepickerWrapper.prop("onChange")).toBeDefined();
  });

  test("Should change valid-until value", () => {
    container
      .find("r[id='form-valid-until']")
      .simulate("change", new Date(1609495200000));
    const date = container
      .find("r[id='form-valid-until']")
      .prop("selected")
      .toISOString();
    expect(date).toBe("2021-01-01T10:00:00.000Z");
  });
});
