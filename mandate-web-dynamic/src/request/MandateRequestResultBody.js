// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import AdaptiveQRComponent from "../common/AdaptiveQRComponent";
import AdaptiveSpinnerComponent from "../common/AdaptiveSpinnerComponent";
import spinner from "../styles/nc4/img/spinner-snake-blue-light.svg";
import { CopyToClipboard } from "react-copy-to-clipboard";
import { useState } from "react";

/**
 * @author Lukas Nakas
 * @description Mandate request result which is received after submitting form
 * @Return {JSX.Element}
 */
const MandateRequestResultBody = ({
  qrValue,
  errorMessage,
  ssiCommsResponse,
}) => {
  const [ssiCommsClipboardResponse, setSsiCommsClipboardResponse] =
    useState("");
  const [showUrl, setShowUrl] = useState(false);

  return (
    <div className="row">
      {errorMessage && errorMessage !== "" ? (
        <div className="col text-center">
          <div className="alert alert-danger" role="alert">
            <strong>Error!</strong>
            {" " + errorMessage}
          </div>
        </div>
      ) : qrValue ? (
        <div className="col text-center">
          <AdaptiveQRComponent qrValue={qrValue} />
        </div>
      ) : ssiCommsResponse ? (
        <div className="col text-center">
          {showUrl && (
            <div className="ssicomms-response">
              <strong>Invitation URL: </strong>
              {ssiCommsResponse}
            </div>
          )}
          <button
            className="btn"
            id="button-show-url"
            onClick={() => setShowUrl(true)}
          >
            Show invitation URL
          </button>
          <CopyToClipboard
            text={ssiCommsResponse}
            onCopy={() =>
              setSsiCommsClipboardResponse(
                "Invitation URL copied to clipboard."
              )
            }
          >
            <button className="btn btn-primary" id="button-copy-clipboard">
              Copy URL to clipboard
            </button>
          </CopyToClipboard>
          <p>{ssiCommsClipboardResponse}</p>
        </div>
      ) : (
        <div className="col text-center">
          <AdaptiveSpinnerComponent spinner={spinner} divisionMultiplier={4} />
        </div>
      )}
    </div>
  );
};

export default MandateRequestResultBody;

