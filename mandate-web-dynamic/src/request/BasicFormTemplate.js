// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useState } from "react";
import useSigningForm from "./MandateSigningFormHooks";
import MandateRequestResult from "./MandateRequestResult";
import ActionButtons from "../common/ActionButtons";
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

/**
 * @author Viktorija
 * @description MandateSigningBasicForm
 * @Return {JSX.Element}
 */
function BasicFormTemplate(props) {
  const { data } = props;
  const {
    handleSubmit,
    handleInputChange,
    handleDateChange,
    handleShowResultModal,
    inputs,
    showResultModal,
    qrValue,
    errorMessage,
    ssiCommsResponse: ssiCommsResponse,
  } = useSigningForm(props);

  const [validFrom, setValidFrom] = useState(null);
  const [validUntil, setValidUntil] = useState(null);

  const handleBack = () => {
    props.schemaSelectionCB();
  };

  const formatDate = (date) => {
    const options = [
      { year: "numeric" },
      { month: "2-digit" },
      { day: "2-digit" },
    ];

    const getFormatter = (map) => {
      return new Intl.DateTimeFormat("en", map).format(date);
    };

    return options.map(getFormatter).join("-");
  };

  return (
    <div className="col-lg-8">
      <MandateRequestResult
        qrValue={qrValue}
        ssiCommsResponse={ssiCommsResponse}
        errorMessage={errorMessage}
        show={showResultModal}
        onHide={handleShowResultModal}
      />
      <form className="form-horizontal" onSubmit={handleSubmit}>
        <div className="form-group required">
          <label
            htmlFor="form-schema"
            className="col-sm-3 col-md-2 control-label"
          >
            Schema Name
          </label>
          <div className="col-sm-9 col-md-10">
            <input
              type="text"
              className="form-control"
              id="form-schema"
              required
              readOnly
              value={data.title}
              onChange={handleInputChange}
              name="schema"
            />
          </div>
        </div>
        {data.attributes.map((x) => (
          <div className="form-group required" key={x.name}>
            <label htmlFor={x.name} className="col-sm-3 col-md-2 control-label">
              {x.title}
            </label>
            <div className="col-sm-9 col-md-10">
              {x.type === "date" ? (
                <DatePicker
                  dateFormat="yyyy-MM-dd"
                  id={`form-${x.name}`}
                  className="form-control"
                  required
                  selected={x.name === "valid-from" ? validFrom : validUntil}
                  name={x.name}
                  autoComplete="off"
                  showPopperArrow={false}
                  selectsStart={x.name === "valid-from"}
                  selectsEnd={x.name === "valid-until"}
                  maxDate={x.name === "valid-until" ? validUntil : null}
                  minDate={x.name === "valid-from" ? validFrom : null}
                  isClearable
                  onChange={(date) => {
                    x.name === "valid-from"
                      ? setValidFrom(date)
                      : setValidUntil(date);
                    handleDateChange(x.name, formatDate(date));
                  }}
                />
              ) : (
                <input
                  type={x.type}
                  className="form-control"
                  name={x.name}
                  onChange={handleInputChange}
                  value={inputs[x.name] || ""}
                  required
                  id={x.name}
                />
              )}
            </div>
          </div>
        ))}
        <div className="form-group offset-sm-3 offset-md-2 no-gutters">
          <ActionButtons handleBack={handleBack} />
        </div>
      </form>
    </div>
  );
}

export default BasicFormTemplate;
