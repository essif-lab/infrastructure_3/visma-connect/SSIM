// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React from "react";
import { useHistory } from "react-router-dom";
import MandateRequestResultBody from "./MandateRequestResultBody";

/**
 * @author Lukas Nakas
 * @description Modal component for showing response to requesting mandate
 * @Return {JSX.Element}
 */
const MandateRequestResult = (props) => {
  const history = useHistory();

  const handleBack = () => {
    history.push("/home");
  };

  return (
    <div
      id="modal-qr-result"
      className={`modal ${props.show ? "in" : ""}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="modal-title"
      aria-hidden="false"
    >
      <div className="modal-dialog" role="document" style={{ width: "470px" }}>
        <div className="modal-content">
          <div className="modal-header text-center">
            {props.qrValue ? (
              <h4 id="modal-title" className="modal-title">
                Scan the QR code below to initiate connection and receive issued
                credentials
              </h4>
            ) : props.ssiCommsResponse ? (
              <h4 id="modal-title" className="modal-title">
                Copy the invitation URL below and use it to initiate connection
                and receive issued credentials
              </h4>
            ) : (
              <h4 id="modal-title" className="modal-title">
                Waiting for mandate creation to finish...
              </h4>
            )}
          </div>
          <div className="modal-body">
            <MandateRequestResultBody
              qrValue={props.qrValue}
              ssiCommsResponse={props.ssiCommsResponse}
              errorMessage={props.errorMessage}
            />
          </div>
          <div className="modal-footer">
            <div className="row justify-content-between">
              <div className="col-12 col-xs-6 text-center text-xs-left order-last order-xs-first">
                <button
                  id="modal-btn-cancel"
                  type="button"
                  className="btn btn-default"
                  data-toggle="modal"
                  data-target="#modal-qr-result"
                  onClick={props.onHide}
                >
                  Cancel
                </button>
              </div>
              <div className="col-12 col-xs-6 text-center text-xs-right order-first order-xs-last">
                <button
                  id="modal-btn-finish"
                  type="button"
                  className="btn btn-primary"
                  onClick={handleBack}
                  disabled={
                    !(props.qrValue || props.ssiCommsResponse) &&
                    !props.errorMsg
                  }
                >
                  Finish
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MandateRequestResult;
