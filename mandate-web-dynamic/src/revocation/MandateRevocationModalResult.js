// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React from "react";
import { useHistory } from "react-router-dom";
import MandateRevocationResultBody from "./MandateRevocationResultBody";
import { useState } from "react";

/**
 * @author Lukas Nakas
 * @description Modal component for showing response to revoking credentials
 * @Return {JSX.Element}
 */
const MandateRevocationModalResult = (props) => {
  const history = useHistory();
  const [revocationConfirmed, setRevocationConfirmed] = useState(false);

  const handleBack = () => {
    setRevocationConfirmed(false);
    history.push("/home");
  };

  const handleCancel = () => {
    setRevocationConfirmed(false);
    props.onHide();
  };

  const handleConfirm = () => {
    props.handleSubmit();
    setRevocationConfirmed(true);
  };

  return (
    <div
      id="modal-revocation-result"
      className={`modal ${props.show ? "in" : ""}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="modal-title"
      aria-hidden="false"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header text-center">
            <h4 id="modal-title" className="modal-title">
              Revocation confirmation
            </h4>
          </div>
          <div className="modal-body">
            {revocationConfirmed ? (
              <MandateRevocationResultBody
                revokeSuccessful={props.revokeSuccessful}
                errorMessage={props.errorMessage}
              />
            ) : (
              <div>Are you sure you want to revoke credentials?</div>
            )}
          </div>
          <div className="modal-footer">
            <div className="row justify-content-between">
              <div className="col-12 col-xs-6 text-center text-xs-left order-last order-xs-first">
                <button
                  id="modal-btn-cancel"
                  type="button"
                  className="btn btn-default"
                  data-toggle="modal"
                  data-target="#modal-revocation-result"
                  onClick={handleCancel}
                >
                  Cancel
                </button>
              </div>
              {revocationConfirmed ? (
                <div>
                  <div className="col-12 col-xs-6 text-center text-xs-right order-first order-xs-last">
                    <button
                      id="modal-btn-finish"
                      type="button"
                      className="btn btn-primary"
                      onClick={handleBack}
                    >
                      Finish
                    </button>
                  </div>
                </div>
              ) : (
                <div className="col-12 col-xs-6 text-center text-xs-right order-first order-xs-last">
                  <button
                    id="modal-btn-confirm"
                    type="button"
                    className="btn btn-primary"
                    onClick={handleConfirm}
                  >
                    Confirm
                  </button>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MandateRevocationModalResult;
