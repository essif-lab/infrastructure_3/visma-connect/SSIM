// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import useRevocationForm from "./MandateRevocationFormHooks";
import MandateRevocationModalResult from "./MandateRevocationModalResult";
import MandateRevocationList from "./MandateRevocationList";

/**
 * @author Lukas Nakas
 * @description MandateRevocationForm
 * @Return {JSX.Element}
 */
function MandateRevocationForm() {
  const [dropdownValue, setDropdownValue] = useState("Select a credential...");
  const [selectedCredential, setSelectedCredential] = useState("");
  const [dropdownMenuActive, setDropdownMenuActive] = useState(false);
  const dropdownRef = React.createRef();
  const history = useHistory();
  const [showResultModal, setShowResultModal] = useState(false);
  const {
    credentials,
    handleSubmit,
    handleInputChange,
    revokeSuccessful,
    errorMessage,
  } = useRevocationForm();

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  });

  const handleClickOutside = (e) => {
    if (dropdownRef.current && !dropdownRef.current.contains(e.target)) {
      setDropdownMenuActive(false);
    }
  };

  const dropdownMenuOpen = (e) => {
    e.preventDefault();
    e.stopPropagation();
    setDropdownMenuActive(!dropdownMenuActive);
  };

  const handleSelect = (credential) => {
    if (credential.connectionId !== "") {
      setDropdownValue(credential.schemaName + ":" + credential.connectionId);
      setSelectedCredential(credential);
      handleInputChange(credential);
    } else {
      setDropdownValue("No data");
      setSelectedCredential("");
    }
    setDropdownMenuActive(false);
  };

  const handleBack = () => {
    history.push("/home");
  };

  const handleRevoke = () => {
    if (selectedCredential !== "") {
      setShowResultModal(true);
    } else {
      console.log("No credentials selected");
    }
  };

  return (
    <div className="request-body">
      <div className="container">
        <h1 className="headings">Select a credential to be revoked</h1>
        <div className="row rows justify-content-center">
          <div className="col columns text-center">
            <div className="row rows justify-content-center">
              <div className="col columns text-center">
                <MandateRevocationModalResult
                  revokeSuccessful={revokeSuccessful}
                  errorMessage={errorMessage}
                  show={showResultModal}
                  onHide={() => setShowResultModal(false)}
                  handleSubmit={handleSubmit}
                />
                <div className="row">
                  <div className="col">
                    <h4 className="description">
                      Please select a credential from dropdown menu below and
                      hit button "Revoke" to revoke credential.
                    </h4>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <div
                      className={`dropdown ${dropdownMenuActive ? "open" : ""}`}
                      ref={dropdownRef}
                      style={{ width: "40%" }}
                    >
                      <button
                        type="button"
                        id="dropdown-basic-button"
                        className="btn btn-default dropdown-toggle btn-lg"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                        onClick={dropdownMenuOpen}
                        style={{ width: "100%" }}
                      >
                        {dropdownValue}
                      </button>
                      <MandateRevocationList
                        credentials={credentials}
                        handleSelect={handleSelect}
                      />
                    </div>
                  </div>
                </div>
                {selectedCredential !== "" ? (
                  <div className="row">
                    <div className="col">
                      <div className="row">
                        <div className="col">
                          <h2>
                            <strong>Credential details</strong>
                          </h2>
                        </div>
                      </div>
                      <div className="row justify-content-center">
                        <div className="col-4">
                          <div className="row justify-content-start">
                            <div className="col text-left">
                              <h4>
                                <strong>Schema name:</strong>
                              </h4>
                            </div>
                          </div>
                          <div className="row justify-content-start">
                            <div className="col offset-1 text-left">
                              <h4>{selectedCredential.schemaName}</h4>
                            </div>
                          </div>
                          <div className="row justify-content-start">
                            <div className="col text-left">
                              <h4>
                                <strong>Values:</strong>
                              </h4>
                            </div>
                          </div>
                          <div className="row justify-content-start">
                            <div className="col offset-1 text-left">
                              {selectedCredential.values.map((value) => (
                                <h4>
                                  <strong>{value.credName}: </strong>
                                  {value.credValue}
                                </h4>
                              ))}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ) : (
                  <div></div>
                )}
              </div>
            </div>
            <div className="row rows justify-content-between">
              <div className="col-2 columns text-left">
                <button
                  type="button"
                  className="btn btn-default btn-lg btn-block"
                  onClick={handleBack}
                  id="button-back"
                  style={{ width: "100%" }}
                >
                  Back
                </button>
              </div>
              <div className="col-2 columns text-right">
                <button
                  type="button"
                  className="btn btn-primary btn-lg btn-block"
                  onClick={handleRevoke}
                  id="button-revoke"
                  style={{ width: "100%" }}
                >
                  Revoke
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MandateRevocationForm;
