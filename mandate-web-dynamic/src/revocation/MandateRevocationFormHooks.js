// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { useState, useEffect } from "react";
import axios from "axios";

const useRevocationForm = () => {
  const API_DOMAIN = process.env.REACT_APP_DOMAIN || "unknown";
  console.log("api-domain", API_DOMAIN);
  const url = `${API_DOMAIN}/mandates`;
  const [credentials, setCredentials] = useState([]);
  const [inputs, setInputs] = useState({});
  const [revokeSuccessful, setRevokeSuccessful] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    fetchCredentials();
  }, []);

  const fetchCredentials = () => {
    axios
      .get(url + "/issued")
      .then((res) => {
        let credentials = [];
        if (res.data !== null && res.data.length > 0) {
          res.data.forEach((credential) => {
            const credentialObj = {
              schemaName: credential.schemaName,
              values: credential.values,
              credRevId: credential.credRevId,
              revRegId: credential.revRegId,
              connectionId: credential.connectionId,
            };
            credentials.push(credentialObj);
          });
        }
        setCredentials(credentials);
      })
      .catch((error) => {
        // Error occurred in our service
        // todo: some error handling?
        console.error(
          "Something went wrong when fetching credentials: ",
          error
        );
      });
  };

  const handleSubmit = () => {
    axios
      .post(url + "/revoke", inputs)
      .then((response) => {
        setRevokeSuccessful(true);
      })
      .catch((error) => {
        // Error occurred in our service
        setErrorMessage("Unable to revoke credential");
      });
  };

  const handleInputChange = (credential) => {
    setInputs({
      revRegId: credential.revRegId,
      credRevId: credential.credRevId,
    });
  };

  return {
    credentials,
    handleSubmit,
    handleInputChange,
    inputs,
    revokeSuccessful,
    errorMessage,
  };
};
export default useRevocationForm;
