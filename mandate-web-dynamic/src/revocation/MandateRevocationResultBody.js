// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import React from "react";
import AdaptiveSpinnerComponent from "../common/AdaptiveSpinnerComponent";
import spinner from "../styles/nc4/img/spinner-snake-blue-light.svg";

/**
 * @author Lukas Nakas
 * @description Mandate revocation result which is received after submitting credentials to revoke
 * @Return {JSX.Element}
 */
const MandateRevocationResultBody = ({ revokeSuccessful, errorMessage }) => {
  return (
    <React.Fragment>
      <div className="row justify-content-center">
        {revokeSuccessful ? (
          <div className="col text-center">
            <div
              id="alert-revoked"
              className="alert alert-success"
              role="alert"
            >
              <strong>Success!</strong> The user's credentials have been
              revoked!
            </div>
          </div>
        ) : errorMessage ? (
          <div className="col text-center">
            <div id="alert-failed" className="alert alert-danger" role="alert">
              <strong>Failed!</strong> {errorMessage}
            </div>
          </div>
        ) : (
          <div className="col text-center">
            <AdaptiveSpinnerComponent
              spinner={spinner}
              divisionMultiplier={4}
            />
          </div>
        )}
      </div>
    </React.Fragment>
  );
};

export default MandateRevocationResultBody;
