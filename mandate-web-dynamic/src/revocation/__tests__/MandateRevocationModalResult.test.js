// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import MandateRevocationModalResult from "../MandateRevocationModalResult";
import React from "react";

const mockHistoryPush = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe("<MandateRevocationModalResult /> context depending on props", () => {
  test("Should match old snapshot", () => {
    const mandateRevocationModalResultTree = renderer
      .create(<MandateRevocationModalResult />)
      .toJSON();
    expect(mandateRevocationModalResultTree).toMatchSnapshot();
  });

  test("Should have modal but not render it", () => {
    const props = {
      show: false,
    };
    const container = shallow(<MandateRevocationModalResult {...props} />);
    const modalWrapper = container.find("div[id='modal-revocation-result']");
    expect(modalWrapper.exists()).toBeTruthy();
  });

  test("Should have modal and render it", () => {
    const props = {
      show: true,
    };
    const container = shallow(<MandateRevocationModalResult {...props} />);
    const modalWrapper = container.find("div[id='modal-revocation-result']");
    expect(modalWrapper.exists()).toBeTruthy();
  });

  test("Should have MandateRevocationResultBody component", () => {
    const container = shallow(
      <MandateRevocationModalResult handleSubmit={() => jest.fn()} />
    );
    const mockedEvent = { target: {}, preventDefault: () => {} };
    container
      .find("button[id='modal-btn-confirm']")
      .simulate("click", mockedEvent);
    const modalWrapper = container.find("MandateRevocationResultBody");
    expect(modalWrapper.exists()).toBeTruthy();
  });
});

describe("<MandateRevocationModalResult /> buttons", () => {
  let container;
  beforeEach(() => {
    container = shallow(
      <MandateRevocationModalResult
        onHide={() => jest.fn()}
        handleSubmit={() => jest.fn()}
      />
    );
  });

  test("Should render finish button", () => {
    const mockedEvent = { target: {}, preventDefault: () => {} };
    container
      .find("button[id='modal-btn-confirm']")
      .simulate("click", mockedEvent);
    const finishButtonWrapper = container.find("button[id='modal-btn-finish']");
    expect(finishButtonWrapper.exists()).toBeTruthy();
    expect(finishButtonWrapper.prop("onClick")).toBeDefined();
  });

  test("Should render cancel button", () => {
    const cancelButtonWrapper = container.find("button[id='modal-btn-cancel']");
    expect(cancelButtonWrapper.exists()).toBeTruthy();
    expect(cancelButtonWrapper.prop("onClick")).toBeDefined();
  });

  test("Should trigger handleCancel function", () => {
    const mockedEvent = { target: {}, preventDefault: () => {} };
    container
      .find("button[id='modal-btn-cancel']")
      .simulate("click", mockedEvent);
    expect(
      container.find("div[id='modal-revocation-result']").prop("className")
    ).toBe("modal ");
  });

  test("Should trigger handleBack function", () => {
    const mockedEvent = { target: {}, preventDefault: () => {} };
    container
      .find("button[id='modal-btn-confirm']")
      .simulate("click", mockedEvent);
    container
      .find("button[id='modal-btn-finish']")
      .simulate("click", mockedEvent);
    expect(mockHistoryPush).toHaveBeenCalledWith("/home");
  });
});
