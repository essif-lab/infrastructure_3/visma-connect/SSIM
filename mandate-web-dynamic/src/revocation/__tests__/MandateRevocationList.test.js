// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import MandateRevocationList from "../MandateRevocationList";
import React from "react";

describe("<MandateRevocationList /> without hooks", () => {
  test("Should match old snapshot", () => {
    const revocationListTree = renderer
      .create(
        <MandateRevocationList credentials={[]} handleSelect={() => {}} />
      )
      .toJSON();
    expect(revocationListTree).toMatchSnapshot();
  });

  test("Should have proper dropdown buttons", () => {
    const props = {
      credentials: [
        {
          schemaName: "schemaName",
          values: [{ credName: "credValue" }],
          credRevId: "credRevId",
          revRegId: "revRegId",
          connectionId: "connectionId",
        },
      ],
      handleSelect: () => {},
    };
    const container = shallow(<MandateRevocationList {...props} />);
    const dropdownButtons = container
      .find("ul[id='dropdown-menu-list']")
      .children();
    expect(dropdownButtons.at(0).childAt(0).prop("onClick")).toBeDefined();
  });

  test("Should have no schemas found button", () => {
    const props = {
      credentials: [],
    };
    const container = shallow(<MandateRevocationList {...props} />);
    const dropdownButtons = container
      .find("ul[id='dropdown-menu-list']")
      .children();
    expect(dropdownButtons.at(0).childAt(0).prop("onClick")).not.toBeDefined();
  });
});
