// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import { mount } from "enzyme";
import MandateRevocationForm from "../../MandateRevocationForm";
import React from "react";
import { act } from "react-dom/test-utils";

jest.mock("../../MandateRevocationFormHooks", () => {
  const inputs = {
    schemaName: "schemaName",
    credName: "credName",
    credValue: "credValue",
    revRegId: "revRegId",
    credRevId: "credRevId",
  };
  const handleInputChange = jest.fn();
  const handleSubmit = jest.fn();
  const credentials = [
    {
      schemaName: "schemaName",
      values: [{ credName: "credName", credValue: "credValue" }],
      credRevId: "credRevId",
      revRegId: "revRegId",
      connectionId: "",
    },
  ];
  const revokeSuccessful = false;
  const errorMessage = "";

  return () => ({
    handleSubmit,
    handleInputChange,
    inputs,
    credentials,
    revokeSuccessful,
    errorMessage,
  });
});

const mockHistoryPush = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe("<MandateRevocationForm /> with missing details", () => {
  test("Should handle dropdown button select", () => {
    const container = mount(<MandateRevocationForm />);
    const dropdownButtons = container
      .find("MandateRevocationList")
      .find("ul[id='dropdown-menu-list']")
      .children();
    const buttonWrapper = dropdownButtons.at(0).childAt(0);
    buttonWrapper.simulate("click", {});
    expect(container.find("h2").exists()).toBeFalsy();
  });

  test("Should handle revoke button without showing modal", () => {
    const container = mount(<MandateRevocationForm />);
    const dropdownButtons = container
      .find("MandateRevocationList")
      .find("ul[id='dropdown-menu-list']")
      .children();
    const buttonWrapper = dropdownButtons.at(0).childAt(0);
    buttonWrapper.simulate("click", {});

    const revokeButtonWrapper = container.find("button[id='button-revoke']");
    expect(revokeButtonWrapper.exists()).toBeTruthy();
    act(() => {
      revokeButtonWrapper.simulate("click", {});
    });
    container.update();
    expect(
      container.find("MandateRevocationModalResult").prop("show")
    ).toBeFalsy();
  });
});
