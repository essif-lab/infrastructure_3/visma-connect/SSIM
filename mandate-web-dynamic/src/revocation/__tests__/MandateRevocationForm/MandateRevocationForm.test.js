// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import renderer from "react-test-renderer";
import { shallow, mount } from "enzyme";
import MandateRevocationForm from "../../MandateRevocationForm";
import React from "react";
import { act } from "react-dom/test-utils";

jest.mock("../../MandateRevocationFormHooks", () => {
  const inputs = {
    schemaName: "schemaName",
    credName: "credName",
    credValue: "credValue",
    revRegId: "revRegId",
    credRevId: "credRevId",
  };
  const handleInputChange = jest.fn();
  const handleSubmit = jest.fn();
  const credentials = [
    {
      schemaName: "schemaName",
      values: [{ credName: "credName", credValue: "credValue" }],
      credRevId: "credRevId",
      revRegId: "revRegId",
      connectionId: "connectionId",
    },
  ];
  const revokeSuccessful = false;
  const errorMessage = "";

  return () => ({
    handleSubmit,
    handleInputChange,
    inputs,
    credentials,
    revokeSuccessful,
    errorMessage,
  });
});

const mockHistoryPush = jest.fn();
jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe("<MandateRevocationForm /> without hooks", () => {
  let container;
  beforeEach(() => {
    container = shallow(<MandateRevocationForm />);
  });

  test("Should match old snapshot", () => {
    const mandateRevocationFormTree = renderer
      .create(<MandateRevocationForm />)
      .toJSON();
    expect(mandateRevocationFormTree).toMatchSnapshot();
  });

  test("Should have heading", () => {
    const headingWrapper = container.find("h1[className='headings']");
    expect(headingWrapper.exists()).toBeTruthy();
  });

  test("Should have description", () => {
    const descriptionWrapper = container.find("h4[className='description']");
    expect(descriptionWrapper.exists()).toBeTruthy();
  });

  test("Should have MandateRevocationList component", () => {
    const revocationListWrapper = container.find("MandateRevocationList");
    expect(revocationListWrapper.exists()).toBeTruthy();
  });

  test("Should have dropdown button with proper props", () => {
    const dropdownWrapper = container.find(
      "button[id='dropdown-basic-button']"
    );
    expect(dropdownWrapper.exists()).toBeTruthy();
    expect(dropdownWrapper.prop("onClick")).toBeDefined();
  });

  test("Should render back button", () => {
    const backButtonWrapper = container.find("button[id='button-back']");
    expect(backButtonWrapper.exists()).toBeTruthy();
    expect(backButtonWrapper.prop("onClick")).toBeDefined();
  });

  test("Should have MandateRevocationModalResult component", () => {
    const resultWrapper = container.find("MandateRevocationModalResult");
    expect(resultWrapper.exists()).toBeTruthy();
  });

  test("Should handle revoke button and cancel", () => {
    const container = mount(<MandateRevocationForm />);
    const dropdownButtons = container
      .find("MandateRevocationList")
      .find("ul[id='dropdown-menu-list']")
      .children();
    const buttonWrapper = dropdownButtons.at(0).childAt(0);
    buttonWrapper.simulate("click", {});

    const revokeButtonWrapper = container.find("button[id='button-revoke']");
    expect(revokeButtonWrapper.exists()).toBeTruthy();
    act(() => {
      revokeButtonWrapper.simulate("click", {});
    });
    container.update();
    expect(
      container.find("MandateRevocationModalResult").prop("show")
    ).toBeTruthy();

    act(() => {
      container.find("button[id='modal-btn-cancel']").simulate("click", {});
    });
    container.update();
    expect(
      container.find("MandateRevocationModalResult").prop("show")
    ).toBeFalsy();
  });
});

describe("<MandateRevocationForm /> with hooks", () => {
  let container;
  beforeEach(() => {
    container = shallow(<MandateRevocationForm />);
  });

  test("Should trigger handleBack function", () => {
    const mockedEvent = { target: {}, preventDefault: () => {} };
    container.find("button[id='button-back']").simulate("click", mockedEvent);
    expect(mockHistoryPush).toHaveBeenCalledWith("/home");
  });

  test("Should open dropdown menu", () => {
    const mockedEvent = { stopPropagation: () => {}, preventDefault: () => {} };
    act(() => {
      container
        .find("button[id='dropdown-basic-button']")
        .simulate("click", mockedEvent);
    });
    const dropdownWrapper = container.find("div[className='dropdown open']");
    expect(dropdownWrapper.exists()).toBeTruthy();
  });

  test("Should have proper dropdown buttons", () => {
    const container = mount(<MandateRevocationForm />);
    const dropdownButtons = container
      .find("MandateRevocationList")
      .find("ul[id='dropdown-menu-list']")
      .children();
    expect(dropdownButtons.at(0).childAt(0).text()).toBe(
      "schemaName:connectionId"
    );
    expect(dropdownButtons.at(0).childAt(0).prop("onClick")).toBeDefined();
  });

  test("Should handle dropdown button select", () => {
    const container = mount(<MandateRevocationForm />);
    const dropdownButtons = container
      .find("MandateRevocationList")
      .find("ul[id='dropdown-menu-list']")
      .children();
    const buttonWrapper = dropdownButtons.at(0).childAt(0);
    buttonWrapper.simulate("click", {});
    expect(container.find("h2").exists()).toBeTruthy();
  });
});

describe("<MandateRevocationForm /> with useEffect", () => {
  test("Should handle mousedown listener", () => {
    jest
      .spyOn(document, "addEventListener")
      .mockImplementation(() => jest.fn());
    jest
      .spyOn(document, "removeEventListener")
      .mockImplementation(() => jest.fn());
    const container = mount(<MandateRevocationForm />);
    expect(document.addEventListener).toHaveBeenLastCalledWith(
      "mousedown",
      expect.any(Function)
    );
    act(() => {
      container.unmount();
    });
    expect(document.removeEventListener).toHaveBeenLastCalledWith(
      "mousedown",
      expect.any(Function)
    );
  });

  test("Should handle click outside when dropdown ref does not contain target", async () => {
    const map = {};
    document.addEventListener = jest.fn((event, callback) => {
      map[event] = callback;
    });
    const wrapper = mount(<MandateRevocationForm />);
    const mockedEvent = { stopPropagation: () => {}, preventDefault: () => {} };
    act(() => {
      wrapper
        .find("button[id='dropdown-basic-button']")
        .simulate("click", mockedEvent);
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
    act(() => {
      map.mousedown({ target: null });
    });
    wrapper.update();
    expect(wrapper.find("div[className='dropdown ']").exists()).toBeTruthy();
  });

  test("Should handle click outside when dropdown ref contains target", async () => {
    const map = {};
    document.addEventListener = jest.fn((event, callback) => {
      map[event] = callback;
    });
    const wrapper = mount(<MandateRevocationForm />);
    const mockedEvent = { stopPropagation: () => {}, preventDefault: () => {} };
    act(() => {
      wrapper
        .find("button[id='dropdown-basic-button']")
        .simulate("click", mockedEvent);
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
    act(() => {
      map.mousedown({
        target: wrapper.find("button[id='dropdown-basic-button']").getDOMNode(),
      });
    });
    wrapper.update();
    expect(
      wrapper.find("div[className='dropdown open']").exists()
    ).toBeTruthy();
    expect(wrapper.find("button[id='dropdown-basic-button']").text()).toBe(
      "Select a credential..."
    );
  });
});
