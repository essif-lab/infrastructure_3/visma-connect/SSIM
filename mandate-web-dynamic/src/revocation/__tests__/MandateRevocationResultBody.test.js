// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import MandateRevocationResultBody from "../MandateRevocationResultBody";

describe("<MandateRevocationResultBody />", () => {
  test("Should match old snapshot", () => {
    const mandateRevocationResultBodyTree = renderer
      .create(<MandateRevocationResultBody />)
      .toJSON();
    expect(mandateRevocationResultBodyTree).toMatchSnapshot();
  });

  test("Should render error Alert", () => {
    const revokeSuccessful = false;
    const errorMsg = "ErrorMessage";
    const mandateRevocationResultBody = (
      <MandateRevocationResultBody
        revokeSuccessful={revokeSuccessful}
        errorMessage={errorMsg}
      />
    );

    const mandateRevocationResultBodyWrapper = shallow(
      mandateRevocationResultBody
    );
    const alertWrapper = mandateRevocationResultBodyWrapper.find(
      "div[id='alert-failed']"
    );
    expect(alertWrapper.exists()).toBeTruthy();
    expect(alertWrapper.render().text()).toBe("Failed! ErrorMessage");
  });

  test("Should render revoked Alert", () => {
    const revokeSuccessful = true;
    const mandateRevocationResultBody = (
      <MandateRevocationResultBody revokeSuccessful={revokeSuccessful} />
    );

    const mandateRevocationResultBodyWrapper = shallow(
      mandateRevocationResultBody
    );
    const alertWrapper = mandateRevocationResultBodyWrapper.find(
      "div[id='alert-revoked']"
    );
    expect(alertWrapper.exists()).toBeTruthy();
  });
});
