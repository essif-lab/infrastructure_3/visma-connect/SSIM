// SSI Mandate Application
// Copyright (C) 2022  Visma Connect
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
// See LICENSE in the project root for license information.
import useRevocationForm from "../MandateRevocationFormHooks";
import axios from "axios";
import { act } from "react-dom/test-utils";
import ReactDOM from "react-dom";

jest.mock("axios", () => ({
  ...jest.requireActual("axios"),
  post: jest.fn(),
  get: jest.fn(),
}));

let result;

function TestComponent() {
  result = useRevocationForm();
  return null;
}

let containerTest;

beforeEach(() => {
  const data = [
    {
      schemaName: "schemaName",
      values: [{ credName: "credValue" }],
      credRevId: "credRevId123",
      revRegId: "revRegId456",
      connectionId: "connectionId789",
    },
  ];
  axios.get.mockImplementationOnce(() => Promise.resolve(data));

  containerTest = document.createElement("div");
  document.body.appendChild(containerTest);
});

afterEach(() => {
  document.body.removeChild(containerTest);
  containerTest = null;
});

describe("<MandateRevocationFormHooks />", () => {
  test("Should have proper initial values", () => {
    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
    });

    expect(result.credentials).toEqual([]);
    expect(result.revokeSuccessful).toBeFalsy();
    expect(result.inputs).toEqual({});
    expect(result.errorMessage).toBe("");
  });

  test("Should have empty credentials", () => {
    axios.get.mockImplementationOnce(() => Promise.resolve(null));
    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
    });

    expect(result.credentials).toEqual([]);
    expect(result.revokeSuccessful).toBeFalsy();
    expect(result.inputs).toEqual({});
    expect(result.errorMessage).toBe("");
  });

  test("Should handle submit successfully", async () => {
    axios.post.mockImplementationOnce(() => Promise.resolve("success"));

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleSubmit();
    });
    await expect(result.revokeSuccessful).toBeTruthy();
  });

  test("Should handle submit with error", async () => {
    axios.post.mockImplementationOnce(() =>
      Promise.reject(new Error("Internal error occured"))
    );

    await act(async () => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleSubmit();
    });
    await expect(result.revokeSuccessful).toBeFalsy();
    await expect(result.errorMessage).toBe("Unable to revoke credential");
  });

  test("Should handle input change", () => {
    const credential = {
      revRegId: "revRegId123",
      credRevId: "credRevId456",
    };
    act(() => {
      ReactDOM.render(<TestComponent />, containerTest);
      result.handleInputChange(credential);
    });
    expect(result.inputs).toEqual({
      revRegId: "revRegId123",
      credRevId: "credRevId456",
    });
  });
});
