#!/bin/bash

SCRIPT_DIR=$(dirname $0)
NGROK_ENDPOINT=$(echo "$SCRIPT_DIR/docker/scripts/get_ngrok_endpoint.sh")

date "+TIME: %H:%M:%S"
TIME_START=$(date "+TIME: %H:%M:%S")

JQ=${JQ:-`which jq`}
if [ -x "$JQ" ]; then
  echo "jq: $JQ"
else
  echo "jq not found"
  exit 1
fi

NC=${NC:-`which nc`}
if [ -x "$NC" ]; then
  echo "nc: $NC"
else
  echo "nc not found, trying ncat"
  NC=${NC:-`which ncat`}
  if [ -x "$NC" ]; then
    echo "nc: $NC"
  else
    echo "ncat not found"
    exit 1
  fi
  exit 1
fi

# Init git submodules
git submodule init
git submodule update
# Force reset of shell scripts in Unix EOL mode
rm -rf docker
git checkout docker

docker-compose build

HTTPS_TRAEFIK=$($SCRIPT_DIR/docker/scripts/start-traefik.sh)
HTTPS_TAILS=$($SCRIPT_DIR/docker/scripts/start-tails.sh)
HTTPS_ACAPY=$($SCRIPT_DIR/docker/scripts/start-acapy.sh)
$SCRIPT_DIR/docker/scripts/start-mandate-service.sh
$SCRIPT_DIR/docker/scripts/start-opa.sh
HTTPS_MANDATE_API=$($SCRIPT_DIR/docker/scripts/start-mandate-api-service.sh)
HTTP_WEB_APP=$($SCRIPT_DIR/docker/scripts/start-web-app.sh)

printf "\n\n\n"
echo "#####################################"
echo "Traefik: ${HTTPS_TRAEFIK}"
echo "Mandate API: ${HTTPS_MANDATE_API}"
echo "Tails: ${HTTPS_TAILS}"
echo "Acapy: $HTTPS_ACAPY"
echo "#####################################"
printf "\n\n\n"

echo "Waiting for services to start up. Suggestion is that you run 'docker-compose logs -f' in another tab to see the output"

echo "#####################################"
echo "Start Alice with ./docker/scripts/start-alice.sh"
echo "#####################################"
printf "\n\n\n"

./docker/scripts/wait_until_endpoint_is_served.sh "Mandate API" 8443
./docker/scripts/wait_until_endpoint_is_served.sh "Web application" 3000

TIME_END=$(date "+TIME: %H:%M:%S")

echo $TIME_START " - " $TIME_END

unameOut="$(uname -s)"
if [[ -n "$IS_WSL" || -n "$WSL_DISTRO_NAME" ]]; then
  cmd.exe /C start $HTTP_WEB_APP
elif [[ "${unameOut}" = "Darwin" ]]; then
  open $HTTP_WEB_APP
else
  xdg-open $HTTP_WEB_APP
fi

printf "\n\n\n"
echo "#####################################"
echo "Mandate API: ${HTTPS_MANDATE_API}"
echo "Tails: ${HTTPS_TAILS}"
echo "Acapy: $HTTPS_ACAPY"
echo "Alice: ${HTTPS_ALICE}"
echo "Web: $HTTP_WEB_APP"
echo "#####################################"
