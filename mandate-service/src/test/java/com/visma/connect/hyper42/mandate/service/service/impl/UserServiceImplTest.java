/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.dao.UserDao;
import com.visma.connect.hyper42.mandate.service.model.User;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.UserResponse;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {
    @Mock
    private UserDao userDao;

    @InjectMocks
    private UserServiceImpl userServiceImpl;

    @Captor
    private ArgumentCaptor<String> emailCaptor;

    @Test
    public void testGetUserByEmail() {
        String email = "someEmail";
        User user = new User("someId", email, "someRole");

        when(userDao.findByEmail(emailCaptor.capture())).thenReturn(Optional.of(user));

        UserResponse userResponse = userServiceImpl.getUserByEmail(email);
        assertEquals("someId", userResponse.getId());
        assertEquals("someEmail", userResponse.getEmail());
        assertEquals("someRole", userResponse.getRole());
    }

    @Test
    void testGetUserByEmailNotFound() {
        String email = "someEmail";
        when(userDao.findByEmail(emailCaptor.capture())).thenReturn(Optional.empty());
        assertThrows(ApplicationRuntimeException.class, () -> userServiceImpl.getUserByEmail(email));
    }

}
