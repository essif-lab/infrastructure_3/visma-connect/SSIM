/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.mapper.impl;

import com.visma.connect.hyper42.mandate.service.TestDataGenerator;
import com.visma.connect.hyper42.mandate.service.model.generated.Attribute_;
import com.visma.connect.hyper42.mandate.service.model.generated.Condition;
import com.visma.connect.hyper42.mandate.service.model.generated.Predicate;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.Schema;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class SchemaMapperImplTest {

    @InjectMocks
    private SchemaMapperImpl schemaMapperImpl;

    @Test
    void testToSchema() {
        AddSchemaRequest addSchemaRequest = TestDataGenerator.getAddSchemaRequest();

        Schema schema = schemaMapperImpl.toSchema("schemaId", addSchemaRequest);

        assertEquals("schema-name", schema.getName());
        assertEquals("Schema Name", schema.getTitle());
        assertEquals("1.0", schema.getVersion());
        assertEquals("attr-name", schema.getAttributes().get(0).getName());
        assertEquals("Attr Name", schema.getAttributes().get(0).getTitle());
        assertEquals("text", schema.getAttributes().get(0).getType());
        assertEquals("pred-name", schema.getPredicates().get(0).getName());
        assertEquals("text", schema.getPredicates().get(0).getType());
        assertEquals("Cond Name", schema.getPredicates().get(0).getConditions().get(0).getTitle());
        assertEquals(">", schema.getPredicates().get(0).getConditions().get(0).getValue());
        assertTrue(schema.isSsicommsEnabled());
    }

    @Test
    void testToSchemaDefinitionResponseDto() {
        Schema schema = TestDataGenerator.getSchema();

        SchemaDefinitionResponse schemaDefinitionResponse = schemaMapperImpl.toSchemaDefinitionResponseDto(schema);

        Attribute_ attribute = schemaDefinitionResponse.getAttributes().get(0);
        Predicate predicate = schemaDefinitionResponse.getPredicates().get(0);
        Condition condition = predicate.getCondition().get(0);

        assertEquals("someSchemaId", schemaDefinitionResponse.getSchemaId());
        assertEquals("someName", schemaDefinitionResponse.getName());
        assertEquals("someTitle", schemaDefinitionResponse.getTitle());
        assertEquals("someVersion", schemaDefinitionResponse.getVersion());
        assertEquals("attr-name", attribute.getName());
        assertEquals("Attr Name", attribute.getTitle());
        assertEquals("text", attribute.getType());
        assertEquals("pred-name", predicate.getName());
        assertEquals("number", predicate.getType());
        assertEquals("Condition Name", condition.getTitle());
        assertEquals(">=", condition.getValue());
        assertFalse(schemaDefinitionResponse.getSsicommsEnabled());
    }

}
