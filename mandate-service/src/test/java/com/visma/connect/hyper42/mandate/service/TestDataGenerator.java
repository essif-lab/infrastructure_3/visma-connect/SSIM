/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.service.model.SchemaDefinition;
import com.visma.connect.hyper42.mandate.service.model.generated.Attribute_;
import com.visma.connect.hyper42.mandate.service.model.generated.AuthorizationSchema;
import com.visma.connect.hyper42.mandate.service.model.generated.Condition;
import com.visma.connect.hyper42.mandate.service.model.MandateRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.CreateInvitationResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefinitionRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.Invitation;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.Predicate;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.Attribute;
import com.visma.connect.hyper42.mandate.service.model.schema.Schema;
import com.visma.connect.hyper42.mandate.service.model.generated.IssuedMandateResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.RevokeCredentialRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationCreateRequestRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationExchange;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class TestDataGenerator {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final String PRESENTATION_EXCHANGE_ID = "5cad6e82-d7c3-4605-8cda-42891f945036";
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final String CONNECTION_ID = "someConnectionId";
    private static final String INVITATION_URL = "http://aries-cloudagent-runner:8020?c_i=someBase64String";
    private static final String LABEL = "someAgent";
    private static final String INVITATION_ID = "someInvitationId";
    private static final String INVITATION_TYPE = "did:sov:someDid;spec/connections/1.0/invitation";
    private static final String SERVICE_ENDPOINT = "http://some-cloudagent-runner:8020";
    private static final String RECIPIENT_KEY_1 = "someKeyForRecipient1";
    private static final String RECIPIENT_KEY_2 = "someKeyForRecipient2";
    private static final String SCHEMA_NAME = "mandate-signing-basic:1.0";

    private static final String V10_PRESENTATION_EXCHANGE_JSON = "{\n"
            + "  \"thread_id\": \"e6dcf97a-be64-4d9d-b0bf-577f962ea72b\",\n"
            + "  \"role\": \"verifier\",\n"
            + "  \"presentation_request\": {\n"
            + "    \"name\": \"mandate proof\",\n"
            + "    \"nonce\": \"1234567890\",\n"
            + "    \"requested_attributes\": {\n"
            + "      \"additionalProp1\": {\n"
            + "        \"name\": \"authorization\",\n"
            + "        \"restrictions\": [\n"
            + "          {\n"
            + "            \"cred_def_id\": \"55MgvkQDB815vtbFky4kEt:3:CL:8:default\"\n"
            + "          }\n"
            + "        ]\n"
            + "      },\n"
            + "      \"additionalProp2\": {\n"
            + "        \"name\": \"spending-limit\",\n"
            + "        \"restrictions\": [\n"
            + "          {\n"
            + "            \"cred_def_id\": \"55MgvkQDB815vtbFky4kEt:3:CL:8:default\"\n"
            + "          }\n"
            + "        ]\n"
            + "      }\n"
            + "    },\n"
            + "    \"requested_predicates\": {\n"
            + "      \"additionalProp1\": {\n"
            + "        \"name\": \"spending-limit\",\n"
            + "        \"p_type\": \">=\",\n"
            + "        \"p_value\": 100,\n"
            + "        \"restrictions\": [\n"
            + "          {\n"
            + "            \"cred_def_id\": \"55MgvkQDB815vtbFky4kEt:3:CL:8:default\"\n"
            + "          }\n"
            + "        ]\n"
            + "      }\n"
            + "    },\n"
            + "    \"version\": \"1.0\"\n"
            + "  },\n"
            + "  \"created_at\": \"2021-02-23 08:20:57.141714Z\",\n"
            + "  \"initiator\": \"self\",\n"
            + "  \"presentation_request_dict\": {\n"
            + "    \"@type\": \"did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/present-proof/1.0/request-presentation\",\n"
            + "    \"@id\": \"e6dcf97a-be64-4d9d-b0bf-577f962ea72b\",\n"
            + "    \"~trace\": {\n"
            + "      \"target\": \"log\",\n"
            + "      \"full_thread\": true,\n"
            + "      \"trace_reports\": []\n"
            + "    },\n"
            + "    \"request_presentations~attach\": [\n"
            + "      {\n"
            + "        \"@id\": \"libindy-request-presentation-0\",\n"
            + "        \"mime-type\": \"application/json\",\n"
            + "        \"data\": {\n"
            + "          \"base64\": \"eyJuYW1lIjogIm1hbmRhdGUgcHJvb2YiLCAibm9uY2UiOiAiMTIzNDU2Nzg5MCIsICJyZXF1ZXN0ZWRfYXR0cmlidXRlcyI6IHsiYWRkaXRpb25hbFByb3AxIjogeyJuYW1lIjogImF1dGhvcml6YXRpb24iLCAicmVzdHJpY3Rpb25zIjogW3siY3JlZF9kZWZfaWQiOiAiNTVNZ3ZrUURCODE1dnRiRmt5NGtFdDozOkNMOjg6ZGVmYXVsdCJ9XX0sICJhZGRpdGlvbmFsUHJvcDIiOiB7Im5hbWUiOiAic3BlbmRpbmctbGltaXQiLCAicmVzdHJpY3Rpb25zIjogW3siY3JlZF9kZWZfaWQiOiAiNTVNZ3ZrUURCODE1dnRiRmt5NGtFdDozOkNMOjg6ZGVmYXVsdCJ9XX19LCAicmVxdWVzdGVkX3ByZWRpY2F0ZXMiOiB7ImFkZGl0aW9uYWxQcm9wMSI6IHsibmFtZSI6ICJzcGVuZGluZy1saW1pdCIsICJwX3R5cGUiOiAiPj0iLCAicF92YWx1ZSI6IDEwMCwgInJlc3RyaWN0aW9ucyI6IFt7ImNyZWRfZGVmX2lkIjogIjU1TWd2a1FEQjgxNXZ0YkZreTRrRXQ6MzpDTDo4OmRlZmF1bHQifV19fSwgInZlcnNpb24iOiAiMS4wIn0=\"\n"
            + "        }\n"
            + "      }\n"
            + "    ],\n"
            + "    \"comment\": \"test\"\n"
            + "  },\n"
            + "  \"trace\": true,\n"
            + "  \"state\": \"request_sent\",\n"
            + "  \"auto_present\": false,\n"
            + "  \"presentation_exchange_id\": \"" + PRESENTATION_EXCHANGE_ID + "\",\n"
            + "  \"updated_at\": \"2021-02-23 08:20:57.141714Z\"\n"
            + "}";

    private static final String V10_PRESENTATION_CREATE_REQUEST_REQUEST_JSON = "{\n" +
            "   \"trace\":false,\n" +
            "   \"proof_request\":{\n" +
            "      \"name\":\"mandate proof\",\n" +
            "      \"nonce\":\"6267504296\",\n" +
            "      \"requested_attributes\":{\n" +
            "         \"62912d67-3993-46e8-b66c-a5f465ec8b74\":{\n" +
            "            \"names\":[\n" +
            "               \"authorization\"\n" +
            "            ],\n" +
            "            \"restrictions\":[\n" +
            "               {\n" +
            "                  \"cred_def_id\":\"55MgvkQDB815vtbFky4kEt:3:CL:8:default\",\n" +
            "                  \"schema_id\":\"WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0\"\n" +
            "               }\n" +
            "            ]\n" +
            "         }\n" +
            "      },\n" +
            "      \"requested_predicates\":{\n" +
            "         \"164feb83-2c3f-443a-8c28-1856ec3aa4cb\":{\n" +
            "            \"name\":\"spending-limit\",\n" +
            "            \"p_type\":\">=\",\n" +
            "            \"p_value\":100,\n" +
            "            \"restrictions\":[\n" +
            "               {\n" +
            "                  \"cred_def_id\":\"55MgvkQDB815vtbFky4kEt:3:CL:8:default\",\n" +
            "                  \"schema_id\":\"WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0\"\n" +
            "               }\n" +
            "            ]\n" +
            "         }\n" +
            "      },\n" +
            "      \"version\":\"1.0\"\n" +
            "   }\n" +
            "}";

    public static CreateInvitationResponse createInvitationResponse(String alias) {
        return new CreateInvitationResponse()
                .withAlias(alias)
                .withConnectionId(CONNECTION_ID)
                .withInvitationUrl(INVITATION_URL)
                .withInvitation(createInvitation());
    }

    public static Invitation createInvitation() {
        return new Invitation()
                .withId(INVITATION_ID)
                .withType(INVITATION_TYPE)
                .withLabel(LABEL)
                .withServiceEndpoint(SERVICE_ENDPOINT)
                .withRecipientKeys(Arrays.asList(RECIPIENT_KEY_1, RECIPIENT_KEY_2));
    }

    public static String createAliasString() {
        return UUID.randomUUID().toString();
    }

    public static String createSchemaString() {
        return "{\n" +
                "   \"mandate-schema-id\":\"MandateRequest\",\n" +
                "   \"requested-schema\":\"3ccxRR8oeQR2MKmVLDvHgL:2:mandate-signing-basic:1.0\",\n" +
                "   \"representative\":\"Kevin Employee\",\n" +
                "   \"dependant\":\"7617e320-0c64-4875-a17b-e51e9048e2a0\",\n" +
                "   \"authorizations\":[\n" +
                "      {\n" +
                "         \"key\":\"authorization\",\n" +
                "         \"value\":\"signing\"\n" +
                "      },\n" +
                "      {\n" +
                "         \"key\":\"spending-limit\",\n" +
                "         \"value\":\"9001\"\n" +
                "      }\n" +
                "   ]\n" +
                "}";
    }

    public static String createMandateRequestJsonString() {
        return "{\n" +
                "   \"authorization\":\"test\",\n" +
                "   \"spending-limit\":\"1234\",\n" +
                "}";
    }

    public static RevokeCredentialRequest createCredentialRevocationRequest() {
        return new RevokeCredentialRequest()
                .withCredRevId("12345")
                .withRevRegId("1234");
    }

    public static String createRequestJson() {
        return "{\"some\":\"json\"}";
    }

    public static String createSchemaName() {
        return SCHEMA_NAME;
    }

    public static IssueCredentialSendResponse createIssueCredentialSendResponse() {
        return new IssueCredentialSendResponse()
                .withAutoIssue(false)
                .withAutoRemove(false)
                .withAutoOffer(false)
                .withConnectionId("connectionToCoen")
                .withCredentialDefinitionId("WgWxqztrNooG92RXvxSTWv:3:CL:20:tag")
                .withRole("issuer")
                .withSchemaId("WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0")
                .withRevocRegId("someRevocRegId")
                .withRevocationId("someRevocationId")
                .withState("credential_issued");
    }

    public static String createVerifierRequestJson() {
        return "[\"authorization\",\"spending-limit\"]";
    }

    public static V10PresentationExchange createV10PresentationExchange() throws JsonMappingException, JsonProcessingException {
        return OBJECT_MAPPER.readValue(V10_PRESENTATION_EXCHANGE_JSON, V10PresentationExchange.class);
    }

    public static V10PresentationCreateRequestRequest createV10PresentationCreateRequestRequest() throws JsonMappingException, JsonProcessingException {
        return OBJECT_MAPPER.readValue(V10_PRESENTATION_CREATE_REQUEST_REQUEST_JSON, V10PresentationCreateRequestRequest.class);
    }

    public static String getPresentationExchangeId() {
        return PRESENTATION_EXCHANGE_ID;
    }

    public static List<MandateRequest> formIssuedMandateRequests() {
        MandateRequest mandateRequest = new MandateRequest("someId", "someConnectionId", "someSchemaName",
                "{\"authorization\":\"Books\",\"spending-limit\":\"9001\"}");
        mandateRequest.setStatus("issued");
        mandateRequest.setCredRevId("someCredRevId");
        mandateRequest.setRevRegId("someRevRegId");
        return Collections.singletonList(mandateRequest);
    }

    public static List<IssuedMandateResponse> formIssuedMandateResponses() {
        return Collections.singletonList(
                new IssuedMandateResponse()
                        .withConnectionId("someConnectionId")
                        .withSchemaName("someSchemaName")
                        .withCredRevId("someCredRevId")
                        .withRevRegId("someRevRegId")
                        .withRequest("{\"authorization\":\"Books\",\"spending-limit\":\"9001\"}"));
    }

    public static SchemaDefinitionResponse getSchemaDefinitionResponse() {
        return new SchemaDefinitionResponse()
                .withSchemaId("someSchemaId")
                .withTitle("someTitle")
                .withAttributes(getSchemaDefinitionResponseAttributes())
                .withPredicates(getSchemaDefinitionResponsePredicates())
                .withName("someName")
                .withVersion("someVersion")
                .withSsicommsEnabled(false);
    }

    public static Schema getSchema() {
        return new Schema("someId", "someSchemaId", "someName", "someTitle", "someVersion", getSchemaAttributes(),
                getSchemaPredicates(), false);
    }

    public static SchemaDefinition getSchemaDefinition() {
        return new SchemaDefinition("someId", "someName", "someDefinition");
    }

    public static SchemaResponse createSchemaResponse() {
        SchemaResponse schemaResponse = new SchemaResponse();
        com.visma.connect.hyper42.mandate.service.model.generated.Schema schema = new com.visma.connect.hyper42.mandate.service.model.generated.Schema();
        schema.setId("someId");
        schema.setName("someName");
        schema.setSeqNo(10L);
        schema.setVer("1.0");
        schema.setVersion("1.0");
        schema.setAttrNames(Arrays.asList("signature", "representativeIdentifier"));
        schemaResponse.setSchema(schema);
        schemaResponse.setSchemaId("someSchemaId");
        return schemaResponse;
    }

    public static AuthorizationSchema createAuthorizationSchema() {
        List<String> attrList = Arrays.asList("signature", "representativeIdentifier");
        AuthorizationSchema authorizationSchema = new AuthorizationSchema();
        authorizationSchema.setAttributes(attrList);
        authorizationSchema.setSchemaName("someschema");
        authorizationSchema.setSchemaVersion("1.0");
        return authorizationSchema;
    }

    public static AddSchemaRequest getAddSchemaRequest() {
        return new AddSchemaRequest()
                .withName("schema-name")
                .withTitle("Schema Name")
                .withVersion("1.0")
                .withAttributes(getSchemaAttributesDto())
                .withPredicates(getSchemaAPredicatesDto())
                .withSsicommsEnabled(true);
    }

    public static CredentialDefinitionRequest credentialDefinitionRequest() {
        CredentialDefinitionRequest credentialDefinitionRequest = new CredentialDefinitionRequest();
        credentialDefinitionRequest.setTag("default");
        credentialDefinitionRequest.setSchemaId("someSchemaId");
        credentialDefinitionRequest.setSupportRevocation(false);
        credentialDefinitionRequest.setRevocationRegistrySize(1000L);
        return credentialDefinitionRequest;
    }

    public static CredentialDefinitionResponse credentialDefinitionResponse() {
        CredentialDefinitionResponse credentialDefinitionResponse = new CredentialDefinitionResponse();
        credentialDefinitionResponse.setCredentialDefinitionId("someDefId");
        return credentialDefinitionResponse;
    }

    private static List<Attribute_> getSchemaDefinitionResponseAttributes() {
        return Collections.singletonList(
                new Attribute_()
                        .withName("attr-name")
                        .withTitle("Attr Name")
                        .withType("text"));
    }

    private static List<Predicate> getSchemaDefinitionResponsePredicates() {
        return Collections.singletonList(
                new Predicate()
                        .withName("pred-name")
                        .withType("number")
                        .withCondition(getSchemaDefinitionResponseConditions()));
    }

    private static List<Condition> getSchemaDefinitionResponseConditions() {
        return Collections.singletonList(
                new Condition()
                        .withTitle("Condition Name")
                        .withValue(">="));
    }

    private static List<Attribute> getSchemaAttributes() {
        return Collections.singletonList(
                new Attribute("attr-name", "Attr Name", "text"));
    }

    private static List<com.visma.connect.hyper42.mandate.service.model.schema.Predicate> getSchemaPredicates() {
        return Collections.singletonList(
                new com.visma.connect.hyper42.mandate.service.model.schema.Predicate("pred-name", "number", getSchemaConditions()));
    }

    private static List<com.visma.connect.hyper42.mandate.service.model.schema.Condition> getSchemaConditions() {
        return Collections.singletonList(
                new com.visma.connect.hyper42.mandate.service.model.schema.Condition("Condition Name", ">="));
    }

    private static List<Attribute_> getSchemaAttributesDto() {
        return Collections.singletonList(
                new Attribute_()
                        .withName("attr-name")
                        .withTitle("Attr Name")
                        .withType("text"));
    }

    private static List<Predicate> getSchemaAPredicatesDto() {
        return Collections.singletonList(
                new Predicate()
                        .withName("pred-name")
                        .withType("text")
                        .withCondition(getSchemaConditionsDto()));
    }

    private static List<Condition> getSchemaConditionsDto() {
        return Collections.singletonList(
                new Condition()
                        .withTitle("Cond Name")
                        .withValue(">"));
    }

    public static JsonNode createJsonNode(String jsonString) throws JsonProcessingException {
        return MAPPER.readTree(jsonString);
    }
}
