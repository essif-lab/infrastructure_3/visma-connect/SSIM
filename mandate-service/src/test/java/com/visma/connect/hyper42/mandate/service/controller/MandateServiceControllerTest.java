/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.visma.connect.hyper42.mandate.service.TestDataGenerator;
import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.IssuedMandateResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.RevokeCredentialRequest;
import com.visma.connect.hyper42.mandate.service.service.MandateService;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

@ExtendWith(MockitoExtension.class)
class MandateServiceControllerTest {

    @InjectMocks
    private MandateServiceController mandateServiceController;

    @Mock
    private MandateService mandateService;

    @Captor
    private ArgumentCaptor<String> schemaNameCaptor;

    @Captor
    private ArgumentCaptor<String> jsonCaptor;

    @Captor
    private ArgumentCaptor<String> aliasCaptor;

    @Captor
    private ArgumentCaptor<String> commentCaptor;

    @Captor
    private ArgumentCaptor<String> connectionIdCaptor;

    @Captor
    private ArgumentCaptor<Boolean> ssicommsEnabledCaptor;

    @Captor
    private ArgumentCaptor<IssueCredentialSendResponse> issueCredentialSendResponseArgumentCaptor;

    @Captor
    private ArgumentCaptor<RevokeCredentialRequest> revokeCredentialRequestArgumentCaptor;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    void testCreateMandateRequestSuccess() throws JsonProcessingException {
        String alias = TestDataGenerator.createAliasString();
        String schema = TestDataGenerator.createSchemaString();
        String schemaName = "someSchema:1.0";
        String qrCodeUrl = "someBase64String";
        boolean ssicommsEnabled = false;

        when(mandateService.saveMandateRequest(aliasCaptor.capture(), jsonCaptor.capture(), schemaNameCaptor.capture(), ssicommsEnabledCaptor.capture()))
                .thenReturn(qrCodeUrl);

        ResponseEntity<String> responseEntity = mandateServiceController.createNewMandateRequest(schemaName, alias, ssicommsEnabled, schema);
        String response = responseEntity.getBody();
        assertEquals(qrCodeUrl, response);

        assertEquals(schemaName, schemaNameCaptor.getValue());
        assertEquals(schema, jsonCaptor.getValue());
        assertFalse(ssicommsEnabledCaptor.getValue());
    }

    @Test
    void testCreateMandateRequestEncodedSchemaNameSuccess() throws JsonProcessingException {
        String alias = TestDataGenerator.createAliasString();
        String schema = TestDataGenerator.createSchemaString();
        String schemaName = "someSchema%3A1.0";
        String schemaNameDecoded = "someSchema:1.0";
        String qrCodeUrl = "someBase64String";
        boolean ssicommsEnabled = false;

        when(mandateService.saveMandateRequest(aliasCaptor.capture(), jsonCaptor.capture(), schemaNameCaptor.capture(), ssicommsEnabledCaptor.capture()))
                .thenReturn(qrCodeUrl);

        ResponseEntity<String> responseEntity = mandateServiceController.createNewMandateRequest(schemaName, alias, ssicommsEnabled, schema);
        String response = responseEntity.getBody();
        assertEquals(qrCodeUrl, response);

        assertEquals(schemaNameDecoded, schemaNameCaptor.getValue());
        assertEquals(schema, jsonCaptor.getValue());
        assertFalse(ssicommsEnabledCaptor.getValue());
    }

    @Test
    void testCreateMandateRequestFails() {
        String alias = TestDataGenerator.createAliasString();
        String schema = TestDataGenerator.createSchemaString();
        String schemaName = "someSchema:1.0";
        boolean ssicommsEnabled = false;

        when(mandateService.saveMandateRequest(aliasCaptor.capture(), jsonCaptor.capture(), schemaNameCaptor.capture(), ssicommsEnabledCaptor.capture()))
                .thenThrow(new ApplicationRuntimeException("Could not create mandate request data"));

        assertThrows(ApplicationRuntimeException.class, () -> mandateServiceController.createNewMandateRequest(schemaName, alias, ssicommsEnabled, schema));
        assertEquals(schemaName, schemaNameCaptor.getValue());
        assertEquals(schema, jsonCaptor.getValue());
        assertEquals(schema, jsonCaptor.getValue());
        assertFalse(ssicommsEnabledCaptor.getValue());
    }

    @Test
    void testProcessConnectionActiveSuccess() throws Exception {
        String connectionId = "someConnectionId";
        String responseJson = "{\"some\":\"json\"}";

        when(mandateService.createMandateRequest(connectionIdCaptor.capture())).thenReturn(responseJson);

        ResponseEntity<String> responseEntity = mandateServiceController.processConnection(connectionId, "active");
        String response = responseEntity.getBody();
        assertEquals(responseJson, response);

        assertEquals(connectionId, connectionIdCaptor.getValue());
        assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    void testProcessConnectionRequestSuccess() throws Exception {
        String connectionId = "someConnectionId";
        String responseJson = "{\"some\":\"json\"}";

        when(mandateService.acceptConnectionRequest(connectionIdCaptor.capture())).thenReturn(responseJson);

        ResponseEntity<String> responseEntity = mandateServiceController.processConnection(connectionId, "request");
        String response = responseEntity.getBody();
        assertEquals(responseJson, response);

        assertEquals(connectionId, connectionIdCaptor.getValue());
        assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    void testProcessConnectionRequestSSIComms() throws Exception {
        String mpId = "someMpId";
        String responseJson = "{\"some\":\"json\"}";

        when(mandateService.createConnectionInvitation(connectionIdCaptor.capture())).thenReturn(responseJson);

        ResponseEntity<String> responseEntity = mandateServiceController.processConnection(mpId, "ssicomms");
        String response = responseEntity.getBody();
        assertEquals(responseJson, response);

        assertEquals(mpId, connectionIdCaptor.getValue());
        assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    void testProcessConnectionRequestResponse() throws Exception {
        String connectionId = "someConnectionId";
        String responseJson = "{\"some\":\"json\"}";

        when(mandateService.pingConnection(connectionIdCaptor.capture())).thenReturn(responseJson);

        ResponseEntity<String> responseEntity = mandateServiceController.processConnection(connectionId, "response");
        String response = responseEntity.getBody();
        assertEquals(responseJson, response);

        assertEquals(connectionId, connectionIdCaptor.getValue());
        assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    void testProcessConnectionActiveNoData() throws Exception {
        String connectionId = "someConnectionId";
        when(mandateService.createMandateRequest(connectionIdCaptor.capture()))
                .thenThrow(new DataNotFoundException("Connection Id someConnectionId doesnt exists"));

        assertThrows(DataNotFoundException.class, () -> mandateServiceController.processConnection(connectionId, "active"));
        assertEquals(connectionId, connectionIdCaptor.getValue());
    }

    @Test
    void testProcessConnectionActiveFails() throws Exception {
        String connectionId = "someConnectionId";
        when(mandateService.createMandateRequest(connectionIdCaptor.capture()))
                .thenThrow(new ApplicationRuntimeException("Exception occurred while retrieving data"));

        assertThrows(ApplicationRuntimeException.class, () -> mandateServiceController.processConnection(connectionId, "active"));
        assertEquals(connectionId, connectionIdCaptor.getValue());
    }

    @Test
    void testUpdateCredentialDetailsSuccess() throws DataNotFoundException {
        String connectionId = "someConnetionId";
        IssueCredentialSendResponse requestJson = TestDataGenerator.createIssueCredentialSendResponse();
        String responseJson = "{\"some\":\"json\"}";

        when(mandateService.enrichCredentialDetails(connectionIdCaptor.capture(), issueCredentialSendResponseArgumentCaptor.capture()))
                .thenReturn(responseJson);

        ResponseEntity<String> responseEntity = mandateServiceController.updateCredentialDetails(connectionId, requestJson);
        String response = responseEntity.getBody();
        assertEquals("ok", response);

        assertEquals(connectionId, connectionIdCaptor.getValue());
        assertEquals(requestJson, issueCredentialSendResponseArgumentCaptor.getValue());
    }

    @Test
    void testRevokeCredentialSuccess() throws JsonProcessingException, DataNotFoundException {
        RevokeCredentialRequest requestJson = TestDataGenerator.createCredentialRevocationRequest();
        String response = "success";

        when(mandateService.revokeCredential(revokeCredentialRequestArgumentCaptor.capture())).thenReturn(response);

        ResponseEntity<String> responseEntity = mandateServiceController.revokeCredential(requestJson);
        String responseBody = responseEntity.getBody();

        RevokeCredentialRequest requestCaptured = revokeCredentialRequestArgumentCaptor.getValue();
        assertEquals(response, responseBody);
        assertEquals(requestJson.getCredRevId(), requestCaptured.getCredRevId());
        assertEquals(requestJson.getRevRegId(), requestCaptured.getRevRegId());
    }

    @Test
    void testGetIssuedMandates() {
        List<IssuedMandateResponse> issuedMandateResponses = TestDataGenerator.formIssuedMandateResponses();
        when(mandateService.retrieveIssuedMandates()).thenReturn(issuedMandateResponses);

        ResponseEntity<List<IssuedMandateResponse>> issuedMandatesResponseEntity = mandateServiceController.getIssuedMandates();
        IssuedMandateResponse singleResponse = issuedMandatesResponseEntity.getBody().get(0);

        assertEquals(1, issuedMandatesResponseEntity.getBody().size());
        assertEquals("someSchemaName", singleResponse.getSchemaName());
        assertEquals("someConnectionId", singleResponse.getConnectionId());
        assertEquals("{\"authorization\":\"Books\",\"spending-limit\":\"9001\"}", singleResponse.getRequest());
        assertEquals("someCredRevId", singleResponse.getCredRevId());
        assertEquals("someRevRegId", singleResponse.getRevRegId());
    }

    @Test
    void testGetIssuedMandatesEmpty() {
        when(mandateService.retrieveIssuedMandates()).thenReturn(Collections.emptyList());

        ResponseEntity<List<IssuedMandateResponse>> issuedMandatesResponseEntity = mandateServiceController.getIssuedMandates();
        assertNotNull(issuedMandatesResponseEntity.getBody());
        assertTrue(issuedMandatesResponseEntity.getBody().isEmpty());
    }

}
