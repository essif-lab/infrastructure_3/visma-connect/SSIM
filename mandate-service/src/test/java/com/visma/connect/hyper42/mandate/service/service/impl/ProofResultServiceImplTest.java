/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.service.dao.MandateVerifierRequestBulkDao;
import com.visma.connect.hyper42.mandate.service.dao.MandateVerifierRequestDao;
import com.visma.connect.hyper42.mandate.service.dao.SchemaDao;
import com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.ProofState;
import com.visma.connect.hyper42.mandate.service.model.schema.Attribute;
import com.visma.connect.hyper42.mandate.service.model.schema.Schema;
import com.visma.connect.hyper42.mandate.service.rest.MandateApiRestClient;
import com.visma.connect.hyper42.mandate.service.rest.OpenPolicyAgentRestClient;
import com.visma.connect.hyper42.mandate.service.service.PolicyService;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;;

@ExtendWith(MockitoExtension.class)
class ProofResultServiceImplTest {
    @Mock
    private MandateVerifierRequestDao mandateVerifierRequestDao;

    @Mock
    private MandateApiRestClient mandateApiRestClient;

    @Mock
    private MandateVerifierRequestBulkDao mandateVerifierRequestBulkDao;

    @Mock
    private PolicyService policyService;

    @Mock
    private OpenPolicyAgentRestClient opaClient;

    @Mock
    private SchemaDao schemaDao;

    @InjectMocks
    private ProofResultServiceImpl proofServiceImpl;

    @Captor
    private ArgumentCaptor<MandateVerifierRequest> mandateVerifierRequestCaptor;

    @Captor
    private ArgumentCaptor<ProofState> proofReceivedCaptor;

    @Captor
    private ArgumentCaptor<String> threadIdCaptor;

    @Captor
    private ArgumentCaptor<String> stateCaptor;

    @Captor
    private ArgumentCaptor<Long> timestampCaptor;

    @Captor
    private ArgumentCaptor<List<String>> idsCaptor;

    @Captor
    private ArgumentCaptor<ProofState> proofStateCaptor;

    @Captor
    private ArgumentCaptor<String> newStateCaptor;

    @Captor
    private ArgumentCaptor<String> uuidCaptor;

    @Test
    void testProofReceivedSucceeded() {
        Optional<MandateVerifierRequest> mandateVerifierRequest = Optional.of(new MandateVerifierRequest("id01", null, null, null));
        when(mandateVerifierRequestDao.findByThreadId(threadIdCaptor.capture())).thenReturn(mandateVerifierRequest);
        when(mandateVerifierRequestDao.save(mandateVerifierRequestCaptor.capture())).thenReturn(new MandateVerifierRequest("id02", null, null, null));
        when(mandateApiRestClient.updateProofState(proofReceivedCaptor.capture())).thenReturn("ok");
        when(policyService.getPolicyContentById(any())).thenReturn(Optional.empty());

        proofServiceImpl.processAndValidateProofReceived("1-2-3-4", "verified", "true", "{someProofJson}");

        assertEquals("1-2-3-4", threadIdCaptor.getValue());
        assertEquals("id01", proofReceivedCaptor.getValue().getId());
        assertEquals("succeeded", proofReceivedCaptor.getValue().getState());
        MandateVerifierRequest verifierRequest = mandateVerifierRequestCaptor.getValue();
        assertEquals("id01", verifierRequest.getId());
        assertEquals("verified", verifierRequest.getState());
        assertFalse(Instant.now().isBefore(Instant.ofEpochSecond(verifierRequest.getTimestamp())));
    }

    @Test
    void testProofReceivedFailed() {
        Optional<MandateVerifierRequest> mandateVerifierRequest = Optional.of(new MandateVerifierRequest("id01", null, null, null));
        when(mandateVerifierRequestDao.findByThreadId(threadIdCaptor.capture())).thenReturn(mandateVerifierRequest);
        when(mandateVerifierRequestDao.save(mandateVerifierRequestCaptor.capture())).thenReturn(new MandateVerifierRequest("id02", null, null, null));
        when(mandateApiRestClient.updateProofState(proofReceivedCaptor.capture())).thenReturn("ok");
        when(policyService.getPolicyContentById(any())).thenReturn(Optional.empty());

        proofServiceImpl.processAndValidateProofReceived("1-2-3-4", "verified", "false", "{someProofJson}");

        assertEquals("1-2-3-4", threadIdCaptor.getValue());
        assertEquals("id01", proofReceivedCaptor.getValue().getId());
        assertEquals("failed", proofReceivedCaptor.getValue().getState());
        MandateVerifierRequest verifierRequest = mandateVerifierRequestCaptor.getValue();
        assertEquals("id01", verifierRequest.getId());
        assertEquals("verified", verifierRequest.getState());
        assertFalse(Instant.now().isBefore(Instant.ofEpochSecond(verifierRequest.getTimestamp())));
    }

    @Test
    void testProofReceivedError() {
        Optional<MandateVerifierRequest> mandateVerifierRequest = Optional.of(new MandateVerifierRequest("id01", null, null, null));
        when(mandateVerifierRequestDao.findByThreadId(threadIdCaptor.capture())).thenReturn(mandateVerifierRequest);
        when(mandateVerifierRequestDao.save(mandateVerifierRequestCaptor.capture())).thenReturn(new MandateVerifierRequest("id02", null, null, null));
        when(mandateApiRestClient.updateProofState(proofReceivedCaptor.capture())).thenReturn("ok");
        when(policyService.getPolicyContentById(any())).thenReturn(Optional.empty());

        proofServiceImpl.processAndValidateProofReceived("1-2-3-4", "some_weird_state", "false", "{someProofJson}");

        assertEquals("1-2-3-4", threadIdCaptor.getValue());
        assertEquals("id01", proofReceivedCaptor.getValue().getId());
        assertEquals("error", proofReceivedCaptor.getValue().getState());
        MandateVerifierRequest verifierRequest = mandateVerifierRequestCaptor.getValue();
        assertEquals("id01", verifierRequest.getId());
        assertEquals("some_weird_state", verifierRequest.getState());
        assertFalse(Instant.now().isBefore(Instant.ofEpochSecond(verifierRequest.getTimestamp())));
    }

    @Test
    void testProofReceivedNotFound() {
        Optional<MandateVerifierRequest> mandateVerifierRequest = Optional.empty();
        when(mandateVerifierRequestDao.findByThreadId(threadIdCaptor.capture())).thenReturn(mandateVerifierRequest);

        proofServiceImpl.processAndValidateProofReceived("1-2-3-4", "verified", "false", "{someProofJson}");

        assertEquals("1-2-3-4", threadIdCaptor.getValue());
        verifyNoMoreInteractions(mandateApiRestClient);
    }

    @Test
    void testProcessProofRequestTimeout() {
        long start = Instant.now().getEpochSecond();
        List<MandateVerifierRequest> mandateVerifierRequests =
                Arrays.asList(new MandateVerifierRequest("id01", null, null, null), new MandateVerifierRequest("id02", null, null, null));
        when(mandateVerifierRequestDao.findByStateAndTimestampLessThan(stateCaptor.capture(), timestampCaptor.capture())).thenReturn(mandateVerifierRequests);

        when(mandateApiRestClient.updateProofState(proofStateCaptor.capture())).thenReturn("ok");

        doNothing().when(mandateVerifierRequestBulkDao).updateState(idsCaptor.capture(), newStateCaptor.capture(), timestampCaptor.capture());

        proofServiceImpl.processProofRequestTimeout();

        assertEquals("request_sent", stateCaptor.getValue());

        List<ProofState> proofStates = proofStateCaptor.getAllValues();
        assertEquals(2, proofStates.size());
        assertEquals("id01", proofStates.get(0).getId());
        assertEquals("timed-out", proofStates.get(0).getState());
        assertEquals("id02", proofStates.get(1).getId());
        assertEquals("timed-out", proofStates.get(1).getState());

        List<String> ids = idsCaptor.getValue();
        assertEquals(2, ids.size());
        assertTrue(ids.contains("id01"));
        assertTrue(ids.contains("id02"));
        List<Long> timestamps = timestampCaptor.getAllValues();
        assertEquals(2, timestamps.size());
        assertTrue(start <= timestamps.get(0));
        assertTrue(timestamps.get(0) <= Instant.now().getEpochSecond());
        assertTrue(start <= timestamps.get(1));
        assertTrue(timestamps.get(1) <= Instant.now().getEpochSecond());
        assertEquals("timed-out", newStateCaptor.getValue());
    }

    @Test
    void testProcessProofRequestTimeoutNothingFound() {
        List<MandateVerifierRequest> mandateVerifierRequests = new ArrayList<>();
        when(mandateVerifierRequestDao.findByStateAndTimestampLessThan(stateCaptor.capture(), timestampCaptor.capture())).thenReturn(mandateVerifierRequests);

        proofServiceImpl.processProofRequestTimeout();
        verifyNoMoreInteractions(mandateApiRestClient, mandateVerifierRequestBulkDao);
    }

    static Stream<Arguments> testRetrieveProofRequestLoginParameters() {
        return Stream.of(
                arguments("mandate-login-schema:1.0", "someEmail", "someEmail"),
                arguments("mandate-login-schema:1.0", "", "not_found"),
                arguments("otherSchema", "", "not_found"));
    }

    @ParameterizedTest
    @MethodSource("testRetrieveProofRequestLoginParameters")
    void testRetrieveProofRequestLogin(String schema, String comment, String expectedResult) {
        String uuid = "someUuid";
        MandateVerifierRequest request = new MandateVerifierRequest(uuid, schema, "someRequestJson", comment);

        when(mandateVerifierRequestDao.findById(uuidCaptor.capture())).thenReturn(Optional.of(request));

        String result = proofServiceImpl.retrieveProofRequestLoginEmail(uuid);
        assertEquals("someUuid", uuidCaptor.getValue());
        assertEquals(expectedResult, result);
    }

    @Test
    void testRetrieveProofRequestLoginEmailNotFound() {
        String uuid = "someUuid";

        when(mandateVerifierRequestDao.findById(uuidCaptor.capture())).thenReturn(Optional.empty());

        String result = proofServiceImpl.retrieveProofRequestLoginEmail(uuid);
        assertEquals("someUuid", uuidCaptor.getValue());
        assertEquals("not_found", result);
    }

    @Test
    void testPolicyValidationForReceivedProofSuccessfulNumberValue() throws JsonMappingException, JsonProcessingException {
        // Given
        String proof = "{\n"
                + "    \"revealed_attrs\": {},\n"
                + "    \"revealed_attr_groups\": {\n"
                + "        \"0_nummer_uuid\": {\n"
                + "            \"sub_proof_index\": 0,\n"
                + "            \"values\": {\n"
                + "                \"nummer\": {\n"
                + "                    \"raw\": \"14\",\n"
                + "                    \"encoded\": \"14\"\n"
                + "                }\n"
                + "            }\n"
                + "        }\n"
                + "    },\n"
                + "    \"self_attested_attrs\": {},\n"
                + "    \"unrevealed_attrs\": {},\n"
                + "    \"predicates\": {}\n"
                + "}";
        when(mandateVerifierRequestDao.findByThreadId("thread_id"))
                .thenReturn(Optional.of(new MandateVerifierRequest("id", "schemaName:1.0", "request", "comment")));
        when(mandateVerifierRequestDao.save(mandateVerifierRequestCaptor.capture())).thenReturn(new MandateVerifierRequest("id02", null, null, null));
        when(policyService.getPolicyContentById("schemaName:1.0")).thenReturn(Optional.of("policyContent"));
        ArgumentCaptor<String> dataPathCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> inputCaptor = ArgumentCaptor.forClass(String.class);
        when(opaClient.queryDataWithInput(dataPathCaptor.capture(), inputCaptor.capture())).thenReturn("{\"result\":true}");
        when(mandateApiRestClient.updateProofState(proofReceivedCaptor.capture())).thenReturn("ok");
        ArrayList<Attribute> attributes = new ArrayList<>();
        attributes.add(new Attribute("nummer", "title", "number"));
        Schema schema = new Schema("id", "schemaId", "name", "title", "version", attributes, null, false);
        when(schemaDao.findByNameAndVersion("schemaName", "1.0")).thenReturn(Optional.of(schema));

        // When
        proofServiceImpl.processAndValidateProofReceived("thread_id", "verified", "true", proof);

        // Then
        assertEquals("schemaName_1_0", dataPathCaptor.getValue());

        assertEquals("id", proofReceivedCaptor.getValue().getId());
        assertEquals("succeeded", proofReceivedCaptor.getValue().getState());
        assertEquals("verified", mandateVerifierRequestCaptor.getValue().getState());

        JsonNode input = new ObjectMapper().readTree("{\"input\": {\"nummer\":14}}");
        JsonNode inputCaptured = new ObjectMapper().readTree(inputCaptor.getValue());
        assertEquals(input, inputCaptured);
    }

    @Test
    void testPolicyValidationForReceivedProofSuccessfulStringValue() throws JsonMappingException, JsonProcessingException {
        // Given
        String proof = "{\n"
                + "    \"revealed_attrs\": {},\n"
                + "    \"revealed_attr_groups\": {\n"
                + "        \"0_nummer_uuid\": {\n"
                + "            \"sub_proof_index\": 0,\n"
                + "            \"values\": {\n"
                + "                \"string_key\": {\n"
                + "                    \"raw\": \"A string value\",\n"
                + "                    \"encoded\": \"14\"\n"
                + "                }\n"
                + "            }\n"
                + "        }\n"
                + "    },\n"
                + "    \"self_attested_attrs\": {},\n"
                + "    \"unrevealed_attrs\": {},\n"
                + "    \"predicates\": {}\n"
                + "}";
        when(mandateVerifierRequestDao.save(mandateVerifierRequestCaptor.capture())).thenReturn(new MandateVerifierRequest("id02", null, null, null));
        when(mandateVerifierRequestDao.findByThreadId("thread_id"))
                .thenReturn(Optional.of(new MandateVerifierRequest("id", "schemaName:1.0", "request", "comment")));
        when(policyService.getPolicyContentById("schemaName:1.0")).thenReturn(Optional.of("policyContent"));
        ArgumentCaptor<String> dataPathCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> inputCaptor = ArgumentCaptor.forClass(String.class);
        when(opaClient.queryDataWithInput(dataPathCaptor.capture(), inputCaptor.capture())).thenReturn("{\"result\":true}");
        when(mandateApiRestClient.updateProofState(proofReceivedCaptor.capture())).thenReturn("ok");
        ArrayList<Attribute> attributes = new ArrayList<>();
        attributes.add(new Attribute("string_key", "title", "text"));
        Schema schema = new Schema("id", "schemaId", "name", "title", "version", attributes, null, false);
        when(schemaDao.findByNameAndVersion("schemaName", "1.0")).thenReturn(Optional.of(schema));

        // When
        proofServiceImpl.processAndValidateProofReceived("thread_id", "verified", "true", proof);

        // Then
        assertEquals("schemaName_1_0", dataPathCaptor.getValue());

        assertEquals("id", proofReceivedCaptor.getValue().getId());
        assertEquals("succeeded", proofReceivedCaptor.getValue().getState());
        assertEquals("verified", mandateVerifierRequestCaptor.getValue().getState());

        JsonNode input = new ObjectMapper().readTree("{\"input\": {\"string_key\":\"A string value\"}}");
        JsonNode inputCaptured = new ObjectMapper().readTree(inputCaptor.getValue());
        assertEquals(input, inputCaptured);
    }

    private static List<Arguments> policyValidationTestArguments() {
        return Arrays.asList(
                arguments("{\"some\": \"proof\"}", Optional.empty(), null, "succeeded", "verified"),
                arguments("{\"some\": \"proof\"}", Optional.of("policyContent"), "{\"result\":false}", "policy validation failed", "policy validation failed"),
                arguments("{\"some\": \"proof\"}", Optional.of("policyContent"), "{}", "policy validation failed", "policy validation failed"),
                arguments("{\"some\": \"{my:faulty{json}un\"readably{object\"}", Optional.of("policyContent"), null, "policy validation failed",
                        "policy validation failed"),
                arguments("{\"some\": \"proof\"}", Optional.of("policyContent"), "{\\aaarrg{not-}rea\"dabl}e_result}\":true}", "policy validation failed",
                        "policy validation failed"));
    }

    @ParameterizedTest
    @MethodSource("policyValidationTestArguments")
    void testPolicyValidation(String proof, Optional<String> policyContent, String proofValidationResult, String assertProofState, String state)
            throws JsonMappingException, JsonProcessingException {
        // Given
        when(mandateVerifierRequestDao.findByThreadId("thread_id"))
                .thenReturn(Optional.of(new MandateVerifierRequest("id", "schemaName:1.0", "request", "comment")));
        when(mandateVerifierRequestDao.save(mandateVerifierRequestCaptor.capture())).thenReturn(new MandateVerifierRequest("id02", null, null, null));
        when(policyService.getPolicyContentById("schemaName:1.0")).thenReturn(policyContent);
        if (proofValidationResult != null) {
            when(opaClient.queryDataWithInput(anyString(), anyString())).thenReturn(proofValidationResult);
        }
        when(mandateApiRestClient.updateProofState(proofReceivedCaptor.capture())).thenReturn("ok");

        // When
        proofServiceImpl.processAndValidateProofReceived("thread_id", "verified", "true", proof);

        // Then
        assertEquals(assertProofState, proofReceivedCaptor.getValue().getState());
        assertEquals(state, mandateVerifierRequestCaptor.getValue().getState());
        if (proofValidationResult == null) {
            verify(opaClient, never()).queryDataWithInput(anyString(), anyString());
        }
    }

    @Test
    void testPolicyValidationMissingVerifierRequestResultsInOk() {
        // Given
        String proof = "{\"some\": \"proof\"}";

        when(mandateVerifierRequestDao.findByThreadId("thread_id"))
                .thenReturn(Optional.empty());

        // When
        proofServiceImpl.processAndValidateProofReceived("thread_id", "verified", "true", proof);

        // Then
        verify(policyService, never()).getPolicyContentById(anyString());
        verify(opaClient, never()).queryDataWithInput(anyString(), anyString());
    }
}
