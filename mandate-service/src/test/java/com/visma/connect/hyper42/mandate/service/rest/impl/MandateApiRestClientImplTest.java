/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.rest.impl;

import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.model.generated.ProofState;
import java.lang.reflect.Field;
import java.net.URI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MandateApiRestClientImplTest {

    @InjectMocks
    private MandateApiRestClientImpl mandateApiRestClient;

    @Captor
    private ArgumentCaptor<URI> uriCaptor;

    @Captor
    private ArgumentCaptor<HttpMethod> httpMethodCaptor;

    @Captor
    private ArgumentCaptor<HttpEntity<ProofState>> requestEntityCaptor;

    @Mock
    private static RestTemplate restTemplate;

    @BeforeEach
    public void setup() {
        Field field = ReflectionUtils.findField(MandateApiRestClientImpl.class, "mandateApiUrl");
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, mandateApiRestClient, "http://myserver:8080");
    }

    @Test
    void testUpdateProofState() {
        when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), requestEntityCaptor.capture(), Mockito.any(Class.class)))
                .thenReturn(ResponseEntity.ok("ok"));

        ProofState proofState = new ProofState().withId("123").withState("done");
        String result = mandateApiRestClient.updateProofState(proofState);

        assertEquals("ok", result);
        assertEquals("http://myserver:8080/internal/proofs/123", uriCaptor.getValue().toString());
        assertEquals(HttpMethod.PATCH, httpMethodCaptor.getValue());
        assertEquals(proofState, requestEntityCaptor.getValue().getBody());
    }

    @Test
    void testUpdateProofStateNotFound() {
        when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), requestEntityCaptor.capture(), Mockito.any(Class.class)))
                .thenReturn(ResponseEntity.notFound().build());

        ProofState proofState = new ProofState().withId("123").withState("done");

        assertThrows(ApplicationRuntimeException.class, () -> mandateApiRestClient.updateProofState(proofState));
    }

    @Test
    void testUpdateProofStateError() {
        when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), requestEntityCaptor.capture(), Mockito.any(Class.class)))
                .thenThrow(new RestClientException("oops"));

        ProofState proofState = new ProofState().withId("123").withState("done");

        assertThrows(ApplicationRuntimeException.class, () -> mandateApiRestClient.updateProofState(proofState));
    }

}
