/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.controller;

import com.visma.connect.hyper42.mandate.service.TestDataGenerator;
import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.model.generated.Attribute_;
import com.visma.connect.hyper42.mandate.service.model.generated.AvailableSchemasResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.Condition;
import com.visma.connect.hyper42.mandate.service.model.generated.Predicate;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaResponse;
import com.visma.connect.hyper42.mandate.service.service.SchemaService;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SchemaControllerTest {
    @Mock
    private SchemaService schemaService;
    @InjectMocks
    private SchemaController schemaController;

    @Test
    void testGetListOfSchemas() {
        AvailableSchemasResponse schema1 = new AvailableSchemasResponse().withSchemaId("schema01").withTitle("Schema 01").withSsicommsEnabled(true);
        AvailableSchemasResponse schema2 = new AvailableSchemasResponse().withSchemaId("schema02").withTitle("Schema 02").withSsicommsEnabled(false);
        when(schemaService.getAllCreatedSchema()).thenReturn(Arrays.asList(schema1, schema2));

        List<AvailableSchemasResponse> schemas = schemaController.getListOfSchemas();

        assertEquals(2, schemas.size());
        assertTrue(schemas.contains(schema1));
        assertTrue(schemas.contains(schema2));
    }

    @Test
    void testGetListOfUserSchemas() {
        AvailableSchemasResponse schema1 = new AvailableSchemasResponse().withSchemaId("schema01").withTitle("Schema 01").withSsicommsEnabled(true);
        AvailableSchemasResponse schema2 = new AvailableSchemasResponse().withSchemaId("schema02").withTitle("Schema 02").withSsicommsEnabled(false);
        when(schemaService.getAllUserSchema()).thenReturn(Arrays.asList(schema1, schema2));

        List<AvailableSchemasResponse> schemas = schemaController.getListOfUserSchemas();

        assertEquals(2, schemas.size());
        assertTrue(schemas.contains(schema1));
        assertTrue(schemas.contains(schema2));
    }

    @Test
    void testGetListOfSchemasEmpty() {
        when(schemaService.getAllCreatedSchema()).thenReturn(Arrays.asList());

        List<AvailableSchemasResponse> schemas = schemaController.getListOfSchemas();

        assertEquals(0, schemas.size());
    }

    @Test
    void testGetSchemaDefinition() {
        String schemaId = "someSchemaId";
        SchemaDefinitionResponse response = TestDataGenerator.getSchemaDefinitionResponse();

        when(schemaService.getSchemaDefinition(schemaId)).thenReturn(response);

        SchemaDefinitionResponse schemaDefinitionResponse = schemaController.getSchemaDefinition(schemaId);
        Attribute_ attribute = schemaDefinitionResponse.getAttributes().get(0);
        Predicate predicate = schemaDefinitionResponse.getPredicates().get(0);
        Condition condition = predicate.getCondition().get(0);

        assertEquals("someSchemaId", schemaDefinitionResponse.getSchemaId());
        assertEquals("someName", schemaDefinitionResponse.getName());
        assertEquals("someTitle", schemaDefinitionResponse.getTitle());
        assertEquals("someVersion", schemaDefinitionResponse.getVersion());
        assertEquals("attr-name", attribute.getName());
        assertEquals("Attr Name", attribute.getTitle());
        assertEquals("text", attribute.getType());
        assertEquals("pred-name", predicate.getName());
        assertEquals("number", predicate.getType());
        assertEquals("Condition Name", condition.getTitle());
        assertEquals(">=", condition.getValue());
        assertFalse(schemaDefinitionResponse.getSsicommsEnabled());
    }

    @Test
    void testGetSchemaDefinitionNull() {
        String schemaId = "someSchemaId";

        when(schemaService.getSchemaDefinition(schemaId)).thenReturn(null);

        SchemaDefinitionResponse schemaDefinitionResponse = schemaController.getSchemaDefinition(schemaId);
        assertNull(schemaDefinitionResponse);
    }

    @Test
    void testAddSchema() {
        AddSchemaRequest addSchemaRequest = TestDataGenerator.getAddSchemaRequest();
        AddSchemaResponse addSchemaResponse = new AddSchemaResponse().withTitle("Schema Name").withSchemaId("mySchemaId").withStatus("ok");

        when(schemaService.addSchema(addSchemaRequest)).thenReturn(addSchemaResponse);

        AddSchemaResponse response = schemaController.addSchema(addSchemaRequest);

        assertEquals("ok", response.getStatus());
        assertEquals("Schema Name", response.getTitle());
        assertEquals("mySchemaId", response.getSchemaId());
    }

    @Test
    void testGetSchemaJsonDefinition() throws DataNotFoundException {
        String schemaName = "someSchemaName";
        String definition = "someDefinition";

        when(schemaService.getSchemaJsonDefinition(schemaName)).thenReturn(definition);

        String result = schemaController.getSchemaJsonDefinition(schemaName);
        assertEquals("someDefinition", result);
    }

    @Test
    void testGetSchemaJsonDefinitionNull() throws DataNotFoundException {
        String schemaName = "someSchemaName";

        when(schemaService.getSchemaJsonDefinition(schemaName)).thenReturn(null);

        String result = schemaController.getSchemaJsonDefinition(schemaName);
        assertNull(result);
    }
}
