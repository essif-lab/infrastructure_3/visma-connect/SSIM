/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.dao.impl;

import com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest;
import java.util.Arrays;
import java.util.List;
import org.bson.Document;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MandateVerifierRequestBulkDaoImplTest {
    @Mock
    private MongoTemplate mongoTemplate;
    @InjectMocks
    private MandateVerifierRequestBulkDaoImpl mandateVerifierRequestBulkDaoImpl;

    @Mock
    private BulkOperations bulkOperations;

    @Captor
    private ArgumentCaptor<BulkMode> modeCaptor;

    @Captor
    private ArgumentCaptor<Class> classCaptor;

    @Captor
    private ArgumentCaptor<Query> queryCaptor;

    @Captor
    private ArgumentCaptor<Update> insertCaptor;

    @Test
    void testUpdateState() {
        when(mongoTemplate.bulkOps(modeCaptor.capture(), classCaptor.capture())).thenReturn(bulkOperations);

        when(bulkOperations.updateOne(queryCaptor.capture(), insertCaptor.capture())).thenReturn(null);
        when(bulkOperations.execute()).thenReturn(null);

        mandateVerifierRequestBulkDaoImpl.updateState(Arrays.asList("id01", "id02"), "done", 112233L);

        assertEquals(BulkMode.UNORDERED, modeCaptor.getValue());
        assertEquals(MandateVerifierRequest.class, classCaptor.getValue());

        List<Query> queries = queryCaptor.getAllValues();
        assertEquals(2, queries.size());
        assertEquals("id01", queries.get(0).getQueryObject().get("id"));
        assertEquals("id02", queries.get(1).getQueryObject().get("id"));
        List<Update> updates = insertCaptor.getAllValues();
        assertEquals(2, updates.size());
        Document updateObject = (Document) updates.get(0).getUpdateObject().get("$set");
        assertEquals("done", updateObject.get("state"));
        assertEquals(112233L, updateObject.getLong("timestamp"));
        updateObject = (Document) updates.get(1).getUpdateObject().get("$set");
        assertEquals("done", updateObject.get("state"));
        assertEquals(112233L, updateObject.getLong("timestamp"));
    }

}
