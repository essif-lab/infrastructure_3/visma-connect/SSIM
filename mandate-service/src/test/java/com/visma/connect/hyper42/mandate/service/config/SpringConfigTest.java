/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.config;

import java.lang.reflect.Field;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SpringConfigTest {

    private SpringConfig springConfig = new SpringConfig();

    @Mock
    private RestTemplateBuilder restTemplateBuilder;

    @Test
    void testRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        when(restTemplateBuilder.build()).thenReturn(restTemplate);

        RestTemplate template = springConfig.restTemplate(restTemplateBuilder);
        assertEquals(restTemplate, template);
    }

    @Test
    void testRequestLoggingFilter() {
        CommonsRequestLoggingFilter filter = springConfig.requestLoggingFilter();
        assertNotNull(filter);
        assertTrue(getBooleanField(filter, "includeClientInfo"));
        assertTrue(getBooleanField(filter, "includeQueryString"));
        assertTrue(getBooleanField(filter, "includePayload"));
        assertEquals(64000, getIntField(filter, "maxPayloadLength"));
    }

    private Boolean getBooleanField(CommonsRequestLoggingFilter filter, String fieldname) {
        Field field = ReflectionUtils.findField(CommonsRequestLoggingFilter.class, fieldname);
        ReflectionUtils.makeAccessible(field);
        try {
            return field.getBoolean(filter);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private int getIntField(CommonsRequestLoggingFilter filter, String fieldname) {
        Field field = ReflectionUtils.findField(CommonsRequestLoggingFilter.class, fieldname);
        ReflectionUtils.makeAccessible(field);
        try {
            return field.getInt(filter);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
