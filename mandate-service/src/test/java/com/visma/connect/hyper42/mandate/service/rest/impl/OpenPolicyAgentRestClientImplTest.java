/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.rest.impl;

import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import java.lang.reflect.Field;
import java.net.URI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpClientErrorException.BadRequest;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OpenPolicyAgentRestClientImplTest {

    @InjectMocks
    private OpenPolicyAgentRestClientImpl opaRestClient;

    @Captor
    private ArgumentCaptor<URI> uriCaptor;

    @Captor
    private ArgumentCaptor<HttpMethod> httpMethodCaptor;

    @Captor
    private ArgumentCaptor<HttpEntity<String>> requestEntityCaptor;

    @Mock
    private static RestTemplate restTemplate;

    @BeforeEach
    public void setup() {
        Field field = ReflectionUtils.findField(OpenPolicyAgentRestClientImpl.class, "opaUrl");
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, opaRestClient, "http://my-opa-server:8181");
    }

    @Test
    void shouldReturnPolicyIdWhenSuccessfulSaved() {
        // Given
        when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), requestEntityCaptor.capture(), ArgumentMatchers.<Class<String>>any()))
                .thenReturn(ResponseEntity.ok("123"));

        // When
        String result = opaRestClient.createOrUpdatePolicy("123", "policyContent");

        // Then
        assertEquals("123", result);
        assertEquals("http://my-opa-server:8181/v1/policies/123", uriCaptor.getValue().toString());
        assertEquals(HttpMethod.PUT, httpMethodCaptor.getValue());
        assertEquals("policyContent", requestEntityCaptor.getValue().getBody());
    }

    @Test
    void shouldThrowHttpClientErrorExceptionOnBadRequest() {
        // Given
        when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), requestEntityCaptor.capture(), ArgumentMatchers.<Class<String>>any()))
                .thenThrow(BadRequest.class);

        // When/Then
        assertThrows(HttpClientErrorException.class, () -> opaRestClient.createOrUpdatePolicy("policyId", "Content with error"));
    }

    @Test
    void shouldThrowApplicationRuntimeExceptionOnRestClientException() {
        // Given
        when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), requestEntityCaptor.capture(), ArgumentMatchers.<Class<String>>any()))
                .thenThrow(RestClientException.class);

        // When/Then
        ApplicationRuntimeException ex =
                assertThrows(ApplicationRuntimeException.class, () -> opaRestClient.createOrUpdatePolicy("policyId", "Content with error"));
        assertEquals("Something went wrong while sending policy to OPA.", ex.getMessage());
    }

    @Test
    void shouldThrowApplicationRuntimeExceptionWehnNot2xxResponse() {
        // Given
        when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), requestEntityCaptor.capture(), ArgumentMatchers.<Class<String>>any()))
                .thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());

        // When/Then
        ApplicationRuntimeException ex =
                assertThrows(ApplicationRuntimeException.class, () -> opaRestClient.createOrUpdatePolicy("policyId", "Content with error"));
        assertEquals("Problem with sending policy to OPA", ex.getMessage());
    }

    @Test
    void shouldQueryDataWithInputSuccessful() {
        // Given
        ResponseEntity<String> httpResponse = new ResponseEntity<String>("status_ok", HttpStatus.OK);
        ArgumentCaptor<Class<String>> responseTypeCaptor = ArgumentCaptor.<Class<String>, Class>forClass(Class.class);
        when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), requestEntityCaptor.capture(), responseTypeCaptor.capture()))
                .thenReturn(httpResponse);
        String request = "{\"my\": \"input\"}";

        // When
        String result = opaRestClient.queryDataWithInput("dataPath", request);

        // Then
        assertEquals("status_ok", result);
        assertEquals("/v1/data/dataPath/valid", uriCaptor.getValue().getPath());
        assertEquals(HttpMethod.POST, httpMethodCaptor.getValue());
        assertEquals(request, requestEntityCaptor.getValue().getBody());
        assertEquals(String.class, responseTypeCaptor.getValue());
    }

    @Test
    void shouldQueryDataWithInputThrowRestClientRequest() {
        // Given
        when(restTemplate.exchange(any(URI.class), any(HttpMethod.class), ArgumentMatchers.<HttpEntity<String>>any(), ArgumentMatchers.<Class<String>>any()))
                .thenThrow(new RestClientException("rest client exception"));
        String request = "{\"my\": \"input\"}";

        // When
        ApplicationRuntimeException exc = assertThrows(ApplicationRuntimeException.class, () -> opaRestClient.queryDataWithInput("dataPath", request));

        // Then
        assertEquals("Something went wrong while getting data from OPA.", exc.getMessage());
        assertEquals("rest client exception", exc.getCause().getMessage());
    }

    @Test
    void shouldQueryDataWithInputThrowApplicationRuntimeExceptionOnNot2xxResult() {
        // Given
        when(restTemplate.exchange(any(URI.class), any(HttpMethod.class), ArgumentMatchers.<HttpEntity<String>>any(), ArgumentMatchers.<Class<String>>any()))
                .thenReturn(new ResponseEntity<>(HttpStatus.FOUND));
        String request = "{\"my\": \"input\"}";

        // When
        ApplicationRuntimeException exc = assertThrows(ApplicationRuntimeException.class, () -> opaRestClient.queryDataWithInput("dataPath", request));

        // Then
        assertEquals("Problem with getting data from OPA", exc.getMessage());
    }
}
