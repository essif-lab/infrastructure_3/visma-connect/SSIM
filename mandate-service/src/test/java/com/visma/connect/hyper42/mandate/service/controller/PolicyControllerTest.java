/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.controller;

import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddPolicyResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.GetPolicyResponse;
import com.visma.connect.hyper42.mandate.service.service.PolicyService;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PolicyControllerTest {

    @InjectMocks
    private PolicyController policyController;

    @Mock
    private PolicyService policyService;

    @Test
    void shouldCallCreateOrUpdatePolicySuccessful() {
        // Given
        when(policyService.createOrUpdatePolicy(anyString(), anyString())).thenReturn("newId");

        // When
        AddPolicyResponse result = policyController.createOrUpdatePolicy("id", "content");

        // Then
        assertEquals("newId", result.getId());
    }

    @Test
    void shouldGetPolicy() {
        // Given
        when(policyService.getPolicyContentById(anyString())).thenReturn(Optional.of("Content of the policy"));

        // When
        ResponseEntity<GetPolicyResponse> result = policyController.getPolicy("myPolicyId");

        // Then
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("myPolicyId", result.getBody().getId());
        assertEquals("Content of the policy", result.getBody().getContent());
    }

    @Test
    void shouldGetNotFoundWhenPolicyNotExists() {
        // Given
        when(policyService.getPolicyContentById(anyString())).thenReturn(Optional.empty());

        // When
        ResponseEntity<GetPolicyResponse> result = policyController.getPolicy("myPolicyId");

        // Then
        assertEquals(HttpStatus.NOT_FOUND, result.getStatusCode());
    }
}
