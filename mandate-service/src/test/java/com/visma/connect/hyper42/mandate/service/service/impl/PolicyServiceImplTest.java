/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.visma.connect.hyper42.mandate.service.dao.PolicyDao;
import com.visma.connect.hyper42.mandate.service.model.Policy;
import com.visma.connect.hyper42.mandate.service.rest.OpenPolicyAgentRestClient;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PolicyServiceImplTest {

    @InjectMocks
    private PolicyServiceImpl policyService;

    @Mock
    private OpenPolicyAgentRestClient opaClient;

    @Mock
    private PolicyDao policyDao;

    @Captor
    ArgumentCaptor<String> policyIdDaoCaptor;

    @Captor
    ArgumentCaptor<String> policyIdClientCaptor;

    @Captor
    ArgumentCaptor<String> policyContentCaptor;

    @Captor
    ArgumentCaptor<Policy> policyCaptor;

    @Test
    void shouldCreateNewPolicy() {
        // Given
        Policy policy = new Policy("testSavedId", "testSavedContent");
        when(opaClient.createOrUpdatePolicy(policyIdClientCaptor.capture(), policyContentCaptor.capture())).thenReturn("testPolicyId");
        when(policyDao.findByPolicyId(policyIdDaoCaptor.capture())).thenReturn(Optional.empty());
        when(policyDao.save(policyCaptor.capture())).thenReturn(policy);

        // When
        String result = policyService.createOrUpdatePolicy("testId", "testPolicyContent");

        // Then
        assertEquals("testSavedId", result);
        // find in dao
        assertEquals("testPolicyId", policyIdDaoCaptor.getValue());
        // save to dao
        assertEquals("testPolicyId", policyCaptor.getValue().getPolicyId());
        assertEquals("testPolicyContent", policyCaptor.getValue().getPolicyContent());
        // call to opa client
        assertEquals("testId", policyIdClientCaptor.getValue());
        assertEquals("testPolicyContent", policyContentCaptor.getValue());
    }

    @Test
    void shouldUpdateExistingPolicy() {
        // Given
        Policy policy = new Policy("testSavedId", "testSavedContent");
        Policy updatedPolicy = new Policy("testSavedId", "updatedContent");
        when(opaClient.createOrUpdatePolicy(policyIdClientCaptor.capture(), policyContentCaptor.capture())).thenReturn("testPolicyId");
        when(policyDao.findByPolicyId(policyIdDaoCaptor.capture())).thenReturn(Optional.of(policy));
        when(policyDao.save(policyCaptor.capture())).thenReturn(updatedPolicy);

        // When
        String result = policyService.createOrUpdatePolicy("testSavedId", "updatedContent");

        // Then
        assertEquals("testSavedId", result);
        // find in dao
        assertEquals("testPolicyId", policyIdDaoCaptor.getValue());
        // save to dao
        assertEquals("testSavedId", policyCaptor.getValue().getPolicyId());
        assertEquals("updatedContent", policyCaptor.getValue().getPolicyContent());
        // call to opa client
        assertEquals("testSavedId", policyIdClientCaptor.getValue());
        assertEquals("updatedContent", policyContentCaptor.getValue());
    }

    @Test
    void shouldGetPolicyById() {
        // Given
        Policy p = new Policy("myPolicyId2", "my policy content");
        when(policyDao.findByPolicyId(policyIdDaoCaptor.capture())).thenReturn(Optional.of(p));

        // When
        Optional<String> content = policyService.getPolicyContentById("myPolicyId");

        // Then
        assertTrue(content.isPresent());
        assertEquals("my policy content", content.get());
    }

    @Test
    void shouldReturnNullWhenPolicyByIdNotFound() {
        // Given
        when(policyDao.findByPolicyId(policyIdDaoCaptor.capture())).thenReturn(Optional.empty());

        // When
        Optional<String> content = policyService.getPolicyContentById("myPolicyId");

        // Then
        assertTrue(content.isEmpty());
    }
}
