/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class NonceGeneratorUtilTest {

    @Test
    public void testGeneratNonce() {
        // nonce is a random number. To check that the first number is never 0, this test is repeated 100 times.
        for (int i = 0; i < 100; i++) {
            String nonce = NonceGeneratorUtil.generatNonce();
            assertNotNull(nonce);
            assertEquals(10, nonce.length());
            assertTrue(Long.valueOf(nonce) > 999999999L);
        }
    }
}
