/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.service.TestDataGenerator;
import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.dao.MandateRequestDao;
import com.visma.connect.hyper42.mandate.service.model.MandateRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.Attribute;
import com.visma.connect.hyper42.mandate.service.model.generated.ConnectionAcceptRequestResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CreateInvitationResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefCreatedResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.DidResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.IssuedMandateResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.Result;
import com.visma.connect.hyper42.mandate.service.model.generated.RevocationRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.RevokeCredentialRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PingRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PingRequestResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.SchemasCreatedResults;
import com.visma.connect.hyper42.mandate.service.rest.CloudAgentRestClient;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MandateServiceImplTest {
    @InjectMocks
    MandateServiceImpl mandateService;
    @Mock
    CloudAgentRestClient agentRestClient;
    @Mock
    MandateRequestDao mandateRequestDao;
    @Mock
    ObjectMapper objectMapper;

    @Captor
    ArgumentCaptor<MandateRequest> mandateRequestArgumentCaptor;

    @Captor
    ArgumentCaptor<String> connectionIdCaptor;

    @Captor
    ArgumentCaptor<String> schemaNameCaptor;

    @Captor
    ArgumentCaptor<String> schemaVersionCaptor;

    @Captor
    ArgumentCaptor<IssueCredentialSendRequest> credentialSendRequestArgumentCaptor;

    @Captor
    private ArgumentCaptor<PingRequest> pingRequestCaptor;

    @Captor
    private ArgumentCaptor<PingRequestResponse> pingRequestResultCaptor;

    @Captor
    private ArgumentCaptor<ConnectionAcceptRequestResponse> connectionAcceptRequestResponseCaptor;

    @Captor
    private ArgumentCaptor<RevocationRequest> revocationRequestArgumentCaptor;

    @Captor
    private ArgumentCaptor<String> credRevIdArgumentCaptor;

    @Captor
    private ArgumentCaptor<String> revRegIdArgumentCaptor;

    @Test
    void saveMandateRequestOK() {
        String alias = TestDataGenerator.createAliasString();
        String jsonRequest = TestDataGenerator.createMandateRequestJsonString();
        String schemaName = "someSchema:1.0";
        boolean ssicomsEnabled = false;
        CreateInvitationResponse mockInvitationResponse = TestDataGenerator.createInvitationResponse(alias);

        when(agentRestClient.createInvitation(alias, true, false, false)).thenReturn(mockInvitationResponse);
        when(mandateRequestDao.save(mandateRequestArgumentCaptor.capture())).thenReturn(formMandateRequest());

        String response = mandateService.saveMandateRequest(alias, jsonRequest, schemaName, ssicomsEnabled);

        assertEquals(mockInvitationResponse.getInvitationUrl(), response);
        MandateRequest mandateRequest = mandateRequestArgumentCaptor.getValue();
        assertNotNull(UUID.fromString(mandateRequest.getId()));
        assertEquals(jsonRequest, mandateRequest.getRequestJson());
        assertEquals(mockInvitationResponse.getConnectionId(), mandateRequest.getConnectionId());
        assertEquals(schemaName, mandateRequest.getSchemaName());
    }

    @Test
    void saveMandateRequestOKForSSICommsMandate() {
        String alias = TestDataGenerator.createAliasString();
        String jsonRequest = TestDataGenerator.createMandateRequestJsonString();
        String schemaName = "someSchema:1.0";
        boolean ssicommsEnabled = true;
        CreateInvitationResponse mockInvitationResponse = TestDataGenerator.createInvitationResponse(alias);
        when(agentRestClient.createInvitation(alias, true, false, false)).thenReturn(mockInvitationResponse);

        when(mandateRequestDao.save(mandateRequestArgumentCaptor.capture())).thenReturn(formMandateRequestSSIComms());

        String response = mandateService.saveMandateRequest(alias, jsonRequest, schemaName, ssicommsEnabled);

        MandateRequest mandateRequest = mandateRequestArgumentCaptor.getValue();
        assertNotNull(UUID.fromString(mandateRequest.getId()));
        assertEquals(jsonRequest, mandateRequest.getRequestJson());
        assertEquals(schemaName, mandateRequest.getSchemaName());
        assertEquals("http://aries-cloudagent-runner:8020?c_i=someBase64String", response);
    }

    @Test
    void saveMandateRequestNotOK() {
        String alias = TestDataGenerator.createAliasString();
        String schema = TestDataGenerator.createSchemaString();
        String schemaName = "someSchema:1.0";
        boolean ssicommsEnabled = false;

        CreateInvitationResponse mockInvitationResponse = TestDataGenerator.createInvitationResponse(alias);
        when(agentRestClient.createInvitation(alias, true, false, false)).thenReturn(mockInvitationResponse);
        when(mandateRequestDao.save(mandateRequestArgumentCaptor.capture())).thenReturn(null);

        assertThrows(ApplicationRuntimeException.class, () -> {
            mandateService.saveMandateRequest(alias, schema, schemaName, ssicommsEnabled);
        });
    }

    @Test
    void createMandateRequestOk() throws Exception {
        String connectionId = "someConnectionId";
        IssueCredentialSendResponse issueCredentialSendResponse = TestDataGenerator.createIssueCredentialSendResponse();
        Optional<MandateRequest> mandateRequest = Optional.of(formMandateRequest());
        JsonNode jsonNode = formJsonNode(TestDataGenerator.createRequestJson());
        DidResponse didResponse = formDidResponse();
        when(agentRestClient.getPublicDidDetails()).thenReturn(didResponse);
        when(objectMapper.readTree(TestDataGenerator.createRequestJson())).thenReturn(jsonNode);
        when(agentRestClient.getSchemaCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture())).thenReturn(formSchemasCreatedResults());
        when(agentRestClient.getCredentialDefinitionCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture()))
                .thenReturn(formCredentialDefCreatedResponse());
        when(agentRestClient.issueCredentialSend(credentialSendRequestArgumentCaptor.capture())).thenReturn(issueCredentialSendResponse);
        when(mandateRequestDao.findByConnectionId(connectionIdCaptor.capture())).thenReturn(mandateRequest);

        String response = mandateService.createMandateRequest(connectionId);

        assertEquals("someSchema", schemaNameCaptor.getAllValues().get(0));
        assertEquals("someSchema", schemaNameCaptor.getAllValues().get(1));
        assertEquals("1.0", schemaVersionCaptor.getAllValues().get(0));
        assertEquals("1.0", schemaVersionCaptor.getAllValues().get(1));
        assertEquals("success", response);

        IssueCredentialSendRequest credentialSendRequest = credentialSendRequestArgumentCaptor.getValue();
        assertFalse(credentialSendRequest.getAutoRemove());
        assertFalse(credentialSendRequest.getTrace());
        assertEquals(didResponse.getResult().getDid(), credentialSendRequest.getIssuerDid());
        assertEquals(connectionId, credentialSendRequest.getConnectionId());
        assertEquals("issue-credential/1.0/credential-preview", credentialSendRequest.getCredentialProposal().getType());
        List<Attribute> attributes = credentialSendRequest.getCredentialProposal().getAttributes();
        assertEquals(1, attributes.size());
        assertEquals("some", attributes.get(0).getName());
        assertEquals("json", attributes.get(0).getValue());
        assertEquals("55MgvkQDB815vtbFky4kEt:2:mandate-schema:1.1", credentialSendRequest.getSchemaId());
        assertEquals("someSchema", credentialSendRequest.getSchemaName());
        assertEquals("1.0", credentialSendRequest.getSchemaVersion());
        assertEquals(didResponse.getResult().getDid(), credentialSendRequest.getSchemaIssuerDid());
        assertEquals("WgWxqztrNooG92RXvxSTWv:3:CL:20:tag", credentialSendRequest.getCredDefId());
    }

    @Test
    void createMandateRequestNotFound() throws Exception {
        String connectionId = "someConnectionId";
        when(mandateRequestDao.findByConnectionId(connectionIdCaptor.capture())).thenReturn(Optional.empty());
        assertThrows(DataNotFoundException.class, () -> {
            mandateService.createMandateRequest(connectionId);
        });
    }

    @Test
    void createMandateRequestNotOk() throws Exception {
        String connectionId = "someConnectionId";
        when(mandateRequestDao.findByConnectionId(connectionIdCaptor.capture()))
                .thenThrow(new ApplicationRuntimeException("Connection Id someConnectionId doesnt exists"));
        assertThrows(ApplicationRuntimeException.class, () -> {
            mandateService.createMandateRequest(connectionId);
        });
    }

    @Test
    void createMandateRequestIssueCredentialNotOk() throws Exception {
        String connectionId = "someConnectionId";
        Optional<MandateRequest> mandateRequest = Optional.of(formMandateRequest());
        JsonNode jsonNode = formJsonNode(TestDataGenerator.createRequestJson());
        when(agentRestClient.getPublicDidDetails()).thenReturn(formDidResponse());
        when(objectMapper.readTree(TestDataGenerator.createRequestJson())).thenReturn(jsonNode);
        when(agentRestClient.getSchemaCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture())).thenReturn(formSchemasCreatedResults());
        when(agentRestClient.getCredentialDefinitionCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture()))
                .thenReturn(formCredentialDefCreatedResponse());
        when(mandateRequestDao.findByConnectionId(connectionIdCaptor.capture())).thenReturn(mandateRequest);
        when(agentRestClient.issueCredentialSend(credentialSendRequestArgumentCaptor.capture())).thenReturn(null);

        assertThrows(ApplicationRuntimeException.class, () -> {
            mandateService.createMandateRequest(connectionId);
        });

        assertEquals("someSchema", schemaNameCaptor.getAllValues().get(0));
        assertEquals("someSchema", schemaNameCaptor.getAllValues().get(1));
        assertEquals("1.0", schemaVersionCaptor.getAllValues().get(0));
        assertEquals("1.0", schemaVersionCaptor.getAllValues().get(1));
    }

    @Test
    void createMandateRequestNoSchema() throws Exception {
        String connectionId = "someConnectionId";
        Optional<MandateRequest> mandateRequest = Optional.of(formMandateRequest());
        JsonNode jsonNode = formJsonNode(TestDataGenerator.createRequestJson());
        when(agentRestClient.getPublicDidDetails()).thenReturn(formDidResponse());
        when(objectMapper.readTree(TestDataGenerator.createRequestJson())).thenReturn(jsonNode);
        when(agentRestClient.getSchemaCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture())).thenReturn(new SchemasCreatedResults());
        when(mandateRequestDao.findByConnectionId(connectionIdCaptor.capture())).thenReturn(mandateRequest);

        ApplicationRuntimeException applicationRuntimeException = assertThrows(ApplicationRuntimeException.class, () -> {
            mandateService.createMandateRequest(connectionId);
        });

        assertTrue(applicationRuntimeException.getCause().getMessage().startsWith("No schema found for"));
        assertEquals("someSchema", schemaNameCaptor.getAllValues().get(0));
        assertEquals("1.0", schemaVersionCaptor.getAllValues().get(0));
    }

    @Test
    void createMandateRequestNoCredentialDef() throws Exception {
        String connectionId = "someConnectionId";
        Optional<MandateRequest> mandateRequest = Optional.of(formMandateRequest());
        JsonNode jsonNode = formJsonNode(TestDataGenerator.createRequestJson());
        when(agentRestClient.getPublicDidDetails()).thenReturn(formDidResponse());
        when(objectMapper.readTree(TestDataGenerator.createRequestJson())).thenReturn(jsonNode);
        when(agentRestClient.getSchemaCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture())).thenReturn(formSchemasCreatedResults());
        when(agentRestClient.getCredentialDefinitionCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture()))
                .thenReturn(new CredentialDefCreatedResponse());
        when(mandateRequestDao.findByConnectionId(connectionIdCaptor.capture())).thenReturn(mandateRequest);

        ApplicationRuntimeException applicationRuntimeException =
                assertThrows(ApplicationRuntimeException.class, () -> mandateService.createMandateRequest(connectionId));

        assertTrue(applicationRuntimeException.getCause().getMessage().startsWith("No credential definition found"));
        assertEquals("someSchema", schemaNameCaptor.getAllValues().get(0));
        assertEquals("someSchema", schemaNameCaptor.getAllValues().get(1));
        assertEquals("1.0", schemaVersionCaptor.getAllValues().get(0));
        assertEquals("1.0", schemaVersionCaptor.getAllValues().get(1));
    }

    private MandateRequest formMandateRequest() {
        return new MandateRequest("someId", "someConnection", "someSchema:1.0", "{\"some\":\"json\"}");
    }

    private MandateRequest formMandateRequestSSIComms() {
        MandateRequest mandateRequest = new MandateRequest();
        mandateRequest.setId("someId");
        mandateRequest.setMpId("someMpId");
        mandateRequest.setSchemaName("someSchema:1.0");
        mandateRequest.setRequestJson("{\"some\":\"json\"}");
        return mandateRequest;
    }

    private MandateRequest formUpdatedMandateRequest(MandateRequest mandateRequest, IssueCredentialSendResponse response) {
        MandateRequest updatedMandateRequest = new MandateRequest(mandateRequest.getId(), mandateRequest.getConnectionId(),
                mandateRequest.getSchemaName(), mandateRequest.getRequestJson());
        updatedMandateRequest.setStatus("issued");
        updatedMandateRequest.setCredRevId(response.getRevocationId());
        updatedMandateRequest.setRevRegId(response.getRevocRegId());
        return updatedMandateRequest;
    }

    private DidResponse formDidResponse() {
        DidResponse didResponse = new DidResponse();
        Result result = new Result();
        result.setDid("WgWxqztrNooG92RXvxSTWv");
        result.setPosture("wallet_only");
        result.setVerkey("H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV");
        didResponse.setResult(result);
        return didResponse;
    }

    private SchemasCreatedResults formSchemasCreatedResults() {
        return new SchemasCreatedResults()
                .withSchemaIds(Arrays.asList("55MgvkQDB815vtbFky4kEt:2:mandate-schema:1.1"));
    }

    private CredentialDefCreatedResponse formCredentialDefCreatedResponse() {
        return new CredentialDefCreatedResponse()
                .withCredentialDefinitionIds(Arrays.asList("WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"));
    }

    private JsonNode formJsonNode(String jsonRequest) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(jsonRequest);
        return jsonNode;
    }

    @Test
    void testPingConnection() throws JsonProcessingException {
        when(agentRestClient.sendTrustPing(connectionIdCaptor.capture(), pingRequestCaptor.capture()))
                .thenReturn(new PingRequestResponse().withThreadId("wire"));
        when(objectMapper.writeValueAsString(pingRequestResultCaptor.capture())).thenReturn("jsonresult");

        String result = mandateService.pingConnection("letgetconnected");

        assertEquals("jsonresult", result);
        assertEquals("letgetconnected", connectionIdCaptor.getValue());
        assertEquals("wire", pingRequestResultCaptor.getValue().getThreadId());
    }

    @Test
    void testAcceptConnectionRequest() throws JsonProcessingException {
        when(agentRestClient.acceptConnectionRequest(connectionIdCaptor.capture()))
                .thenReturn(new ConnectionAcceptRequestResponse().withConnectionId("azaqwsx"));
        when(objectMapper.writeValueAsString(connectionAcceptRequestResponseCaptor.capture())).thenReturn("done");

        String response = mandateService.acceptConnectionRequest("edcvfr");

        assertEquals("done", response);
        assertEquals("edcvfr", connectionIdCaptor.getValue());
        assertEquals("azaqwsx", connectionAcceptRequestResponseCaptor.getValue().getConnectionId());
    }

    @Test
    void testAcceptConnectionRequestThrowsJsonProcessingException() throws JsonProcessingException {
        when(agentRestClient.acceptConnectionRequest(connectionIdCaptor.capture()))
                .thenReturn(new ConnectionAcceptRequestResponse().withConnectionId("azaqwsx"));
        when(objectMapper.writeValueAsString(connectionAcceptRequestResponseCaptor.capture())).thenThrow(new JsonGenerationException("throwing"));

        assertThrows(ApplicationRuntimeException.class, () -> mandateService.acceptConnectionRequest("edcvfr"));

        assertEquals("edcvfr", connectionIdCaptor.getValue());
        assertEquals("azaqwsx", connectionAcceptRequestResponseCaptor.getValue().getConnectionId());
    }

    @Test
    void testAcceptConnectionRequestJsonParseException() throws JsonProcessingException {
        when(agentRestClient.acceptConnectionRequest(connectionIdCaptor.capture()))
                .thenReturn(new ConnectionAcceptRequestResponse().withConnectionId("azaqwsx"));
        when(objectMapper.writeValueAsString(connectionAcceptRequestResponseCaptor.capture())).thenReturn("done");

        String response = mandateService.acceptConnectionRequest("edcvfr");

        assertEquals("done", response);
        assertEquals("edcvfr", connectionIdCaptor.getValue());
        assertEquals("azaqwsx", connectionAcceptRequestResponseCaptor.getValue().getConnectionId());
    }

    @Test
    void enrichCredentialDetailsOK() throws DataNotFoundException, JsonProcessingException {
        String connectionId = "someConnection";
        IssueCredentialSendResponse jsonRequest = TestDataGenerator.createIssueCredentialSendResponse();
        MandateRequest request = formMandateRequest();
        MandateRequest updatedMandateRequest = formUpdatedMandateRequest(request, jsonRequest);
        String jsonResponse = "{\"some\":\"json\"}";

        when(mandateRequestDao.findByConnectionId(connectionIdCaptor.capture())).thenReturn(Optional.of(request));
        when(mandateRequestDao.save(mandateRequestArgumentCaptor.capture())).thenReturn(updatedMandateRequest);
        when(objectMapper.writeValueAsString(updatedMandateRequest)).thenReturn(jsonResponse);

        String response = mandateService.enrichCredentialDetails(connectionId, jsonRequest);

        MandateRequest mandateRequest = mandateRequestArgumentCaptor.getValue();
        assertEquals(connectionId, mandateRequest.getConnectionId());
        assertEquals("issued", mandateRequest.getStatus());
        assertEquals(jsonRequest.getRevocRegId(), mandateRequest.getRevRegId());
        assertEquals(jsonRequest.getRevocationId(), mandateRequest.getCredRevId());
        assertEquals(jsonResponse, response);
    }

    @Test
    void enrichCredentialDetailsNOK() throws DataNotFoundException {
        String connectionId = "someConnection";
        IssueCredentialSendResponse jsonRequest = TestDataGenerator.createIssueCredentialSendResponse();

        when(mandateRequestDao.findByConnectionId(connectionIdCaptor.capture())).thenReturn(Optional.empty());

        String response = mandateService.enrichCredentialDetails(connectionId, jsonRequest);
        assertEquals("", response);
    }

    @Test
    void revokeCredentialOk() throws Exception {
        RevokeCredentialRequest revocationJson = TestDataGenerator.createCredentialRevocationRequest();
        MandateRequest mandateRequest = formMandateRequest();
        mandateRequest.setRevRegId("1234");
        mandateRequest.setCredRevId("12345");
        mandateRequest.setStatus("issued");
        String result = "success";

        when(agentRestClient.revokeCredential(revocationRequestArgumentCaptor.capture())).thenReturn(result);
        when(mandateRequestDao.findByCredRevIdAndRevRegId(credRevIdArgumentCaptor.capture(), revRegIdArgumentCaptor.capture()))
                .thenReturn(Optional.of(mandateRequest));
        when(mandateRequestDao.save(mandateRequestArgumentCaptor.capture())).thenReturn(mandateRequest);

        String response = mandateService.revokeCredential(revocationJson);

        RevocationRequest revocationRequestCaptured = revocationRequestArgumentCaptor.getValue();
        MandateRequest mandateRequestCapture = mandateRequestArgumentCaptor.getValue();
        assertTrue(revocationRequestCaptured.getPublish());
        assertEquals("12345", revocationRequestCaptured.getCredRevId());
        assertEquals("1234", revocationRequestCaptured.getRevRegId());
        assertEquals("12345", credRevIdArgumentCaptor.getValue());
        assertEquals("1234", revRegIdArgumentCaptor.getValue());
        assertEquals(mandateRequest.getRequestJson(), mandateRequestCapture.getRequestJson());
        assertEquals(mandateRequest.getRevRegId(), mandateRequestCapture.getRevRegId());
        assertEquals(mandateRequest.getSchemaName(), mandateRequestCapture.getSchemaName());
        assertEquals(mandateRequest.getConnectionId(), mandateRequestCapture.getConnectionId());
        assertEquals(mandateRequest.getCredRevId(), mandateRequestCapture.getCredRevId());
        assertEquals(mandateRequest.getId(), mandateRequestCapture.getId());
        assertEquals("revoked", mandateRequestCapture.getStatus());
        assertEquals(result, response);
    }

    @Test
    void revokeCredentialNok() throws Exception {
        RevokeCredentialRequest revocationJson = TestDataGenerator.createCredentialRevocationRequest();
        String result = "failed";

        when(agentRestClient.revokeCredential(revocationRequestArgumentCaptor.capture())).thenReturn(result);

        ApplicationRuntimeException applicationRuntimeException = assertThrows(ApplicationRuntimeException.class, () -> {
            mandateService.revokeCredential(revocationJson);
        });

        assertEquals("Exception occurred while revoking credential", applicationRuntimeException.getMessage());

        RevocationRequest revocationRequest = revocationRequestArgumentCaptor.getValue();
        assertTrue(revocationRequest.getPublish());
        assertEquals("12345", revocationRequest.getCredRevId());
        assertEquals("1234", revocationRequest.getRevRegId());
    }

    @Test
    void testRetrieveIssuedMandatesSuccess() {
        List<MandateRequest> mandateRequests = TestDataGenerator.formIssuedMandateRequests();
        when(mandateRequestDao.findAllByStatusEquals("issued")).thenReturn(mandateRequests);

        List<IssuedMandateResponse> issuedMandateResponses = mandateService.retrieveIssuedMandates();
        IssuedMandateResponse singleResponse = issuedMandateResponses.get(0);

        assertEquals(1, issuedMandateResponses.size());
        assertEquals("someSchemaName", singleResponse.getSchemaName());
        assertEquals("someConnectionId", singleResponse.getConnectionId());
        assertEquals("{\"authorization\":\"Books\",\"spending-limit\":\"9001\"}", singleResponse.getRequest());
        assertEquals("someCredRevId", singleResponse.getCredRevId());
        assertEquals("someRevRegId", singleResponse.getRevRegId());
    }

    @Test
    void testRetrieveIssuedMandatesEmpty() {
        when(mandateRequestDao.findAllByStatusEquals("issued")).thenReturn(Collections.emptyList());

        List<IssuedMandateResponse> issuedMandateResponses = mandateService.retrieveIssuedMandates();

        assertEquals(0, issuedMandateResponses.size());
    }

    @Test
    void testCreateConnectionInvitation() throws DataNotFoundException, JsonProcessingException {
        String mpId = "someMpId";
        MandateRequest existingMandate = formMandateRequestSSIComms();
        MandateRequest savedMandate = formMandateRequest();
        savedMandate.setMpId(mpId);
        CreateInvitationResponse mockInvitationResponse = TestDataGenerator.createInvitationResponse("someAlias");
        String invitationJson = "{\"type\":\"did:sov:someDid;spec/connections/1.0/invitation\",\"id\":\"someInvitationId\","
                + "\"serviceEndpoint\":\"http://some-cloudagent-runner:8020\",\"recipientKeys\":[\"someKeyForRecipient1\",\"someKeyForRecipient2\"],"
                + "\"label\":\"someAgent\"}";

        when(mandateRequestDao.findByMpId(mpId)).thenReturn(Optional.of(existingMandate));
        when(agentRestClient.createInvitation(anyString(), anyBoolean(), anyBoolean(), anyBoolean())).thenReturn(mockInvitationResponse);
        when(mandateRequestDao.save(mandateRequestArgumentCaptor.capture())).thenReturn(savedMandate);
        when(objectMapper.writeValueAsString(mockInvitationResponse.getInvitation())).thenReturn(invitationJson);

        String response = mandateService.createConnectionInvitation(mpId);

        MandateRequest mandateRequest = mandateRequestArgumentCaptor.getValue();
        assertEquals("someId", mandateRequest.getId());
        assertEquals("{\"some\":\"json\"}", mandateRequest.getRequestJson());
        assertEquals("someSchema:1.0", mandateRequest.getSchemaName());
        assertEquals(
                "{\"type\":\"did:sov:someDid;spec/connections/1.0/invitation\",\"id\":\"someInvitationId\",\"serviceEndpoint\":\"http://some-cloudagent-runner:8020\",\"recipientKeys\":[\"someKeyForRecipient1\",\"someKeyForRecipient2\"],\"label\":\"someAgent\"}",
                response);
    }

    @Test
    void testCreateConnectionInvitationJsonProcessingException() throws JsonProcessingException {
        String mpId = "someMpId";
        MandateRequest existingMandate = formMandateRequestSSIComms();
        MandateRequest savedMandate = formMandateRequest();
        savedMandate.setMpId(mpId);
        CreateInvitationResponse mockInvitationResponse = TestDataGenerator.createInvitationResponse("someAlias");

        when(mandateRequestDao.findByMpId(mpId)).thenReturn(Optional.of(existingMandate));
        when(agentRestClient.createInvitation(anyString(), anyBoolean(), anyBoolean(), anyBoolean())).thenReturn(mockInvitationResponse);
        when(mandateRequestDao.save(mandateRequestArgumentCaptor.capture())).thenReturn(savedMandate);
        when(objectMapper.writeValueAsString(mockInvitationResponse.getInvitation())).thenThrow(JsonProcessingException.class);

        assertThrows(ApplicationRuntimeException.class, () -> mandateService.createConnectionInvitation(mpId));
        MandateRequest mandateRequest = mandateRequestArgumentCaptor.getValue();
        assertEquals("someId", mandateRequest.getId());
        assertEquals("{\"some\":\"json\"}", mandateRequest.getRequestJson());
        assertEquals("someSchema:1.0", mandateRequest.getSchemaName());
    }

    @Test
    void testCreateConnectionInvitationNotFound() {
        String mpId = "someMpId";

        when(mandateRequestDao.findByMpId(mpId)).thenReturn(Optional.empty());

        assertThrows(DataNotFoundException.class, () -> mandateService.createConnectionInvitation(mpId));
    }

}
