/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.controller;

import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.controller.RestControllerExceptionAdvice.ExceptionResponse;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RestControllerExceptionAdviceTest {

    private RestControllerExceptionAdvice restControllerExceptionAdvice = new RestControllerExceptionAdvice();

    @Test
    void testHandleApplicationRuntimeException() {
        ResponseEntity<ExceptionResponse> responseEntity =
                restControllerExceptionAdvice.handleApplicationRuntimeException(new ApplicationRuntimeException("test"));
        assertEquals(500, responseEntity.getStatusCodeValue());
        assertEquals("test", responseEntity.getBody().getMessage());
    }

    @Test
    void testHandleHttpClientErrorException() {
        ResponseEntity<ExceptionResponse> responseEntity =
                restControllerExceptionAdvice.handleHttpClientErrorException(new HttpClientErrorException(HttpStatus.valueOf(400), "test"));
        assertEquals(400, responseEntity.getStatusCodeValue());
        assertEquals("400 test", responseEntity.getBody().getMessage());
    }

    @Test
    void testHandleDataNotFoundException() {
        ResponseEntity<ExceptionResponse> responseEntity =
                restControllerExceptionAdvice.handleDataNotFoundException(new DataNotFoundException("test"));
        assertEquals(204, responseEntity.getStatusCodeValue());
        assertEquals("test", responseEntity.getBody().getMessage());
    }

}
