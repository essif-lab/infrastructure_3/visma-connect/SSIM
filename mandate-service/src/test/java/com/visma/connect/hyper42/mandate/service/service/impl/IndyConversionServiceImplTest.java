/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefCreatedResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.ProofRequestEntry;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.ProofRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedAttributes;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedAttributesProperty;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedPredicates;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedPredicatesProperty;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.Restriction;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.Restriction_;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.SchemasCreatedResults;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationCreateRequestRequest;
import com.visma.connect.hyper42.mandate.service.service.IndyConversionService;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class IndyConversionServiceImplTest {
    @Autowired
    IndyConversionService indyConversionService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void makePresentationExchangeRequestWithAttributesOK() throws JsonProcessingException {
        long startTime = ZonedDateTime.now().toEpochSecond();
        CredentialDefCreatedResponse credentialDefCreatedResponse =
                objectMapper.readValue("{\"credential_definition_ids\": [\"55MgvkQDB815vtbFky4kEt:3:CL:8:default\"]}", CredentialDefCreatedResponse.class);
        SchemasCreatedResults schemaResponse =
                objectMapper.readValue("{\"schema_ids\": [\"WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0\"]}", SchemasCreatedResults.class);
        ProofRequestEntry entry1 = new ProofRequestEntry().withCredential("authorization");
        ProofRequestEntry entry2 = new ProofRequestEntry().withCredential("spending-limit").withPredicate(">=").withCondition("100");
        List<ProofRequestEntry> entries = new ArrayList<>();
        entries.add(entry1);
        entries.add(entry2);

        V10PresentationCreateRequestRequest request =
                indyConversionService.makePresentationExchangeRequest(credentialDefCreatedResponse, schemaResponse, entries);

        assertFalse(request.getTrace());
        assertNull(request.getComment());
        assertTrue(request.getAdditionalProperties().isEmpty());

        ProofRequest proofRequest = request.getProofRequest();
        assertEquals("mandate proof", proofRequest.getName());
        assertNotNull(proofRequest.getNonce());
        assertEquals(10, proofRequest.getNonce().length());
        assertTrue(Long.valueOf(proofRequest.getNonce()) > 999999999L);
        assertRequestedAttributes(request.getProofRequest().getRequestedAttributes());
        assertRequestedPredicates(request.getProofRequest().getRequestedPredicates());

        assertNotNull(proofRequest.getNonRevoked());
        long endTime = ZonedDateTime.now().toEpochSecond();

        assertTrue(proofRequest.getNonRevoked().getTo() >= startTime);
        assertTrue(proofRequest.getNonRevoked().getTo() <= endTime);
        assertNull(proofRequest.getNonRevoked().getFrom());
    }

    @Test
    void makePresentationExchangeRequestWithoutAttributesOK() throws JsonProcessingException {
        CredentialDefCreatedResponse credentialDefCreatedResponse =
                objectMapper.readValue("{\"credential_definition_ids\": [\"55MgvkQDB815vtbFky4kEt:3:CL:8:default\"]}", CredentialDefCreatedResponse.class);
        SchemasCreatedResults schemaResponse =
                objectMapper.readValue("{\"schema_ids\": [\"WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0\"]}", SchemasCreatedResults.class);
        ProofRequestEntry entry1 = new ProofRequestEntry().withCredential("spending-limit").withPredicate(">=").withCondition("100");
        List<ProofRequestEntry> entries = new ArrayList<>();
        entries.add(entry1);

        V10PresentationCreateRequestRequest request =
                indyConversionService.makePresentationExchangeRequest(credentialDefCreatedResponse, schemaResponse, entries);

        assertFalse(request.getTrace());
        assertNull(request.getComment());
        assertTrue(request.getAdditionalProperties().isEmpty());

        ProofRequest proofRequest = request.getProofRequest();
        assertEquals("mandate proof", proofRequest.getName());
        assertNotNull(proofRequest.getNonce());
        assertEquals(10, proofRequest.getNonce().length());
        assertTrue(Long.valueOf(proofRequest.getNonce()) > 999999999L);
        assertTrue(request.getProofRequest().getRequestedAttributes().getAdditionalProperties().isEmpty());
        assertRequestedPredicates(request.getProofRequest().getRequestedPredicates());
    }

    private void assertRequestedPredicates(RequestedPredicates requestedPredicates) {
        Map<String, RequestedPredicatesProperty> additionalProperties = requestedPredicates.getAdditionalProperties();
        assertEquals(1, additionalProperties.size());
        Set<Map.Entry<String, RequestedPredicatesProperty>> entrySet = additionalProperties.entrySet();
        Map.Entry<String, RequestedPredicatesProperty> entry = entrySet.iterator().next();
        assertNotNull(entry.getKey());
        RequestedPredicatesProperty value = entry.getValue();
        assertEquals("spending-limit", value.getName());
        assertEquals(">=", value.getPType().toString());
        assertEquals(Long.valueOf(100), value.getPValue());
        List<Restriction_> restrictions = value.getRestrictions();
        assertEquals(1, restrictions.size());
        assertEquals("55MgvkQDB815vtbFky4kEt:3:CL:8:default", restrictions.get(0).getCredDefId());
        assertEquals("WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0", restrictions.get(0).getSchemaId());
    }

    private void assertRequestedAttributes(RequestedAttributes requestedAttributes) {
        Map<String, RequestedAttributesProperty> additionalProperties = requestedAttributes.getAdditionalProperties();
        assertEquals(1, additionalProperties.size());
        Set<String> keySetRequested = additionalProperties.keySet();
        Iterator<String> iterator = keySetRequested.iterator();
        String key1 = iterator.next();
        assertNotNull(UUID.fromString(key1));
        RequestedAttributesProperty property1 = additionalProperties.get(key1);
        assertRequestedAttributeProperties(property1);
    }

    private void assertRequestedAttributeProperties(RequestedAttributesProperty property1) {
        assertNotNull(property1.getNames());
        assertEquals(1, property1.getNames().size());
        assertEquals("authorization", property1.getNames().get(0));
        List<Restriction> restrictions = property1.getRestrictions();
        assertEquals(1, restrictions.size());
        Map<String, String> additionalProperties = restrictions.get(0).getAdditionalProperties();
        assertEquals(2, additionalProperties.size());
        assertEquals("55MgvkQDB815vtbFky4kEt:3:CL:8:default", additionalProperties.get("cred_def_id"));
        assertEquals("WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0", additionalProperties.get("schema_id"));
    }
}
