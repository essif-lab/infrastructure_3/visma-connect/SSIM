/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.service.TestDataGenerator;
import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.model.generated.RevokeCredentialRequest;
import com.visma.connect.hyper42.mandate.service.service.MandateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.HttpClientErrorException;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers = MandateServiceController.class)
class MandateServiceControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MandateService mandateService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testCreateNewMandateRequestSuccess() throws Exception {
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        String schemaName = TestDataGenerator.createSchemaName();
        boolean ssicommsEnabled = false;
        String qrCodeUrl = "someBase64String";

        when(mandateService.saveMandateRequest(alias, requestJson, schemaName, ssicommsEnabled)).thenReturn(qrCodeUrl);

        this.mockMvc.perform(post("/mandates/" + schemaName)
                .queryParam("alias", alias)
                .queryParam("ssicommsEnabled", Boolean.toString(ssicommsEnabled))
                .header("Origin", "www.example.com").content(requestJson))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(qrCodeUrl));
    }

    @Test
    void testCreateNewMandateRequestClientError() throws Exception {
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        String schemaName = TestDataGenerator.createSchemaName();
        boolean ssicommsEnabled = false;

        when(mandateService.saveMandateRequest(alias, requestJson, schemaName, ssicommsEnabled))
                .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));

        this.mockMvc.perform(post("/mandates/" + schemaName + "?alias=" + alias)
                .header("Origin", "www.example.com").content(requestJson))
                .andExpect(status().is4xxClientError());
    }

    @Test
    void testCreateNewMandateRequestInternalServerError() throws Exception {
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        String schemaName = TestDataGenerator.createSchemaName();
        boolean ssicommsEnabled = false;

        when(mandateService.saveMandateRequest(alias, requestJson, schemaName, ssicommsEnabled))
                .thenThrow(new ApplicationRuntimeException("Could not create mandate request data"));

        this.mockMvc.perform(post("/mandates/" + schemaName)
                .queryParam("alias", alias)
                .queryParam("ssicommsEnabled", Boolean.toString(ssicommsEnabled))
                .header("Origin", "www.example.com").content(requestJson))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void testProcessConnectionRequestSuccess() throws Exception {
        String responseJson = TestDataGenerator.createMandateRequestJsonString();
        String connectionId = "someConnectionId";

        when(mandateService.acceptConnectionRequest(connectionId)).thenReturn(responseJson);

        this.mockMvc.perform(get("/mandates/" + connectionId + "?state=request")
                .header("Origin", "www.example.com"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(responseJson));
    }

    @Test
    void testProcessConnectionSuccess() throws Exception {
        String responseJson = TestDataGenerator.createMandateRequestJsonString();
        String connectionId = "someConnectionId";

        when(mandateService.createMandateRequest(connectionId)).thenReturn(responseJson);

        this.mockMvc.perform(get("/mandates/" + connectionId + "?state=active")
                .header("Origin", "www.example.com"))
                .andExpect(status().is2xxSuccessful());
        // .andExpect(content().string(responseJson));
    }

    @Test
    void testProcessConnectionForSSICommsMandate() throws Exception {
        String responseJson = TestDataGenerator.createMandateRequestJsonString();
        String mpId = "someMpId";

        when(mandateService.createConnectionInvitation(mpId)).thenReturn(responseJson);

        this.mockMvc.perform(get("/mandates/" + mpId + "?state=ssicomms")
                .header("Origin", "www.example.com"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    void testProcessConnectionNoData() throws Exception {
        String connectionId = "someConnectionId";

        when(mandateService.createMandateRequest(connectionId)).thenThrow(new DataNotFoundException("Connection Id someConnectionId doesnt exists"));

        this.mockMvc.perform(get("/mandates/" + connectionId + "?state=active")
                .header("Origin", "www.example.com"))
                .andExpect(status().is2xxSuccessful());
        // .andExpect(status().isNoContent());
    }

    @Test
    void testRevokeCredentialSuccess() throws Exception {
        RevokeCredentialRequest requestJson = TestDataGenerator.createCredentialRevocationRequest();
        String response = "success";

        when(mandateService.revokeCredential(requestJson)).thenReturn(response);

        this.mockMvc.perform(post("/mandates/revoke")
                .header("Origin", "www.example.com")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(requestJson)))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(response));
    }
}
