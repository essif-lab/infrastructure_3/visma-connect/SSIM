/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.rest.impl;

import com.visma.connect.hyper42.mandate.service.TestDataGenerator;
import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.model.generated.Attribute;
import com.visma.connect.hyper42.mandate.service.model.generated.AuthorizationSchema;
import com.visma.connect.hyper42.mandate.service.model.generated.ConnectionAcceptRequestResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CreateInvitationResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefCreatedResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefinitionRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialProposal;
import com.visma.connect.hyper42.mandate.service.model.generated.DidResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.Result;
import com.visma.connect.hyper42.mandate.service.model.generated.RevocationRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.Schema;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.DIDList;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PingRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PingRequestResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.SchemasCreatedResults;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationCreateRequestRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationExchange;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.Arrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CloudAgentRestClientImplTest {

    @Mock
    private static RestTemplate restTemplate;
    @InjectMocks
    private CloudAgentRestClientImpl agentRestClient;
    @Captor
    private ArgumentCaptor<URI> uriCaptor;

    @Captor
    private ArgumentCaptor<Object> requestBodyCaptor;

    @Captor
    private ArgumentCaptor<IssueCredentialSendRequest> issueCredentialSendRequestArgumentCaptor;

    @Captor
    private ArgumentCaptor<PingRequest> pingRequestCaptor;

    @Captor
    private ArgumentCaptor<V10PresentationCreateRequestRequest> v10PresentationCreateRequestRequestCaptor;

    @Captor
    private ArgumentCaptor<RevocationRequest> revocationRequestArgumentCaptor;

    @Captor
    private ArgumentCaptor<AuthorizationSchema> authorizationSchemaArgumentCaptor;

    @Captor
    private ArgumentCaptor<CredentialDefinitionRequest> credentialDefinitionRequestArgumentCaptor;

    @BeforeEach
    public void setup() {
        Field field = ReflectionUtils.findField(CloudAgentRestClientImpl.class, "agentUrl");
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, agentRestClient, "http://myserver:8080");
    }

    @Test
    void createInvitationOK() {
        String alias = "someone";
        boolean isAutoAccept = true;
        boolean isMultiUse = false;
        boolean isPublic = false;
        CreateInvitationResponse mockResponse = TestDataGenerator.createInvitationResponse(alias);

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(ResponseEntity.ok(mockResponse));

        CreateInvitationResponse response = agentRestClient.createInvitation(alias, isAutoAccept, isMultiUse, isPublic);
        Assertions.assertEquals(mockResponse, response);
        Assertions.assertNull(requestBodyCaptor.getValue());
        URI uri = uriCaptor.getValue();
        assertTrue(uri.getPath().contains("/connections/create-invitation"));
        assertTrue(uri.getQuery().contains("alias=" + alias));
        assertTrue(uri.getQuery().contains("auto_accept=" + isAutoAccept));
        assertTrue(uri.getQuery().contains("multi_use=" + isMultiUse));
        assertTrue(uri.getQuery().contains("public=" + isPublic));
    }

    @Test
    void testIssueCredentialSend() {
        when(restTemplate.postForEntity(uriCaptor.capture(), issueCredentialSendRequestArgumentCaptor.capture(), any(Class.class)))
                .thenReturn(ResponseEntity.ok(formIssueCredentialSendResponse()));
        IssueCredentialSendResponse response = agentRestClient.issueCredentialSend(getIssueCredentialSendRequest());
        URI uri = uriCaptor.getValue();
        assertTrue(uri.getPath().contains("issue-credential/send"));
        assertEquals("WgWxqztrNooG92RXvxSTWv:3:CL:20:tag", response.getCredentialDefinitionId());
        assertEquals("WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0", response.getSchemaId());
        assertEquals("connectionToCoen", response.getConnectionId());
    }

    @Test
    void testIssueCredentialSendRestClientException() {
        when(restTemplate.postForEntity(uriCaptor.capture(), issueCredentialSendRequestArgumentCaptor.capture(), any(Class.class)))
                .thenThrow(new RestClientException("Something went wrong while issuing the credential"));
        IssueCredentialSendRequest request = getIssueCredentialSendRequest();
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.issueCredentialSend(request));
        URI uri = uriCaptor.getValue();
        assertTrue(uri.getPath().contains("issue-credential/send"));
    }

    @Test
    void testIssueCredentialSendException() {
        when(restTemplate.postForEntity(uriCaptor.capture(), issueCredentialSendRequestArgumentCaptor.capture(), any(Class.class)))
                .thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
        IssueCredentialSendRequest request = getIssueCredentialSendRequest();
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.issueCredentialSend(request));
    }

    @Test
    void testGetSchema() {
        String schemaId = "someSchemaId";
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenReturn(ResponseEntity.ok(formSchemaResponse()));
        SchemaResponse response = agentRestClient.getSchema(schemaId);
        URI uri = uriCaptor.getValue();
        assertTrue(uri.getPath().contains("/schemas/someSchemaId"));
        assertEquals(formSchemaResponse(), response);
    }

    @Test
    void testGetSchemaRestClientException() {
        String schemaId = "someSchemaId";
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class)))
                .thenThrow(new RestClientException("Something went wrong while getting the schema"));
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.getSchema(schemaId));
    }

    @Test
    void testGetSchemaException() {
        String schemaId = "someSchemaId";
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.getSchema(schemaId));
    }

    @Test
    void testGetCredentialDefinitionCreated() {
        String schemaName = "schemaName";
        String schemaVersion = "1.0";
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenReturn(ResponseEntity.ok(formCredentialDefCreatedResponse()));

        CredentialDefCreatedResponse response = agentRestClient.getCredentialDefinitionCreated(schemaName, schemaVersion);

        URI uri = uriCaptor.getValue();
        assertTrue(uri.getPath().contains("/credential-definitions/created"));
        assertTrue(uri.getQuery().contains("schema_name=schemaName"));
        assertTrue(uri.getQuery().contains("schema_version=1.0"));
        assertEquals(formCredentialDefCreatedResponse(), response);
    }

    @Test
    void testGetCredentialDefinitionRestClientException() {
        String schemaName = "schemaName";
        String schemaVersion = "1.0";
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class)))
                .thenThrow(new RestClientException("Something went wrong while getting the schema"));
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.getCredentialDefinitionCreated(schemaName, schemaVersion));
    }

    @Test
    void testGetCredentialDefinitionException() {
        String schemaName = "schemaName";
        String schemaVersion = "1.0";
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.getCredentialDefinitionCreated(schemaName, schemaVersion));
    }

    @Test
    void testGetMandateDetails() {
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenReturn(ResponseEntity.ok(formDidResponse()));
        DidResponse response = agentRestClient.getPublicDidDetails();
        URI uri = uriCaptor.getValue();
        assertTrue(uri.getPath().contains("wallet/did/public"));
        assertEquals(formDidResponse(), response);
    }

    @Test
    void testGetMandateDetailsException() {
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.getPublicDidDetails());
    }

    @Test
    void testGetMandateDetailsRestClientException() {
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class)))
                .thenThrow(new RestClientException("Something went wrong while getting the did details"));
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.getPublicDidDetails());
    }

    @Test
    void testGetSchemaCreated() {
        String schemaName = "schemaName";
        String schemaVersion = "1.0";
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenReturn(ResponseEntity.ok(formSchemaCreatedResponse()));

        SchemasCreatedResults response = agentRestClient.getSchemaCreated(schemaName, schemaVersion);

        URI uri = uriCaptor.getValue();
        assertTrue(uri.getPath().contains("/schemas/created"));
        assertTrue(uri.getQuery().contains("schema_name=schemaName"));
        assertTrue(uri.getQuery().contains("schema_version=1.0"));
        assertEquals(formSchemaCreatedResponse(), response);
    }

    @Test
    void testGetSchemaCreatedRestClientException() {
        String schemaName = "schemaName";
        String schemaVersion = "1.0";
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class)))
                .thenThrow(new RestClientException("Something went wrong while getting the schema created"));
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.getSchemaCreated(schemaName, schemaVersion));
    }

    @Test
    void testGetSchemaCreatedException() {
        String schemaName = "schemaName";
        String schemaVersion = "1.0";
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.getSchemaCreated(schemaName, schemaVersion));
    }

    @Test
    void testAcceptConnectionRequest() {
        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), any(Class.class)))
                .thenReturn(ResponseEntity.ok(new ConnectionAcceptRequestResponse().withConnectionId("abc")));

        ConnectionAcceptRequestResponse result = agentRestClient.acceptConnectionRequest("ab132qer132");
        assertEquals("abc", result.getConnectionId());
        assertEquals("http://myserver:8080/connections/ab132qer132/accept-request", uriCaptor.getValue().toString());
    }

    @Test
    void testAcceptConnectionRequestWithException() {
        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), any(Class.class)))
                .thenThrow(new RestClientException("oeps an error"));

        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.acceptConnectionRequest("ab132qer132"));
        assertEquals("http://myserver:8080/connections/ab132qer132/accept-request", uriCaptor.getValue().toString());
    }

    @Test
    void testSendTrustPing() {
        when(restTemplate.postForEntity(uriCaptor.capture(), pingRequestCaptor.capture(), any(Class.class)))
                .thenReturn(ResponseEntity.ok(new PingRequestResponse().withThreadId("draadje")));

        PingRequestResponse result = agentRestClient.sendTrustPing("qdta564ad", new PingRequest().withComment("hello"));
        assertEquals("draadje", result.getThreadId());
        assertEquals("hello", pingRequestCaptor.getValue().getComment());
        assertEquals("http://myserver:8080/connections/qdta564ad/send-ping", uriCaptor.getValue().toString());
    }

    @Test
    void testSendTrustPingWithException() {
        when(restTemplate.postForEntity(uriCaptor.capture(), pingRequestCaptor.capture(), any(Class.class)))
                .thenThrow(new RestClientException("oepsie"));

        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.sendTrustPing("qdta564ad", new PingRequest().withComment("hello")));
        assertEquals("hello", pingRequestCaptor.getValue().getComment());
        assertEquals("http://myserver:8080/connections/qdta564ad/send-ping", uriCaptor.getValue().toString());
    }

    @Test
    void testRetrieveWalletDids() {
        DIDList didList = new DIDList();
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenReturn(ResponseEntity.ok(didList));

        DIDList retrieveWalletDids = agentRestClient.retrieveWalletDids();

        assertEquals("http://myserver:8080/wallet/did?posture=wallet_only", uriCaptor.getValue().toString());
        assertEquals(didList, retrieveWalletDids);
    }

    @Test
    void testRetrieveWalletDidsException() {
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenThrow(new RestClientException("oepsie"));

        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.retrieveWalletDids());

        assertEquals("http://myserver:8080/wallet/did?posture=wallet_only", uriCaptor.getValue().toString());
    }

    @Test
    void testCreateProofRequest() {
        V10PresentationExchange v10PresentationExchange = new V10PresentationExchange();
        when(restTemplate.postForEntity(uriCaptor.capture(), v10PresentationCreateRequestRequestCaptor.capture(), any(Class.class)))
                .thenReturn(ResponseEntity.ok(v10PresentationExchange));

        V10PresentationCreateRequestRequest request = new V10PresentationCreateRequestRequest();
        V10PresentationExchange result = agentRestClient.createProofRequest(request);

        assertEquals(v10PresentationExchange, result);
        assertEquals("http://myserver:8080/present-proof/create-request", uriCaptor.getValue().toString());
        assertEquals(request, v10PresentationCreateRequestRequestCaptor.getValue());
    }

    @Test
    void testCreateProofRequestException() {
        when(restTemplate.postForEntity(uriCaptor.capture(), v10PresentationCreateRequestRequestCaptor.capture(), any(Class.class)))
                .thenThrow(new RestClientException("oepsie"));

        V10PresentationCreateRequestRequest request = new V10PresentationCreateRequestRequest();
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.createProofRequest(request));

        assertEquals("http://myserver:8080/present-proof/create-request", uriCaptor.getValue().toString());
        assertEquals(request, v10PresentationCreateRequestRequestCaptor.getValue());
    }

    private IssueCredentialSendResponse formIssueCredentialSendResponse() {
        IssueCredentialSendResponse issueCredentialSendResponse = new IssueCredentialSendResponse();
        issueCredentialSendResponse.setAutoIssue(false);
        issueCredentialSendResponse.setAutoRemove(false);
        issueCredentialSendResponse.setAutoOffer(false);
        issueCredentialSendResponse.setConnectionId("connectionToCoen");
        issueCredentialSendResponse.setCredentialDefinitionId("WgWxqztrNooG92RXvxSTWv:3:CL:20:tag");
        issueCredentialSendResponse.setRole("issuer");
        issueCredentialSendResponse.setSchemaId("WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0");
        return issueCredentialSendResponse;
    }

    private IssueCredentialSendRequest getIssueCredentialSendRequest() {
        CredentialProposal credentialProposal = new CredentialProposal();
        credentialProposal.setAttributes(Arrays.asList(new Attribute().withName("signing").withValue("tsgjnkk")));
        IssueCredentialSendRequest issueCredentialSendRequest = new IssueCredentialSendRequest();
        issueCredentialSendRequest.setAutoRemove(true);
        issueCredentialSendRequest.setComment("");
        issueCredentialSendRequest.setIssuerDid("issuerDid");
        issueCredentialSendRequest.setConnectionId("connectionToCoen");
        issueCredentialSendRequest.setCredDefId("WgWxqztrNooG92RXvxSTWv:3:CL:20:tag");
        issueCredentialSendRequest.setSchemaIssuerDid("issuerDid");
        issueCredentialSendRequest.setSchemaName("schema_name");
        issueCredentialSendRequest.setSchemaVersion("1.0");
        issueCredentialSendRequest.setSchemaId("WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0");
        issueCredentialSendRequest.setCredentialProposal(credentialProposal);
        issueCredentialSendRequest.setTrace(false);
        return issueCredentialSendRequest;
    }

    private SchemaResponse formSchemaResponse() {
        SchemaResponse schemaResponse = new SchemaResponse();
        Schema schema = new Schema();
        schema.setAttrNames(Arrays.asList("signature", "representativeIdentifier"));
        schema.setVersion("1.1");
        schema.setName("prefs");
        schema.setId("55MgvkQDB815vtbFky4kEt:2:mandate-schema:1.1");
        schema.setVer("1.0");
        schema.setSeqNo(13L);
        schemaResponse.setSchema(schema);
        schemaResponse.setSchemaId("55MgvkQDB815vtbFky4kEt:2:mandate-schema:1.1");
        return schemaResponse;
    }

    private CredentialDefCreatedResponse formCredentialDefCreatedResponse() {
        return new CredentialDefCreatedResponse()
                .withCredentialDefinitionIds(Arrays.asList("WgWxqztrNooG92RXvxSTWv:3:CL:20:tag"));
    }

    private DidResponse formDidResponse() {
        DidResponse didResponse = new DidResponse();
        Result result = new Result();
        result.setDid("WgWxqztrNooG92RXvxSTWv");
        result.setPosture("wallet_only");
        result.setVerkey("H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV");
        didResponse.setResult(result);
        return didResponse;
    }

    private SchemasCreatedResults formSchemaCreatedResponse() {
        return new SchemasCreatedResults()
                .withSchemaIds(Arrays.asList("55MgvkQDB815vtbFky4kEt:2:mandate-schema:1.1"));
    }

    @Test
    void testGetSchemasCreated() {
        SchemasCreatedResults schemasCreated = new SchemasCreatedResults().withSchemaIds(Arrays.asList("schema1", "schema2"));
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenReturn(ResponseEntity.ok(schemasCreated));

        SchemasCreatedResults result = agentRestClient.getSchemasCreated();

        assertEquals(2, result.getSchemaIds().size());
        assertTrue(result.getSchemaIds().contains("schema1"));
        assertTrue(result.getSchemaIds().contains("schema2"));
    }

    @Test
    void testGetSchemasCreatedEmpty() {
        SchemasCreatedResults schemasCreated = new SchemasCreatedResults();
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenReturn(ResponseEntity.ok(schemasCreated));

        SchemasCreatedResults result = agentRestClient.getSchemasCreated();

        assertEquals(0, result.getSchemaIds().size());
    }

    @Test
    void testGetSchemasCreatedException() {
        SchemasCreatedResults schemasCreated = new SchemasCreatedResults();
        when(restTemplate.getForEntity(uriCaptor.capture(), any(Class.class))).thenThrow(new RestClientException("oepsie"));

        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.getSchemasCreated());
    }

    @Test
    void testRevokeCredentialSuccess() {
        RevocationRequest revocationRequest = new RevocationRequest()
                .withPublish(true)
                .withRevRegId("1234")
                .withCredRevId("12345");
        when(restTemplate.postForEntity(uriCaptor.capture(), revocationRequestArgumentCaptor.capture(), any(Class.class)))
                .thenReturn(ResponseEntity.ok("{}"));

        String result = agentRestClient.revokeCredential(revocationRequest);

        RevocationRequest requestActual = revocationRequestArgumentCaptor.getValue();
        assertEquals("success", result);
        assertEquals(revocationRequest.getRevRegId(), requestActual.getRevRegId());
        assertEquals(revocationRequest.getCredRevId(), requestActual.getCredRevId());
        assertEquals(revocationRequest.getPublish(), requestActual.getPublish());
        assertEquals("http://myserver:8080/revocation/revoke", uriCaptor.getValue().toString());
    }

    @Test
    void testRevokeCredentialNok() {
        RevocationRequest revocationRequest = new RevocationRequest()
                .withPublish(true)
                .withRevRegId("1234")
                .withCredRevId("12345");
        when(restTemplate.postForEntity(uriCaptor.capture(), revocationRequestArgumentCaptor.capture(), any(Class.class)))
                .thenReturn(ResponseEntity.badRequest().build());

        ApplicationRuntimeException applicationRuntimeException = assertThrows(ApplicationRuntimeException.class,
                () -> agentRestClient.revokeCredential(revocationRequest));

        assertEquals("Credential revocation finished unexpected", applicationRuntimeException.getMessage());

        RevocationRequest requestActual = revocationRequestArgumentCaptor.getValue();
        assertEquals(revocationRequest.getRevRegId(), requestActual.getRevRegId());
        assertEquals(revocationRequest.getCredRevId(), requestActual.getCredRevId());
        assertEquals(revocationRequest.getPublish(), requestActual.getPublish());
        assertEquals("http://myserver:8080/revocation/revoke", uriCaptor.getValue().toString());
    }

    @Test
    void testRevokeCredentialRestClientException() {
        RevocationRequest revocationRequest = new RevocationRequest()
                .withPublish(true)
                .withRevRegId("1234")
                .withCredRevId("12345");
        when(restTemplate.postForEntity(uriCaptor.capture(), revocationRequestArgumentCaptor.capture(), any(Class.class)))
                .thenThrow(new RestClientException("Something went wrong while revoking the credential"));

        ApplicationRuntimeException applicationRuntimeException = assertThrows(ApplicationRuntimeException.class,
                () -> agentRestClient.revokeCredential(revocationRequest));

        assertTrue(applicationRuntimeException.getCause().getMessage().startsWith("Something went wrong while revoking the credential"));

        RevocationRequest requestActual = revocationRequestArgumentCaptor.getValue();
        assertEquals(revocationRequest.getRevRegId(), requestActual.getRevRegId());
        assertEquals(revocationRequest.getCredRevId(), requestActual.getCredRevId());
        assertEquals(revocationRequest.getPublish(), requestActual.getPublish());
        assertEquals("http://myserver:8080/revocation/revoke", uriCaptor.getValue().toString());
    }

    @Test
    void createSchemaTest() {
        AuthorizationSchema authorizationSchema = TestDataGenerator.createAuthorizationSchema();
        SchemaResponse schemaResponse = TestDataGenerator.createSchemaResponse();

        when(restTemplate.postForEntity(any(), authorizationSchemaArgumentCaptor.capture(), any())).thenReturn(ResponseEntity.ok().body(schemaResponse));

        SchemaResponse response = agentRestClient.createSchema(authorizationSchema);
        assertEquals("someSchemaId", response.getSchemaId());
        assertEquals("someId", response.getSchema().getId());
        assertEquals("someName", response.getSchema().getName());
        assertEquals("1.0", response.getSchema().getVer());
        assertEquals("1.0", response.getSchema().getVersion());
        assertEquals(10L, response.getSchema().getSeqNo());
        assertEquals("signature", response.getSchema().getAttrNames().get(0));
    }

    @Test
    void testCreateSchemaNotOk() {
        AuthorizationSchema authorizationSchema = TestDataGenerator.createAuthorizationSchema();
        when(restTemplate.postForEntity(any(), authorizationSchemaArgumentCaptor.capture(), any())).thenReturn(ResponseEntity.badRequest().build());
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.createSchema(authorizationSchema));
    }

    @Test
    void testCreateSchemaHttpClientErrorException() {
        AuthorizationSchema authorizationSchema = TestDataGenerator.createAuthorizationSchema();
        when(restTemplate.postForEntity(any(), authorizationSchemaArgumentCaptor.capture(), any()))
                .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Schema already exists on ledger, but attributes do not match"));
        assertThrows(HttpClientErrorException.class, () -> agentRestClient.createSchema(authorizationSchema));
    }

    @Test
    void testCreateSchemaRestClientException() {
        AuthorizationSchema authorizationSchema = TestDataGenerator.createAuthorizationSchema();
        when(restTemplate.postForEntity(any(), authorizationSchemaArgumentCaptor.capture(), any()))
                .thenThrow(new RestClientException("Creating schema finished unexpected"));
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.createSchema(authorizationSchema));
    }

    @Test
    void testCreateCredentialDefinition() {
        CredentialDefinitionRequest credentialDefinitionRequest = TestDataGenerator.credentialDefinitionRequest();
        CredentialDefinitionResponse credentialDefinitionResponse = TestDataGenerator.credentialDefinitionResponse();

        when(restTemplate.postForEntity(anyString(), credentialDefinitionRequestArgumentCaptor.capture(), any()))
                .thenReturn(ResponseEntity.ok().body(credentialDefinitionResponse));

        CredentialDefinitionResponse response = agentRestClient.createCredentialDefinition(credentialDefinitionRequest);
        assertEquals("someDefId", response.getCredentialDefinitionId());
    }

    @Test
    void testCreateCredDefNotOk() {
        CredentialDefinitionRequest credentialDefinitionRequest = TestDataGenerator.credentialDefinitionRequest();
        when(restTemplate.postForEntity(anyString(), credentialDefinitionRequestArgumentCaptor.capture(), any()))
                .thenReturn(ResponseEntity.badRequest().build());
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.createCredentialDefinition(credentialDefinitionRequest));
    }

    @Test
    void testCreateDefRestClientException() {
        CredentialDefinitionRequest credentialDefinitionRequest = TestDataGenerator.credentialDefinitionRequest();
        when(restTemplate.postForEntity(anyString(), credentialDefinitionRequestArgumentCaptor.capture(), any()))
                .thenThrow(new RestClientException("Creating credential definition finished unexpected"));
        assertThrows(ApplicationRuntimeException.class, () -> agentRestClient.createCredentialDefinition(credentialDefinitionRequest));
    }
}