/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.service.TestDataGenerator;
import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.dao.SchemaDao;
import com.visma.connect.hyper42.mandate.service.dao.SchemaDefinitionDao;
import com.visma.connect.hyper42.mandate.service.mapper.SchemaMapper;
import com.visma.connect.hyper42.mandate.service.model.SchemaDefinition;
import com.visma.connect.hyper42.mandate.service.model.generated.Attribute_;
import com.visma.connect.hyper42.mandate.service.model.generated.AuthorizationSchema;
import com.visma.connect.hyper42.mandate.service.model.generated.AvailableSchemasResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.Condition;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefinitionRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.Predicate;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.Schema;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaResponse;
import com.visma.connect.hyper42.mandate.service.rest.CloudAgentRestClient;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SchemaServiceImplTest {
    @Mock
    private SchemaDao schemaDao;

    @Mock
    private SchemaMapper schemaMapper;

    @Mock
    private CloudAgentRestClient agentRestClient;

    @Mock
    private ObjectMapper mapper;

    @Mock
    private SchemaDefinitionDao schemaDefinitionDao;

    @InjectMocks
    private SchemaServiceImpl schemaServiceImpl;

    @Captor
    private ArgumentCaptor<String> schemaNameCaptor;

    @Captor
    private ArgumentCaptor<String> schemaIdCaptor;

    @Captor
    private ArgumentCaptor<String> schemaVersionCaptor;

    @Captor
    private ArgumentCaptor<String> contentCaptor;

    @Captor
    private ArgumentCaptor<Schema> schemaArgumentCaptor;

    @Captor
    private ArgumentCaptor<SchemaDefinition> schemaDefinitionArgumentCaptor;

    @Captor
    private ArgumentCaptor<AddSchemaRequest> addSchemaRequestArgumentCaptor;

    @Captor
    private ArgumentCaptor<AuthorizationSchema> authorizationSchemaArgumentCaptor;

    @Captor
    private ArgumentCaptor<CredentialDefinitionRequest> credentialDefinitionRequestArgumentCaptor;

    @Test
    void testGetAllCreatedSchema() {
        Schema schema1 = new Schema("someId", "someSchemaId", "someName", "someTitle", "someVersion",
                new ArrayList<>(), new ArrayList<>(), true);
        Schema schema2 = new Schema("someId2", "someSchemaId2", "someName2", "someTitle2", "someVersion2",
                new ArrayList<>(), new ArrayList<>(), false);
        when(schemaDao.findAll()).thenReturn(List.of(schema1, schema2));

        List<AvailableSchemasResponse> schemasTitleResponse = schemaServiceImpl.getAllCreatedSchema();

        assertEquals(2, schemasTitleResponse.size());
        assertEquals("someSchemaId", schemasTitleResponse.get(0).getSchemaId());
        assertEquals("someTitle", schemasTitleResponse.get(0).getTitle());
        assertEquals("someSchemaId2", schemasTitleResponse.get(1).getSchemaId());
        assertEquals("someTitle2", schemasTitleResponse.get(1).getTitle());
    }

    @Test
    void testGetAllCreatedSchemaEmpty() {
        when(schemaDao.findAll()).thenReturn(new ArrayList<>());

        List<AvailableSchemasResponse> schemas = schemaServiceImpl.getAllCreatedSchema();

        assertEquals(0, schemas.size());
    }

    @Test
    void testGetAllUserSchema() {
        Schema schema1 = new Schema("someId", "someSchemaId", "someName", "someTitle", "someVersion",
                new ArrayList<>(), new ArrayList<>(), true);
        Schema schema2 = new Schema("someId2", "someSchemaId2", "mandate-login-schema", "Login Schema", "someVersion2",
                new ArrayList<>(), new ArrayList<>(), false);
        when(schemaDao.findAll()).thenReturn(List.of(schema1, schema2));

        List<AvailableSchemasResponse> schemasTitleResponse = schemaServiceImpl.getAllUserSchema();

        assertEquals(1, schemasTitleResponse.size());
        assertEquals("someSchemaId", schemasTitleResponse.get(0).getSchemaId());
        assertEquals("someTitle", schemasTitleResponse.get(0).getTitle());
        assertEquals("someName", schemasTitleResponse.get(0).getName());
    }

    @Test
    void testGetSchemaDefinition() {
        String schemaId = "someSchemaId";
        Schema schema = TestDataGenerator.getSchema();
        SchemaDefinitionResponse response = TestDataGenerator.getSchemaDefinitionResponse();

        when(schemaDao.findBySchemaId(schemaId)).thenReturn(Optional.of(schema));
        when(schemaMapper.toSchemaDefinitionResponseDto(schema)).thenReturn(response);

        SchemaDefinitionResponse schemaDefinitionResponse = schemaServiceImpl.getSchemaDefinition(schemaId);
        Attribute_ attribute = schemaDefinitionResponse.getAttributes().get(0);
        Predicate predicate = schemaDefinitionResponse.getPredicates().get(0);
        Condition condition = predicate.getCondition().get(0);

        assertEquals("someSchemaId", schemaDefinitionResponse.getSchemaId());
        assertEquals("someName", schemaDefinitionResponse.getName());
        assertEquals("someTitle", schemaDefinitionResponse.getTitle());
        assertEquals("someVersion", schemaDefinitionResponse.getVersion());
        assertEquals("attr-name", attribute.getName());
        assertEquals("Attr Name", attribute.getTitle());
        assertEquals("text", attribute.getType());
        assertEquals("pred-name", predicate.getName());
        assertEquals("number", predicate.getType());
        assertEquals("Condition Name", condition.getTitle());
        assertEquals(">=", condition.getValue());
        assertFalse(schemaDefinitionResponse.getSsicommsEnabled());
    }

    @Test
    void testGetSchemaDefinitionNull() {
        String schemaId = "someSchemaId";

        when(schemaDao.findBySchemaId(schemaId)).thenReturn(Optional.empty());

        SchemaDefinitionResponse schemaDefinitionResponse = schemaServiceImpl.getSchemaDefinition(schemaId);
        assertNull(schemaDefinitionResponse);
    }

    @Test
    void testAddSchemaSuccess() throws JsonProcessingException {
        AddSchemaRequest addSchemaRequest = TestDataGenerator.getAddSchemaRequest();
        Schema schema = TestDataGenerator.getSchema();
        SchemaResponse schemaResponse = new SchemaResponse().withSchemaId("schemaId");
        CredentialDefinitionResponse credentialDefinitionResponse = new CredentialDefinitionResponse().withCredentialDefinitionId("someDefId");
        String attributes = "{\"attr-name\":\"string\"}";
        JsonNode node = TestDataGenerator.createJsonNode(attributes);
        SchemaDefinition definition = new SchemaDefinition("someId", "someName", "someDefinition");
        Schema savedSchemaFromDao = TestDataGenerator.getSchema();
        savedSchemaFromDao.setSchemaId("testMySchemaId");

        when(schemaDao.findByNameAndVersion(schemaNameCaptor.capture(), schemaVersionCaptor.capture())).thenReturn(Optional.empty());
        when(agentRestClient.createSchema(authorizationSchemaArgumentCaptor.capture())).thenReturn(schemaResponse);
        when(agentRestClient.createCredentialDefinition(credentialDefinitionRequestArgumentCaptor.capture())).thenReturn(credentialDefinitionResponse);
        when(mapper.readTree(contentCaptor.capture())).thenReturn(node);
        when(schemaDefinitionDao.save(schemaDefinitionArgumentCaptor.capture())).thenReturn(definition);
        when(schemaMapper.toSchema(schemaIdCaptor.capture(), addSchemaRequestArgumentCaptor.capture())).thenReturn(schema);
        when(schemaDao.save(schemaArgumentCaptor.capture())).thenReturn(savedSchemaFromDao);

        AddSchemaResponse addSchemaResponse = schemaServiceImpl.addSchema(addSchemaRequest);

        Schema savedSchema = schemaArgumentCaptor.getValue();

        assertEquals("{\"attr-name\":\"string\"}", contentCaptor.getValue());
        assertEquals("schema-name", schemaNameCaptor.getValue());
        assertEquals("1.0", schemaVersionCaptor.getValue());
        assertEquals("schemaId", schemaIdCaptor.getValue());
        assertEquals("schemaId", credentialDefinitionRequestArgumentCaptor.getValue().getSchemaId());
        assertEquals("default", credentialDefinitionRequestArgumentCaptor.getValue().getTag());
        assertEquals(1000L, credentialDefinitionRequestArgumentCaptor.getValue().getRevocationRegistrySize());
        assertTrue(credentialDefinitionRequestArgumentCaptor.getValue().getSupportRevocation());
        assertEquals("schema-name", authorizationSchemaArgumentCaptor.getValue().getSchemaName());
        assertEquals("1.0", authorizationSchemaArgumentCaptor.getValue().getSchemaVersion());
        assertFalse(authorizationSchemaArgumentCaptor.getValue().getAttributes().isEmpty());
        assertEquals("Schema Name", addSchemaResponse.getTitle());
        assertEquals("ok", addSchemaResponse.getStatus());
        assertEquals("testMySchemaId", addSchemaResponse.getSchemaId());
        assertEquals("someName", savedSchema.getName());
        assertEquals("someTitle", savedSchema.getTitle());
        assertEquals("someVersion", savedSchema.getVersion());
        assertEquals("attr-name", savedSchema.getAttributes().get(0).getName());
        assertEquals("Attr Name", savedSchema.getAttributes().get(0).getTitle());
        assertEquals("text", savedSchema.getAttributes().get(0).getType());
        assertEquals("pred-name", savedSchema.getPredicates().get(0).getName());
        assertEquals("number", savedSchema.getPredicates().get(0).getType());
        assertEquals("Condition Name", savedSchema.getPredicates().get(0).getConditions().get(0).getTitle());
        assertEquals(">=", savedSchema.getPredicates().get(0).getConditions().get(0).getValue());
        assertFalse(savedSchema.isSsicommsEnabled());
    }

    @Test
    void testAddSchemaInvalidJsonDefinition() throws JsonProcessingException {
        AddSchemaRequest addSchemaRequest = TestDataGenerator.getAddSchemaRequest();
        SchemaResponse schemaResponse = new SchemaResponse().withSchemaId("schemaId");
        CredentialDefinitionResponse credentialDefinitionResponse = new CredentialDefinitionResponse().withCredentialDefinitionId("someDefId");

        when(schemaDao.findByNameAndVersion(schemaNameCaptor.capture(), schemaVersionCaptor.capture())).thenReturn(Optional.empty());
        when(agentRestClient.createSchema(authorizationSchemaArgumentCaptor.capture())).thenReturn(schemaResponse);
        when(agentRestClient.createCredentialDefinition(credentialDefinitionRequestArgumentCaptor.capture())).thenReturn(credentialDefinitionResponse);
        when(mapper.readTree(contentCaptor.capture())).thenThrow(JsonProcessingException.class);

        assertThrows(ApplicationRuntimeException.class, () -> schemaServiceImpl.addSchema(addSchemaRequest));

        assertEquals("schema-name", schemaNameCaptor.getValue());
        assertEquals("1.0", schemaVersionCaptor.getValue());
        assertEquals("schemaId", credentialDefinitionRequestArgumentCaptor.getValue().getSchemaId());
        assertEquals("default", credentialDefinitionRequestArgumentCaptor.getValue().getTag());
        assertEquals(1000L, credentialDefinitionRequestArgumentCaptor.getValue().getRevocationRegistrySize());
        assertTrue(credentialDefinitionRequestArgumentCaptor.getValue().getSupportRevocation());
        assertEquals("schema-name", authorizationSchemaArgumentCaptor.getValue().getSchemaName());
        assertEquals("1.0", authorizationSchemaArgumentCaptor.getValue().getSchemaVersion());
        assertFalse(authorizationSchemaArgumentCaptor.getValue().getAttributes().isEmpty());
    }

    @Test
    void testAddSchemaAlreadyExist() {
        Schema schema = new Schema("someId", "someSchemaId", "schema-name", "someTitle", "someVersion",
                new ArrayList<>(), new ArrayList<>(), true);

        AddSchemaRequest addSchemaRequest = TestDataGenerator.getAddSchemaRequest();
        when(schemaDao.findByNameAndVersion(addSchemaRequest.getName(), addSchemaRequest.getVersion())).thenReturn(Optional.of(schema));

        AddSchemaResponse addSchemaResponse = schemaServiceImpl.addSchema(addSchemaRequest);

        assertEquals("Schema Name", addSchemaResponse.getTitle());
        assertEquals("already_exists", addSchemaResponse.getStatus());
    }

    @Test
    void testAddSchemaFailed() throws JsonProcessingException {
        AddSchemaRequest addSchemaRequest = TestDataGenerator.getAddSchemaRequest();
        SchemaResponse schemaResponse = new SchemaResponse().withSchemaId("schemaId");
        CredentialDefinitionResponse credentialDefinitionResponse = new CredentialDefinitionResponse().withCredentialDefinitionId("someDefId");
        String attributes = "{\"attribute\":\"string\"}";
        JsonNode node = TestDataGenerator.createJsonNode(attributes);
        SchemaDefinition definition = new SchemaDefinition("someId", "someName", "someDefinition");

        when(schemaDao.findByNameAndVersion(addSchemaRequest.getName(), addSchemaRequest.getVersion())).thenReturn(Optional.empty());
        when(agentRestClient.createSchema(authorizationSchemaArgumentCaptor.capture())).thenReturn(schemaResponse);
        when(agentRestClient.createCredentialDefinition(credentialDefinitionRequestArgumentCaptor.capture())).thenReturn(credentialDefinitionResponse);
        when(mapper.readTree(contentCaptor.capture())).thenReturn(node);
        when(schemaDefinitionDao.save(schemaDefinitionArgumentCaptor.capture())).thenReturn(definition);
        when(schemaDao.save(schemaArgumentCaptor.capture())).thenThrow(IllegalArgumentException.class);

        AddSchemaResponse addSchemaResponse = schemaServiceImpl.addSchema(addSchemaRequest);

        assertEquals("Schema Name", addSchemaResponse.getTitle());
        assertEquals("failed", addSchemaResponse.getStatus());
        assertEquals("schemaId", credentialDefinitionRequestArgumentCaptor.getValue().getSchemaId());
        assertEquals("default", credentialDefinitionRequestArgumentCaptor.getValue().getTag());
        assertEquals(1000L, credentialDefinitionRequestArgumentCaptor.getValue().getRevocationRegistrySize());
        assertTrue(credentialDefinitionRequestArgumentCaptor.getValue().getSupportRevocation());
        assertEquals("schema-name", authorizationSchemaArgumentCaptor.getValue().getSchemaName());
        assertEquals("1.0", authorizationSchemaArgumentCaptor.getValue().getSchemaVersion());
        assertFalse(authorizationSchemaArgumentCaptor.getValue().getAttributes().isEmpty());
    }

    @Test
    void testGetSchemaJsonDefinition() throws DataNotFoundException {
        String schemaName = "someSchemaName";
        SchemaDefinition definition = TestDataGenerator.getSchemaDefinition();

        when(schemaDefinitionDao.findByName(schemaName)).thenReturn(Optional.of(definition));

        String jsonDefinition = schemaServiceImpl.getSchemaJsonDefinition(schemaName);
        assertEquals("someDefinition", jsonDefinition);
    }

    @Test
    void testGetSchemaJsonDefinitionNotFound() {
        String schemaName = "someSchemaName";
        when(schemaDefinitionDao.findByName(schemaName)).thenReturn(Optional.empty());
        assertThrows(DataNotFoundException.class, () -> schemaServiceImpl.getSchemaJsonDefinition(schemaName));
    }

}
