/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.scheduler;

import com.visma.connect.hyper42.mandate.service.service.ProofResultService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.doNothing;

@ExtendWith(MockitoExtension.class)
class ProofRequestScheduledTasksTest {
    @Mock
    private ProofResultService proofResultService;

    @InjectMocks
    private ProofRequestScheduledTasks proofRequestScheduledTasks;

    @Test
    void testScheduledCheckTimeout() {
        doNothing().when(proofResultService).processProofRequestTimeout();

        proofRequestScheduledTasks.scheduledCheckTimeout();
    }
}
