/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.visma.connect.hyper42.mandate.service.TestDataGenerator;
import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.dao.MandateVerifierRequestDao;
import com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefCreatedResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.ProofRequestEntry;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.DIDList;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PresentationRequestDict;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.ProofRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedAttributes;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedAttributesProperty;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedPredicates;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedPredicatesProperty;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.Restriction;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.Restriction_;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.Result;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.Result.Posture;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.SchemasCreatedResults;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationCreateRequestRequest;
import com.visma.connect.hyper42.mandate.service.rest.CloudAgentRestClient;
import com.visma.connect.hyper42.mandate.service.service.IndyConversionService;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.ReflectionUtils;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProofInitServiceImplTest {
    private static final String REQUEST =
            "[ { \"credential\": \"authorization\" }, { \"credential\": \"spending-limit\", \"predicate\": \">=\", \"condition\": \"100\" }]";
    @Mock
    private CloudAgentRestClient agentRestClient;

    @Mock
    private MandateVerifierRequestDao mandateVerifierRequestDao;

    @Mock
    private IndyConversionService indyConversionService;

    private final ObjectMapper objectMapper = new ObjectMapper();

    @InjectMocks
    private ProofInitServiceImpl proofServiceImpl;

    @Captor
    private ArgumentCaptor<String> schemaNameCaptor;

    @Captor
    private ArgumentCaptor<String> schemaVersionCaptor;

    @Captor
    private ArgumentCaptor<V10PresentationCreateRequestRequest> proofRequestCaptor;

    @Captor
    private ArgumentCaptor<String> uuidCaptor;

    @Captor
    private ArgumentCaptor<CredentialDefCreatedResponse> credentialDefCaptor;

    @Captor
    private ArgumentCaptor<SchemasCreatedResults> schemaResponseCaptor;

    @Captor
    private ArgumentCaptor<List<ProofRequestEntry>> proofEntryCaptor;

    @Captor
    private ArgumentCaptor<MandateVerifierRequest> mandateVerifierRequestCaptor;

    @BeforeEach
    public void init() {
        Field field = ReflectionUtils.findField(ProofInitServiceImpl.class, "objectMapper");
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, proofServiceImpl, objectMapper);
        field = ReflectionUtils.findField(ProofInitServiceImpl.class, "agentExternalUrl");
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, proofServiceImpl, "http://myariesagent:8080");
    }

    @Test
    void testRetrieveProofRequestOK() throws JsonProcessingException {
        MandateVerifierRequest mandateVerifierRequest = new MandateVerifierRequest("1", "mandate-signing-basic:1.0",
                new String(Base64.getEncoder().encode(REQUEST.getBytes(Charset.defaultCharset()))), "testComment");
        Mockito.when(mandateVerifierRequestDao.findById(uuidCaptor.capture())).thenReturn(Optional.of(mandateVerifierRequest));

        CredentialDefCreatedResponse credentialDefCreatedResponse =
                objectMapper.readValue("{\"credential_definition_ids\": [\"55MgvkQDB815vtbFky4kEt:3:CL:8:default\"]}", CredentialDefCreatedResponse.class);
        SchemasCreatedResults schemaResponse =
                objectMapper.readValue("{\"schema_ids\": [\"WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0\"]}", SchemasCreatedResults.class);
        Mockito.when(agentRestClient.getCredentialDefinitionCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture()))
                .thenReturn(credentialDefCreatedResponse);
        Mockito.when(agentRestClient.getSchemaCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture()))
                .thenReturn(schemaResponse);

        when(indyConversionService.makePresentationExchangeRequest(credentialDefCaptor.capture(), schemaResponseCaptor.capture(), proofEntryCaptor.capture()))
                .thenReturn(TestDataGenerator.createV10PresentationCreateRequestRequest());

        Mockito.when(mandateVerifierRequestDao.save(mandateVerifierRequestCaptor.capture())).thenReturn(mandateVerifierRequest);

        Mockito.when(agentRestClient.createProofRequest(proofRequestCaptor.capture())).thenReturn(TestDataGenerator.createV10PresentationExchange());

        List<com.visma.connect.hyper42.mandate.service.model.schema.generated.Result> dids =
                Collections.singletonList(new Result()
                        .withDid("walletdid")
                        .withVerkey("walletver")
                        .withPosture(Posture.WALLET_ONLY));
        DIDList didList = new DIDList().withResults(dids);
        when(agentRestClient.retrieveWalletDids()).thenReturn(didList);

        Optional<PresentationRequestDict> resultOptional = proofServiceImpl.retrieveProofRequest("1-2-3-4");

        assertTrue(resultOptional.isPresent());
        PresentationRequestDict result = resultOptional.get();

        assertEquals("1-2-3-4", uuidCaptor.getValue());

        assertEquals("mandate-signing-basic", schemaNameCaptor.getValue());
        assertEquals("1.0", schemaVersionCaptor.getValue());

        MandateVerifierRequest verifierRequest = mandateVerifierRequestCaptor.getValue();
        assertEquals(TestDataGenerator.getPresentationExchangeId(), verifierRequest.getPresentationExchangeId());
        assertTrue(verifierRequest.getTimestamp().longValue() <= Instant.now().getEpochSecond());
        assertEquals("request_sent", verifierRequest.getState());

        assertEquals(credentialDefCreatedResponse, credentialDefCaptor.getValue());
        assertEquals(schemaResponse, schemaResponseCaptor.getValue());
        List<ProofRequestEntry> entries = proofEntryCaptor.getValue();
        assertEquals(2, entries.size());
        assertEquals("authorization", entries.get(0).getCredential());
        assertNull(entries.get(0).getPredicate());
        assertNull(entries.get(0).getCondition());
        assertEquals("spending-limit", entries.get(1).getCredential());
        assertEquals(">=", entries.get(1).getPredicate());
        assertEquals("100", entries.get(1).getCondition());

        V10PresentationCreateRequestRequest v10PresentationCreateRequestRequest = proofRequestCaptor.getValue();
        ProofRequest proofRequest = v10PresentationCreateRequestRequest.getProofRequest();
        assertEquals("mandate proof", proofRequest.getName());
        assertNotNull(proofRequest.getNonce());
        assertEquals(10, proofRequest.getNonce().length());

        assertRequestedAttributes(proofRequest.getRequestedAttributes());
        assertRequestedPredicates(proofRequest.getRequestedPredicates());
        Map<String, Object> additionalProperties = result.getAdditionalProperties();
        assertEquals("did:sov:BzCbsNYhMrjHiqZDTUASHg;spec/present-proof/1.0/request-presentation", additionalProperties.get("@type"));
        assertEquals("e6dcf97a-be64-4d9d-b0bf-577f962ea72b", additionalProperties.get("@id"));
        assertEquals("test", additionalProperties.get("comment"));

        ArrayList<Map<String, Object>> requestPresentationsAttach = (ArrayList<Map<String, Object>>) additionalProperties.get("request_presentations~attach");
        Map<String, Object> requestPresentationsAttachMap = requestPresentationsAttach.get(0);
        assertEquals("libindy-request-presentation-0", requestPresentationsAttachMap.get("@id"));
        assertEquals("application/json", requestPresentationsAttachMap.get("mime-type"));
        Map<String, Object> data = (Map<String, Object>) requestPresentationsAttachMap.get("data");
        assertEquals(
                "eyJuYW1lIjogIm1hbmRhdGUgcHJvb2YiLCAibm9uY2UiOiAiMTIzNDU2Nzg5MCIsICJyZXF1ZXN0ZWRfYXR0cmlidXRlcyI6IHsiYWRkaXRpb25hbFByb3AxIjogeyJuYW1l"
                        + "IjogImF1dGhvcml6YXRpb24iLCAicmVzdHJpY3Rpb25zIjogW3siY3JlZF9kZWZfaWQiOiAiNTVNZ3ZrUURCODE1dnRiRmt5NGtFdDozOkNMOjg6ZGVmYXVsdCJ9XX0sIC"
                        + "JhZGRpdGlvbmFsUHJvcDIiOiB7Im5hbWUiOiAic3BlbmRpbmctbGltaXQiLCAicmVzdHJpY3Rpb25zIjogW3siY3JlZF9kZWZfaWQiOiAiNTVNZ3ZrUURCODE1dnRiRmt5"
                        + "NGtFdDozOkNMOjg6ZGVmYXVsdCJ9XX19LCAicmVxdWVzdGVkX3ByZWRpY2F0ZXMiOiB7ImFkZGl0aW9uYWxQcm9wMSI6IHsibmFtZSI6ICJzcGVuZGluZy1saW1pdCIsIC"
                        + "JwX3R5cGUiOiAiPj0iLCAicF92YWx1ZSI6IDEwMCwgInJlc3RyaWN0aW9ucyI6IFt7ImNyZWRfZGVmX2lkIjogIjU1TWd2a1FEQjgxNXZ0YkZreTRrRXQ6MzpDTDo4OmRl"
                        + "ZmF1bHQifV19fSwgInZlcnNpb24iOiAiMS4wIn0=",
                data.get("base64"));

        ObjectNode service = (ObjectNode) additionalProperties.get("~service");
        JsonNode endpoint = service.get("serviceEndpoint");
        assertEquals("http://myariesagent:8080", endpoint.asText());
        ArrayNode recipientKeys = (ArrayNode) service.get("recipientKeys");
        assertEquals(1, recipientKeys.size());
        assertEquals("walletver", recipientKeys.get(0).asText());
    }

    @Test
    void testRetrieveProofRequestEmptyWalledDids() throws JsonProcessingException {
        MandateVerifierRequest mandateVerifierRequest = new MandateVerifierRequest("1", "mandate-signing-basic:1.0",
                new String(Base64.getEncoder().encode(REQUEST.getBytes(Charset.defaultCharset()))), "testComment");
        Mockito.when(mandateVerifierRequestDao.findById(uuidCaptor.capture())).thenReturn(Optional.of(mandateVerifierRequest));

        CredentialDefCreatedResponse credentialDefCreatedResponse =
                objectMapper.readValue("{\"credential_definition_ids\": [\"55MgvkQDB815vtbFky4kEt:3:CL:8:default\"]}", CredentialDefCreatedResponse.class);
        SchemasCreatedResults schemaResponse =
                objectMapper.readValue("{\"schema_ids\": [\"WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0\"]}", SchemasCreatedResults.class);
        Mockito.when(agentRestClient.getCredentialDefinitionCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture()))
                .thenReturn(credentialDefCreatedResponse);
        Mockito.when(agentRestClient.getSchemaCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture()))
                .thenReturn(schemaResponse);

        when(indyConversionService.makePresentationExchangeRequest(credentialDefCaptor.capture(), schemaResponseCaptor.capture(), proofEntryCaptor.capture()))
                .thenReturn(TestDataGenerator.createV10PresentationCreateRequestRequest());

        Mockito.when(mandateVerifierRequestDao.save(mandateVerifierRequestCaptor.capture())).thenReturn(mandateVerifierRequest);

        Mockito.when(agentRestClient.createProofRequest(proofRequestCaptor.capture())).thenReturn(TestDataGenerator.createV10PresentationExchange());

        List<com.visma.connect.hyper42.mandate.service.model.schema.generated.Result> dids =
                Collections.emptyList();
        DIDList didList = new DIDList().withResults(dids);
        when(agentRestClient.retrieveWalletDids()).thenReturn(didList);

        try {
            proofServiceImpl.retrieveProofRequest("1-2-3-4");
        } catch (ApplicationRuntimeException are) {
            assertEquals("no wallet dids found", are.getMessage());
        }

        assertEquals("1-2-3-4", uuidCaptor.getValue());

        assertEquals("mandate-signing-basic", schemaNameCaptor.getValue());
        assertEquals("1.0", schemaVersionCaptor.getValue());

        MandateVerifierRequest verifierRequest = mandateVerifierRequestCaptor.getValue();
        assertEquals(TestDataGenerator.getPresentationExchangeId(), verifierRequest.getPresentationExchangeId());
        assertTrue(verifierRequest.getTimestamp().longValue() <= Instant.now().getEpochSecond());
        assertEquals("request_sent", verifierRequest.getState());

        assertEquals(credentialDefCreatedResponse, credentialDefCaptor.getValue());
        assertEquals(schemaResponse, schemaResponseCaptor.getValue());
        List<ProofRequestEntry> entries = proofEntryCaptor.getValue();
        assertEquals(2, entries.size());
        assertEquals("authorization", entries.get(0).getCredential());
        assertNull(entries.get(0).getPredicate());
        assertNull(entries.get(0).getCondition());
        assertEquals("spending-limit", entries.get(1).getCredential());
        assertEquals(">=", entries.get(1).getPredicate());
        assertEquals("100", entries.get(1).getCondition());

        V10PresentationCreateRequestRequest v10PresentationCreateRequestRequest = proofRequestCaptor.getValue();
        ProofRequest proofRequest = v10PresentationCreateRequestRequest.getProofRequest();
        assertEquals("mandate proof", proofRequest.getName());
        assertNotNull(proofRequest.getNonce());
        assertEquals(10, proofRequest.getNonce().length());

        assertRequestedAttributes(proofRequest.getRequestedAttributes());
        assertRequestedPredicates(proofRequest.getRequestedPredicates());
    }

    @Test
    void testRetrieveProofRequestEmptySchemaDef() throws JsonProcessingException {
        MandateVerifierRequest mandateVerifierRequest = new MandateVerifierRequest("1", "mandate-signing-basic:1.0",
                new String(Base64.getEncoder().encode(REQUEST.getBytes(Charset.defaultCharset()))), "testComment");
        Mockito.when(mandateVerifierRequestDao.findById(uuidCaptor.capture())).thenReturn(Optional.of(mandateVerifierRequest));

        CredentialDefCreatedResponse credentialDefCreatedResponse =
                objectMapper.readValue("{\"credential_definition_ids\": [\"55MgvkQDB815vtbFky4kEt:3:CL:8:default\"]}", CredentialDefCreatedResponse.class);
        SchemasCreatedResults schemaResponse =
                objectMapper.readValue("{\"schema_ids\": []}", SchemasCreatedResults.class);
        Mockito.when(agentRestClient.getCredentialDefinitionCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture()))
                .thenReturn(credentialDefCreatedResponse);
        Mockito.when(agentRestClient.getSchemaCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture()))
                .thenReturn(schemaResponse);

        try {
            proofServiceImpl.retrieveProofRequest("1-2-3-4");
        } catch (ApplicationRuntimeException are) {
            assertEquals("schema definition formandate-signing-basic:1.0 not found", are.getMessage());
        }

        assertEquals("1-2-3-4", uuidCaptor.getValue());

        assertEquals("mandate-signing-basic", schemaNameCaptor.getValue());
        assertEquals("1.0", schemaVersionCaptor.getValue());
    }

    @Test
    void createVerifierMandateRequestOK() {
        String schemaName = "someSchema:1.0";
        String comment = "someComment";
        String user = "someuser";
        String jsonRequest = "{\"some\":\"thing\"}";
        when(mandateVerifierRequestDao.save(mandateVerifierRequestCaptor.capture())).thenReturn(formVerifierMandateRequest());

        String response = proofServiceImpl.createVerifierMandateRequest(schemaName, comment, user, jsonRequest);

        assertEquals(formVerifierMandateRequest().getId(), response);
        MandateVerifierRequest mandateVerifierRequest = mandateVerifierRequestCaptor.getValue();
        assertEquals(schemaName, mandateVerifierRequest.getSchemaName());
        assertEquals(comment, mandateVerifierRequest.getComment());
        assertEquals(user, mandateVerifierRequest.getId());
        assertEquals(jsonRequest, new String(Base64.getDecoder().decode(mandateVerifierRequest.getRequestJson().getBytes(Charset.defaultCharset()))));
    }

    @Test
    void createVerifierMandateRequestNotOK() {
        String schema = TestDataGenerator.createVerifierRequestJson();
        String schemaName = "someSchema:1.0";
        String comment = "someComment";
        String user = "someuser";
        when(mandateVerifierRequestDao.save(mandateVerifierRequestCaptor.capture())).thenReturn(null);

        assertThrows(ApplicationRuntimeException.class, () -> {
            proofServiceImpl.createVerifierMandateRequest(schema, schemaName, user, comment);
        });
    }

    private void assertRequestedPredicates(RequestedPredicates requestedPredicates) {
        Map<String, RequestedPredicatesProperty> additionalProperties = requestedPredicates.getAdditionalProperties();
        assertEquals(1, additionalProperties.size());
        Set<Entry<String, RequestedPredicatesProperty>> entrySet = additionalProperties.entrySet();
        Entry<String, RequestedPredicatesProperty> entry = entrySet.iterator().next();
        assertNotNull(entry.getKey());
        RequestedPredicatesProperty value = entry.getValue();
        assertEquals("spending-limit", value.getName());
        assertEquals(">=", value.getPType().toString());
        assertEquals(Long.valueOf(100), value.getPValue());
        List<Restriction_> restrictions = value.getRestrictions();
        assertEquals(1, restrictions.size());
        assertEquals("55MgvkQDB815vtbFky4kEt:3:CL:8:default", restrictions.get(0).getCredDefId());
        assertEquals("WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0", restrictions.get(0).getSchemaId());
    }

    private void assertRequestedAttributes(RequestedAttributes requestedAttributes) {
        Map<String, RequestedAttributesProperty> additionalProperties = requestedAttributes.getAdditionalProperties();
        assertEquals(1, additionalProperties.size());
        Set<String> keySetRequested = additionalProperties.keySet();
        Iterator<String> iterator = keySetRequested.iterator();
        String key1 = iterator.next();
        assertNotNull(UUID.fromString(key1));
        RequestedAttributesProperty property1 = additionalProperties.get(key1);
        assertRequestedAttributeProperties(property1);
    }

    private void assertRequestedAttributeProperties(RequestedAttributesProperty property1) {
        assertNotNull(property1.getNames());
        assertEquals(1, property1.getNames().size());
        assertEquals("authorization", property1.getNames().get(0));
        List<Restriction> restrictions = property1.getRestrictions();
        assertEquals(1, restrictions.size());
        Map<String, String> additionalProperties = restrictions.get(0).getAdditionalProperties();
        assertEquals(2, additionalProperties.size());
        assertEquals("55MgvkQDB815vtbFky4kEt:3:CL:8:default", additionalProperties.get("cred_def_id"));
        assertEquals("WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0", additionalProperties.get("schema_id"));
    }

    private MandateVerifierRequest formVerifierMandateRequest() {
        return new MandateVerifierRequest("someId", "someSchema:1.0", "someComment", "[\"some\",\"json\"]");
    }

    @Test
    void testRetrieveProofRequestNoCreatedCredentials() throws JsonProcessingException {
        CredentialDefCreatedResponse credentialDefCreatedResponse =
                objectMapper.readValue("{\"credential_definition_ids\": []}", CredentialDefCreatedResponse.class);
        Mockito.when(agentRestClient.getCredentialDefinitionCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture()))
                .thenReturn(credentialDefCreatedResponse);

        MandateVerifierRequest mandateVerifierRequest = new MandateVerifierRequest("1", "mandate-signing-basic:1.0", "testComment",
                new String(Base64.getEncoder().encode(REQUEST.getBytes(Charset.defaultCharset()))));
        Mockito.when(mandateVerifierRequestDao.findById(uuidCaptor.capture())).thenReturn(Optional.of(mandateVerifierRequest));

        assertThrows(ApplicationRuntimeException.class, () -> proofServiceImpl.retrieveProofRequest("1-2-3-4"));

    }

    @Test
    void testRetrieveProofRequestInvalidRequest() throws JsonProcessingException {
        CredentialDefCreatedResponse credentialDefCreatedResponse =
                objectMapper.readValue("{\"credential_definition_ids\": [\"55MgvkQDB815vtbFky4kEt:3:CL:8:default\"]}", CredentialDefCreatedResponse.class);
        SchemasCreatedResults schemaResponse =
                objectMapper.readValue("{\"schema_ids\": [\"WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0\"]}", SchemasCreatedResults.class);
        Mockito.when(agentRestClient.getCredentialDefinitionCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture()))
                .thenReturn(credentialDefCreatedResponse);
        Mockito.when(agentRestClient.getSchemaCreated(schemaNameCaptor.capture(), schemaVersionCaptor.capture()))
                .thenReturn(schemaResponse);

        MandateVerifierRequest mandateVerifierRequest = new MandateVerifierRequest("1", "mandate-signing-basic:1.0", "testComment",
                new String(Base64.getEncoder().encode("emptyjson".getBytes())));
        Mockito.when(mandateVerifierRequestDao.findById(uuidCaptor.capture())).thenReturn(Optional.of(mandateVerifierRequest));

        assertThrows(ApplicationRuntimeException.class, () -> proofServiceImpl.retrieveProofRequest("1-2-3-4"));

    }

    @Test
    void testRetrieveProofRequestNotInMongodb() {
        Mockito.when(mandateVerifierRequestDao.findById(uuidCaptor.capture())).thenReturn(Optional.empty());

        Optional<PresentationRequestDict> result = proofServiceImpl.retrieveProofRequest("1-2-3-4");
        assertTrue(result.isEmpty());
    }
}
