/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.service.TestDataGenerator;
import com.visma.connect.hyper42.mandate.service.model.generated.ProofReceivedStatus;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PresentationRequestDict;
import com.visma.connect.hyper42.mandate.service.service.ProofInitService;
import com.visma.connect.hyper42.mandate.service.service.ProofResultService;
import java.lang.reflect.Field;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ReflectionUtils;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProofServiceControllerTest {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Mock
    private ProofInitService proofInitService;

    @Mock
    private ProofResultService proofResultService;

    @InjectMocks
    private ProofServiceController proofServiceController;

    @Captor
    private ArgumentCaptor<String> schameNameCaptor;

    @Captor
    private ArgumentCaptor<String> commentCapture;

    @Captor
    private ArgumentCaptor<String> requestCaptor;

    @Captor
    private ArgumentCaptor<String> uuidCaptor;

    @Captor
    private ArgumentCaptor<String> userCaptor;

    @Captor
    private ArgumentCaptor<String> threadIdCaptor;

    @Captor
    private ArgumentCaptor<String> stateCaptor;

    @Captor
    private ArgumentCaptor<String> verifiedCaptor;

    @Captor
    private ArgumentCaptor<String> proofCaptor;

    @BeforeEach
    public void init() {
        Field field = ReflectionUtils.findField(ProofServiceController.class, "objectMapper");
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, proofServiceController, objectMapper);
    }

    @Test
    void testCreateProofRequest() {

        when(
                proofInitService.createVerifierMandateRequest(schameNameCaptor.capture(), commentCapture.capture(), userCaptor.capture(),
                        requestCaptor.capture()))
                                .thenReturn("someId");
        ResponseEntity<String> responseEntity = proofServiceController.createProofRequest("mandate-signing-basic%253A1.0", "test", "someuser",
                "[{\"credential\":\"authorization\"},{\"credential\":\"spending-limit\",\"predicate\":\">=\",\"condition\":\"100\"}]");

        assertEquals(200, responseEntity.getStatusCodeValue());
        assertEquals("someId", responseEntity.getBody());
        assertEquals("mandate-signing-basic%3A1.0", schameNameCaptor.getValue());
        assertEquals("test", commentCapture.getValue());
        assertEquals("someuser", userCaptor.getValue());
    }

    @Test
    void testRetrieveProofRequest() throws JsonProcessingException {
        Optional<PresentationRequestDict> proofRequest = Optional.of(TestDataGenerator.createV10PresentationExchange().getPresentationRequestDict());
        when(proofInitService.retrieveProofRequest(uuidCaptor.capture())).thenReturn(proofRequest);

        ResponseEntity<String> result = proofServiceController.retrieveProofRequest("abc");

        assertEquals("abc", uuidCaptor.getValue());
        assertEquals(200, result.getStatusCodeValue());
        assertEquals(
                "eyJyZXF1ZXN0X3ByZXNlbnRhdGlvbnN+YXR0YWNoIjpbeyJAaWQiOiJsaWJpbmR5LXJlcXVlc3QtcHJlc2VudGF0aW9uLTAiLCJtaW1lLXR5cGUiOiJhcHBsaWNhdGlvbi9q"
                        + "c29uIiwiZGF0YSI6eyJiYXNlNjQiOiJleUp1WVcxbElqb2dJbTFoYm1SaGRHVWdjSEp2YjJZaUxDQWlibTl1WTJVaU9pQWlNVEl6TkRVMk56ZzVNQ0lzSUNKeV"
                        + "pYRjFaWE4wWldSZllYUjBjbWxpZFhSbGN5STZJSHNpWVdSa2FYUnBiMjVoYkZCeWIzQXhJam9nZXlKdVlXMWxJam9nSW1GMWRHaHZjbWw2WVhScGIyNGlMQ0Fp"
                        + "Y21WemRISnBZM1JwYjI1eklqb2dXM3NpWTNKbFpGOWtaV1pmYVdRaU9pQWlOVFZOWjNaclVVUkNPREUxZG5SaVJtdDVOR3RGZERvek9rTk1Pamc2WkdWbVlYVn"
                        + "NkQ0o5WFgwc0lDSmhaR1JwZEdsdmJtRnNVSEp2Y0RJaU9pQjdJbTVoYldVaU9pQWljM0JsYm1ScGJtY3RiR2x0YVhRaUxDQWljbVZ6ZEhKcFkzUnBiMjV6SWpv"
                        + "Z1czc2lZM0psWkY5a1pXWmZhV1FpT2lBaU5UVk5aM1pyVVVSQ09ERTFkblJpUm10NU5HdEZkRG96T2tOTU9qZzZaR1ZtWVhWc2RDSjlYWDE5TENBaWNtVnhkV1"
                        + "Z6ZEdWa1gzQnlaV1JwWTJGMFpYTWlPaUI3SW1Ga1pHbDBhVzl1WVd4UWNtOXdNU0k2SUhzaWJtRnRaU0k2SUNKemNHVnVaR2x1Wnkxc2FXMXBkQ0lzSUNKd1gz"
                        + "UjVjR1VpT2lBaVBqMGlMQ0FpY0Y5MllXeDFaU0k2SURFd01Dd2dJbkpsYzNSeWFXTjBhVzl1Y3lJNklGdDdJbU55WldSZlpHVm1YMmxrSWpvZ0lqVTFUV2QyYT"
                        + "FGRVFqZ3hOWFowWWtacmVUUnJSWFE2TXpwRFREbzRPbVJsWm1GMWJIUWlmVjE5ZlN3Z0luWmxjbk5wYjI0aU9pQWlNUzR3SW4wPSJ9fV0sIkB0eXBlIjoiZGlk"
                        + "OnNvdjpCekNic05ZaE1yakhpcVpEVFVBU0hnO3NwZWMvcHJlc2VudC1wcm9vZi8xLjAvcmVxdWVzdC1wcmVzZW50YXRpb24iLCJjb21tZW50IjoidGVzdCIsIk"
                        + "BpZCI6ImU2ZGNmOTdhLWJlNjQtNGQ5ZC1iMGJmLTU3N2Y5NjJlYTcyYiIsIn50cmFjZSI6eyJ0YXJnZXQiOiJsb2ciLCJmdWxsX3RocmVhZCI6dHJ1ZSwidHJh"
                        + "Y2VfcmVwb3J0cyI6W119fQ==",
                result.getBody());
    }

    @Test
    void testRetrieveProofRequestNotFound() {
        Optional<PresentationRequestDict> proofRequest = Optional.empty();
        when(proofInitService.retrieveProofRequest(uuidCaptor.capture())).thenReturn(proofRequest);

        ResponseEntity<String> result = proofServiceController.retrieveProofRequest("abc");
        assertEquals(404, result.getStatusCodeValue());
    }

    @Test
    void testReceiveProofOk() {
        Mockito.doNothing().when(proofResultService).processAndValidateProofReceived(threadIdCaptor.capture(), stateCaptor.capture(), verifiedCaptor.capture(),
                proofCaptor.capture());
        ResponseEntity<ProofReceivedStatus> response = proofServiceController.receiveProof("1234", "verified", "true", "{someProofJson}");

        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());
        assertEquals("ok", response.getBody().getStatus());
        assertEquals("1234", threadIdCaptor.getValue());
        assertEquals("verified", stateCaptor.getValue());
        assertEquals("true", verifiedCaptor.getValue());
        assertEquals("{someProofJson}", proofCaptor.getValue());
    }

    @Test
    void testReceiveProofLoginEmail() {
        String uuid = "someUuid";
        when(proofResultService.retrieveProofRequestLoginEmail(uuidCaptor.capture())).thenReturn("someEmail");

        ResponseEntity<String> response = proofServiceController.retrieveProofRequestLoginEmail(uuid);

        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());
        assertEquals("someEmail", response.getBody());
        assertEquals("someUuid", uuidCaptor.getValue());
    }
}
