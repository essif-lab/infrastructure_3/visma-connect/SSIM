/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.dao;

import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.model.MandateRequest;
import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Dao to gather mandate request data in mongodb
 *
 * @author Ragesh Shunmugam, Lukas Nakas
 */
@Repository
public interface MandateRequestDao extends MongoRepository<MandateRequest, String> {

    /**
     * Get MandateRequest object for the given connectionId
     *
     * @param connectionId connectionId
     * @return Optional of MandateRequest
     * @throws DataNotFoundException exception when object is not found
     */
    Optional<MandateRequest> findByConnectionId(String connectionId) throws DataNotFoundException;

    /**
     * Get MandateRequest object for the given credRevId and revRegId combination
     *
     * @param credRevId credential revocation id
     * @param revRegId revocation registry id
     * @return Optional of MandateRequest
     * @throws DataNotFoundException exception when object is not found
     */
    Optional<MandateRequest> findByCredRevIdAndRevRegId(String credRevId, String revRegId) throws DataNotFoundException;

    /**
     * Get MandateRequest objects which have the given status
     *
     * @param status status
     * @return List of MandateRequest
     */
    List<MandateRequest> findAllByStatusEquals(String status);

    /**
     * Get MandateRequest object for the given connectionId
     *
     * @param mpId mandate proposal id
     * @return Optional of MandateRequest
     */
    Optional<MandateRequest> findByMpId(String mpId);
}
