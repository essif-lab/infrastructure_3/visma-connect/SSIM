/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.model.schema;

import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The Attribute holds the data for schema's attributes
 *
 * @author Lukas Nakas
 */
public class Attribute {

    /**
     * Holds Attribute Name
     */
    @Field("name")
    private String name;

    /**
     * Holds Attribute Title
     */
    @Field("title")
    private String title;

    /**
     * Holds Attribute Type
     */
    @Field("type")
    private String type;

    /**
     * All args constructor
     *
     * @param name as string
     * @param title as string
     * @param type as string
     */
    public Attribute(String name, String title, String type) {
        this.name = name;
        this.title = title;
        this.type = type;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Attribute#name}
     */
    public String getName() {
        return name;
    }

    /**
     * Setter.
     *
     * @param name {@link com.visma.connect.hyper42.mandate.service.model.schema.Attribute#name}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Attribute#title}
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter.
     *
     * @param title {@link com.visma.connect.hyper42.mandate.service.model.schema.Attribute#title}
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Attribute#type}
     */
    public String getType() {
        return type;
    }

    /**
     * Setter.
     *
     * @param type {@link com.visma.connect.hyper42.mandate.service.model.schema.Attribute#type}
     */
    public void setType(String type) {
        this.type = type;
    }
}
