/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.controller;

import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.model.generated.AvailableSchemasResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaResponse;
import com.visma.connect.hyper42.mandate.service.service.SchemaService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The RestController for manipulating Schema's and Credential Definitions.
 *
 * @author Micha Wensveen, Lukas Nakas
 */
@RestController
@RequestMapping("/schemas")
public class SchemaController {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(SchemaController.class);

    /**
     * the SchemaService.
     */
    @Autowired
    private SchemaService schemaService;

    /**
     * Get a list of all schemas that are created by the mandate service..
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.controller.SchemaController#listOfSchemas}
     */
    @GetMapping("/all")
    public List<AvailableSchemasResponse> getListOfSchemas() {
        LOG.info("Getting a list of all schemas");
        List<AvailableSchemasResponse> allSchema = schemaService.getAllCreatedSchema();
        LOG.debug("Retrieved schemas ({}) {}", allSchema.size(), allSchema);
        return allSchema;
    }

    /**
     * Get a list of all user schemas that are created by the mandate service..
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.controller.SchemaController#listOfUserSchemas}
     */
    @GetMapping
    public List<AvailableSchemasResponse> getListOfUserSchemas() {
        LOG.info("Getting a list of all schemas for user");
        List<AvailableSchemasResponse> allUserSchema = schemaService.getAllUserSchema();
        LOG.debug("Retrieved schemas ({}) {}", allUserSchema.size(), allUserSchema);
        return allUserSchema;
    }

    /**
     * Get a schema definition
     *
     * @param schemaId - String
     * @return {@link com.visma.connect.hyper42.mandate.service.controller.SchemaController#schemaDefinition}
     */
    @GetMapping("/{schemaId}")
    public SchemaDefinitionResponse getSchemaDefinition(@PathVariable String schemaId) {
        LOG.info("Getting a schema definition by schemaId {}", schemaId);
        SchemaDefinitionResponse schemaDefinitionResponse = schemaService.getSchemaDefinition(schemaId);
        LOG.debug("Retrieved schema {}", schemaDefinitionResponse);
        return schemaDefinitionResponse;
    }

    /**
     * Add new schema
     *
     * @param addSchemaRequest object with information of the new schema
     * @return {@link com.visma.connect.hyper42.mandate.service.controller.SchemaController#addSchemaResponse}
     */
    @PostMapping
    public AddSchemaResponse addSchema(@RequestBody AddSchemaRequest addSchemaRequest) {
        LOG.info("Adding new schema with title: {}, ssicommsEnabled: {}", addSchemaRequest.getTitle(), addSchemaRequest.getSsicommsEnabled());
        AddSchemaResponse addSchemaResponse = schemaService.addSchema(addSchemaRequest);
        LOG.debug("Schema addition status: schemaTitle -> {}, status -> {}", addSchemaResponse.getTitle(), addSchemaResponse.getStatus());
        return addSchemaResponse;
    }

    /**
     * Get a schema JSON definition
     *
     * @param schemaName - String
     * @return {@link com.visma.connect.hyper42.mandate.service.controller.SchemaController#schemaJsonDefinition}
     * @throws DataNotFoundException thrown when json schema definition is not found
     */
    @GetMapping("/definitions/{schemaName}")
    public String getSchemaJsonDefinition(@PathVariable String schemaName) throws DataNotFoundException {
        LOG.info("Getting a schema JSON definition by schemaName {}", schemaName);
        String schemaJsonDefinition = schemaService.getSchemaJsonDefinition(schemaName);
        LOG.debug("Retrieved schema JSON definition {}", schemaJsonDefinition);
        return schemaJsonDefinition;
    }
}
