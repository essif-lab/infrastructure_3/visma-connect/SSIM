/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.rest.impl;

import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.rest.OpenPolicyAgentRestClient;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpClientErrorException.BadRequest;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Rest client for the Open Policy Agent, implementation of {@link OpenPolicyAgentRestClient} interface.
 * 
 * @author Rik Sonderkamp
 */
@Service
public class OpenPolicyAgentRestClientImpl implements OpenPolicyAgentRestClient {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(OpenPolicyAgentRestClientImpl.class);

    /**
     * Rest template
     */
    @Autowired
    private RestTemplate restTemplate;

    /**
     * url for the Open Policy Agent
     */
    @Value("${opa.url}")
    private String opaUrl;

    @Override
    public String createOrUpdatePolicy(String policyId, String policy) {
        LOG.info("Create or update policy in Open Policy Agent for ID: {}", policyId);
        try {
            URI targetUrl = UriComponentsBuilder.fromUriString(opaUrl + "/v1/policies/{id}").build(policyId);

            HttpEntity<String> request = new HttpEntity<>(policy);
            ResponseEntity<String> result = restTemplate.exchange(targetUrl, HttpMethod.PUT, request, String.class);
            if (!result.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Problem with sending policy to OPA");
            }
            return policyId;
        } catch (BadRequest e) {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (RestClientException e) {
            throw new ApplicationRuntimeException("Something went wrong while sending policy to OPA.", e);
        }
    }

    @Override
    public String queryDataWithInput(String dataPath, String input) {
        LOG.debug("Get data from OPA using path: {} and input: {}", dataPath, input);
        try {
            URI targetUrl = UriComponentsBuilder.fromUriString(opaUrl + "/v1/data/{path}/valid").build(dataPath);

            HttpEntity<String> request = new HttpEntity<>(input);
            ResponseEntity<String> result = restTemplate.exchange(targetUrl, HttpMethod.POST, request, String.class);
            if (!result.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Problem with getting data from OPA");
            }
            return result.getBody();
        } catch (RestClientException e) {
            throw new ApplicationRuntimeException("Something went wrong while getting data from OPA.", e);
        }
    }

}
