/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.model.schema;

import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The Condition holds the data for predicate's conditions
 *
 * @author Lukas Nakas
 */
public class Condition {

    /**
     * Holds Condition Title
     */
    @Field("title")
    private String title;

    /**
     * Holds Condition Value
     */
    @Field("value")
    private String value;

    /**
     * All args constructor
     *
     * @param title as string
     * @param value as string
     */
    public Condition(String title, String value) {
        this.title = title;
        this.value = value;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Condition#title}
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter.
     *
     * @param title {@link com.visma.connect.hyper42.mandate.service.model.schema.Condition#title}
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Condition#value}
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter.
     *
     * @param value {@link com.visma.connect.hyper42.mandate.service.model.schema.Condition#value}
     */
    public void setValue(String value) {
        this.value = value;
    }
}
