/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.scheduler;

import com.visma.connect.hyper42.mandate.service.service.ProofResultService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Component class for enabling scheduled invocations of methods for proofrequests
 *
 * @author Kevin Kerkhoven
 */
@Component
public class ProofRequestScheduledTasks {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ProofRequestScheduledTasks.class);

    /**
     * The ProofService
     */
    @Autowired
    private ProofResultService proofResultService;

    /**
     * Check all proof requests which are initiated and have passed a certain timestamp
     */
    @Scheduled(fixedDelayString = "${scheduler.interval}")
    public void scheduledCheckTimeout() {
        LOG.debug("Starting scheduledCheckTimeout");
        proofResultService.processProofRequestTimeout();
        LOG.debug("finished scheduledCheckTimeout");
    }
}