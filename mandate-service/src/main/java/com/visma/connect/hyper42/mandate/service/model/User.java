/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The User holds the data of user
 *
 * @author Lukas Nakas
 */
@Document("user")
public class User {

    /**
     * Holds Id
     */
    @Id
    @Field("_id")
    private String id;

    /**
     * Holds User Email
     */
    @Field("email")
    @Indexed
    private String email;

    /**
     * Holds User Role
     */
    @Field(name = "role")
    private String role;

    /**
     * All args constructor
     *
     * @param id as string
     * @param email as string
     * @param role as string
     */
    public User(String id, String email, String role) {
        this.id = id;
        this.email = email;
        this.role = role;
    }

    /**
     * Getter.
     *
     * @return {@link User#id}
     */
    public String getId() {
        return id;
    }

    /**
     * Setter.
     *
     * @param id {@link User#id}
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter.
     *
     * @return {@link User#email}
     */
    public String getEmail() {
        return email;
    }

    /**
     * Setter.
     *
     * @param email {@link User#email}
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Getter.
     *
     * @return {@link User#role}
     */
    public String getRole() {
        return role;
    }

    /**
     * Setter.
     *
     * @param role {@link User#role}
     */
    public void setRole(String role) {
        this.role = role;
    }
}