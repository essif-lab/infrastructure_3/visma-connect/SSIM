/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.controller;

import com.visma.connect.hyper42.mandate.service.model.schema.generated.UserResponse;
import com.visma.connect.hyper42.mandate.service.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The RestController for manipulating Users.
 *
 * @author Lukas Nakas
 */
@RestController
@RequestMapping("/users")
public class UserController {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    /**
     * the UserService.
     */
    @Autowired
    private UserService userService;

    /**
     * Get user by email
     *
     * @param email - String
     * @return {@link UserController#user}
     */
    @GetMapping("/{email}")
    public UserResponse getUserByEmail(@PathVariable String email) {
        LOG.info("Getting an user by email {}", email);
        UserResponse userResponse = userService.getUserByEmail(email);
        LOG.debug("Retrieved user {}", userResponse);
        return userResponse;
    }
}
