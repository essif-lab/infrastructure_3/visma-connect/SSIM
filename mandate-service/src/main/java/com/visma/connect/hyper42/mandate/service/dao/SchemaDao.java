/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.dao;

import com.visma.connect.hyper42.mandate.service.model.schema.Schema;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Dao to gather schema data in mongodb
 *
 * @author Lukas Nakas
 */
@Repository
public interface SchemaDao extends MongoRepository<Schema, String> {

    /**
     * Get Schema object for the given schemaId
     *
     * @param schemaId
     * @return Optional of Schema
     */
    Optional<Schema> findBySchemaId(String schemaId);

    /**
     * Get Schema object for the given schema name and version
     *
     * @param name
     * @param version
     * @return Optional of Schema
     */
    Optional<Schema> findByNameAndVersion(String name, String version);

}
