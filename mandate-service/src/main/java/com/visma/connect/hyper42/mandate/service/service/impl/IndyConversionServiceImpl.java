/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefCreatedResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.ProofRequestEntry;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.NonRevoked;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.ProofRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedAttributes;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedAttributesProperty;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedPredicates;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedPredicatesProperty;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.RequestedPredicatesProperty.PType;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.Restriction;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.Restriction_;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.SchemasCreatedResults;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationCreateRequestRequest;
import com.visma.connect.hyper42.mandate.service.service.IndyConversionService;
import com.visma.connect.hyper42.mandate.service.util.NonceGeneratorUtil;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

/**
 * Implementation of IndyConversionService
 *
 * @author Micha Wensveen, Kevin Kerkhoven
 */
@Service
public class IndyConversionServiceImpl implements IndyConversionService {

    @Override
    public V10PresentationCreateRequestRequest makePresentationExchangeRequest(CredentialDefCreatedResponse credentialDefs,
            SchemasCreatedResults schemaResponse, List<ProofRequestEntry> proofRequestEntities) {
        NonRevoked nonRevoked = createNonRevoked();

        RequestedAttributes requestedAttributes = new RequestedAttributes();
        RequestedAttributesProperty attributesProperty =
                toRequestedAttributesProperty(proofRequestEntities, credentialDefs.getCredentialDefinitionIds().get(0), schemaResponse.getSchemaIds().get(0),
                        nonRevoked);
        requestedAttributes.setAdditionalProperty(UUID.randomUUID().toString(), attributesProperty);

        RequestedPredicates requestedPredicates = new RequestedPredicates();
        proofRequestEntities.stream().filter(entry -> StringUtils.isNotBlank(entry.getPredicate()))
                .map(entry -> toRequestedPredicatesProperty(entry, credentialDefs.getCredentialDefinitionIds().get(0), schemaResponse.getSchemaIds().get(0),
                        nonRevoked))
                .forEach(rpp -> requestedPredicates.withAdditionalProperty(UUID.randomUUID().toString(), rpp));

        ProofRequest proofRequest = new ProofRequest()
                .withName("mandate proof")
                .withVersion("1.0")
                .withNonce(nonceGenerator())
                .withRequestedPredicates(requestedPredicates)
                .withNonRevoked(nonRevoked);

        if (!attributesProperty.getNames().isEmpty()) {
            proofRequest = proofRequest.withRequestedAttributes(requestedAttributes);
        } else {
            proofRequest = proofRequest.withRequestedAttributes(new RequestedAttributes());
        }

        return new V10PresentationCreateRequestRequest().withProofRequest(proofRequest).withTrace(false);
    }

    private NonRevoked createNonRevoked() {
        long now = ZonedDateTime.now().toEpochSecond();
        return new NonRevoked().withTo(now);
    }

    private RequestedPredicatesProperty toRequestedPredicatesProperty(ProofRequestEntry entry, String credentialDef, String schemaDef, NonRevoked nonRevoked) {
        return new RequestedPredicatesProperty()
                .withName(entry.getCredential())
                .withPType(PType.fromValue(entry.getPredicate()))
                .withPValue(Long.valueOf(entry.getCondition()))
                .withRestrictions(Collections.singletonList(new Restriction_()
                        .withCredDefId(credentialDef)
                        .withSchemaId(schemaDef)))
                .withNonRevoked(nonRevoked);
    }

    private RequestedAttributesProperty toRequestedAttributesProperty(List<ProofRequestEntry> proofRequestEntities, String credentialDef, String schemaDef,
            NonRevoked nonRevoked) {
        RequestedAttributesProperty attributesProperty = new RequestedAttributesProperty();
        attributesProperty.withNonRevoked(nonRevoked);
        proofRequestEntities.stream().filter(entry -> StringUtils.isBlank(entry.getPredicate()))
                .forEach(entry -> attributesProperty.getNames().add(entry.getCredential()));
        attributesProperty.getRestrictions().add(new Restriction()
                .withAdditionalProperty("cred_def_id", credentialDef)
                .withAdditionalProperty("schema_id", schemaDef));
        return attributesProperty;
    }

    private String nonceGenerator() {
        return NonceGeneratorUtil.generatNonce();
    }
}