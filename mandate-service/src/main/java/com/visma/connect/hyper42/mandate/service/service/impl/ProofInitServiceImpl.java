/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.dao.MandateVerifierRequestDao;
import com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefCreatedResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.ProofRequestEntry;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.DIDList;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PresentationRequestDict;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.Result;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.SchemasCreatedResults;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationCreateRequestRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationExchange;
import com.visma.connect.hyper42.mandate.service.rest.CloudAgentRestClient;
import com.visma.connect.hyper42.mandate.service.service.IndyConversionService;
import com.visma.connect.hyper42.mandate.service.service.ProofInitService;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Implementation of ProofInitService.
 *
 * @author Ragesh Shunmugan, Micha Wensveen, Kevin Kerkhoven
 */
@Service
public class ProofInitServiceImpl implements ProofInitService {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ProofInitServiceImpl.class);

    /**
     * Object mapper.
     */
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * The Indy Conversion Service
     */
    @Autowired
    private IndyConversionService indyConversionService;

    /**
     * The agent rest client
     */
    @Autowired
    private CloudAgentRestClient agentRestClient;

    /**
     * The aries agent service URL
     */
    @Value("${cloud.aries.agent.external.url}")
    private String agentExternalUrl;

    /**
     * Instantiates mandateVerifierRequestRepository
     */
    @Autowired
    private MandateVerifierRequestDao mandateVerifierRequestDao;

    @Override
    public String createVerifierMandateRequest(String schemaName, String comment, String user, String requestJson) {
        // add schemaName, comment and requestJson to MongoDB
        MandateVerifierRequest request = mandateVerifierRequestDao.save(formMandateVerifierRequest(schemaName, comment, user, requestJson));
        if (null == request) {
            LOG.error("Exception occurred while inserting mandate proof request data");
            throw new ApplicationRuntimeException("Could not create mandate proof request data");
        }
        return request.getId();
    }

    @Override
    public Optional<PresentationRequestDict> retrieveProofRequest(String uuid) {
        Optional<PresentationRequestDict> result = Optional.empty();

        // get from mongo
        LOG.debug("Get mongodb with key {}", uuid);
        Optional<MandateVerifierRequest> mandateVerifierRequestOptional = mandateVerifierRequestDao.findById(uuid);
        if (mandateVerifierRequestOptional.isPresent()) {
            MandateVerifierRequest mandateVerifierRequest = mandateVerifierRequestOptional.get();
            LOG.debug("from mongodb: schemaName {}, comment {}, requestJson {}", mandateVerifierRequest.getSchemaName(), mandateVerifierRequest.getComment(),
                    mandateVerifierRequest.getRequestJson());
            V10PresentationExchange presentationExchange =
                    convertAndSendRequestToIndy(mandateVerifierRequest.getSchemaName(), mandateVerifierRequest.getComment(),
                            new String(Base64.getDecoder().decode(mandateVerifierRequest.getRequestJson())));
            LOG.debug("Presentation exchange {}", presentationExchange);
            mandateVerifierRequest.setThreadId(presentationExchange.getThreadId());
            mandateVerifierRequest.setPresentationExchangeId(presentationExchange.getPresentationExchangeId());
            mandateVerifierRequest.setState(presentationExchange.getState());
            mandateVerifierRequest.setTimestamp(Instant.now().getEpochSecond());
            mandateVerifierRequestDao.save(mandateVerifierRequest);

            result = Optional.of(decorateWithService(presentationExchange.getPresentationRequestDict()));
        }
        return result;
    }

    private PresentationRequestDict decorateWithService(PresentationRequestDict presentationRequestDict) {
        DIDList didList = agentRestClient.retrieveWalletDids();
        if (didList.getResults().isEmpty()) {
            throw new ApplicationRuntimeException("no wallet dids found");
        }
        Result walletDid = didList.getResults().get(didList.getResults().size() - 1);

        ObjectNode serviceNode = objectMapper.createObjectNode();
        serviceNode.put("serviceEndpoint", agentExternalUrl);

        ArrayNode recipientKeys = objectMapper.createArrayNode();
        recipientKeys.add(walletDid.getVerkey());
        serviceNode.set("recipientKeys", recipientKeys);

        presentationRequestDict.withAdditionalProperty("~service", serviceNode);
        return presentationRequestDict;
    }

    private List<ProofRequestEntry> parseRequest(String requestJson) {
        List<ProofRequestEntry> proofRequestEntities;
        try {
            proofRequestEntities = Arrays.asList(objectMapper.readValue(requestJson, ProofRequestEntry[].class));
        } catch (JsonProcessingException e) {
            throw new ApplicationRuntimeException("Cannot parse request", e);
        }
        return proofRequestEntities;
    }

    private V10PresentationExchange convertAndSendRequestToIndy(String schema, String comment, String requestJson) {
        LOG.debug("Creating proofrequest for {}, {}, {}", schema, comment, requestJson);
        String[] schemaAttr = schema.split(":");
        String schemaName = schemaAttr[0];
        String schemaVersion = schemaAttr[1];

        CredentialDefCreatedResponse credentialDefs = agentRestClient.getCredentialDefinitionCreated(schemaName, schemaVersion);
        if (credentialDefs.getCredentialDefinitionIds().isEmpty()) {
            throw new ApplicationRuntimeException("credential definition for" + schema + " not found");
        }

        SchemasCreatedResults schemaResponse = agentRestClient.getSchemaCreated(schemaName, schemaVersion);
        if (schemaResponse.getSchemaIds().isEmpty()) {
            throw new ApplicationRuntimeException("schema definition for" + schema + " not found");
        }

        List<ProofRequestEntry> proofRequestEntities = parseRequest(requestJson);

        V10PresentationCreateRequestRequest request =
                indyConversionService.makePresentationExchangeRequest(credentialDefs, schemaResponse, proofRequestEntities);
        return agentRestClient.createProofRequest(request);
    }

    private MandateVerifierRequest formMandateVerifierRequest(String schemaName, String comment, String user, String requestJson) {
        return new MandateVerifierRequest(user, schemaName, Base64.getEncoder().encodeToString(requestJson.getBytes(StandardCharsets.UTF_8)), comment);
    }

}
