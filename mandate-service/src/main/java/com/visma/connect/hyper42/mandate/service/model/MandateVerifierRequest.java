/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The MandateVerifierRequest holds the request data for requesting a mandate credential verification
 *
 * @author Ragesh Shunmugam
 */
@Document("mandate_verifier_request")
@CompoundIndex(def = "{'state':1, 'timestamp':-1}", name = "state_timestamp_index")
public class MandateVerifierRequest {

    /**
     * Holds Id
     */
    @Id
    @Field("_id")
    private String id;

    /**
     * Holds Schema Name
     */
    @Field(name = "schemaName")
    private String schemaName;

    /**
     * Holds request json
     */
    @Field(name = "requestJson")
    private String requestJson;

    /**
     * Holds comment
     */
    @Field(name = "comment")
    private String comment;

    /**
     * Holds the presentation_exchange_id
     */
    @Field(name = "presentation_exchange_id")
    @Indexed(name = "presentation_exchange_id_index")
    private String presentationExchangeId;

    @Field(name = "thread_id")
    @Indexed(name = "thread_id")
    private String threadId;

    /**
     * Holds the state of this verifier request.
     */
    @Field(name = "state")
    private String state;

    /**
     * The timestamp that the state was set.
     * In epoch seconds, thus always in UTC.
     * Eg Instant.now().getEpochSecond();
     */
    @Field(name = "timestamp")
    private Long timestamp;

    /**
     * All args constructor
     *
     * @param id as string
     * @param schemaName as string
     * @param requestJson as string
     * @param comment as string
     */
    public MandateVerifierRequest(String id, String schemaName, String requestJson, String comment) {
        this.id = id;
        this.schemaName = schemaName;
        this.requestJson = requestJson;
        this.comment = comment;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest#id}
     */
    public String getId() {
        return id;
    }

    /**
     * Setter.
     *
     * @param id {@link com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest#id}
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest#schemaName}
     */
    public String getSchemaName() {
        return schemaName;
    }

    /**
     * Setter
     *
     * @param schemaName {@link com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest#requestJson}
     */
    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest#requestJson}
     */
    public String getRequestJson() {
        return requestJson;
    }

    /**
     * Setter.
     *
     * @param requestJson {@link com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest#requestJson}
     */
    public void setRequestJson(String requestJson) {
        this.requestJson = requestJson;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest#comment}
     */
    public String getComment() {
        return comment;
    }

    /**
     * Setter.
     *
     * @param comment {@link com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest#comment}
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest#presentationExchangeId}
     */
    public String getPresentationExchangeId() {
        return presentationExchangeId;
    }

    /**
     * Setter.
     *
     * @param presentationExchangeId {@link com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest#presentationExchangeId}
     */
    public void setPresentationExchangeId(String presentationExchangeId) {
        this.presentationExchangeId = presentationExchangeId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getThreadId() {
        return threadId;
    }

    public void setThreadId(String threadId) {
        this.threadId = threadId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

}
