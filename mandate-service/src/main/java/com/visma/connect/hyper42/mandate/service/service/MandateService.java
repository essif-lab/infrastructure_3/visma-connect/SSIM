/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service;

import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.IssuedMandateResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.RevokeCredentialRequest;
import java.util.List;

/**
 * Service for handling API calls for mandates, like requesting and revoking
 *
 * @author Kevin Kerkhoven, Lukas Nakas
 */
public interface MandateService {
    /**
     * Save the mandate request, so it can be sent when the connection is accepted by the other party.
     * The ID of the invitation will be stored in our database with the jsonSchema data
     *
     * @param alias Chosen alias for user for connection
     * @param requestJson Validated schema to add to our MongoDB
     * @param schemaName Name of schema
     * @param ssicomsEnabled Value indicating is mandate is SSIComms enabled or not
     * @return Base64 String of invitation
     */
    String saveMandateRequest(String alias, String requestJson, String schemaName, boolean ssicomsEnabled);

    /**
     * Retrieve earlier stored mandate request information based on connectionId.
     * Send the new credential to the connection.
     *
     * @param connectionId The id used to retrieve the needed information
     * @return The stored Json
     */
    String createMandateRequest(String connectionId) throws DataNotFoundException;

    /**
     * Accept the connection request.
     *
     * @param connectionId - String
     * @return the String
     */
    String acceptConnectionRequest(String connectionId);

    /**
     * After the connection is made send a Trust Ping to the connection
     * This will result in the connection becoming active.
     *
     * @param connectionId - String
     * @return the String
     */
    String pingConnection(String connectionId);

    /**
     * Enrich existing credentials details with revocation ID
     * and revocation registry ID. Update state accordingly as well
     *
     * @param connectionId - String
     * @param issueCredentialSendResponse - Object
     */
    String enrichCredentialDetails(String connectionId, IssueCredentialSendResponse issueCredentialSendResponse) throws DataNotFoundException;

    /**
     * Revoke credential with given revocation ID
     * and revocation registry ID.
     *
     * @param revokeCredentialRequest - Object
     * @return the String of revocation status
     */
    String revokeCredential(RevokeCredentialRequest revokeCredentialRequest) throws DataNotFoundException;

    /**
     * Retrieve mandate requests with status 'issued'
     *
     * @return List of IssuedMandateResponse
     */
    List<IssuedMandateResponse> retrieveIssuedMandates();

    /**
     * Create new connection invitation for already saved, SSICom enabled mandate proposal
     *
     * @param mpId existing mandate proposal id
     * @return Base64 String of invitation
     */
    String createConnectionInvitation(String mpId) throws DataNotFoundException;
}
