/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.visma.connect.hyper42.mandate.service.dao.PolicyDao;
import com.visma.connect.hyper42.mandate.service.model.Policy;
import com.visma.connect.hyper42.mandate.service.rest.OpenPolicyAgentRestClient;
import com.visma.connect.hyper42.mandate.service.service.PolicyService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link PolicyService} interface.
 * 
 * @author Rik Sonderkamp
 */
@Service
public class PolicyServiceImpl implements PolicyService {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(PolicyServiceImpl.class);

    /**
     * Rest client to OPA
     */
    @Autowired
    private OpenPolicyAgentRestClient opaClient;

    @Autowired
    private PolicyDao policyDao;

    @Override
    public String createOrUpdatePolicy(String policyId, String policyContent) {
        String opaPolicyId = opaClient.createOrUpdatePolicy(policyId, policyContent);

        LOG.info("Save or update policy to MongoDB for id: {}", policyId);
        Policy policy = policyDao.findByPolicyId(opaPolicyId).orElseGet(() -> {
            LOG.info("Policy ID {} not found. Creating a new policy", policyId);
            return new Policy(opaPolicyId, policyContent);
        });
        policy.setPolicyContent(policyContent);
        Policy savedPolicy = policyDao.save(policy);

        return savedPolicy.getPolicyId();
    }

    @Override
    public Optional<String> getPolicyContentById(String policyId) {
        LOG.debug("Get policy by id with id: {}", policyId);
        Optional<Policy> policy = policyDao.findByPolicyId(policyId);
        if (policy.isPresent()) {
            return Optional.of(policy.get().getPolicyContent());
        }
        LOG.info("Policy not found for id: {}", policyId);
        return Optional.empty();
    }

}
