/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.model;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The MandateRequest holds the request data for requesting a new mandate credential
 *
 * @author Ragesh Shunmugam
 */
@Document("mandate_request")
@CompoundIndex(def = "{'credRevId': 1, 'revRegId': 1}", name = "credRevId_revRegId_index")
public class MandateRequest implements Serializable {

    /**
     * UID for serializable Object.
     */
    private static final long serialVersionUID = 3523827062136755640L;

    /**
     * Holds Id
     */
    @Id
    @Field("_id")
    private String id;

    /**
     * Holds Mandate Proposal Id
     */
    @Field("mpId")
    @Indexed
    private String mpId;

    /**
     * Holds Invitation Id
     */
    @Field("connectionId")
    private String connectionId;

    /**
     * Holds Schema Name
     */
    @Field(name = "schemaName")
    private String schemaName;

    /**
     * Holds request json
     */
    @Field(name = "requestJson")
    private String requestJson;

    /**
     * Holds Revocation Registry Id
     */
    @Field(name = "revRegId")
    private String revRegId;

    /**
     * Holds Credential Revocation Id
     */
    @Field(name = "credRevId")
    private String credRevId;

    /**
     * Holds Status
     */
    @Field(name = "status")
    private String status;

    /**
     * All args constructor
     *
     * @param id as string
     * @param connectionId as string
     * @param schemaName as string
     * @param requestJson as string
     */
    public MandateRequest(String id, String connectionId, String schemaName, String requestJson) {
        this.id = id;
        this.connectionId = connectionId;
        this.schemaName = schemaName;
        this.requestJson = requestJson;
    }

    /**
     * No args constructor
     */
    public MandateRequest() {
        // No args constructor
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#id}
     */
    public String getId() {
        return id;
    }

    /**
     * Setter.
     *
     * @param id {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#id}
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#mpId}
     */
    public String getMpId() {
        return mpId;
    }

    /**
     * Setter.
     *
     * @param mpId {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#mpId}
     */
    public void setMpId(String mpId) {
        this.mpId = mpId;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#connectionId}
     */
    public String getConnectionId() {
        return connectionId;
    }

    /**
     * Setter.
     *
     * @param connectionId {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#connectionId}
     */
    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#schemaName}
     */
    public String getSchemaName() {
        return schemaName;
    }

    /**
     * Setter
     *
     * @param schemaName {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#requestJson}
     */
    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#requestJson}
     */
    public String getRequestJson() {
        return requestJson;
    }

    /**
     * Setter.
     *
     * @param requestJson {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#requestJson}
     */
    public void setRequestJson(String requestJson) {
        this.requestJson = requestJson;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#revRegId}
     */
    public String getRevRegId() {
        return revRegId;
    }

    /**
     * Setter
     *
     * @param revRegId {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#revRegId}
     */
    public void setRevRegId(String revRegId) {
        this.revRegId = revRegId;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#credRevId}
     */
    public String getCredRevId() {
        return credRevId;
    }

    /**
     * Setter
     *
     * @param credRevId {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#credRevId}
     */
    public void setCredRevId(String credRevId) {
        this.credRevId = credRevId;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#status}
     */
    public String getStatus() {
        return status;
    }

    /**
     * Setter
     *
     * @param status {@link com.visma.connect.hyper42.mandate.service.model.MandateRequest#status}
     */
    public void setStatus(String status) {
        this.status = status;
    }
}