/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The SchemaDefinition holds the data of schema's attributes for validation purposes
 *
 * @author Lukas Nakas
 */
@Document("schema_definition")
public class SchemaDefinition {

    /**
     * Holds Id
     */
    @Id
    @Field("_id")
    private String id;

    /**
     * Holds Schema Name
     */
    @Field("name")
    private String name;

    /**
     * Holds Schema definition
     */
    @Field(name = "definition")
    private String definition;

    /**
     * All args constructor
     *
     * @param id as string
     * @param name as string
     * @param definition as string
     */
    public SchemaDefinition(String id, String name, String definition) {
        this.id = id;
        this.name = name;
        this.definition = definition;
    }

    /**
     * Getter.
     *
     * @return {@link SchemaDefinition#id}
     */
    public String getId() {
        return id;
    }

    /**
     * Setter.
     *
     * @param id {@link SchemaDefinition#id}
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter.
     *
     * @return {@link SchemaDefinition#name}
     */
    public String getName() {
        return name;
    }

    /**
     * Setter.
     *
     * @param name {@link SchemaDefinition#name}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter.
     *
     * @return {@link SchemaDefinition#definition}
     */
    public String getDefinition() {
        return definition;
    }

    /**
     * Setter.
     *
     * @param definition {@link SchemaDefinition#definition}
     */
    public void setDefinition(String definition) {
        this.definition = definition;
    }
}