/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.rest.impl;

import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.common.CommonConstants;
import com.visma.connect.hyper42.mandate.service.model.generated.AuthorizationSchema;
import com.visma.connect.hyper42.mandate.service.model.generated.ConnectionAcceptRequestResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CreateInvitationResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefCreatedResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefinitionRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.DidResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.RevocationRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.DIDList;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PingRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PingRequestResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.SchemasCreatedResults;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationCreateRequestRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationExchange;
import com.visma.connect.hyper42.mandate.service.rest.CloudAgentRestClient;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Service Implementation class for CloudAgentRestClient
 *
 * @author Kevin Kerkhoven, Lukas Nakas
 */
@Service
public class CloudAgentRestClientImpl implements CloudAgentRestClient {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(CloudAgentRestClientImpl.class);
    /**
     * The resttemplate
     */
    @Autowired
    private RestTemplate restTemplate;
    /**
     * The aries agent service URL
     */
    @Value("${cloud.aries.agent.url}")
    private String agentUrl;

    @Override
    public CreateInvitationResponse createInvitation(String alias, boolean isAutoAccept, boolean isMultiUse, boolean isPublic) {
        URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/connections/create-invitation")
                .query("alias={alias}")
                .query("auto_accept={isAutoAccept}")
                .query("multi_use={isMultiUse}")
                .query("public={isPublic}")
                .build(alias, isAutoAccept, isMultiUse, isPublic);
        LOG.debug("Calling agent for create invitation with URI {}", targetUrl.toString());
        ResponseEntity<CreateInvitationResponse> restTemplateForEntity = restTemplate.postForEntity(targetUrl, null, CreateInvitationResponse.class);
        return restTemplateForEntity.getBody();
    }

    @Override
    public IssueCredentialSendResponse issueCredentialSend(IssueCredentialSendRequest issueCredentialSendRequest) {
        try {
            URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "issue-credential/send").build().toUri();
            LOG.debug("Calling agent with postForEntity with url {}", targetUrl);
            ResponseEntity<IssueCredentialSendResponse> restTemplateForEntity = restTemplate.postForEntity(targetUrl,
                    issueCredentialSendRequest, IssueCredentialSendResponse.class);
            if (!restTemplateForEntity.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Issuing credential finished unexpected");
            }
            return restTemplateForEntity.getBody();
        } catch (RestClientException e) {
            LOG.error("Exception occurred while issuing the credential ", e);
            throw new ApplicationRuntimeException("Something went wrong while issuing the credential.", e);
        }
    }

    @Override
    public DidResponse getPublicDidDetails() {
        try {
            URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "wallet/did/public").build().toUri();
            ResponseEntity<DidResponse> restTemplateForEntity = restTemplate.getForEntity(targetUrl, DidResponse.class);
            if (!restTemplateForEntity.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Getting Public Did Details finished unexpected");
            }
            return restTemplateForEntity.getBody();
        } catch (RestClientException e) {
            LOG.error("Exception occurred while getting public issuer did details ", e);
            throw new ApplicationRuntimeException("Something went wrong while getting the did details.", e);
        }
    }

    @Override
    public CredentialDefCreatedResponse getCredentialDefinitionCreated(String schemaName, String schemaVersion) {
        try {
            Map<String, String> paramsDef = new HashMap<>();
            paramsDef.put("schema_name", schemaName);
            paramsDef.put("schema_version", schemaVersion);
            MultiValueMap<String, String> parms = new LinkedMultiValueMap<>();
            parms.add("schema_name", schemaName);
            parms.add("schema_version", schemaVersion);
            URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/credential-definitions/created").queryParams(parms).build(paramsDef);
            ResponseEntity<CredentialDefCreatedResponse> restTemplateForEntity = restTemplate.getForEntity(targetUrl, CredentialDefCreatedResponse.class);
            if (!restTemplateForEntity.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Getting Credential Definition created finished unexpected");
            }
            return restTemplateForEntity.getBody();
        } catch (RestClientException e) {
            LOG.error("Exception occurred while getting credential definition ", e);
            throw new ApplicationRuntimeException("Something went wrong while getting the credential definition created.", e);
        }
    }

    @Override
    public SchemaResponse getSchema(String schemaId) {
        try {
            URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/schemas/{schema_id}").build(schemaId);
            ResponseEntity<SchemaResponse> restTemplateForEntity = restTemplate.getForEntity(targetUrl, SchemaResponse.class);
            if (!restTemplateForEntity.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Getting schema finished unexpected");
            }
            return restTemplateForEntity.getBody();
        } catch (RestClientException e) {
            LOG.error("Exception occurred while getting schema ", e);
            throw new ApplicationRuntimeException("Something went wrong while getting the schema.", e);
        }
    }

    @Override
    public SchemasCreatedResults getSchemaCreated(String schemaName, String schemaVersion) {
        try {
            MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
            params.add("schema_name", schemaName);
            params.add("schema_version", schemaVersion);
            URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/schemas/created").queryParams(params).build().toUri();
            ResponseEntity<SchemasCreatedResults> restTemplateForEntity = restTemplate.getForEntity(targetUrl, SchemasCreatedResults.class);
            if (!restTemplateForEntity.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Getting schema created finished unexpected");
            }
            return restTemplateForEntity.getBody();
        } catch (RestClientException e) {
            LOG.error("Exception occurred while getting schema ", e);
            throw new ApplicationRuntimeException("Something went wrong while getting the schema created.", e);
        }
    }

    @Override
    public ConnectionAcceptRequestResponse acceptConnectionRequest(String connectionId) {
        try {
            URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/connections/{conn_id}/accept-request").build(connectionId);
            LOG.debug("Calling agent with postForEntity with url {}", targetUrl);
            ResponseEntity<ConnectionAcceptRequestResponse> restTemplateForEntity = restTemplate.postForEntity(targetUrl, null,
                    ConnectionAcceptRequestResponse.class);
            ConnectionAcceptRequestResponse body = restTemplateForEntity.getBody();
            LOG.debug("New inviation created by agent {}", body);
            return body;
        } catch (RestClientException e) {
            throw new ApplicationRuntimeException("Something went wrong while accepting the connection request.", e);
        }
    }

    @Override
    public V10PresentationExchange createProofRequest(V10PresentationCreateRequestRequest request) {
        try {
            Map<String, String> vars = new HashMap<>();
            URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/present-proof/create-request").build(vars);
            ResponseEntity<V10PresentationExchange> restTemplateForEntity = restTemplate.postForEntity(targetUrl, request, V10PresentationExchange.class);
            LOG.debug("Proof created by cloud agent {}", restTemplateForEntity.getBody());
            return restTemplateForEntity.getBody();
        } catch (RestClientException e) {
            throw new ApplicationRuntimeException("Something went wrong while creating a proof request.", e);
        }
    }

    @Override
    public PingRequestResponse sendTrustPing(String connectionId, PingRequest pingRequest) {
        try {
            URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/connections/{conn_id}/send-ping").build(connectionId);
            LOG.debug("Calling agent with postForEntity with url {}", targetUrl);
            ResponseEntity<PingRequestResponse> restTemplateForEntity = restTemplate.postForEntity(targetUrl, pingRequest,
                    PingRequestResponse.class);
            PingRequestResponse body = restTemplateForEntity.getBody();
            LOG.debug("Ping send with result: {}", body);
            return body;
        } catch (RestClientException e) {
            throw new ApplicationRuntimeException("Something went wrong while sending a ping.", e);
        }
    }

    @Override
    public DIDList retrieveWalletDids() {
        try {
            URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/wallet/did").queryParam("posture", "wallet_only").build().toUri();
            LOG.debug("Calling agent with getForEntity with url {}", targetUrl);
            ResponseEntity<DIDList> responseEntity = restTemplate.getForEntity(targetUrl, DIDList.class);
            DIDList body = responseEntity.getBody();
            LOG.debug("Retrieved Dids: {}", body);
            return body;
        } catch (RestClientException e) {
            throw new ApplicationRuntimeException("Something went wrong while retrieving wallet dids.", e);
        }
    }

    @Override
    public SchemasCreatedResults getSchemasCreated() {
        try {
            URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/schemas/created").build().toUri();
            LOG.debug("Calling agent with getForEntity with url {}", targetUrl);
            ResponseEntity<SchemasCreatedResults> responseEntity = restTemplate.getForEntity(targetUrl, SchemasCreatedResults.class);
            SchemasCreatedResults body = responseEntity.getBody();
            LOG.debug("Retrieved Schemas: {}", body);
            return body;
        } catch (RestClientException e) {
            throw new ApplicationRuntimeException("Something went wrong while retrieving wallet dids.", e);
        }
    }

    @Override
    public String revokeCredential(RevocationRequest revocationRequest) {
        try {
            URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "revocation/revoke").build().toUri();
            LOG.debug("Calling agent with postForEntity with url {}", targetUrl);
            ResponseEntity<String> restTemplateForEntity = restTemplate.postForEntity(targetUrl, revocationRequest, String.class);
            if (!restTemplateForEntity.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Credential revocation finished unexpected");
            }
            return CommonConstants.RESPONSE_SUCCESS;
        } catch (RestClientException e) {
            LOG.error("Exception occurred while revoking the credential ", e);
            throw new ApplicationRuntimeException("Something went wrong while revoking the credential.", e);
        }
    }

    @Override
    public SchemaResponse createSchema(AuthorizationSchema authorizationSchema) {
        try {
            URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "schemas").build().toUri();
            LOG.debug("Calling agent with postForEntity with url {}", targetUrl);
            ResponseEntity<SchemaResponse> restTemplateForEntity = restTemplate.postForEntity(targetUrl, authorizationSchema, SchemaResponse.class);
            if (!restTemplateForEntity.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Something went wrong while creating the schema");
            }
            return restTemplateForEntity.getBody();
        } catch (HttpClientErrorException e) {
            LOG.warn("Exception occurred while creating schema ", e);
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Schema already exists on ledger, but attributes do not match." + e);
        } catch (RestClientException e) {
            LOG.warn("Exception occurred while creating schema ", e);
            throw new ApplicationRuntimeException("Something went wrong while creating the schema.", e);
        }
    }

    @Override
    public CredentialDefinitionResponse createCredentialDefinition(CredentialDefinitionRequest credentialDefinitionRequest) {
        try {
            ResponseEntity<CredentialDefinitionResponse> restTemplateForEntity = restTemplate.postForEntity(agentUrl + "credential-definitions",
                    credentialDefinitionRequest, CredentialDefinitionResponse.class);
            if (!restTemplateForEntity.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Creating credential definition finished unexpected");
            }
            return restTemplateForEntity.getBody();
        } catch (RestClientException e) {
            LOG.error("Exception occurred while creating schema ", e);
            throw new ApplicationRuntimeException("Something went wrong while creating the credential definition.", e);
        }
    }
}
