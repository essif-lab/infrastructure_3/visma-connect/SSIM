/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.dao;

import com.visma.connect.hyper42.mandate.service.model.Policy;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Dao class for storing policies into MongoDB.
 * 
 * @author Rik Sonderkamp
 */
@Repository
public interface PolicyDao extends MongoRepository<Policy, String> {

    /**
     * Get a policy by its id
     * 
     * @param policyId id of the policy
     * @return {@link Policy} object
     */
    Optional<Policy> findByPolicyId(String policyId);
}
