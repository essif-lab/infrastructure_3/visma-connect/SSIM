/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.rest;

/**
 * Rest client for the Open Policy Agent.
 * 
 * @author Rik Sonderkamp
 */
public interface OpenPolicyAgentRestClient {

    /**
     * Create or update a policy. This calls the PUT /v1/policies/{id} of OPA.
     * 
     * @param policyId id of the policy to create/update
     * @param policy content of the policy
     * @return error message when policy is invalid
     */
    String createOrUpdatePolicy(String policyId, String policy);

    /**
     * Perform a query using a query and input. This calls the POST /v1/data/dataPath of OPA.
     * 
     * @param dataPath path to the data, generally this is package followed by the variable (eg. mypackage/myvariable).
     * @param input data used for the request
     * @return result from OPA
     */
    String queryDataWithInput(String dataPath, String input);
}
