/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.mapper.impl;

import com.visma.connect.hyper42.mandate.service.mapper.SchemaMapper;
import com.visma.connect.hyper42.mandate.service.model.generated.Attribute_;
import com.visma.connect.hyper42.mandate.service.model.generated.Condition;
import com.visma.connect.hyper42.mandate.service.model.generated.Predicate;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.Attribute;
import com.visma.connect.hyper42.mandate.service.model.schema.Schema;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaRequest;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

/**
 * Implementation of SchemaMapper.
 *
 * @author Lukas Nakas
 */
@Component
public class SchemaMapperImpl implements SchemaMapper {

    @Override
    public Schema toSchema(String schemaId, AddSchemaRequest addSchemaRequest) {
        return new Schema(UUID.randomUUID().toString(),
                schemaId,
                addSchemaRequest.getName(),
                addSchemaRequest.getTitle(),
                addSchemaRequest.getVersion(),
                formEntityAttributes(addSchemaRequest.getAttributes()),
                formEntityPredicates(addSchemaRequest.getPredicates()),
                addSchemaRequest.getSsicommsEnabled());
    }

    @Override
    public SchemaDefinitionResponse toSchemaDefinitionResponseDto(Schema schema) {
        return new SchemaDefinitionResponse()
                .withSchemaId(schema.getSchemaId())
                .withName(schema.getName())
                .withVersion(schema.getVersion())
                .withTitle(schema.getTitle())
                .withAttributes(formDtoAttributes(schema.getAttributes()))
                .withPredicates(formDtoPredicates(schema.getPredicates()))
                .withSsicommsEnabled(schema.isSsicommsEnabled());
    }

    private List<Attribute> formEntityAttributes(List<Attribute_> attributes) {
        return attributes.stream()
                .map(attribute -> new Attribute(
                        attribute.getName(),
                        attribute.getTitle(),
                        attribute.getType()))
                .collect(Collectors.toList());
    }

    private List<com.visma.connect.hyper42.mandate.service.model.schema.Predicate> formEntityPredicates(List<Predicate> predicates) {
        return predicates.stream()
                .map(predicate -> new com.visma.connect.hyper42.mandate.service.model.schema.Predicate(
                        predicate.getName(),
                        predicate.getType(),
                        formEntityConditions(predicate.getCondition())))
                .collect(Collectors.toList());
    }

    private List<com.visma.connect.hyper42.mandate.service.model.schema.Condition> formEntityConditions(List<Condition> conditions) {
        return conditions.stream()
                .map(condition -> new com.visma.connect.hyper42.mandate.service.model.schema.Condition(
                        condition.getTitle(),
                        condition.getValue()))
                .collect(Collectors.toList());
    }

    private List<Attribute_> formDtoAttributes(List<Attribute> attributes) {
        return attributes.stream()
                .map(attribute -> new Attribute_()
                        .withName(attribute.getName())
                        .withTitle(attribute.getTitle())
                        .withType(attribute.getType()))
                .collect(Collectors.toList());
    }

    private List<Predicate> formDtoPredicates(List<com.visma.connect.hyper42.mandate.service.model.schema.Predicate> predicates) {
        return predicates.stream()
                .map(predicate -> new Predicate()
                        .withName(predicate.getName())
                        .withType(predicate.getType())
                        .withCondition(formDtoConditions(predicate.getConditions())))
                .collect(Collectors.toList());
    }

    private List<Condition> formDtoConditions(List<com.visma.connect.hyper42.mandate.service.model.schema.Condition> conditions) {
        return conditions.stream()
                .map(condition -> new Condition()
                        .withTitle(condition.getTitle())
                        .withValue(condition.getValue()))
                .collect(Collectors.toList());
    }

}
