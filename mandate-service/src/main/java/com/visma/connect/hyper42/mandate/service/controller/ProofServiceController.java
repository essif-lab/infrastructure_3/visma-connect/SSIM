/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.model.generated.ProofReceivedStatus;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PresentationRequestDict;
import com.visma.connect.hyper42.mandate.service.service.ProofInitService;
import com.visma.connect.hyper42.mandate.service.service.ProofResultService;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller of Proof Service
 *
 * @author Ragesh Shunmugam
 */
@RestController
@RequestMapping("/proofs")
public class ProofServiceController {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(ProofServiceController.class);

    /**
     * Proof start service for starting and getting the Proof
     */
    @Autowired
    private ProofInitService proofInitService;

    /**
     * Proof process service for handling the existing Proof.
     */
    @Autowired
    private ProofResultService proofResultService;

    /**
     * Object mapper.
     */
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Create a create proof request.
     *
     * @param schemaName - String
     * @param comment comment for this proof request
     * @param user user id
     * @param requestJson The requested mandate schema
     * @return CreateProofRequestResponse
     */
    @PostMapping("/{schemaName}")
    public ResponseEntity<String> createProofRequest(@PathVariable(name = "schemaName") String schemaName, @RequestParam(name = "comment") String comment,
            @RequestParam(name = "user") String user, @RequestBody String requestJson) {
        LOG.debug("Received proof request for schema {} and user {} with json {}", schemaName, user, requestJson);

        // schemaName contains a ":" and that char needs be be encoded if it is to be used in a path parameter.
        String unencodedSchemaName = URLDecoder.decode(schemaName, StandardCharsets.UTF_8);
        String unencodedComment = URLDecoder.decode(comment, StandardCharsets.UTF_8);

        return ResponseEntity.ok(proofInitService.createVerifierMandateRequest(unencodedSchemaName, unencodedComment, user, requestJson));
    }

    /**
     * Retrieve proof request.
     *
     * @param uuid - String
     * @return the ResponseEntity
     */
    @GetMapping("/{uuid}")
    public ResponseEntity<String> retrieveProofRequest(@PathVariable(name = "uuid") String uuid) {
        ResponseEntity<String> response = ResponseEntity.notFound().build();
        Optional<PresentationRequestDict> proofRequest = proofInitService.retrieveProofRequest(uuid);
        if (proofRequest.isPresent()) {
            response = encodeProofRequestToResponse(proofRequest.get());
        }
        return response;
    }

    /**
     * Retrieve proof request login's email.
     *
     * @param uuid - String
     * @return the ResponseEntity
     */
    @GetMapping("/login/{uuid}")
    public ResponseEntity<String> retrieveProofRequestLoginEmail(@PathVariable(name = "uuid") String uuid) {
        String proofRequestLoginEmail = proofResultService.retrieveProofRequestLoginEmail(uuid);
        LOG.info("Retrieved email -> {} for proof request ID -> {}", proofRequestLoginEmail, uuid);
        return ResponseEntity.ok(proofRequestLoginEmail);
    }

    /**
     * Process the proof that has been received.
     *
     * @param threadId - the id of the proof request.
     * @param state - the state of the proofrequest.
     * @param verified - the verification result of the proofrequest.
     * @param proof - the proof of the proofrequest, can be sent for different services for actions like logging in.
     * @return the ResponseEntity
     */
    @PatchMapping("/{threadId}")
    public ResponseEntity<ProofReceivedStatus> receiveProof(@PathVariable String threadId, @RequestParam String state,
            @RequestParam String verified, @RequestBody String proof) {
        LOG.info("Proof received with ID: {}  with state {} verified {}", threadId, state, verified);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Proof object for ID {}: {}", threadId, proof.replaceAll("[\n\r\t]", "_"));
        }
        proofResultService.processAndValidateProofReceived(threadId, state, verified, proof);

        return ResponseEntity.ok(new ProofReceivedStatus().withStatus("ok"));
    }

    private ResponseEntity<String> encodeProofRequestToResponse(PresentationRequestDict proofRequest) {
        ResponseEntity<String> response;
        try {
            byte[] encodeResponse = Base64.getEncoder().encode(objectMapper.writeValueAsString(proofRequest).getBytes(Charset.defaultCharset()));
            response = ResponseEntity.ok(new String(encodeResponse));
        } catch (JsonProcessingException e) {
            throw new ApplicationRuntimeException("Problem converting response to a jsonstring", e);
        }
        return response;
    }
}
