/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Model for a policy to be used for a MongoDB repository
 * 
 * @author Rik Sonderkamp
 */
public class Policy {

    /**
     * The technical id, a MongoDB Object id
     */
    @Id
    @Field("_id")
    private ObjectId id;

    /**
     * Id of the policy
     */
    @Indexed
    @Field("policyId")
    private String policyId;

    /**
     * Content of the policy
     */
    @Field("policyContent")
    private String policyContent;

    /**
     * Constructor with all fields
     * 
     * @param policyId policyId
     * @param policyContent content for the policy
     */
    public Policy(String policyId, String policyContent) {
        super();
        this.policyId = policyId;
        this.policyContent = policyContent;
    }

    /**
     * Getter for policy id
     * 
     * @return id
     */
    public String getPolicyId() {
        return policyId;
    }

    /**
     * Setter for policy id
     * 
     * @param policyId id
     */
    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    /**
     * Getter for the policy content
     * 
     * @return policy content
     */
    public String getPolicyContent() {
        return policyContent;
    }

    /**
     * Setter for the policy content
     * 
     * @param policyContent policy content
     */
    public void setPolicyContent(String policyContent) {
        this.policyContent = policyContent;
    }

}
