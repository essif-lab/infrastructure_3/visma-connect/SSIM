/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.dao.impl;

import com.visma.connect.hyper42.mandate.service.dao.MandateVerifierRequestBulkDao;
import com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

/**
 * Implementation of MandateVerifierRequestBulkDao.
 *
 * @author Micha Wensveen
 */
@Repository
public class MandateVerifierRequestBulkDaoImpl implements MandateVerifierRequestBulkDao {

    /**
     * Mongo template.
     */
    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void updateState(List<String> ids, String newState, Long timestamp) {
        BulkOperations bulkOps = mongoTemplate.bulkOps(BulkMode.UNORDERED, MandateVerifierRequest.class);
        ids.forEach(id -> bulkOps.updateOne(new Query(new Criteria("id").is(id)),
                new Update().set("state", newState).set("timestamp", timestamp)));
        bulkOps.execute();
    }

}
