/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.common.CommonConstants;
import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.dao.MandateRequestDao;
import com.visma.connect.hyper42.mandate.service.model.MandateRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.Attribute;
import com.visma.connect.hyper42.mandate.service.model.generated.ConnectionAcceptRequestResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CreateInvitationResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefCreatedResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialProposal;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.IssuedMandateResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.RevocationRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.RevokeCredentialRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PingRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PingRequestResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.SchemasCreatedResults;
import com.visma.connect.hyper42.mandate.service.rest.CloudAgentRestClient;
import com.visma.connect.hyper42.mandate.service.service.MandateService;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Implementation of the mandate api service
 *
 * @author Kevin Kerkhoven, Lukas Nakas
 */
@Service
public class MandateServiceImpl implements MandateService {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(com.visma.connect.hyper42.mandate.service.service.impl.MandateServiceImpl.class);

    /**
     * Instantiates mandateRequestRepository
     */
    @Autowired
    private MandateRequestDao mandateRequestDao;

    /**
     * The agent rest client
     */
    @Autowired
    private CloudAgentRestClient agentRestClient;

    /**
     * Instantiates ObjectMapper
     */
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Wait time before the secure ping is sent to the connection.
     */
    @Value("${ping.waittime}")
    private long pingWaitTime;

    /**
     * Implementation of create mandate request.
     * <p>
     * For now, we use the following values when creating a connection invitation:
     * auto_accept: true, will automatically accept the invitation from our agent
     * multi-use: false, can only be used once
     * public: false, invitation is private between agents
     *
     * @param alias random alias chosen by user
     * @param requestJson schema to add to db
     * @param schemaName name of schema
     * @param ssicomsEnabled value indicating is mandate is SSICom enabled or not
     * @return base64 URL for connection invitation
     */
    @Override
    public String saveMandateRequest(String alias, String requestJson, String schemaName, boolean ssicomsEnabled) {
        LOG.debug("Creating a new invitation for mandate with alias {}", alias);
        // Gather invitation for mandate
        CreateInvitationResponse invitationResponse = agentRestClient.createInvitation(alias, true, false, false);
        LOG.debug("Received invitation response for alias {} -> invitation ID: {}, url: {}.", alias, invitationResponse.getInvitation().getId(),
                invitationResponse.getInvitationUrl());
        LOG.debug("Invitation ==> {}", toJsonString(invitationResponse.getInvitation()));

        // add invitation ID, schemaName and requestJson to MongoDB
        LOG.debug("Saving mandate request for connection {}", invitationResponse.getConnectionId());
        MandateRequest request = formMandateRequest(invitationResponse.getConnectionId(), requestJson, schemaName);
        if (ssicomsEnabled) {
            request.setMpId(UUID.randomUUID().toString());
        }
        request = mandateRequestDao.save(request);

        if (null == request) {
            LOG.error("Exception occurred while inserting mandate data");
            throw new ApplicationRuntimeException("Could not create mandate request data");
        }
        return invitationResponse.getInvitationUrl();
    }

    @Override
    public String createMandateRequest(String connectionId) throws DataNotFoundException {
        LOG.debug("Creating a credential based on the saved mandate request belonging to connection {}", connectionId);
        Optional<MandateRequest> optionalRequest = Optional.empty();

        try {
            optionalRequest = mandateRequestDao.findByConnectionId(connectionId);
            if (optionalRequest.isPresent()) {
                IssueCredentialSendRequest issueCredentialSendRequest = formIssueCredentialRequest(optionalRequest.get(), connectionId);
                LOG.debug("Calling agent to issueCredential {}", issueCredentialSendRequest);
                IssueCredentialSendResponse issueCredentialSendResponse = agentRestClient.issueCredentialSend(issueCredentialSendRequest);
                if (null == issueCredentialSendResponse) {
                    throw new ApplicationRuntimeException("Exception occurred while issue credential");
                }
                LOG.debug("Agent responded with {}", issueCredentialSendResponse);
            } else {
                throw new DataNotFoundException("Connection Id " + connectionId + " doesnt exists");
            }
        } catch (ApplicationRuntimeException | JsonProcessingException e) {
            throw new ApplicationRuntimeException("Exception occurred while retrieving data", e);
        }
        return CommonConstants.RESPONSE_SUCCESS;
    }

    @Override
    public String acceptConnectionRequest(String connectionId) {
        LOG.info("Accepting connection request {}", connectionId);
        ConnectionAcceptRequestResponse response = agentRestClient.acceptConnectionRequest(connectionId);
        return toJsonString(response);
    }

    @Override
    public String pingConnection(String connectionId) {
        LOG.info("Pinging connection {}", connectionId);
        try {
            // wait 20 MS for the wallet to process the connection.
            Thread.sleep(pingWaitTime);
        } catch (InterruptedException e) {
            // do nothing
        }
        PingRequest pingRequest = new PingRequest().withComment("We will send the mandate now");
        PingRequestResponse response = agentRestClient.sendTrustPing(connectionId, pingRequest);
        return toJsonString(response);
    }

    @Override
    public String enrichCredentialDetails(String connectionId, IssueCredentialSendResponse issueCredentialSendResponse)
            throws DataNotFoundException {
        // Add state, revocation registry ID and credential revocation ID to MongoDB
        LOG.debug("Updating mandate request for connection {} with state {}, revocation registry ID {} and revocation ID {}",
                connectionId,
                issueCredentialSendResponse.getState(),
                issueCredentialSendResponse.getRevocRegId(),
                issueCredentialSendResponse.getRevocationId());
        String jsonResponse = "";
        Optional<MandateRequest> optionalRequest = mandateRequestDao.findByConnectionId(connectionId);
        if (optionalRequest.isPresent()) {
            MandateRequest updatedCredential = updateCredentialDetails(issueCredentialSendResponse, optionalRequest.get());
            jsonResponse = toJsonString(updatedCredential);
        }
        return jsonResponse;
    }

    @Override
    public String revokeCredential(RevokeCredentialRequest revokeCredentialRequest) throws DataNotFoundException {
        LOG.debug("Revoking credential with json {}", revokeCredentialRequest);

        RevocationRequest revocationRequest = formRevocationRequest(revokeCredentialRequest);
        LOG.debug("Calling agent to revokeCredential {}", revocationRequest);
        String result = agentRestClient.revokeCredential(revocationRequest);
        if (!result.equals(CommonConstants.RESPONSE_SUCCESS)) {
            throw new ApplicationRuntimeException("Exception occurred while revoking credential");
        }
        LOG.debug("Revoked credential with revocation ID {} and revocation registry ID {}", revocationRequest.getCredRevId(), revocationRequest.getRevRegId());

        Optional<MandateRequest> mandateRequest =
                mandateRequestDao.findByCredRevIdAndRevRegId(revocationRequest.getCredRevId(), revocationRequest.getRevRegId());
        mandateRequest.ifPresent(request -> updateCredentialStatus(request, "revoked"));

        return CommonConstants.RESPONSE_SUCCESS;
    }

    @Override
    public List<IssuedMandateResponse> retrieveIssuedMandates() {
        LOG.debug("Getting all mandate requests from mongo with status 'issued'");
        List<MandateRequest> mandateRequests = mandateRequestDao.findAllByStatusEquals("issued");
        return mandateRequests.stream()
                .map(this::formIssuedMandateResponse)
                .collect(Collectors.toList());
    }

    @Override
    public String createConnectionInvitation(String mpId) throws DataNotFoundException {
        MandateRequest mandateRequest = mandateRequestDao.findByMpId(mpId)
                .orElseThrow(() -> new DataNotFoundException(String.format("MandateRequest with ID %s not found", mpId)));
        String alias = mandateRequest.getSchemaName() + "_" + Instant.now().toString();
        LOG.info("Creating a new invitation for mandate with mpId {}, alias {}", mpId, alias);
        // Gather invitation for mandate
        CreateInvitationResponse invitationResponse = agentRestClient.createInvitation(alias, true, false, false);
        LOG.info("Received invitation response for alias {} -> invitation ID: {}, invitation: {}.", alias, invitationResponse.getInvitation().getId(),
                invitationResponse.getInvitation());

        // Add connection ID to existing Mandate Request
        LOG.info("Appending mandate request with connection ID {}", invitationResponse.getConnectionId());
        mandateRequest.setConnectionId(invitationResponse.getConnectionId());
        mandateRequestDao.save(mandateRequest);

        try {
            return objectMapper.writeValueAsString(invitationResponse.getInvitation());
        } catch (JsonProcessingException e) {
            throw new ApplicationRuntimeException("Exception occurred while retrieving data", e);
        }
    }

    private IssuedMandateResponse formIssuedMandateResponse(MandateRequest mandateRequest) {
        return new IssuedMandateResponse()
                .withSchemaName(mandateRequest.getSchemaName())
                .withConnectionId(mandateRequest.getConnectionId())
                .withRequest(mandateRequest.getRequestJson())
                .withCredRevId(mandateRequest.getCredRevId())
                .withRevRegId(mandateRequest.getRevRegId());
    }

    private MandateRequest updateCredentialDetails(IssueCredentialSendResponse issueCredentialSendResponse, MandateRequest mandateRequest) {
        mandateRequest.setRevRegId(issueCredentialSendResponse.getRevocRegId());
        mandateRequest.setCredRevId(issueCredentialSendResponse.getRevocationId());
        return updateCredentialStatus(mandateRequest, "issued");
    }

    private MandateRequest updateCredentialStatus(MandateRequest mandateRequest, String status) {
        mandateRequest.setStatus(status);
        LOG.debug("Updating credential's {} status to {}", mandateRequest.getConnectionId(), status);
        return mandateRequestDao.save(mandateRequest);
    }

    private MandateRequest formMandateRequest(String connectionId, String requestJson, String schemaName) {
        return new MandateRequest(UUID.randomUUID().toString(), connectionId, schemaName, requestJson);
    }

    private IssueCredentialSendRequest formIssueCredentialRequest(MandateRequest mandateRequest, String connectionId) throws JsonProcessingException {
        IssueCredentialSendRequest issueCredentialSendRequest = new IssueCredentialSendRequest();
        String[] schema = mandateRequest.getSchemaName().split(CommonConstants.SCHEMA_SPLIT);
        LOG.info("Creating credentials for {} with schema {}", connectionId, schema);
        // Get Issuer Did
        String issuerDid = agentRestClient.getPublicDidDetails().getResult().getDid();

        issueCredentialSendRequest.setAutoRemove(false);
        issueCredentialSendRequest.setTrace(false);
        issueCredentialSendRequest.setIssuerDid(issuerDid);
        issueCredentialSendRequest.setConnectionId(connectionId);
        // Form Credential Proposal
        CredentialProposal credentialProposal = new CredentialProposal();
        credentialProposal.setType(CommonConstants.CREDENTIAL_PROPOSAL_TYPE);
        credentialProposal.setAttributes(toIssueCredentialRequestAttributes(mandateRequest.getRequestJson()));
        issueCredentialSendRequest.setCredentialProposal(credentialProposal);
        // Get Schema Details
        SchemasCreatedResults schemaResponse = agentRestClient.getSchemaCreated(schema[0], schema[1]);
        if (schemaResponse == null || schemaResponse.getSchemaIds() == null || schemaResponse.getSchemaIds().isEmpty()) {
            throw new ApplicationRuntimeException("No schema found for " + schema[0] + " " + schema[1]);
        }
        issueCredentialSendRequest.setSchemaId(schemaResponse.getSchemaIds().get(0));
        issueCredentialSendRequest.setSchemaVersion(schema[1]);
        issueCredentialSendRequest.setSchemaName(schema[0]);
        issueCredentialSendRequest.setSchemaIssuerDid(issuerDid);
        // Get Credential Definition Details
        CredentialDefCreatedResponse credentialDefCreatedResponse = agentRestClient.getCredentialDefinitionCreated(schema[0], schema[1]);
        if (credentialDefCreatedResponse == null || credentialDefCreatedResponse.getCredentialDefinitionIds() == null
                || credentialDefCreatedResponse.getCredentialDefinitionIds().isEmpty()) {
            throw new ApplicationRuntimeException("No credential definition found for " + schema[0] + " " + schema[1]);
        }
        issueCredentialSendRequest.setCredDefId(credentialDefCreatedResponse.getCredentialDefinitionIds().get(0));
        return issueCredentialSendRequest;
    }

    private List<Attribute> toIssueCredentialRequestAttributes(String requestJson) throws JsonProcessingException {
        JsonNode jsonNode = objectMapper.readTree(requestJson);
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(jsonNode.fieldNames(), Spliterator.ORDERED), false)
                .map(s -> formAttributes(s, jsonNode))
                .collect(Collectors.toList());
    }

    private Attribute formAttributes(String key, JsonNode jsonNode) {
        return new Attribute().withName(key).withValue(jsonNode.get(key).asText());
    }

    private RevocationRequest formRevocationRequest(RevokeCredentialRequest revokeCredentialRequest) {
        return new RevocationRequest()
                .withCredRevId(revokeCredentialRequest.getCredRevId())
                .withRevRegId(revokeCredentialRequest.getRevRegId())
                .withPublish(true);
    }

    private String toJsonString(Object response) {
        try {
            return objectMapper.writeValueAsString(response);
        } catch (JsonProcessingException e) {
            LOG.warn("Cannot convert to json {}", response);
            throw new ApplicationRuntimeException("Cannot convert to json", e);
        }
    }

}
