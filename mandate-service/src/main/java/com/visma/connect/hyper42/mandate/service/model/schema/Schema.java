/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.model.schema;

import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The Schema holds the data for requesting detailed information about available schemas
 *
 * @author Lukas Nakas
 */
@Document("schema")
@CompoundIndex(def = "{'name': 1, 'version': 1}", name = "name_version_index")
public class Schema {

    /**
     * Holds Id
     */
    @Id
    @Field("_id")
    private String id;

    /**
     * Holds Schema Id
     */
    @Field("schemaId")
    private String schemaId;

    /**
     * Holds Schema Name
     */
    @Field(name = "name")
    private String name;

    /**
     * Holds Schema Title
     */
    @Field(name = "title")
    private String title;

    /**
     * Holds Schema Version
     */
    @Field(name = "version")
    private String version;

    /**
     * Holds Schema Attributes
     */
    @Field(name = "attributes")
    private List<Attribute> attributes;

    /**
     * Holds Schema Predicates
     */
    @Field(name = "predicates")
    private List<Predicate> predicates;

    /**
     * Holds Schema Indicator about it being SSIComms enabled
     */
    @Field(name = "ssicommsEnabled")
    private boolean ssicommsEnabled;

    /**
     * All args constructor
     *
     * @param id as string
     * @param schemaId as string
     * @param name as string
     * @param title as string
     * @param version as string
     * @param attributes as list of Attributes
     * @param predicates as list of Predicates
     * @param ssicommsEnabled as boolean
     */
    public Schema(String id, String schemaId, String name, String title, String version, List<Attribute> attributes, List<Predicate> predicates,
            boolean ssicommsEnabled) {
        this.id = id;
        this.schemaId = schemaId;
        this.name = name;
        this.title = title;
        this.version = version;
        this.attributes = attributes;
        this.predicates = predicates;
        this.ssicommsEnabled = ssicommsEnabled;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#id}
     */
    public String getId() {
        return id;
    }

    /**
     * Setter.
     *
     * @param id {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#id}
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#schemaId}
     */
    public String getSchemaId() {
        return schemaId;
    }

    /**
     * Setter.
     *
     * @param schemaId {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#schemaId}
     */
    public void setSchemaId(String schemaId) {
        this.schemaId = schemaId;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#name}
     */
    public String getName() {
        return name;
    }

    /**
     * Setter.
     *
     * @param name {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#name}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#title}
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter.
     *
     * @param title {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#title}
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#version}
     */
    public String getVersion() {
        return version;
    }

    /**
     * Setter.
     *
     * @param version {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#version}
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#attributes}
     */
    public List<Attribute> getAttributes() {
        return attributes;
    }

    /**
     * Setter.
     *
     * @param attributes {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#attributes}
     */
    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#predicates}
     */
    public List<Predicate> getPredicates() {
        return predicates;
    }

    /**
     * Setter.
     *
     * @param predicates {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#predicates}
     */
    public void setPredicates(List<Predicate> predicates) {
        this.predicates = predicates;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#ssicommsEnabled}
     */
    public boolean isSsicommsEnabled() {
        return ssicommsEnabled;
    }

    /**
     * Setter.
     *
     * @param ssicommsEnabled {@link com.visma.connect.hyper42.mandate.service.model.schema.Schema#ssicommsEnabled}
     */
    public void setSsicommsEnabled(boolean ssicommsEnabled) {
        this.ssicommsEnabled = ssicommsEnabled;
    }
}
