/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.controller;

import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.IssuedMandateResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.RevokeCredentialRequest;
import com.visma.connect.hyper42.mandate.service.service.MandateService;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller of Mandate Service
 *
 * @author Kevin Kerkhoven, Lukas Nakas
 */
@RestController
@RequestMapping("/mandates")
public class MandateServiceController {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(MandateServiceController.class);

    /**
     * Instantiate Mandate Api Service
     */
    @Autowired
    private MandateService mandateService;

    /**
     * Create a mandate invitation request.
     *
     * @param schemaName - String
     * @param alias The alias of the user for whom we create the invite
     * @param ssicommsEnabled value indicating is mandate is SSIComms enabled or not
     * @param requestJson The requested mandate schema
     * @return base64 String of QR-code image
     */
    @PostMapping("/{schemaName}")
    public ResponseEntity<String> createNewMandateRequest(@PathVariable String schemaName, @RequestParam String alias, @RequestParam Boolean ssicommsEnabled,
            @RequestBody String requestJson) {
        ResponseEntity<String> response;
        LOG.debug("Received mandate request for alias {}, schema {}, ssicommsEnabled {} and json {}", alias, schemaName, ssicommsEnabled, requestJson);

        // schemaName contains a ":" and that char needs be encoded if it is to be used in a path parameter.
        String unencodedSchemaName = URLDecoder.decode(schemaName, StandardCharsets.UTF_8);

        // Gather new invitation for mandate and store data in MongoDB
        String invitationUrl = mandateService.saveMandateRequest(alias, requestJson, unencodedSchemaName, ssicommsEnabled);
        response = ResponseEntity.status(HttpStatus.CREATED).body(invitationUrl);
        LOG.debug("Mandate saved and invitation created for alias {}", alias);
        return response;
    }

    /**
     * Get Mandate Request Json based on given connectionId.
     *
     * @param connectionId - The id of the connection in wallet
     * @param state - The state of the connection. request or active.
     * @return requestJson as String
     * @throws DataNotFoundException the data not found exception
     */
    @GetMapping("/{connectionId}")
    public ResponseEntity<String> processConnection(@PathVariable String connectionId, @RequestParam(name = "state") String state)
            throws DataNotFoundException {
        LOG.debug("process connection with connectionId {} and state {}", connectionId, state);
        String responseJson = "";
        if ("request".equals(state)) {
            responseJson = mandateService.acceptConnectionRequest(connectionId);
        } else if ("response".equals(state)) {
            responseJson = mandateService.pingConnection(connectionId);
        } else if ("active".equals(state)) {
            // Gather new QR connection invitation for mandate and store data in MongoDB
            responseJson = mandateService.createMandateRequest(connectionId);
        } else if ("ssicomms".equals(state)) {
            // Get new QR connection invitation for SSIComms enabled mandate by MP-ID
            responseJson = mandateService.createConnectionInvitation(connectionId);
        }
        return ResponseEntity.status(HttpStatus.OK).body(responseJson);
    }

    /**
     * Update credential details after issuance.
     *
     * @param connectionId - String
     * @param requestJson - Object (received data about issued credential)
     */
    @PutMapping("/{connectionId}")
    public ResponseEntity<String> updateCredentialDetails(@PathVariable String connectionId, @RequestBody IssueCredentialSendResponse requestJson)
            throws DataNotFoundException {
        LOG.debug("Received Issued Credential for connectionId {}, with revocationId {}, registry revocation Id {} and state {}",
                connectionId, requestJson.getRevocationId(), requestJson.getRevocRegId(), requestJson.getState());

        // Update saved credential data with revocation details and new state and store it in MongoDB
        mandateService.enrichCredentialDetails(connectionId, requestJson);
        return ResponseEntity.ok("ok");
    }

    /**
     * Revoke credential.
     *
     * @param revokeCredentialRequest The requested credential revocation details
     * @return String of revocation status
     */
    @PostMapping("/revoke")
    public ResponseEntity<String> revokeCredential(@RequestBody RevokeCredentialRequest revokeCredentialRequest) throws DataNotFoundException {
        ResponseEntity<String> response;
        LOG.debug("Received credential revocation request for json {}", revokeCredentialRequest);

        // Revoke existing credential by using its revocation ID and revocation registry ID
        String result = mandateService.revokeCredential(revokeCredentialRequest);
        response = ResponseEntity.status(HttpStatus.OK).body(result);
        LOG.debug("Credential revoked for revocation details {}", revokeCredentialRequest);

        return response;
    }

    /**
     * Retrieve list of mandate request with status 'issued'
     *
     * @return List of IssuedMandateResponse
     */
    @GetMapping("/issued")
    public ResponseEntity<List<IssuedMandateResponse>> getIssuedMandates() {
        LOG.debug("Retrieving list of all issued credentials");
        ResponseEntity<List<IssuedMandateResponse>> response = ResponseEntity.ok(new ArrayList<>());
        List<IssuedMandateResponse> issuedMandateResponses = mandateService.retrieveIssuedMandates();
        LOG.debug("Retrieved issued credentials ({}): {}", issuedMandateResponses.size(), issuedMandateResponses);
        if (!issuedMandateResponses.isEmpty()) {
            response = ResponseEntity.ok(issuedMandateResponses);
        }
        return response;
    }
}
