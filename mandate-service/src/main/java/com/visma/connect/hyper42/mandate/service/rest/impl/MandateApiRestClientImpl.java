/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.rest.impl;

import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.model.generated.ProofState;
import com.visma.connect.hyper42.mandate.service.rest.MandateApiRestClient;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Implementation of MandateApiRestClient.
 *
 * @author Micha Wensveen
 */
@Service
public class MandateApiRestClientImpl implements MandateApiRestClient {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(MandateApiRestClientImpl.class);

    /**
     * The resttemplate
     */
    @Autowired
    private RestTemplate restTemplate;

    /**
     * Mandate api url.
     */
    @Value("${mandate.api.url}")
    private String mandateApiUrl;

    @Override
    public String updateProofState(ProofState proofState) {
        LOG.info("Update to API with proofstate: {}", proofState);
        try {
            URI targetUrl = UriComponentsBuilder.fromUriString(mandateApiUrl + "/internal/proofs/{id}").build(proofState.getId());
            HttpEntity<ProofState> request = new HttpEntity<>(proofState);
            ResponseEntity<String> result = restTemplate.exchange(targetUrl, HttpMethod.PATCH, request, String.class);
            if (!result.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Problem with sending proof state to API");
            }
            return result.getBody();
        } catch (RestClientException e) {
            throw new ApplicationRuntimeException("Something went wrong while sending proof state to API.", e);
        }
    }

}
