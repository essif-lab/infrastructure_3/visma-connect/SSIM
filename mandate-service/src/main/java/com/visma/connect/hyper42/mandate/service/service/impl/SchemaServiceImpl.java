/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.saasquatch.jsonschemainferrer.JsonSchemaInferrer;
import com.saasquatch.jsonschemainferrer.RequiredPolicies;
import com.saasquatch.jsonschemainferrer.SpecVersion;
import com.saasquatch.jsonschemainferrer.TitleDescriptionGenerators;
import com.visma.connect.hyper42.mandate.service.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.service.common.CommonConstants;
import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.dao.SchemaDao;
import com.visma.connect.hyper42.mandate.service.dao.SchemaDefinitionDao;
import com.visma.connect.hyper42.mandate.service.mapper.SchemaMapper;
import com.visma.connect.hyper42.mandate.service.model.SchemaDefinition;
import com.visma.connect.hyper42.mandate.service.model.generated.Attribute_;
import com.visma.connect.hyper42.mandate.service.model.generated.AuthorizationSchema;
import com.visma.connect.hyper42.mandate.service.model.generated.AvailableSchemasResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefinitionRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.Schema;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaResponse;
import com.visma.connect.hyper42.mandate.service.rest.CloudAgentRestClient;
import com.visma.connect.hyper42.mandate.service.service.SchemaService;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of SchemaService.
 *
 * @author Micha Wensveen, Lukas Nakas
 */
@Service
public class SchemaServiceImpl implements SchemaService {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(SchemaServiceImpl.class);

    /**
     * Login Schema Name used for filtering it out.
     */
    private static final String LOGIN_SCHEMA = "mandate-login-schema";

    /**
     * Instantiates schemaRepository
     */
    @Autowired
    private SchemaDao schemaDao;

    /**
     * Instantiates schemaDefinitionRepository
     */
    @Autowired
    private SchemaDefinitionDao schemaDefinitionDao;

    /**
     * Instantiates schemaMapper
     */
    @Autowired
    private SchemaMapper schemaMapper;

    /**
     * Instantiates ObjectMapper
     */
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * The agent rest client
     */
    @Autowired
    private CloudAgentRestClient agentRestClient;

    /**
     * Json Schema Inferrer for transforming request data into jsonSchema
     */
    private final JsonSchemaInferrer inferrer = JsonSchemaInferrer.newBuilder()
            .setSpecVersion(SpecVersion.DRAFT_07)
            .setRequiredPolicy(RequiredPolicies.nonNullCommonFields())
            .setTitleDescriptionGenerator(TitleDescriptionGenerators.useFieldNamesAsTitles())
            .build();

    @Override
    public List<AvailableSchemasResponse> getAllCreatedSchema() {
        LOG.debug("Getting all created schemas from mongo");
        List<Schema> schemas = schemaDao.findAll();
        return schemas.stream()
                .map(this::formSchemaTitleResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<AvailableSchemasResponse> getAllUserSchema() {
        return getAllCreatedSchema().stream()
                .filter(schema -> !schema.getName().equals(LOGIN_SCHEMA))
                .collect(Collectors.toList());
    }

    @Override
    public SchemaDefinitionResponse getSchemaDefinition(String schemaId) {
        LOG.debug("Getting schema definition from mongo by schemaId {}", schemaId);
        Optional<Schema> schemaOptional = schemaDao.findBySchemaId(schemaId);
        return schemaOptional.map(schemaMapper::toSchemaDefinitionResponseDto).orElse(null);
    }

    @Override
    public AddSchemaResponse addSchema(AddSchemaRequest addSchemaRequest) {
        AddSchemaResponse response;
        Optional<Schema> schemaOptional = schemaDao.findByNameAndVersion(addSchemaRequest.getName(), addSchemaRequest.getVersion());

        if (schemaOptional.isPresent()) {
            LOG.debug("Schema with name {} and version {} already exists", addSchemaRequest.getName(), addSchemaRequest.getVersion());
            response = new AddSchemaResponse().withTitle(addSchemaRequest.getTitle()).withStatus("already_exists");
        } else {
            try {
                SchemaResponse schemaResponse = createSchema(addSchemaRequest);
                LOG.debug("Created schema ID: {}", schemaResponse.getSchemaId());

                CredentialDefinitionResponse credentialDefinitionResponse = createCredentialDefinition(schemaResponse);
                LOG.debug("Created credential definition ID: {}", credentialDefinitionResponse.getCredentialDefinitionId());

                SchemaDefinition savedSchemaDefinition = saveSchemaJsonDefinition(addSchemaRequest);
                LOG.debug("Schema JSON definition created: {}", savedSchemaDefinition.getDefinition());

                LOG.debug("Saving new schema ({}) to mongo", addSchemaRequest.getName());
                Schema schema = schemaMapper.toSchema(schemaResponse.getSchemaId(), addSchemaRequest);
                Schema savedSchema = schemaDao.save(schema);

                response = new AddSchemaResponse().withTitle(addSchemaRequest.getTitle()).withSchemaId(savedSchema.getSchemaId()).withStatus("ok");
            } catch (IllegalArgumentException e) {
                LOG.debug("Failed to add new schema ({}), {}", addSchemaRequest.getName(), e);
                response = new AddSchemaResponse().withTitle(addSchemaRequest.getTitle()).withStatus("failed");
            }
        }
        return response;
    }

    @Override
    public String getSchemaJsonDefinition(String name) throws DataNotFoundException {
        return schemaDefinitionDao.findByName(name)
                .orElseThrow(() -> new DataNotFoundException("Name " + name + " doesnt exist"))
                .getDefinition();
    }

    private SchemaDefinition saveSchemaJsonDefinition(AddSchemaRequest addSchemaRequest) {
        String schemaJson = "{" + formPropertiesForJsonSchema(addSchemaRequest) + "}";
        try {
            JsonNode attributesNode = objectMapper.readTree(schemaJson);
            JsonNode schemaDefinitionNode = inferrer.inferForSample(attributesNode);

            SchemaDefinition schemaDefinition = new SchemaDefinition(
                    UUID.randomUUID().toString(),
                    addSchemaRequest.getName() + ":" + addSchemaRequest.getVersion(),
                    schemaDefinitionNode.toString());

            return schemaDefinitionDao.save(schemaDefinition);
        } catch (JsonProcessingException e) {
            LOG.info("Cannot parse json. Failed to create schema JSON definition. ", e);
            throw new ApplicationRuntimeException("Failed to create schema JSON definition for schema " + addSchemaRequest.getName() + ". " + e);
        }
    }

    private String formPropertiesForJsonSchema(AddSchemaRequest addSchemaRequest) {
        return addSchemaRequest.getAttributes().stream()
                .map(attr -> "\"" + attr.getName() + "\":" + "\"string\"")
                .collect(Collectors.joining(","));
    }

    private SchemaResponse createSchema(AddSchemaRequest addSchemaRequest) {
        LOG.debug("Creating schema {} on ledger", addSchemaRequest.getName());
        AuthorizationSchema authorizationSchema = formAuthorizationSchema(addSchemaRequest);
        return agentRestClient.createSchema(authorizationSchema);
    }

    private CredentialDefinitionResponse createCredentialDefinition(SchemaResponse schemaResponse) {
        LOG.debug("Creating credential definition on ledger for schema ID: {}", schemaResponse.getSchemaId());
        CredentialDefinitionRequest credentialDefinitionRequest = formCredentialDefinitionRequest(schemaResponse);
        return agentRestClient.createCredentialDefinition(credentialDefinitionRequest);
    }

    private AvailableSchemasResponse formSchemaTitleResponse(Schema schema) {
        return new AvailableSchemasResponse()
                .withTitle(schema.getTitle())
                .withSchemaId(schema.getSchemaId())
                .withName(schema.getName())
                .withSsicommsEnabled(schema.isSsicommsEnabled());
    }

    private AuthorizationSchema formAuthorizationSchema(AddSchemaRequest addSchemaRequest) {
        return new AuthorizationSchema()
                .withSchemaName(addSchemaRequest.getName())
                .withSchemaVersion(addSchemaRequest.getVersion())
                .withAttributes(formNewSchemaAttributes(addSchemaRequest.getAttributes()));
    }

    private List<String> formNewSchemaAttributes(List<Attribute_> attributes) {
        return attributes.stream()
                .map(Attribute_::getName)
                .collect(Collectors.toList());
    }

    private CredentialDefinitionRequest formCredentialDefinitionRequest(SchemaResponse schemaResponse) {
        return new CredentialDefinitionRequest()
                .withSchemaId(schemaResponse.getSchemaId())
                .withRevocationRegistrySize(CommonConstants.REVOCATION_REGISTRY_SIZE)
                .withSupportRevocation(CommonConstants.SUPPORT_REVOCATION)
                .withTag(CommonConstants.TAG);
    }
}
