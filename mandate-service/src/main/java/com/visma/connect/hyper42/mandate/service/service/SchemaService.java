/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service;

import com.visma.connect.hyper42.mandate.service.common.DataNotFoundException;
import com.visma.connect.hyper42.mandate.service.model.generated.AvailableSchemasResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddSchemaResponse;
import java.util.List;

/**
 * The Service that handles the businesslogic for the schema's on the indy network.
 *
 * @author Micha Wensveen, Lukas Nakas
 */
public interface SchemaService {

    /**
     * Get a list of all schema's created by the mandate application.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.service.SchemaService#allCreatedSchema}
     */
    List<AvailableSchemasResponse> getAllCreatedSchema();

    /**
     * Get a list of all schemas excluding a set of non-user schemas (ex. login schema).
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.service.SchemaService#allUserSchema}
     */
    List<AvailableSchemasResponse> getAllUserSchema();

    /**
     * Get a schema definition.
     *
     * @param schemaId - String
     * @return {@link com.visma.connect.hyper42.mandate.service.service.SchemaService#getSchemaDefinition(String)}
     */
    SchemaDefinitionResponse getSchemaDefinition(String schemaId);

    /**
     * Add new schema.
     *
     * @param addSchemaRequest - Object
     * @return {@link com.visma.connect.hyper42.mandate.service.service.SchemaService#addSchema(AddSchemaRequest)}
     */
    AddSchemaResponse addSchema(AddSchemaRequest addSchemaRequest);

    /**
     * Get a schema JSON definition.
     *
     * @param name - String
     * @return {@link com.visma.connect.hyper42.mandate.service.service.SchemaService#getSchemaJsonDefinition(String)}
     */
    String getSchemaJsonDefinition(String name) throws DataNotFoundException;
}
