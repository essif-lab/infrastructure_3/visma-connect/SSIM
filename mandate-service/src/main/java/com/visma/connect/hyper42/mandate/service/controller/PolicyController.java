/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.controller;

import com.visma.connect.hyper42.mandate.service.model.schema.generated.AddPolicyResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.GetPolicyResponse;
import com.visma.connect.hyper42.mandate.service.service.PolicyService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller class for managing policies.
 * 
 * @author Rik Sonderkamp
 */
@RestController
@RequestMapping("/policies")
public class PolicyController {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PolicyController.class);

    /**
     * the policy service
     */
    @Autowired
    private PolicyService policyService;

    /**
     * Create or update a policy
     *
     * @param policyId id of the policy
     * @param body request body with the policy content
     * @return {@link AddPolicyResponse} object with the policy id
     */
    @PutMapping("/{policyId}")
    public AddPolicyResponse createOrUpdatePolicy(@PathVariable("policyId") String policyId, @RequestBody String body) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Create or update policy with id: {}", policyId.replaceAll("[\n\r\t]", "_"));
        }
        String result = policyService.createOrUpdatePolicy(policyId, body);
        AddPolicyResponse response = new AddPolicyResponse();
        response.setId(result);
        return response;
    }

    /**
     * Get a policy by its ID
     * 
     * @param policyId id of the policy
     * @return {@link GetPolicyResponse} object with the policy found
     */
    @GetMapping("/{policyId}")
    public ResponseEntity<GetPolicyResponse> getPolicy(@PathVariable("policyId") String policyId) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Get policy with id: {}", policyId.replaceAll("[\n\r\t]", "_"));
        }
        Optional<String> policy = policyService.getPolicyContentById(policyId);
        if (policy.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        GetPolicyResponse response = new GetPolicyResponse();
        response.setId(policyId);
        response.setContent(policy.get());
        return ResponseEntity.ok(response);
    }
}
