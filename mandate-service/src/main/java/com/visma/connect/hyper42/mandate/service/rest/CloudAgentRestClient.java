/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.rest;

import com.visma.connect.hyper42.mandate.service.model.generated.AuthorizationSchema;
import com.visma.connect.hyper42.mandate.service.model.generated.ConnectionAcceptRequestResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CreateInvitationResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefCreatedResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefinitionRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.CredentialDefinitionResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.DidResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.IssueCredentialSendResponse;
import com.visma.connect.hyper42.mandate.service.model.generated.RevocationRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.SchemaResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.DIDList;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PingRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.PingRequestResponse;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.SchemasCreatedResults;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationCreateRequestRequest;
import com.visma.connect.hyper42.mandate.service.model.schema.generated.V10PresentationExchange;

/**
 * Cloud Agent Rest Client Interface for invitations
 *
 * @author Kevin Kerkhoven, Lukas Nakas
 */
public interface CloudAgentRestClient {

    /**
     * Create an invitation via our agent for the user
     *
     * @param alias alias chosen by user
     * @param isAutoAccept whether our agent will accept automatically
     * @param isMultiUse whether the invitation of for multiple uses
     * @param isPublic whether the invitation is public
     * @return CreateInvitationResponse
     */
    CreateInvitationResponse createInvitation(String alias, boolean isAutoAccept, boolean isMultiUse, boolean isPublic);

    /**
     * Issue the credential to the agent
     *
     * @param issueCredentialSendRequest as object
     * @return issueCredentialResponse
     */
    IssueCredentialSendResponse issueCredentialSend(IssueCredentialSendRequest issueCredentialSendRequest);

    /**
     * Get Mandate Did Details
     *
     * @return DidResponse Object
     */
    DidResponse getPublicDidDetails();

    /**
     * Get Credential Definition Created from the ledger
     *
     * @param schemaIdentifier schema-id
     * @return SchemaResponse object
     */
    CredentialDefCreatedResponse getCredentialDefinitionCreated(String schemaName, String schemaVersion);

    /**
     * Get Schema from the ledger
     *
     * @param schemaId String
     * @return SchemaResponse object
     */
    SchemaResponse getSchema(String schemaId);

    /**
     * Get Schema Created from the ledger
     *
     * @param schemaName schema name
     * @param schemaVersion schema version
     * @return SchemaCreatedResponse Object
     */
    SchemasCreatedResults getSchemaCreated(String schemaName, String schemaVersion);

    /**
     * Accept the connection Request for a connection that was created by sending an invitation.
     *
     * @param connectionId - String
     * @return
     */
    ConnectionAcceptRequestResponse acceptConnectionRequest(String connectionId);

    /**
     * Creates the proof request.
     *
     * @param request - V10PresentationCreateRequestRequest
     * @return the V10PresentationExchange
     */
    V10PresentationExchange createProofRequest(V10PresentationCreateRequestRequest request);

    /**
     * Send trust ping to the connection.
     *
     * @param connectionId - String
     * @param pingRequest - String
     * @return the ConnectionAcceptRequestResponse
     */
    PingRequestResponse sendTrustPing(String connectionId, PingRequest pingRequest);

    /**
     * Retrieve the Dids from the wallet.
     * Only Dids with posture WALLET_ONLY will be retrieved.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.rest.CloudAgentRestClient#walletDids}
     */
    DIDList retrieveWalletDids();

    /**
     * Get a list of all schemas created by the mandate service.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.rest.CloudAgentRestClient#schemasCreated}
     */
    SchemasCreatedResults getSchemasCreated();

    /**
     * Revoke credential.
     *
     * @param revocationRequest - RevocationRequest
     * @return String of revocation status
     */
    String revokeCredential(RevocationRequest revocationRequest);

    /**
     * Create schema for the ledger.
     *
     * @param authorizationSchema - AuthorizationSchema
     * @return SchemaResponse object
     */
    SchemaResponse createSchema(AuthorizationSchema authorizationSchema);

    /**
     * Create Credential Definition for the ledger
     *
     * @param credentialDefinitionRequest object
     * @return CredentialDefinitionResponse
     */
    CredentialDefinitionResponse createCredentialDefinition(CredentialDefinitionRequest credentialDefinitionRequest);

}