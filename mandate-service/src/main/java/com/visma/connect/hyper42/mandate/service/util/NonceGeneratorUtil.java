/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.util;

import java.security.SecureRandom;

/**
 * Utility to generate a Nonce.
 *
 * @author Micha Wensveen
 */
public final class NonceGeneratorUtil {

    /**
     * Private constructor to prevent initialization
     */
    private NonceGeneratorUtil() {
        super();
    }

    /**
     * Generate a nonce.
     * The nonce is a 10 digit number. Note that the first number cannot be 0. Else we get an error:
     * "nonce": ["Value 0967437291 is not a positive numeric string"]}
     *
     * @return the String
     */
    public static String generatNonce() {
        SecureRandom secureRandom = new SecureRandom();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(secureRandom.nextInt(9) + 1);
        for (int i = 1; i < 10; i++) {
            stringBuilder.append(secureRandom.nextInt(10));
        }
        return stringBuilder.toString();
    }
}
