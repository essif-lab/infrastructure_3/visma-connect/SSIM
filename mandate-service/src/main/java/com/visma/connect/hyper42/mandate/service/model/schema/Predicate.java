/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.model.schema;

import java.util.List;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The Predicate holds the data for schema's predicates
 *
 * @author Lukas Nakas
 */
public class Predicate {

    /**
     * Holds Predicate Name
     */
    @Field("name")
    private String name;

    /**
     * Holds Predicate Type
     */
    @Field("type")
    private String type;

    /**
     * Holds Predicate Conditions
     */
    @Field("condition")
    private List<Condition> conditions;

    /**
     * All args constructor
     *
     * @param name as string
     * @param type as string
     * @param conditions as list of conditions
     */
    public Predicate(String name, String type, List<Condition> conditions) {
        this.name = name;
        this.type = type;
        this.conditions = conditions;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Predicate#name}
     */
    public String getName() {
        return name;
    }

    /**
     * Setter.
     *
     * @param name {@link com.visma.connect.hyper42.mandate.service.model.schema.Predicate#name}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Predicate#type}
     */
    public String getType() {
        return type;
    }

    /**
     * Setter.
     *
     * @param type {@link com.visma.connect.hyper42.mandate.service.model.schema.Predicate#type}
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.service.model.schema.Predicate#conditions}
     */
    public List<Condition> getConditions() {
        return conditions;
    }

    /**
     * Setter.
     *
     * @param conditions {@link com.visma.connect.hyper42.mandate.service.model.schema.Predicate#conditions}
     */
    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }
}
