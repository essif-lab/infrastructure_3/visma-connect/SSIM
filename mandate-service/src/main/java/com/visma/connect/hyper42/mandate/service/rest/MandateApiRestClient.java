/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.rest;

import com.visma.connect.hyper42.mandate.service.model.generated.ProofState;

/**
 * Rest client used to make calls the the Mandate Api.
 *
 * @author Micha Wensveen
 */
public interface MandateApiRestClient {

    /**
     * Update the state of the proof request to the API service
     *
     * @param proofState - ProofState
     * @return the String
     */
    String updateProofState(ProofState proofState);
}
