/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.common;

/**
 * The DataNotFoundException is used when there is no data for the connectionId in Mongodb.
 *
 * @author ragesh shunmugam
 */
public class DataNotFoundException extends Exception {

    /**
     * UID for serializable Object.
     */
    private static final long serialVersionUID = -5997112448969938667L;

    /**
     * Instantiates a new DataNotFoundException.
     *
     * @param message - String
     */
    public DataNotFoundException(String message) {
        super(message);
    }

    /**
     * Instantiates a new DataNotFoundException.
     *
     * @param e -Exception
     */
    public DataNotFoundException(Exception e) {
        super(e);
    }
}