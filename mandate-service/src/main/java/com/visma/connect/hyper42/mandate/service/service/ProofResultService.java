/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service;

/**
 * The Service to handle existing Proofs and forward the results
 *
 * @author Micha Wensveen, Kevin Kerkhoven
 */
public interface ProofResultService {

    /**
     * Process and validate the proof that has been received.
     *
     * @param threadId - String
     * @param state - String
     * @param verified - String
     * @param proof - String
     */
    void processAndValidateProofReceived(String threadId, String state, String verified, String proof);

    /**
     * Process ProofRequests that have timed-out.
     * Time-out means that that no proof is received within the configured duration.
     */
    void processProofRequestTimeout();

    /**
     * Retrieve stored proof request and return its' comment field as String.
     *
     * @param uuid - String
     * @return String
     */
    String retrieveProofRequestLoginEmail(String uuid);

}
