/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.service.dao.MandateVerifierRequestBulkDao;
import com.visma.connect.hyper42.mandate.service.dao.MandateVerifierRequestDao;
import com.visma.connect.hyper42.mandate.service.dao.SchemaDao;
import com.visma.connect.hyper42.mandate.service.model.MandateVerifierRequest;
import com.visma.connect.hyper42.mandate.service.model.generated.ProofState;
import com.visma.connect.hyper42.mandate.service.model.schema.Attribute;
import com.visma.connect.hyper42.mandate.service.model.schema.Schema;
import com.visma.connect.hyper42.mandate.service.rest.MandateApiRestClient;
import com.visma.connect.hyper42.mandate.service.rest.OpenPolicyAgentRestClient;
import com.visma.connect.hyper42.mandate.service.service.PolicyService;
import com.visma.connect.hyper42.mandate.service.service.ProofResultService;
import java.time.Instant;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Implementation of ProofResultService.
 *
 * @author Ragesh Shunmugan, Micha Wensveen, Kevin Kerkhoven
 */
@Service
public class ProofResultServiceImpl implements ProofResultService {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ProofResultServiceImpl.class);
    /**
     * Timeout state
     */
    private static final String TIMED_OUT_PROOFSTATE = "timed-out";

    /**
     * Error state
     */
    private static final String ERROR_PROOFSTATE = "error";

    /**
     * Failed state
     */
    private static final String FAIL_PROOFSTATE = "failed";

    /**
     * Policy validation failed state
     */
    private static final String POLICY_FAIL_PROOFSTATE = "policy validation failed";

    /**
     * Success state
     */
    private static final String SUCCESS_PROOFSTATE = "succeeded";

    /**
     * Schema data type number
     */
    private static final String SCHEMA_TYPE_NUMBER = "number";

    /**
     * mandate api service rest client
     */
    @Autowired
    private MandateApiRestClient mandateApiRestClient;

    /**
     * The aries agent service URL
     */
    @Value("${proofrequest.max.responsetime}")
    private int proofrequestMaxResponsetime;

    /**
     * Instantiates mandateVerifierRequestRepository
     */
    @Autowired
    private MandateVerifierRequestDao mandateVerifierRequestDao;

    /**
     * Mandate verifier request bulk dao.
     */
    @Autowired
    private MandateVerifierRequestBulkDao mandateVerifierRequestBulkDao;

    /**
     * Policy service
     */
    @Autowired
    private PolicyService policyService;

    /**
     * Schema dao
     */
    @Autowired
    private SchemaDao schemaDao;

    /**
     * Client to Open Policy Agent
     */
    @Autowired
    private OpenPolicyAgentRestClient opaClient;

    @Override
    public void processAndValidateProofReceived(String threadId, String state, String verified, String proof) {
        // For now, we do nothing with the proof value. We can send this value to a different service if necessary in the future
        Optional<MandateVerifierRequest> optionalMandateVerifierRequest = mandateVerifierRequestDao.findByThreadId(threadId);

        if (optionalMandateVerifierRequest.isPresent()) {
            MandateVerifierRequest mandateVerifierRequest = optionalMandateVerifierRequest.get();
            ProofState proofState = determineProofStateVerified(mandateVerifierRequest.getId(), state, verified);
            mandateVerifierRequest.setState(state);

            boolean validResult = validateProofWithPolicy(proof, optionalMandateVerifierRequest.get());
            if (!validResult) {
                proofState.setState(POLICY_FAIL_PROOFSTATE);
                mandateVerifierRequest.setState(POLICY_FAIL_PROOFSTATE);
            }
            updateApiProofState(proofState);
            mandateVerifierRequest.setTimestamp(Instant.now().getEpochSecond());
            mandateVerifierRequestDao.save(mandateVerifierRequest);
        } else {
            LOG.warn("Could not find mandate with thread ID {}", threadId);
        }
    }

    private boolean validateProofWithPolicy(String proof, MandateVerifierRequest mandateVerifierRequest) {
        // get schema for the proof (how to get the schema? schema name and version are functionally unique and should be enough to get the schema)
        String schemaName = mandateVerifierRequest.getSchemaName();

        // get policy using the schema id
        Optional<String> policy = policyService.getPolicyContentById(schemaName);

        if (policy.isEmpty()) {
            return true;
        }

        Optional<Schema> schema = schemaDao.findByNameAndVersion(schemaName.split(":")[0], schemaName.split(":")[1]);
        Map<String, String> schemaAttributes;
        if (schema.isPresent() && schema.get().getAttributes() != null) {
            schemaAttributes = schema.get().getAttributes().stream().collect(Collectors.toMap(Attribute::getName, Attribute::getType));
        } else {
            schemaAttributes = new HashMap<>();
        }

        // transform proof, extract attributes, put them in json object of structure { "input": { "<attribute name": "<proof value>"}} // NOSONAR
        if (LOG.isDebugEnabled()) {
            LOG.debug("transforming proof: {}", proof.replaceAll("[\n\r\t]", "_"));
        }
        StringBuilder builder = new StringBuilder();
        builder.append('{');
        builder.append("\"input\": {");

        JsonNode tree;
        try {
            tree = new ObjectMapper().readTree(proof);
        } catch (JsonProcessingException e) {
            LOG.error("Error reading proof json object", e);
            return false;
        }
        if (null != tree.get("revealed_attrs")) {
            addAttributesToBuilder(builder, tree.get("revealed_attrs"), schemaAttributes);
        }
        if (null != tree.get("revealed_attr_groups")) {
            addAttributesToBuilder(builder, tree.get("revealed_attr_groups"), schemaAttributes);
        }
        if (null != tree.get("self_attested_attrs")) {
            addAttributesToBuilder(builder, tree.get("self_attested_attrs"), schemaAttributes);
        }

        builder.append("}");
        builder.append("}");

        if (LOG.isDebugEnabled()) {
            LOG.debug("Final input: {}", builder);
        }

        // verify policy using data from proof
        String dataPath = schemaName.replace(':', '_').replace('.', '_');
        String policyResult = opaClient.queryDataWithInput(dataPath, builder.toString());

        // process result of the policy validation
        LOG.debug("Policy evaluation result: {}", policyResult);
        try {
            JsonNode resultTree = new ObjectMapper().readTree(policyResult);
            JsonNode result = resultTree.get("result");
            if (result != null && "true".equals(result.asText())) {
                return true;
            }
        } catch (JsonProcessingException e) {
            LOG.error("Error parsing validation result", e);
        }
        return false;
    }

    private void addAttributesToBuilder(StringBuilder builder, JsonNode attributes, Map<String, String> schemaAttributes) {
        Iterator<Entry<String, JsonNode>> it = attributes.fields();
        while (it.hasNext()) {
            Entry<String, JsonNode> entry = it.next();
            JsonNode obj = entry.getValue().get("values");
            Iterator<Entry<String, JsonNode>> fields = obj.fields();
            while (fields.hasNext()) {
                Entry<String, JsonNode> field = fields.next();
                builder.append("\"" + field.getKey() + "\"");
                builder.append(':');
                if (schemaAttributes.get(field.getKey()).equals(SCHEMA_TYPE_NUMBER)) {
                    builder.append(field.getValue().get("raw").asText());
                } else {
                    builder.append("\"" + field.getValue().get("raw").asText() + "\"");
                }
            }
        }
    }

    /**
     * Determine the proofstate to the frontend depending on state and verified values
     *
     * @param presExId - String
     * @param state - String
     * @param verified - String
     * @return Proofstate
     */
    private ProofState determineProofStateVerified(String presExId, String state, String verified) {
        ProofState proofState;
        if ("verified".equals(state)) {
            if ("true".equals(verified)) {
                proofState = new ProofState().withId(presExId).withState(SUCCESS_PROOFSTATE);
            } else {
                proofState = new ProofState().withId(presExId).withState(FAIL_PROOFSTATE);
            }
        } else {
            LOG.error("Unknown state in received proof encountered: {}", state);
            proofState = new ProofState().withId(presExId).withState(ERROR_PROOFSTATE);
        }
        return proofState;
    }

    @Override
    public void processProofRequestTimeout() {
        List<String> expired = findExpiredRequestedProofRequests();
        LOG.debug("Found expired proofrequests {}", expired);
        if (!expired.isEmpty()) {
            expired.stream().map(proofRequestId -> new ProofState().withId(proofRequestId).withState(TIMED_OUT_PROOFSTATE))
                    .forEach(this::updateApiProofState);
            updateExpiredProofRequests(expired);
        }
    }

    @Override
    public String retrieveProofRequestLoginEmail(String uuid) {
        LOG.debug("Get mongodb with key {}", uuid);
        return mandateVerifierRequestDao
                .findById(uuid)
                .filter(request -> request.getSchemaName().contains("mandate-login-schema") && !request.getComment().trim().isEmpty())
                .map(MandateVerifierRequest::getComment)
                .orElse("not_found");
    }

    /**
     * Find the proofrequest that have expired.
     * A proofrequest is expired when no proof has been submitted after a number of seconds.
     *
     * @return the List with ids of the expired proofrequest.
     */
    private List<String> findExpiredRequestedProofRequests() {
        long expired = Instant.now().minusSeconds(proofrequestMaxResponsetime).getEpochSecond();
        LOG.debug("Finding proofrequest with state request_sent and timestamp before {}", expired);
        List<MandateVerifierRequest> expiredRequests = mandateVerifierRequestDao.findByStateAndTimestampLessThan("request_sent", expired);
        return expiredRequests.stream().map(MandateVerifierRequest::getId).collect(Collectors.toList());
    }

    /**
     * Inform the API-service with the new proofstate variables
     *
     * @param proofState
     */
    private void updateApiProofState(ProofState proofState) {
        LOG.info("Sending Proof state to API {}", proofState);
        String result = mandateApiRestClient.updateProofState(proofState);
        LOG.info("Proof state response result from API {}", result);
    }

    /**
     * Update the expired ProofRequests after processing.
     *
     * @param proofRequests - List<String>
     */
    private void updateExpiredProofRequests(List<String> proofRequests) {
        mandateVerifierRequestBulkDao.updateState(proofRequests, TIMED_OUT_PROOFSTATE, Instant.now().getEpochSecond());
    }

}