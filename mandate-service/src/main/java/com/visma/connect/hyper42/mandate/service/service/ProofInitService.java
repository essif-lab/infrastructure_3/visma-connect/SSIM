/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.service.service;

import com.visma.connect.hyper42.mandate.service.model.schema.generated.PresentationRequestDict;
import java.util.Optional;

/**
 * The Service to handle the creation and the presentation of the proof
 *
 * @author Micha Wensveen, Kevin Kerkhoven
 */
public interface ProofInitService {

    /**
     * Retrieve stored proof request and return it as a sovrin PresentationRequestDict.
     *
     * @param uuid - String
     * @return the Optional PresentationRequestDict
     */
    Optional<PresentationRequestDict> retrieveProofRequest(String uuid);

    /**
     * Store verifier mandate request.
     *
     * @param schemaName schemaName with version
     * @param comment comment as string
     * @param user - User id.
     * @param requestJson Validated schema to add to our MongoDB
     * @return Base64 String of QR-code invitation
     */
    String createVerifierMandateRequest(String schemaName, String comment, String user, String requestJson);
}
