SSI Mandate 
===========

Our SSI mandate service is a generic and holistic approach to provide and request mandates. Mandates are SSI credentials. The SSI Mandate service issues a mandate on behalf of a provider for a holder (which becomes the "authorized representative"). These credentials can be used to prove to a verifier that the authorized representative is authorized to act for specific actions on behalf of the provider. The mandate credentials are stored in the wallet of the authorized representative as opposed to a central database in current systems. The provider can revoke this credential at any point in time if he/she no longer wants the authorized representative to act on their behalf by updating a revocation hash on the blockchain. The SSI mandate provides mandates completely peer to peer and isn’t limited to individuals only. A SSI wallet can also represent a device or institution, for example a company can use a SSI company wallet to authorize employees to access the building or use the company credit card up to a certain amount.
Overall, mandates can be used for 2 different purposes:

- To authorize someone to have acess to data of someone else.
- To authorize someone to act on behalf of someone else.

License
=======
    Copyright © 2022 Visma Connect (vco.hyper42@visma.com)

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

