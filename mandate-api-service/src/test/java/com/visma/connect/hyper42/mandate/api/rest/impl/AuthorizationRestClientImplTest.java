/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest.impl;

import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.UserResponse;
import java.net.URI;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthorizationRestClientImplTest {

    @InjectMocks
    private AuthorizationRestClientImpl authorizationRestClientImpl;

    @Mock
    private RestTemplate restTemplate;

    @Captor
    private ArgumentCaptor<URI> uriCaptor;

    @Test
    void testGetUser() {
        UserResponse response = new UserResponse().withId("someId").withEmail("someEmail").withRole("someRole");
        ResponseEntity<Object> restResponse = ResponseEntity.ok(response);
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        UserResponse result = authorizationRestClientImpl.getUser("someId");
        assertEquals("someId", result.getId());
        assertEquals("someEmail", result.getEmail());
        assertEquals("someRole", result.getRole());
    }

    @Test
    void testGetUserNoContent() {
        ResponseEntity<Object> restResponse = ResponseEntity.ok().build();
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        ApplicationRuntimeException exception = assertThrows(ApplicationRuntimeException.class, () -> authorizationRestClientImpl.getUser("someId"));
        assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
    }

    @Test
    void testGetUserClientError() {
        ResponseEntity<Object> restResponse = ResponseEntity.notFound().build();
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> authorizationRestClientImpl.getUser("someId"));
        assertEquals("Request is invalid, please review your input and try again.", exception.getMessage());
    }

    @Test
    void testGetUserClientErrorWithBody() {
        UserResponse response = new UserResponse().withId(null).withEmail(null).withRole(null);
        ResponseEntity<Object> restResponse = ResponseEntity.badRequest().body(response);
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> authorizationRestClientImpl.getUser("someId"));
        assertEquals(response.toString(), exception.getMessage());
    }

    @Test
    void testGetUserServerError() {
        ResponseEntity<Object> restResponse = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        ApplicationRuntimeException exception = assertThrows(ApplicationRuntimeException.class, () -> authorizationRestClientImpl.getUser("someId"));
        assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
    }
}