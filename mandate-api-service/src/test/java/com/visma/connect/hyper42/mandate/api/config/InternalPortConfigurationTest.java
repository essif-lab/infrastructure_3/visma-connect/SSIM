/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.config;

import java.lang.reflect.Field;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.util.ReflectionUtils;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class InternalPortConfigurationTest {

    @InjectMocks
    private InternalPortConfiguration internalPortConfiguration;

    @BeforeEach
    public void setup() {
        Field internalPort = ReflectionUtils.findField(InternalPortConfiguration.class, "internalPort");
        ReflectionUtils.makeAccessible(internalPort);
        ReflectionUtils.setField(internalPort, internalPortConfiguration, 2000);

        Field internalPath = ReflectionUtils.findField(InternalPortConfiguration.class, "internalPath");
        ReflectionUtils.makeAccessible(internalPath);
        ReflectionUtils.setField(internalPath, internalPortConfiguration, "/internalpath/");
    }

    @Test
    void testWebServerFactoryCustomizer() {
        WebServerFactoryCustomizer<TomcatServletWebServerFactory> webServerFactoryCustomizer = internalPortConfiguration.webServerFactoryCustomizer();
        InternalConnectorTomcatServletWebServerFactoryCustomizer customizer =
                (InternalConnectorTomcatServletWebServerFactoryCustomizer) webServerFactoryCustomizer;
        assertEquals(2000, getIntValue(customizer, "internalPort"));
    }

    @Test
    void testTrustedEndpointsFilter() {
        FilterRegistrationBean<InternalEndpointFilter> bean = internalPortConfiguration.trustedEndpointsFilter();
        InternalEndpointFilter filter = bean.getFilter();
        assertEquals(2000, getIntValue(filter, "internalPort"));
        assertEquals("/internalpath/", getStringValue(filter, "internalPath"));
    }

    private String getStringValue(InternalEndpointFilter filter, String fieldname) {
        Field field = getField(fieldname);
        try {
            return (String) field.get(filter);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private Integer getIntValue(InternalEndpointFilter filter, String fieldname) {
        Field field = getField(fieldname);
        try {
            return field.getInt(filter);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private Integer getIntValue(InternalConnectorTomcatServletWebServerFactoryCustomizer customizer, String fieldname) {
        Field field = ReflectionUtils.findField(InternalConnectorTomcatServletWebServerFactoryCustomizer.class, fieldname);
        ReflectionUtils.makeAccessible(field);
        try {
            return field.getInt(customizer);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private Field getField(String fieldname) {
        Field field = ReflectionUtils.findField(InternalEndpointFilter.class, fieldname);
        ReflectionUtils.makeAccessible(field);
        return field;
    }

}
