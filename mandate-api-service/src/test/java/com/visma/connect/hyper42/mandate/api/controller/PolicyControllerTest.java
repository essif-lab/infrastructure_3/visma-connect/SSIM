/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.visma.connect.hyper42.mandate.api.model.generated.PolicyValidationRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/***
 * Policy controller {@link PolicyController}
 *
 * @author Michael van Niekerk
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
class PolicyControllerTest {
    @Autowired
    PolicyController policyController;

    @Test
    void validRegoPolicyPassesValidation() {
        var policy = """
                       package authz
                       import future.keywords
                       allow if {
                           input.path == ["users"]
                           input.method == "POST"
                       }
                       allow if {
                           input.path == ["users", input.user_id]
                           input.method == "GET"
                       }
                """;
        var request = new PolicyValidationRequest().withPolicy(policy);
        var response = policyController.validate(request).getBody();

        assert response != null;
        assertTrue(response.getValid());
    }

    @Test
    void invalidRegoPolicyFailsValidation() {
        var policy = """
                       package authz
                       import future.keywords
                       here be dragons watch out
                       allow if {
                           input.path == ["users"]
                           input.method == "POST"
                       }
                       allow if {
                           input.path == ["users", input.user_id]
                           input.method == "GET"
                       }
                """;
        var request = new PolicyValidationRequest().withPolicy(policy);
        var response = policyController.validate(request).getBody();

        assert response != null;
        assertFalse(response.getValid());
        var errors = response.getErrors();
        assertEquals(5, errors.size());
    }
}
