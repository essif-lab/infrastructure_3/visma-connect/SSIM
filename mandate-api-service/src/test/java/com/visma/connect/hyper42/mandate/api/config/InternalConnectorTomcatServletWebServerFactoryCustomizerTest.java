/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.config;

import java.lang.reflect.Field;
import org.apache.catalina.connector.Connector;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.util.ReflectionUtils;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doNothing;

@ExtendWith(MockitoExtension.class)
class InternalConnectorTomcatServletWebServerFactoryCustomizerTest {
    private InternalConnectorTomcatServletWebServerFactoryCustomizer internalConnectorTomcatServletWebServerFactoryCustomizer;

    @Mock
    private TomcatServletWebServerFactory factory;

    @Captor
    private ArgumentCaptor<Connector> connectorsCaptor;

    @Test
    void testCustomize() {
        doNothing().when(factory).addAdditionalTomcatConnectors(connectorsCaptor.capture());
        internalConnectorTomcatServletWebServerFactoryCustomizer = new InternalConnectorTomcatServletWebServerFactoryCustomizer(2000);
        internalConnectorTomcatServletWebServerFactoryCustomizer.customize(factory);

        Connector connector = connectorsCaptor.getValue();
        assertNotNull(connector);
        assertEquals(2000, connector.getPort());
        assertEquals("http", connector.getScheme());
        assertEquals("HTTP/1.1", connector.getProtocol());
        assertNotNull(getObjectValue(internalConnectorTomcatServletWebServerFactoryCustomizer, "serverProperties"));
    }

    private Object getObjectValue(InternalConnectorTomcatServletWebServerFactoryCustomizer filter, String fieldname) {
        Field field = ReflectionUtils.findField(InternalConnectorTomcatServletWebServerFactoryCustomizer.class, fieldname);
        ReflectionUtils.makeAccessible(field);
        try {
            return field.get(filter);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

}
