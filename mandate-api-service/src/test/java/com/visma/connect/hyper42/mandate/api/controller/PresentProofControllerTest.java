/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.visma.connect.hyper42.mandate.api.TestDataGenerator;
import com.visma.connect.hyper42.mandate.api.service.ProofService;
import com.visma.connect.hyper42.mandate.api.util.JsonProofValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

class PresentProofControllerTest {

    @InjectMocks
    private PresentProofController presentProofController;

    @Mock
    private ProofService proofService;

    @Mock
    private JsonProofValidator jsonProofValidator;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Captor
    private ArgumentCaptor<String> schemaNameCaptor;
    @Captor
    private ArgumentCaptor<String> jsonCaptor;
    @Captor
    private ArgumentCaptor<String> commentCaptor;

    @Captor
    private ArgumentCaptor<String> uuidCaptor;

    @Captor
    private ArgumentCaptor<String> userCaptor;

    @Test
    void testCreateMandateRequestSuccess() throws JsonProcessingException {
        String schemaName = TestDataGenerator.createSchemaName();
        String comment = TestDataGenerator.createProofCommentString();
        String requestJson = TestDataGenerator.createProofRequestJsonString();
        String mockResponse = TestDataGenerator.createInvitationUrlEncoded();

        when(jsonProofValidator.validate(schemaName, requestJson)).thenReturn(true);
        when(proofService.createProofRequest(schemaNameCaptor.capture(), commentCaptor.capture(), jsonCaptor.capture(), userCaptor.capture()))
                .thenReturn(mockResponse);

        ResponseEntity<String> responseEntity = presentProofController.createNewProofRequest(schemaName, comment, requestJson);
        String response = responseEntity.getBody();
        Assertions.assertEquals(mockResponse, response);

        assertEquals(schemaName, schemaNameCaptor.getValue());
        assertEquals(comment, commentCaptor.getValue());
        assertEquals(requestJson, jsonCaptor.getValue());
        assertNotNull(userCaptor.getValue());
    }

    @Test
    void testCreateMandateRequestNoCommentSuccess() throws JsonProcessingException {
        String schemaName = TestDataGenerator.createSchemaName();
        String requestJson = TestDataGenerator.createProofRequestJsonString();
        String mockResponse = TestDataGenerator.createInvitationUrlEncoded();

        when(jsonProofValidator.validate(schemaName, requestJson)).thenReturn(true);
        when(proofService.createProofRequest(schemaNameCaptor.capture(), commentCaptor.capture(), jsonCaptor.capture(), userCaptor.capture()))
                .thenReturn(mockResponse);

        ResponseEntity<String> responseEntity = presentProofController.createNewProofRequest(schemaName, null, requestJson);
        String response = responseEntity.getBody();
        Assertions.assertEquals(mockResponse, response);

        assertEquals(schemaName, schemaNameCaptor.getValue());
        assertNull(commentCaptor.getValue());
        assertEquals(requestJson, jsonCaptor.getValue());
    }

    @Test
    void testCreateMandateRequestInvalidJsonSchemaValidation() {
        String schemaName = TestDataGenerator.createSchemaName();
        String requestJson = TestDataGenerator.createProofRequestJsonString();

        when(jsonProofValidator.validate(schemaNameCaptor.capture(), jsonCaptor.capture())).thenReturn(false);

        ResponseEntity<String> responseEntity = presentProofController.createNewProofRequest(schemaName, null, requestJson);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(schemaName, schemaNameCaptor.getValue());
        assertEquals(requestJson, jsonCaptor.getValue());
    }

    @Test
    void testRetrievePresentationRequest() {
        String proofRequest = "http://hyper42.example.com?d_m=eyJyZXF1ZXN0X3ByZXNlbnRhdGlvbnN%2BYXR0YWNoIjpbeyJAaWQiO";
        Mockito.when(proofService.retrieveProofRequest(uuidCaptor.capture())).thenReturn(proofRequest);

        ResponseEntity<String> result = presentProofController.retrievePresentationRequest("abcde");

        assertEquals(301, result.getStatusCodeValue());
        assertEquals("abcde", uuidCaptor.getValue());
    }
}
