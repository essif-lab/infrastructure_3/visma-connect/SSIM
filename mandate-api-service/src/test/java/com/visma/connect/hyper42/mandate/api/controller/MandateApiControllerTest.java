/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import com.visma.connect.hyper42.mandate.api.TestDataGenerator;
import com.visma.connect.hyper42.mandate.api.model.generated.json.IssuedMandateResponse;
import com.visma.connect.hyper42.mandate.api.service.MandateApiService;
import com.visma.connect.hyper42.mandate.api.util.JsonSchemaValidator;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MandateApiControllerTest {

    @InjectMocks
    private MandateApiController mandateApiController;

    @Mock
    private MandateApiService mandateApiService;

    @Mock
    private JsonSchemaValidator jsonSchemaValidator;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Captor
    private ArgumentCaptor<String> schemaNameCaptor;
    @Captor
    private ArgumentCaptor<String> jsonCaptor;

    @Test
    void testCreateMandateRequestSuccess() {
        String schemaName = TestDataGenerator.createSchemaName();
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;
        String qrCodeUrl = "someBase64String";

        when(jsonSchemaValidator.validate(schemaNameCaptor.capture(), jsonCaptor.capture())).thenReturn(true);
        when(mandateApiService.createMandateRequest(schemaName, alias, requestJson, ssicommsEnabled)).thenReturn(qrCodeUrl);

        ResponseEntity<String> responseEntity = mandateApiController.createNewMandateRequest(schemaName, alias, ssicommsEnabled, requestJson);
        String response = responseEntity.getBody();
        Assertions.assertEquals(qrCodeUrl, response);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(schemaName, schemaNameCaptor.getValue());
        assertEquals(requestJson, jsonCaptor.getValue());
    }

    @Test
    void testCreateMandateRequestNoAliasSuccess() {
        String schemaName = TestDataGenerator.createSchemaName();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;
        String qrCodeUrl = "someBase64String";

        when(jsonSchemaValidator.validate(schemaNameCaptor.capture(), jsonCaptor.capture())).thenReturn(true);
        when(mandateApiService.createMandateRequest(schemaName, null, requestJson, ssicommsEnabled)).thenReturn(qrCodeUrl);

        ResponseEntity<String> responseEntity = mandateApiController.createNewMandateRequest(schemaName, null, ssicommsEnabled, requestJson);
        String response = responseEntity.getBody();
        Assertions.assertEquals(qrCodeUrl, response);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(schemaName, schemaNameCaptor.getValue());
        assertEquals(requestJson, jsonCaptor.getValue());
    }

    @Test
    void testCreateMandateRequestInvalidJsonSchemaValidation() {
        String schemaName = TestDataGenerator.createSchemaName();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;

        when(jsonSchemaValidator.validate(schemaNameCaptor.capture(), jsonCaptor.capture())).thenReturn(false);

        ResponseEntity<String> responseEntity = mandateApiController.createNewMandateRequest(schemaName, null, ssicommsEnabled, requestJson);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(schemaName, schemaNameCaptor.getValue());
        assertEquals(requestJson, jsonCaptor.getValue());
    }

    @Test
    void testGetIssuedMandates() {
        List<IssuedMandateResponse> responses = TestDataGenerator.formIssuedMandateResponses();
        Mockito.when(mandateApiService.retrieveIssuedMandates()).thenReturn(responses);

        ResponseEntity<List<IssuedMandateResponse>> result = mandateApiController.getIssuedMandates();

        IssuedMandateResponse singleResponse = result.getBody().get(0);
        assertEquals(200, result.getStatusCodeValue());
        assertEquals("someSchemaName", singleResponse.getSchemaName());
        assertEquals("someConnectionId", singleResponse.getConnectionId());
        assertEquals("someCredRevId", singleResponse.getCredRevId());
        assertEquals("someRevRegId", singleResponse.getRevRegId());
        assertTrue(singleResponse.getValues().isEmpty());
    }

    @Test
    void testRevokeCredentialSuccess() {
        IssuedMandateResponse issuedMandateResponse = TestDataGenerator.formIssuedMandateResponses().get(0);

        when(mandateApiService.revokeCredential(issuedMandateResponse)).thenReturn("success");

        ResponseEntity<String> responseEntity = mandateApiController.revokeCredential(issuedMandateResponse);
        String response = responseEntity.getBody();

        assertEquals("success", response);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

}

