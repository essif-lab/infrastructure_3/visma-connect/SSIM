/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.config;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

class JsonSchemaMapConfigTest {

    @Test
    void testGetMandateSchemaMap() {
        JsonSchemaMapConfig mapConfig = new JsonSchemaMapConfig();
        assertEquals(3, mapConfig.getMandateSchemaMap().size());
        assertEquals("MandateSchema/mandate-kvk-schema_1.0.json", mapConfig.getMandateSchemaMap().get("mandate-kvk-schema:1.0"));
        assertEquals("MandateSchema/mandate-signing-basic_1.0.json", mapConfig.getMandateSchemaMap().get("mandate-signing-basic:1.0"));
        assertEquals("MandateSchema/fenex-schema_1.0.json", mapConfig.getMandateSchemaMap().get("fenex-schema:1.0"));
    }

}
