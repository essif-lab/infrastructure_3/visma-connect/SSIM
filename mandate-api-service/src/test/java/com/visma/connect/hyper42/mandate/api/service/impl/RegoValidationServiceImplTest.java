/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service.impl;

import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.service.RegoValidationService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.stream.Stream;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Tests {@link RegoValidationServiceImpl}
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
class RegoValidationServiceImplTest {
    @Autowired
    RegoValidationService regoValidationService;

    @Test
    void validRegoFilePasses() throws IOException {
        // Given: A valid file
        var f = File.createTempFile("rego", ".rego");
        f.deleteOnExit();
        try (var ous = new FileOutputStream(f)) {
            Stream.of(
                    "package authz",
                    "import future.keywords",
                    "allow if {",
                    "    input.path == [\"users\"]",
                    "    input.method == \"POST\"",
                    "}",
                    "allow if {",
                    "    input.path == [\"users\", input.user_id]",
                    "    input.method == \"GET\"",
                    "}")
                    .map(s -> s + "\n")
                    .map(String::getBytes)
                    .forEach(b -> {
                        try {
                            ous.write(b);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
        }

        // When: Try to check if the Rego file is valid
        var errors = regoValidationService.checkRegoTextFileForErrors(f.getAbsolutePath());

        assertEquals(0, errors.size());
    }

    @Test
    void invalidRegoFileFails() throws IOException {
        // Given: An invalid file
        var f = File.createTempFile("rego", ".rego");
        f.deleteOnExit();
        try (var ous = new FileOutputStream(f)) {
            Stream.of(
                    "package authz",
                    "import future.keywords",
                    "here be dragons",
                    "allow if {",
                    "    input.path == [\"users\"]",
                    "    input.method == \"POST\"",
                    "}",
                    "allow if {",
                    "    input.path == [\"users\", input.user_id]",
                    "    input.method == \"GET\"",
                    "}")
                    .map(s -> s + "\n")
                    .map(String::getBytes)
                    .forEach(b -> {
                        try {
                            ous.write(b);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
        }

        // When: Try to check if the Rego file is valid
        var errors = regoValidationService.checkRegoTextFileForErrors(f.getAbsolutePath());

        assertEquals(3, errors.size());
        assertEquals(3, Integer.parseInt(errors.get(0).getLocation().getRow()));
    }

    @Test
    @Disabled("Find a way to force an IOException flow")
    void checkRegoTextFileIoErrorThrowsApplicationRuntimeError() {
        try (var file = Mockito.mockStatic(File.class)) {
            file.when(() -> File.createTempFile("opa", "exe")).thenThrow(new IOException());

            var ex = assertThrows(ApplicationRuntimeException.class, () -> regoValidationService.checkRegoTextFileForErrors("/rego.rego"));
            assertEquals(ex.getMessage(), "Could not check file on /rego.rego");
        }
    }

    @Test
    void setupOpaFromResourceFailureSetupReturnsEmptyCommand() {
        try (var file = Mockito.mockStatic(File.class)) {
            file.when(() -> File.createTempFile("opa", "exe")).thenThrow(new IOException());
            var ret = ((RegoValidationServiceImpl) regoValidationService).setupOpaUsingJavaResource();
            assertTrue(ret.isEmpty());
        }
    }

    @Test
    void waitingForProcessToFinishInterruptionReturnsAnEmptyList() throws InterruptedException {
        var service = Mockito.mock(RegoValidationServiceImpl.class);
        Mockito.when(service.waitForProcessToEnd(Mockito.any())).thenCallRealMethod();
        var process = Mockito.mock(Process.class);
        Mockito.when(process.waitFor()).thenThrow(new InterruptedException());
        var ret = service.waitForProcessToEnd(process);
        assertEquals(ret, -1);
    }
}
