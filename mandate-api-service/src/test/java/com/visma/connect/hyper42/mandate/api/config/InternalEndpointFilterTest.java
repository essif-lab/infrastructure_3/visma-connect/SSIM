/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.config;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class InternalEndpointFilterTest {
    private InternalEndpointFilter internalEndpointFilter;

    @Mock
    private HttpServletRequest servletRequest;

    @Mock
    private HttpServletResponse servletResponse;

    @Mock
    private FilterChain filterChain;

    @Captor
    private ArgumentCaptor<ServletRequest> servletRequestCaptor;

    @Captor
    private ArgumentCaptor<ServletResponse> servletResponseCaptor;

    @Mock
    private ServletOutputStream outputStream;

    @Captor
    private ArgumentCaptor<Integer> httpStateCaptor;

    @BeforeEach
    public void setup() {
        internalEndpointFilter = new InternalEndpointFilter(2000, "/somepath/");
    }

    @Test
    void testDoFilterAllowInternal() throws IOException, ServletException {
        when(servletRequest.getRequestURI()).thenReturn("/somepath/resource/123");
        when(servletRequest.getLocalPort()).thenReturn(2000);
        doNothing().when(filterChain).doFilter(servletRequestCaptor.capture(), servletResponseCaptor.capture());

        internalEndpointFilter.doFilter(servletRequest, servletResponse, filterChain);

        assertEquals(servletRequest, servletRequestCaptor.getValue());
        assertEquals(servletResponse, servletResponseCaptor.getValue());
        verifyNoMoreInteractions(servletResponse);
    }

    @Test
    void testDoFilterNotAllowInternal() throws IOException, ServletException {
        when(servletRequest.getRequestURI()).thenReturn("/somepath/resource/123");
        when(servletRequest.getLocalPort()).thenReturn(3000);
        when(servletResponse.getOutputStream()).thenReturn(outputStream);
        doNothing().when(outputStream).close();
        doNothing().when(servletResponse).setStatus(httpStateCaptor.capture());

        internalEndpointFilter.doFilter(servletRequest, servletResponse, filterChain);

        assertEquals(404, httpStateCaptor.getValue().intValue());
        verifyNoMoreInteractions(filterChain);
    }

    @Test
    void testDoFilterAllowExternal() throws IOException, ServletException {
        when(servletRequest.getRequestURI()).thenReturn("/someOTHERpath/resource/123");
        doNothing().when(filterChain).doFilter(servletRequestCaptor.capture(), servletResponseCaptor.capture());

        internalEndpointFilter.doFilter(servletRequest, servletResponse, filterChain);

        assertEquals(servletRequest, servletRequestCaptor.getValue());
        assertEquals(servletResponse, servletResponseCaptor.getValue());
        verifyNoMoreInteractions(servletResponse);
    }
}
