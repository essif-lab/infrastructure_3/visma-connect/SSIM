/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.util;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class JwtTokenUtilTest {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Test
    void testGenerateToken() {
        String userId = "someUserId";
        Clock clock = Clock.fixed(Instant.ofEpochMilli(0), ZoneId.of("Europe/London"));

        String jwt = jwtTokenUtil.generateToken(userId, clock);

        assertEquals(
                "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzb21lVXNlcklkIiwiZXhwIjozNjAwLCJpYXQiOjB9.wAb83PlsFhABDfpTdpXqbKY1Lf5iviE86id0osJHgxWDPN6cFc" +
                        "-vXNiBm6JSmnH6r59fQDzJaUsAXuSsjvG3Cg",
                jwt);
    }
}