/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.util;

import org.junit.jupiter.api.Test;
import static com.visma.connect.hyper42.mandate.api.util.DockerUtil.sanitizeDockerMountPath;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Tests for {@link DockerUtil}
 */
class DockerUtilTest {
    @Test
    void testSanitizeDockerMountPath() {
        // Given: A file path
        var filePath = "/tmp/rego.rego";
        // And: A mount point inside the Docker container
        var mountPoint = "/tmp/rego.rego";
        // And: Not on Windows
        var isWindows = false;

        // When: Get the sanitized path
        var path = sanitizeDockerMountPath(filePath, mountPoint, isWindows);
        // Then: It should be correct
        assertEquals(path, "/tmp/rego.rego:/tmp/rego.rego");

        // Given: A file path with a drive letter included
        filePath = "C:\\Tmp\\rego.rego";
        // And: It is on Windows
        isWindows = true;
        // When: Get the sanitized path
        path = sanitizeDockerMountPath(filePath, mountPoint, isWindows);
        // Then: It should be correct
        assertEquals(path, "//c/Tmp/rego.rego:/tmp/rego.rego");

        // Given: A file path without a drive letter included
        filePath = "\\\\MyWindowsShareHost\\Tmp\\rego.rego";
        // And: It is on Windows
        isWindows = true;
        // When: Get the sanitized path
        path = sanitizeDockerMountPath(filePath, mountPoint, isWindows);
        // Then: It should be correct
        assertEquals(path, "//MyWindowsShareHost/Tmp/rego.rego:/tmp/rego.rego");

    }
}
