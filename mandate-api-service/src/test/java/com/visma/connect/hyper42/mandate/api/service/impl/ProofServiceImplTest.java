/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service.impl;

import com.visma.connect.hyper42.mandate.api.TestDataGenerator;
import com.visma.connect.hyper42.mandate.api.model.generated.ProofResponse;
import com.visma.connect.hyper42.mandate.api.rest.CreateProofRequest;
import com.visma.connect.hyper42.mandate.api.rest.ProofRestClient;
import com.visma.connect.hyper42.mandate.api.service.AuthorizationService;
import java.lang.reflect.Field;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.util.ReflectionUtils;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProofServiceImplTest {
    @InjectMocks
    private ProofServiceImpl proofService;

    @Mock
    private ProofRestClient proofRestClient;

    @Mock
    private AuthorizationService authorizationService;

    @Mock
    private SimpMessagingTemplate simpMessagingTemplate;

    @Captor
    private ArgumentCaptor<String> commentCaptor;

    @Captor
    private ArgumentCaptor<String> uuidCaptor;

    @Captor
    private ArgumentCaptor<String> emailCaptor;

    @Captor
    private ArgumentCaptor<CreateProofRequest> createProofRequestCaptor;

    @Captor
    private ArgumentCaptor<String> destinationCaptor;

    @Captor
    private ArgumentCaptor<Object> payloadCaptor;

    @BeforeEach
    public void setup() {
        Field apiUrl = ReflectionUtils.findField(ProofServiceImpl.class, "apiUrl");
        ReflectionUtils.makeAccessible(apiUrl);
        ReflectionUtils.setField(apiUrl, proofService, "http://localhost:8443");
    }

    @Test
    void createProofRequestOK() {
        String schemaName = TestDataGenerator.createSchemaName();
        String proofComment = TestDataGenerator.createProofCommentString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        String responseDataDecoded = "someId";
        String responseDataEncoded = TestDataGenerator.convertUrlToBase64Utf8("http://localhost:8443/proofs/" + responseDataDecoded);
        when(proofRestClient.createProofRequest(createProofRequestCaptor.capture())).thenReturn(responseDataDecoded);

        String response = proofService.createProofRequest(schemaName, proofComment, requestJson, "a");
        assertEquals(responseDataEncoded, response);

        CreateProofRequest createProofRequest = createProofRequestCaptor.getValue();
        assertEquals(schemaName, createProofRequest.getSchemaName());
        assertEquals(proofComment, createProofRequest.getProofComment());
        assertEquals(requestJson, createProofRequest.getRequestJson());
        assertEquals("a", createProofRequest.getUser());
    }

    @Test
    void createProofRequestGeneratedCommentOK() {
        String schemaName = TestDataGenerator.createSchemaName();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        String responseDataDecoded = "someId";
        String responseDataEncoded = TestDataGenerator.convertUrlToBase64Utf8("http://localhost:8443/proofs/" + responseDataDecoded);

        when(proofRestClient.createProofRequest(createProofRequestCaptor.capture())).thenReturn(responseDataDecoded);

        String response = proofService.createProofRequest(schemaName, null, requestJson, "a");
        assertEquals(responseDataEncoded, response);
        CreateProofRequest createProofRequest = createProofRequestCaptor.getValue();
        assertEquals("Proof request for schema " + schemaName, createProofRequest.getProofComment());
        assertEquals(schemaName, createProofRequest.getSchemaName());
        assertEquals(requestJson, createProofRequest.getRequestJson());
        assertEquals("a", createProofRequest.getUser());
    }

    @Test
    void testRetrieveProofRequest() {
        when(proofRestClient.retrieveProofRequest(uuidCaptor.capture())).thenReturn("my:proofrequest");
        doNothing().when(simpMessagingTemplate).convertAndSendToUser(uuidCaptor.capture(), destinationCaptor.capture(), payloadCaptor.capture());

        String result = proofService.retrieveProofRequest("abcd");

        assertEquals("http://localhost:8443?d_m=my%3Aproofrequest&orig=http://localhost:8443/proofs/abcd", result);
        assertEquals("abcd", uuidCaptor.getValue());
        assertEquals("abcd", uuidCaptor.getValue());
        assertEquals("/present-proof/response", destinationCaptor.getValue());
        ProofResponse proofResponse = (ProofResponse) payloadCaptor.getValue();
        assertEquals(ProofResponse.State.LOADING, proofResponse.getState());
    }

    @Test
    void testUpdateStateSucceeded() {
        doNothing().when(simpMessagingTemplate).convertAndSendToUser(uuidCaptor.capture(), destinationCaptor.capture(), payloadCaptor.capture());

        proofService.updateState("abcd", "succeeded");

        assertEquals("abcd", uuidCaptor.getValue());
        assertEquals("/present-proof/response", destinationCaptor.getValue());
        ProofResponse proofResponse = (ProofResponse) payloadCaptor.getValue();
        assertEquals(ProofResponse.State.SUCCEEDED, proofResponse.getState());
    }

    @Test
    void testUpdateStateFailed() {
        doNothing().when(simpMessagingTemplate).convertAndSendToUser(uuidCaptor.capture(), destinationCaptor.capture(), payloadCaptor.capture());

        proofService.updateState("abcd", "failed");

        assertEquals("abcd", uuidCaptor.getValue());
        assertEquals("/present-proof/response", destinationCaptor.getValue());
        ProofResponse proofResponse = (ProofResponse) payloadCaptor.getValue();
        assertEquals(ProofResponse.State.FAILED, proofResponse.getState());
    }

    @Test
    void testTimeOut() {
        doNothing().when(simpMessagingTemplate).convertAndSendToUser(uuidCaptor.capture(), destinationCaptor.capture(), payloadCaptor.capture());
        proofService.updateState("abcd", "timed-out");
        assertEquals("abcd", uuidCaptor.getValue());
        assertEquals("/present-proof/response", destinationCaptor.getValue());
        ProofResponse proofResponse = (ProofResponse) payloadCaptor.getValue();
        assertEquals(ProofResponse.State.TIMED_OUT, proofResponse.getState());
    }

    @Test
    void testPolicyValidationFailed() {
        doNothing().when(simpMessagingTemplate).convertAndSendToUser(uuidCaptor.capture(), destinationCaptor.capture(), payloadCaptor.capture());
        proofService.updateState("abcd", "policy validation failed");
        assertEquals("abcd", uuidCaptor.getValue());
        assertEquals("/present-proof/response", destinationCaptor.getValue());
        ProofResponse proofResponse = (ProofResponse) payloadCaptor.getValue();
        assertEquals(ProofResponse.State.POLICY_FAILED, proofResponse.getState());
    }

    @Test
    void testUpdateStateUnknownState() {
        proofService.updateState("abcd", "other");

        verifyNoMoreInteractions(simpMessagingTemplate);
    }
}
