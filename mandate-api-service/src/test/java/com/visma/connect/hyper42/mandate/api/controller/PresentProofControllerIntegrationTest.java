/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.visma.connect.hyper42.mandate.api.TestDataGenerator;
import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.service.ProofService;
import com.visma.connect.hyper42.mandate.api.util.JsonProofValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.HttpClientErrorException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers = PresentProofController.class)
class PresentProofControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProofService proofService;

    @MockBean
    private JsonProofValidator jsonProofValidator;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testCreateProofRequestSuccessCORS() throws Exception {
        String schemaName = TestDataGenerator.createSchemaName();
        String comment = TestDataGenerator.createProofCommentString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        String mockResponse = TestDataGenerator.createInvitationUrlEncoded();

        when(jsonProofValidator.validate(schemaName, requestJson)).thenReturn(true);
        when(proofService.createProofRequest(any(String.class), any(String.class), any(String.class), any(String.class))).thenReturn(mockResponse);

        this.mockMvc.perform(post("/proofs?schemaName=" + schemaName + "&comment=" + comment)
                .header("Origin", "www.example.com").content(requestJson))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(mockResponse))
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testCreateProofRequestNoCommentSuccessCORS() throws Exception {
        String schemaName = TestDataGenerator.createSchemaName();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        String mockResponse = TestDataGenerator.createInvitationUrlEncoded();

        when(jsonProofValidator.validate(schemaName, requestJson)).thenReturn(true);
        when(proofService.createProofRequest(any(String.class), isNull(), any(String.class), any(String.class))).thenReturn(mockResponse);

        this.mockMvc.perform(post("/proofs?schemaName=" + schemaName)
                .header("Origin", "www.example.com").content(requestJson))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(mockResponse))
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testCreateProofRequestInvalidJsonClientErrorCORS() throws Exception {
        String schemaName = TestDataGenerator.createSchemaName();
        String comment = TestDataGenerator.createProofCommentString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();

        when(jsonProofValidator.validate(schemaName, requestJson)).thenReturn(false);

        this.mockMvc.perform(post("/proofs?schemaName=" + schemaName + "&comment=" + comment)
                .header("Origin", "www.example.com")
                .content(requestJson))
                .andExpect(status().is4xxClientError())
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testCreateProofRequestClientErrorCORS() throws Exception {
        String schemaName = TestDataGenerator.createSchemaName();
        String comment = TestDataGenerator.createProofCommentString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();

        when(jsonProofValidator.validate(schemaName, requestJson)).thenReturn(true);
        when(proofService.createProofRequest(any(), any(), any(), any())).thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));

        this.mockMvc.perform(post("/proofs?schemaName=" + schemaName + "&comment=" + comment)
                .header("Origin", "www.example.com")
                .content(requestJson))
                .andExpect(status().is4xxClientError())
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testCreateProofRequestInternalServerErrorCORS() throws Exception {
        String schemaName = TestDataGenerator.createSchemaName();
        String comment = TestDataGenerator.createProofCommentString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();

        when(jsonProofValidator.validate(schemaName, requestJson)).thenReturn(true);
        when(proofService.createProofRequest(any(), any(), any(), any())).thenThrow(new ApplicationRuntimeException("internal server error"));

        this.mockMvc.perform(post("/proofs?schemaName=" + schemaName + "&comment=" + comment)
                .header("Origin", "www.example.com")
                .content(requestJson))
                .andExpect(status().is5xxServerError())
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }
}
