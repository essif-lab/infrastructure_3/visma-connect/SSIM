/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.api.TestDataGenerator;
import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.rest.SchemaRestClient;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class JsonSchemaValidatorTest {

    private static final String FENIX_JSon = "{\n"
            + "  \"represented-company-name\": \"company the first one\",\n"
            + "  \"represented-address\": \"Havenstraat 1\",\n"
            + "  \"represented-postal-code\": \"1245 AB\",\n"
            + "  \"represented-place\": \"Aan Zee\",\n"
            + "  \"represented-country\": \"The Netherlands\",\n"
            + "  \"represented-chamber-of-commerce-registration-no\": \"kvk123\",\n"
            + "  \"represented-vat-id-no\": \"btw456\",\n"
            + "  \"represented-eori-number\": \"eori789\",\n"
            + "  \"forwarder-company-name\": \"The Second\",\n"
            + "  \"forwarder-address\": \"Stationsweg 39\",\n"
            + "  \"forwarder-postal-code\": \"6790 DE\",\n"
            + "  \"forwarder-place\": \"Aan het Spoor\",\n"
            + "  \"forwarder-country\": \"Belgium\",\n"
            + "  \"valid-from\": \"2021-01-01\",\n"
            + "  \"valid-until\": \"2021-12-31\"\n"
            + "}";

    @Autowired
    private JsonSchemaValidator jsonSchemaValidator;

    @MockBean
    private ObjectMapper mapper;

    @MockBean
    private SchemaRestClient schemaRestClient;

    @Captor
    private ArgumentCaptor<String> fileName;

    @Captor
    private ArgumentCaptor<String> contentCaptor;

    @Test
    void testValidateChamberOfCommerce() throws JsonProcessingException {
        String schemaName = "mandate-kvk-schema:1.0";
        String json = "{\"chamber-of-commerce-reference\":\"123\", \"director\":\"me\"}";
        String schemaDefinition = TestDataGenerator.createKvkSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createKvkSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(json);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertTrue(jsonSchemaValidator.validate(schemaName, json));
        assertEquals("mandate-kvk-schema:1.0", fileName.getValue());
        assertEquals("{\"chamber-of-commerce-reference\":\"123\", \"director\":\"me\"}", contentCaptor.getValue());
    }

    @Test
    void testValidateSigning() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String json = "{\"authorization\":\"something\", \"spending-limit\":\"12345\"}";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(json);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertTrue(jsonSchemaValidator.validate(schemaName, json));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
        assertEquals("{\"authorization\":\"something\", \"spending-limit\":\"12345\"}", contentCaptor.getValue());
    }

    @Test
    void testValidateFenex() throws JsonProcessingException {
        String schemaName = "fenex-schema:1.0";
        String schemaDefinition = TestDataGenerator.FENEX_SCHEMA_JSON;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.FENEX_SCHEMA_JSON);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(FENIX_JSon);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertTrue(jsonSchemaValidator.validate(schemaName, FENIX_JSon));
        assertEquals("fenex-schema:1.0", fileName.getValue());
        assertEquals(FENIX_JSon, contentCaptor.getValue());
    }

    @Test
    void testValidateSigningEncoded() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic%3A1.0";
        String json = "{\"authorization\":\"something\", \"spending-limit\":\"12345\"}";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(json);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertTrue(jsonSchemaValidator.validate(schemaName, json));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateSigningMissingSchema() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic%3A1.0";
        String json = "{\"authorization\":\"something\", \"spending-limit\":\"12345\"}";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(null);

        assertFalse(jsonSchemaValidator.validate(schemaName, json));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateMissingCCref() throws JsonProcessingException {
        String schemaName = "mandate-kvk-schema:1.0";
        String json = "{\"director\":\"me\"}";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(json);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonSchemaValidator.validate(schemaName, json));
        assertEquals("mandate-kvk-schema:1.0", fileName.getValue());
    }

    @Test
    void testValidateMissingDirector() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String json = "{\"chamber-of-commerce-reference\":\"123\"}";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(json);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonSchemaValidator.validate(schemaName, json));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateInvalidJson() throws JsonProcessingException {
        String schemaName = "mandate-kvk-schema:1.0";
        String json = "{\"chamber-of-commerce-reference:\", \"director\":\"me\"}";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode).thenThrow(JsonProcessingException.class);

        assertFalse(jsonSchemaValidator.validate(schemaName, json));
        assertEquals("mandate-kvk-schema:1.0", fileName.getValue());
    }

    @Test
    void testValidateInvalidSchemaName() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String json = "{\"chamber-of-commerce-reference\":\"123\", \"director\":\"me\"}";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(json);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonSchemaValidator.validate(schemaName, json));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testJsonProcessingException() throws JsonProcessingException {
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenThrow(JsonProcessingException.class);
        assertThrows(ApplicationRuntimeException.class, () -> jsonSchemaValidator.validate("mandate-kvk-schema:1.0", "{\"some\":\"json\"}"));
    }
}