/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest.impl;

import com.visma.connect.hyper42.mandate.api.TestDataGenerator;
import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.json.MandateRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.json.RevokeCredentialRequest;
import com.visma.connect.hyper42.mandate.api.rest.MandateServiceRestClient;
import java.net.URI;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MandateServiceRestClientImplTest {

    @InjectMocks
    private final MandateServiceRestClient mandateServiceRestClient = new MandateServiceRestClientImpl();

    @Mock
    private static RestTemplate restTemplate;

    @Captor
    private ArgumentCaptor<URI> uriCaptor;

    @Captor
    private ArgumentCaptor<Object> requestBodyCaptor;

    @Captor
    private ArgumentCaptor<HttpMethod> httpMethodCaptor;

    @Test
    void CreateMandateRequestOk() {
        String schemaName = TestDataGenerator.createSchemaName();
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;
        String url = TestDataGenerator.createInvitationUrlDecoded();

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(ResponseEntity.ok(url));

        String response = mandateServiceRestClient.createMandateRequest(schemaName, alias, requestJson, ssicommsEnabled);
        Assertions.assertEquals(url, response);
        URI restUri = uriCaptor.getValue();
        Assertions.assertTrue(restUri.getPath().contains("/mandates/" + schemaName));
        Assertions.assertTrue(restUri.getQuery().contains("alias=" + alias));
    }

    @Test
    void CreateMandateRequestNoContent() {
        String schemaName = TestDataGenerator.createSchemaName();
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;
        String error = "A technical error has occurred, please try again later.";

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(ResponseEntity.ok().build());

        try {
            mandateServiceRestClient.createMandateRequest(schemaName, alias, requestJson, ssicommsEnabled);
        } catch (ApplicationRuntimeException are) {
            Assertions.assertEquals(error, are.getMessage());
        }
        URI restUri = uriCaptor.getValue();
        Assertions.assertTrue(restUri.getPath().contains("/mandates/" + schemaName));
        Assertions.assertTrue(restUri.getQuery().contains("alias=" + alias));
    }

    @Test
    void CreateMandateRequestClientErrorSpecific() {
        String schemaName = TestDataGenerator.createSchemaName();
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;
        String error = "someClientError";

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(ResponseEntity.badRequest().body(error));

        try {
            mandateServiceRestClient.createMandateRequest(schemaName, alias, requestJson, ssicommsEnabled);
        } catch (HttpClientErrorException hcee) {
            Assertions.assertEquals(error, hcee.getMessage());
        }
        URI restUri = uriCaptor.getValue();
        Assertions.assertTrue(restUri.getPath().contains("/mandates/" + schemaName));
        Assertions.assertTrue(restUri.getQuery().contains("alias=" + alias));
    }

    @Test
    void CreateMandateRequestClientErrorUnknown() {
        String schemaName = TestDataGenerator.createSchemaName();
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;
        String error = "Request is invalid, please review your input and try again.";

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(ResponseEntity.badRequest().build());

        try {
            mandateServiceRestClient.createMandateRequest(schemaName, alias, requestJson, ssicommsEnabled);
        } catch (HttpClientErrorException hcee) {
            Assertions.assertEquals(error, hcee.getMessage());
        }
        URI restUri = uriCaptor.getValue();
        Assertions.assertTrue(restUri.getPath().contains("/mandates/" + schemaName));
        Assertions.assertTrue(restUri.getQuery().contains("alias=" + alias));
    }

    @Test
    void CreateMandateRequestInternalServerError() {
        String schemaName = TestDataGenerator.createSchemaName();
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;
        String error = "A technical error has occurred, please try again later.";

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any()))
                .thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());

        try {
            mandateServiceRestClient.createMandateRequest(schemaName, alias, requestJson, ssicommsEnabled);
        } catch (ApplicationRuntimeException are) {
            Assertions.assertEquals(error, are.getMessage());
        }
        URI restUri = uriCaptor.getValue();
        Assertions.assertTrue(restUri.getPath().contains("/mandates/" + schemaName));
        Assertions.assertTrue(restUri.getQuery().contains("alias=" + alias));
    }

    @Test
    void testRetrieveIssuedMandates() {
        List<MandateRequest> mandateRequests = TestDataGenerator.formIssuedMandateRequests();
        ResponseEntity<List<MandateRequest>> responseEntity = new ResponseEntity<>(mandateRequests, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(responseEntity);

        List<MandateRequest> result = mandateServiceRestClient.retrieveIssuedMandates();

        assertEquals(1, result.size());
        assertEquals("someConnectionId", result.get(0).getConnectionId());
        assertEquals("someSchemaName", result.get(0).getSchemaName());
        assertEquals("someRevRegId", result.get(0).getRevRegId());
        assertEquals("someCredRevId", result.get(0).getCredRevId());
        assertEquals("{\"authorization\":\"Books\",\"spending-limit\":\"9001\"}", result.get(0).getRequest());

        Assertions.assertTrue(uriCaptor.getValue().getPath().contains("/mandates/issued"));
        assertEquals(HttpMethod.GET, httpMethodCaptor.getValue());
    }

    @Test
    void testRetrieveIssuedMandatesNoContent() {
        ResponseEntity<List<MandateRequest>> responseEntity = new ResponseEntity<>(null, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(responseEntity);

        ApplicationRuntimeException exception = assertThrows(ApplicationRuntimeException.class, mandateServiceRestClient::retrieveIssuedMandates);

        Assertions.assertTrue(uriCaptor.getValue().getPath().contains("/mandates/issued"));
        assertEquals(HttpMethod.GET, httpMethodCaptor.getValue());
        assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
    }

    @Test
    void testRetrieveIssuedMandatesClientError() {
        ResponseEntity<List<MandateRequest>> responseEntity = new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        Mockito.when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(responseEntity);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, mandateServiceRestClient::retrieveIssuedMandates);

        Assertions.assertTrue(uriCaptor.getValue().getPath().contains("/mandates/issued"));
        assertEquals(HttpMethod.GET, httpMethodCaptor.getValue());
        assertEquals("Request is invalid, please review your input and try again.", exception.getMessage());
    }

    @Test
    void testRetrieveIssuedMandatesInternalServerError() {
        List<MandateRequest> mandateRequests = TestDataGenerator.formIssuedMandateRequests();
        ResponseEntity<List<MandateRequest>> responseEntity = new ResponseEntity<>(mandateRequests, HttpStatus.INTERNAL_SERVER_ERROR);
        Mockito.when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(responseEntity);

        ApplicationRuntimeException exception = assertThrows(ApplicationRuntimeException.class, mandateServiceRestClient::retrieveIssuedMandates);

        Assertions.assertTrue(uriCaptor.getValue().getPath().contains("/mandates/issued"));
        assertEquals(HttpMethod.GET, httpMethodCaptor.getValue());
        Assertions.assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
    }

    @Test
    void testRetrieveIssuedMandatesEmptyResponseBody() {
        ResponseEntity<List<MandateRequest>> responseEntity = new ResponseEntity<>(null, HttpStatus.OK);
        Mockito.when(restTemplate.exchange(uriCaptor.capture(), httpMethodCaptor.capture(), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(responseEntity);

        ApplicationRuntimeException exception = assertThrows(ApplicationRuntimeException.class, mandateServiceRestClient::retrieveIssuedMandates);

        Assertions.assertTrue(uriCaptor.getValue().getPath().contains("/mandates/issued"));
        assertEquals(HttpMethod.GET, httpMethodCaptor.getValue());
        Assertions.assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
    }

    @Test
    void testRevokeCredentialOk() {
        String credRevId = "someCredRevId";
        String revRegId = "someRevRegId";

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(ResponseEntity.ok("success"));

        String response = mandateServiceRestClient.revokeCredential(credRevId, revRegId);
        RevokeCredentialRequest request = (RevokeCredentialRequest) requestBodyCaptor.getValue();

        Assertions.assertEquals("success", response);
        Assertions.assertEquals("someCredRevId", request.getCredRevId());
        Assertions.assertEquals("someRevRegId", request.getRevRegId());
        URI restUri = uriCaptor.getValue();
        Assertions.assertTrue(restUri.getPath().contains("/mandates/revoke"));
    }

    @Test
    void testRevokeCredentialNoContent() {
        String credRevId = "someCredRevId";
        String revRegId = "someRevRegId";

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(ResponseEntity.ok().build());

        ApplicationRuntimeException exception =
                assertThrows(ApplicationRuntimeException.class, () -> mandateServiceRestClient.revokeCredential(credRevId, revRegId));
        RevokeCredentialRequest request = (RevokeCredentialRequest) requestBodyCaptor.getValue();

        Assertions.assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
        Assertions.assertEquals("someCredRevId", request.getCredRevId());
        Assertions.assertEquals("someRevRegId", request.getRevRegId());
        URI restUri = uriCaptor.getValue();
        Assertions.assertTrue(restUri.getPath().contains("/mandates/revoke"));
    }

    @Test
    void testRevokeCredentialClientError() {
        String credRevId = "someCredRevId";
        String revRegId = "someRevRegId";

        ResponseEntity<Object> responseEntity = new ResponseEntity<>(HttpStatus.FORBIDDEN);
        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(responseEntity);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> mandateServiceRestClient.revokeCredential(credRevId, revRegId));
        RevokeCredentialRequest request = (RevokeCredentialRequest) requestBodyCaptor.getValue();

        Assertions.assertTrue(uriCaptor.getValue().getPath().contains("/mandates/revoke"));
        Assertions.assertEquals("Request is invalid, please review your input and try again.", exception.getMessage());
        Assertions.assertEquals("someCredRevId", request.getCredRevId());
        Assertions.assertEquals("someRevRegId", request.getRevRegId());
    }

    @Test
    void testRevokeCredentialClientErrorSpecific() {
        String credRevId = "someCredRevId";
        String revRegId = "someRevRegId";

        ResponseEntity<Object> responseEntity = new ResponseEntity<>("forbidden", HttpStatus.FORBIDDEN);
        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(responseEntity);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> mandateServiceRestClient.revokeCredential(credRevId, revRegId));
        RevokeCredentialRequest request = (RevokeCredentialRequest) requestBodyCaptor.getValue();

        Assertions.assertTrue(uriCaptor.getValue().getPath().contains("/mandates/revoke"));
        Assertions.assertEquals("forbidden", exception.getMessage());
        Assertions.assertEquals("someCredRevId", request.getCredRevId());
        Assertions.assertEquals("someRevRegId", request.getRevRegId());
    }

    @Test
    void testRevokeCredentialInternalServerError() {
        String credRevId = "someCredRevId";
        String revRegId = "someRevRegId";

        ResponseEntity<Object> responseEntity = new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(responseEntity);

        ApplicationRuntimeException exception =
                assertThrows(ApplicationRuntimeException.class, () -> mandateServiceRestClient.revokeCredential(credRevId, revRegId));
        RevokeCredentialRequest request = (RevokeCredentialRequest) requestBodyCaptor.getValue();

        Assertions.assertTrue(uriCaptor.getValue().getPath().contains("/mandates/revoke"));
        Assertions.assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
        Assertions.assertEquals("someCredRevId", request.getCredRevId());
        Assertions.assertEquals("someRevRegId", request.getRevRegId());
    }

    @Test
    void testRevokeCredentialEmptyResponseBody() {
        String credRevId = "someCredRevId";
        String revRegId = "someRevRegId";

        ResponseEntity<Object> responseEntity = new ResponseEntity<>(null, HttpStatus.OK);
        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(responseEntity);

        ApplicationRuntimeException exception =
                assertThrows(ApplicationRuntimeException.class, () -> mandateServiceRestClient.revokeCredential(credRevId, revRegId));
        RevokeCredentialRequest request = (RevokeCredentialRequest) requestBodyCaptor.getValue();

        Assertions.assertTrue(uriCaptor.getValue().getPath().contains("/mandates/revoke"));
        Assertions.assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
        Assertions.assertEquals("someCredRevId", request.getCredRevId());
        Assertions.assertEquals("someRevRegId", request.getRevRegId());
    }
}