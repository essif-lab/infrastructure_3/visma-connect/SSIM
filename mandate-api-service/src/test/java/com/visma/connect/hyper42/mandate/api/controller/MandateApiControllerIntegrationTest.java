/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.api.TestDataGenerator;
import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.json.IssuedMandateResponse;
import com.visma.connect.hyper42.mandate.api.service.MandateApiService;
import com.visma.connect.hyper42.mandate.api.util.JsonSchemaValidator;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.HttpClientErrorException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers = MandateApiController.class)
class MandateApiControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MandateApiService mandateApiService;

    @MockBean
    private JsonSchemaValidator jsonSchemaValidator;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testCreateMandateRequestSuccessCORS() throws Exception {
        String schemaName = TestDataGenerator.createSchemaName();
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;
        String qrCodeUrl = "someBase64String";

        when(jsonSchemaValidator.validate(schemaName, requestJson)).thenReturn(true);
        when(mandateApiService.createMandateRequest(schemaName, alias, requestJson, ssicommsEnabled)).thenReturn(qrCodeUrl);

        this.mockMvc.perform(post("/mandates")
                        .queryParam("schemaName", schemaName)
                        .queryParam("alias", alias)
                        .queryParam("ssicommsEnabled", Boolean.toString(ssicommsEnabled))
                        .header("Origin", "www.example.com").content(requestJson))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(qrCodeUrl))
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testCreateMandateRequestNoAliasSuccessCORS() throws Exception {
        String schemaName = TestDataGenerator.createSchemaName();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;
        String qrCodeUrl = "someBase64String";

        when(jsonSchemaValidator.validate(schemaName, requestJson)).thenReturn(true);
        when(mandateApiService.createMandateRequest(schemaName, null, requestJson, ssicommsEnabled)).thenReturn(qrCodeUrl);

        this.mockMvc.perform(post("/mandates")
                        .queryParam("schemaName", schemaName)
                        .queryParam("ssicommsEnabled", Boolean.toString(ssicommsEnabled))
                        .header("Origin", "www.example.com").content(requestJson))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(qrCodeUrl))
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testCreateMandateRequestInvalidJsonClientErrorCORS() throws Exception {
        String schemaName = TestDataGenerator.createSchemaName();
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();

        when(jsonSchemaValidator.validate(schemaName, requestJson)).thenReturn(false);

        this.mockMvc.perform(post("/mandates")
                        .queryParam("schemaName", schemaName)
                        .queryParam("alias", alias)
                        .header("Origin", "www.example.com").content(requestJson))
                .andExpect(status().is4xxClientError())
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testCreateMandateRequestClientErrorCORS() throws Exception {
        String schemaName = TestDataGenerator.createSchemaName();
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;

        when(jsonSchemaValidator.validate(schemaName, requestJson)).thenReturn(true);
        when(mandateApiService.createMandateRequest(schemaName, alias, requestJson, ssicommsEnabled))
                .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));

        this.mockMvc.perform(post("/mandates")
                        .queryParam("schemaName", schemaName)
                        .queryParam("alias", alias)
                        .queryParam("ssicommsEnabled", Boolean.toString(ssicommsEnabled))
                        .header("Origin", "www.example.com").content(requestJson))
                .andExpect(status().is4xxClientError())
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testCreateMandateRequestInternalServerErrorCORS() throws Exception {
        String schemaName = TestDataGenerator.createSchemaName();
        String alias = TestDataGenerator.createAliasString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;

        when(jsonSchemaValidator.validate(schemaName, requestJson)).thenReturn(true);
        when(mandateApiService.createMandateRequest(schemaName, alias, requestJson, ssicommsEnabled))
                .thenThrow(new ApplicationRuntimeException("internal server error"));

        this.mockMvc.perform(post("/mandates")
                        .queryParam("schemaName", schemaName)
                        .queryParam("alias", alias)
                        .queryParam("ssicommsEnabled", Boolean.toString(ssicommsEnabled))
                        .header("Origin", "www.example.com").content(requestJson))
                .andExpect(status().is5xxServerError())
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testGetIssuedMandatesSuccessCORS() throws Exception {
        List<IssuedMandateResponse> responses = TestDataGenerator.formIssuedMandateResponses();
        Mockito.when(mandateApiService.retrieveIssuedMandates()).thenReturn(responses);

        this.mockMvc.perform(get("/mandates/issued")
                        .header("Origin", "www.example.com"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("[{\"schemaName\":\"someSchemaName\",\"connectionId\":\"someConnectionId\",\"values\":[]," +
                        "\"credRevId\":\"someCredRevId\",\"revRegId\":\"someRevRegId\"}]"))
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testGetIssuedMandatesEmptySuccessCORS() throws Exception {
        Mockito.when(mandateApiService.retrieveIssuedMandates()).thenReturn(new ArrayList<>());

        this.mockMvc.perform(get("/mandates/issued")
                        .header("Origin", "www.example.com"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(""))
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testGetIssuedMandatesInternalServerErrorCORS() throws Exception {
        Mockito.when(mandateApiService.retrieveIssuedMandates()).thenThrow(new ApplicationRuntimeException("internal server error"));

        this.mockMvc.perform(get("/mandates/issued")
                        .header("Origin", "www.example.com"))
                .andExpect(status().is5xxServerError())
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testRevokeCredentialInternalServerErrorCORS() throws Exception {
        Mockito.when(mandateApiService.revokeCredential(any())).thenThrow(new ApplicationRuntimeException("internal server error"));

        this.mockMvc.perform(post("/mandates/revoke")
                        .header("Origin", "www.example.com").contentType(MediaType.APPLICATION_JSON).content("{\n" +
                                "  \"schemaName\" : \"mandate-signing-basic:1.0\",\n" +
                                "  \"connectionId\" : \"446a0164-e59d-443f-9287-23554111f7a9\",\n" +
                                "  \"values\": [\n" +
                                "    {\n" +
                                "      \"credName\": \"authorization\",\n" +
                                "      \"credValue\": \"Books\"\n" +
                                "    }\n" +
                                "  ],\n" +
                                "  \"credRevId\": \"1\",\n" +
                                "  \"revRegId\": \"WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0\"\n" +
                                "}"))
                .andExpect(status().is5xxServerError())
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }

    @Test
    void testRevokeCredentialSuccessCORS() throws Exception {
        IssuedMandateResponse issuedMandateResponse = TestDataGenerator.formIssuedMandateResponses().get(0);
        ObjectMapper objectMapper = new ObjectMapper();
        Mockito.when(mandateApiService.revokeCredential(issuedMandateResponse)).thenReturn("success");

        this.mockMvc.perform(post("/mandates/revoke")
                        .header("Origin", "www.example.com").contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(issuedMandateResponse)))
                .andExpect(status().is2xxSuccessful())
                .andExpect(header().string("Access-Control-Allow-Origin", "*"));
    }
}
