/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service.impl;

import com.visma.connect.hyper42.mandate.api.common.BadRequestException;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.Attribute;
import com.visma.connect.hyper42.mandate.api.model.generated.Condition;
import com.visma.connect.hyper42.mandate.api.model.generated.Predicate;
import com.visma.connect.hyper42.mandate.api.model.generated.GetPolicyResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.json.AvailableSchemasResponse;
import com.visma.connect.hyper42.mandate.api.rest.SchemaRestClient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
class SchemaServiceImplTest {
    @Mock
    private SchemaRestClient schemaRestClient;
    @Mock
    private PolicyServiceImpl policyService;
    @InjectMocks
    private SchemaServiceImpl schemaServiceImpl;

    @Test
    void testRetrieveSchemas() {
        AvailableSchemasResponse schema1 = new AvailableSchemasResponse().withSchemaId("schema01").withTitle("Schema 01").withSsicommsEnabled(true);
        AvailableSchemasResponse schema2 = new AvailableSchemasResponse().withSchemaId("schema02").withTitle("Schema 02").withSsicommsEnabled(false);
        when(schemaRestClient.retrieveCreatedSchemas(false)).thenReturn(Arrays.asList(schema1, schema2));

        List<AvailableSchemasResponse> result = schemaServiceImpl.retrieveSchemas(false);

        assertEquals(2, result.size());
        assertTrue(result.contains(schema1));
        assertTrue(result.contains(schema2));
    }

    @Test
    void testRetrieveSchemasEmpty() {
        when(schemaRestClient.retrieveCreatedSchemas(false)).thenReturn(new ArrayList<>());

        List<AvailableSchemasResponse> result = schemaServiceImpl.retrieveSchemas(false);

        assertEquals(0, result.size());
    }

    @Test
    void testRetrieveSchemaDefinition() {
        String schemaId = "someSchemaId";
        SchemaDefinitionResponse response = new SchemaDefinitionResponse()
                .withSchemaId("someSchemaId")
                .withTitle("someTitle")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withName("someName")
                .withVersion("someVersion")
                .withSsicommsEnabled(false);

        when(schemaRestClient.retrieveSchemaDefinition(schemaId)).thenReturn(response);
        when(policyService.getPolicy("someName:someVersion")).thenReturn(null);

        SchemaDefinitionResponse result = schemaServiceImpl.retrieveSchemaDefinition(schemaId);

        assertEquals("someSchemaId", result.getSchemaId());
        assertEquals("someName", result.getName());
        assertEquals("someTitle", result.getTitle());
        assertEquals("someVersion", result.getVersion());
        assertTrue(result.getAttributes().isEmpty());
        assertTrue(result.getPredicates().isEmpty());
        assertNull(result.getPolicy());
        assertFalse(result.getSsicommsEnabled());
    }

    @Test
    void testRetrieveSchemaDefinitionWithPolicy() {
        String schemaId = "someSchemaId";
        SchemaDefinitionResponse response = new SchemaDefinitionResponse()
                .withSchemaId("someSchemaId")
                .withTitle("someTitle")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withName("someName")
                .withVersion("someVersion");

        when(schemaRestClient.retrieveSchemaDefinition(schemaId)).thenReturn(response);
        GetPolicyResponse policyResponse = new GetPolicyResponse();
        policyResponse.setId("responseId");
        policyResponse.setContent("my policy content");
        when(policyService.getPolicy("someName:someVersion")).thenReturn(policyResponse);

        SchemaDefinitionResponse result = schemaServiceImpl.retrieveSchemaDefinition(schemaId);

        assertEquals("someSchemaId", result.getSchemaId());
        assertEquals("someName", result.getName());
        assertEquals("someTitle", result.getTitle());
        assertEquals("someVersion", result.getVersion());
        assertTrue(result.getAttributes().isEmpty());
        assertTrue(result.getPredicates().isEmpty());
        assertEquals("my policy content", result.getPolicy());
    }

    @Test
    void testAddSchema() {
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withSsicommsEnabled(false);
        AddSchemaResponse addSchemaResponse = new AddSchemaResponse().withTitle("someTitle").withStatus("ok");

        when(schemaRestClient.addSchema(addSchemaRequest)).thenReturn(addSchemaResponse);

        AddSchemaResponse response = schemaServiceImpl.addSchema(addSchemaRequest);

        assertEquals("ok", response.getStatus());
        assertEquals("someTitle", response.getTitle());
        verify(policyService, never()).createOrUpdatePolicy(any(), any());
    }

    @Test
    void testAddSchemaSSICommsOK() {
        List<Attribute> attributes = List.of(
                new Attribute().withName("sipid-authorized-representative").withTitle("SIPID Authorizer representative").withType("text"),
                new Attribute().withName("sipid-mandate-provider").withTitle("SIPID Mandate provider").withType("text"),
                new Attribute().withName("normal-attribute").withTitle("Normal attribute").withType("number"));
        List<Predicate> predicates = List.of(
                new Predicate().withName("normal-attribute").withType("number")
                        .withCondition(List.of(new Condition().withTitle("someTitle").withValue(Condition.Value.fromValue(">")))));
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(attributes)
                .withPredicates(predicates)
                .withSsicommsEnabled(true);
        AddSchemaResponse addSchemaResponse = new AddSchemaResponse().withTitle("someTitle").withStatus("ok");

        when(schemaRestClient.addSchema(addSchemaRequest)).thenReturn(addSchemaResponse);

        AddSchemaResponse response = schemaServiceImpl.addSchema(addSchemaRequest);

        assertEquals("ok", response.getStatus());
        assertEquals("someTitle", response.getTitle());
        verify(policyService, never()).createOrUpdatePolicy(any(), any());
    }

    @Test
    void testAddSchemaSSICommsMissingAttributes() {
        List<Attribute> attributes = List.of(
                new Attribute().withName("normal-attribute").withTitle("Normal attribute").withType("number"));
        List<Predicate> predicates = List.of(
                new Predicate().withName("normal-attribute").withType("number")
                        .withCondition(List.of(new Condition().withTitle("someTitle").withValue(Condition.Value.fromValue(">")))));
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(attributes)
                .withPredicates(predicates)
                .withSsicommsEnabled(true);

        BadRequestException badRequestException = assertThrows(BadRequestException.class, () -> schemaServiceImpl.addSchema(addSchemaRequest));
        assertEquals("missing attribute [sipid-authorized-representative], "
                + "missing attribute [sipid-mandate-provider]", badRequestException.getLocalizedMessage());
    }

    @Test
    void testAddSchemaSSICommsPredicatesNotEmpty() {
        List<Attribute> attributes = List.of(
                new Attribute().withName("sipid-authorized-representative").withTitle("SIPID Authorizer representative").withType("text"),
                new Attribute().withName("sipid-mandate-provider").withTitle("SIPID Mandate provider").withType("text"),
                new Attribute().withName("normal-attribute").withTitle("Normal attribute").withType("number"));
        List<Predicate> predicates = List.of(
                new Predicate().withName("sipid-authorized-representative").withType("someType")
                        .withCondition(List.of(new Condition().withTitle("someTitle").withValue(Condition.Value.fromValue(">")))),
                new Predicate().withName("sipid-mandate-provider").withType("someType")
                        .withCondition(List.of(new Condition().withTitle("someTitle").withValue(Condition.Value.fromValue(">")))),
                new Predicate().withName("normal-attribute").withType("number")
                        .withCondition(List.of(new Condition().withTitle("someTitle").withValue(Condition.Value.fromValue(">")))));
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(attributes)
                .withPredicates(predicates)
                .withSsicommsEnabled(true);

        BadRequestException badRequestException = assertThrows(BadRequestException.class, () -> schemaServiceImpl.addSchema(addSchemaRequest));
        assertEquals("predicates for attribute [sipid-authorized-representative] not allowed, "
                + "predicates for attribute [sipid-mandate-provider] not allowed", badRequestException.getLocalizedMessage());
    }

    @Test
    void testAddSchemaSSICommsInvalidAttributeTypes() {
        List<Attribute> attributes = List.of(
                new Attribute().withName("sipid-authorized-representative").withTitle("SIPID Authorizer representative").withType("number"),
                new Attribute().withName("sipid-mandate-provider").withTitle("SIPID Mandate provider").withType("number"),
                new Attribute().withName("normal-attribute").withTitle("Normal attribute").withType("number"));
        List<Predicate> predicates = List.of(
                new Predicate().withName("normal-attribute").withType("number")
                        .withCondition(List.of(new Condition().withTitle("someTitle").withValue(Condition.Value.fromValue(">")))));
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(attributes)
                .withPredicates(predicates)
                .withSsicommsEnabled(true);

        BadRequestException badRequestException = assertThrows(BadRequestException.class, () -> schemaServiceImpl.addSchema(addSchemaRequest));
        assertEquals("incorrect attribute [sipid-authorized-representative] type - must be type [text], "
                + "incorrect attribute [sipid-mandate-provider] type - must be type [text]", badRequestException.getLocalizedMessage());
    }

    @Test
    void testAddSchemaSSICommsDuplicateAttributes() {
        List<Attribute> attributes = List.of(
                new Attribute().withName("sipid-authorized-representative").withTitle("SIPID Authorizer representative").withType("text"),
                new Attribute().withName("sipid-mandate-provider").withTitle("SIPID Mandate provider").withType("text"),
                new Attribute().withName("sipid-mandate-provider").withTitle("SIPID Mandate provider").withType("text"),
                new Attribute().withName("normal-attribute").withTitle("Normal attribute").withType("number"));
        List<Predicate> predicates = List.of(
                new Predicate().withName("normal-attribute").withType("number")
                        .withCondition(List.of(new Condition().withTitle("someTitle").withValue(Condition.Value.fromValue(">")))));
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(attributes)
                .withPredicates(predicates)
                .withSsicommsEnabled(true);

        BadRequestException badRequestException = assertThrows(BadRequestException.class, () -> schemaServiceImpl.addSchema(addSchemaRequest));
        assertEquals("attribute [sipid-mandate-provider] cannot have duplicates", badRequestException.getLocalizedMessage());
    }

    @Test
    void testAddSchemaSSICommsAllErrors() {
        List<Attribute> attributes = List.of(
                new Attribute().withName("sipid-mandate-provider").withTitle("SIPID Mandate provider").withType("number"),
                new Attribute().withName("sipid-mandate-provider").withTitle("SIPID Mandate provider").withType("number"),
                new Attribute().withName("normal-attribute").withTitle("Normal attribute").withType("number"));
        List<Predicate> predicates = List.of(
                new Predicate().withName("sipid-authorized-representative").withType("someType")
                        .withCondition(List.of(new Condition().withTitle("someTitle").withValue(Condition.Value.fromValue(">")))),
                new Predicate().withName("sipid-mandate-provider").withType("someType")
                        .withCondition(List.of(new Condition().withTitle("someTitle").withValue(Condition.Value.fromValue(">")))),
                new Predicate().withName("normal-attribute").withType("number")
                        .withCondition(List.of(new Condition().withTitle("someTitle").withValue(Condition.Value.fromValue(">")))));
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(attributes)
                .withPredicates(predicates)
                .withSsicommsEnabled(true);

        BadRequestException badRequestException = assertThrows(BadRequestException.class, () -> schemaServiceImpl.addSchema(addSchemaRequest));
        assertEquals("missing attribute [sipid-authorized-representative]; "
                + "predicates for attribute [sipid-authorized-representative] not allowed, "
                + "predicates for attribute [sipid-mandate-provider] not allowed; "
                + "incorrect attribute [sipid-mandate-provider] type - must be type [text]; "
                + "attribute [sipid-mandate-provider] cannot have duplicates", badRequestException.getLocalizedMessage());
    }

    @Test
    void testAddSchemaWithPolicy() {
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withPolicy("my very elaborate and correct validation policy");
        AddSchemaResponse addSchemaResponse = new AddSchemaResponse().withTitle("someTitle").withSchemaId("mySchemaId").withStatus("ok");
        SchemaDefinitionResponse schemaDefinitionResult = new SchemaDefinitionResponse().withName("someName").withVersion("1.0");
        GetPolicyResponse getPolicyResponse = new GetPolicyResponse().withContent("my very elaborate and correct validation policy");

        when(schemaRestClient.addSchema(addSchemaRequest)).thenReturn(addSchemaResponse);
        ArgumentCaptor<String> capturePolicyId = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> capturePolicy = ArgumentCaptor.forClass(String.class);
        when(policyService.createOrUpdatePolicy(capturePolicyId.capture(), capturePolicy.capture())).thenReturn("savedPolicyId");
        when(schemaRestClient.retrieveSchemaDefinition(anyString())).thenReturn(schemaDefinitionResult);
        when(policyService.getPolicy(anyString())).thenReturn(getPolicyResponse);

        AddSchemaResponse response = schemaServiceImpl.addSchema(addSchemaRequest);

        assertEquals("ok", response.getStatus());
        assertEquals("someTitle", response.getTitle());
        verify(policyService, times(1)).createOrUpdatePolicy(any(), any());
        assertEquals("someName:1.0", capturePolicyId.getValue());
        assertEquals("my very elaborate and correct validation policy", capturePolicy.getValue());
    }

    @Test
    void testAddSchemaWithEmptyPolicy() {
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withPolicy("");
        AddSchemaResponse addSchemaResponse = new AddSchemaResponse().withTitle("someTitle").withSchemaId("mySchemaId").withStatus("ok");

        when(schemaRestClient.addSchema(addSchemaRequest)).thenReturn(addSchemaResponse);

        AddSchemaResponse response = schemaServiceImpl.addSchema(addSchemaRequest);

        assertEquals("ok", response.getStatus());
        assertEquals("someTitle", response.getTitle());
        verify(policyService, never()).createOrUpdatePolicy(any(), any());
    }
}
