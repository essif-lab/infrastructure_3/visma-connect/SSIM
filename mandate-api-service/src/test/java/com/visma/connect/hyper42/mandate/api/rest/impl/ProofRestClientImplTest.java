/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest.impl;

import com.visma.connect.hyper42.mandate.api.TestDataGenerator;
import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.rest.CreateProofRequest;
import java.net.URI;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProofRestClientImplTest {

    @InjectMocks
    private ProofRestClientImpl proofRestClient;

    @Mock
    private RestTemplate restTemplate;

    @Captor
    private ArgumentCaptor<URI> uriCaptor;

    @Captor
    private ArgumentCaptor<Object> requestBodyCaptor;

    @Test
    void CreateProofRequestOk() {
        String schemaName = TestDataGenerator.createSchemaName();
        String proofComment = TestDataGenerator.createProofCommentString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        String createProofRequestResponse = TestDataGenerator.createInvitationUrlDecoded();

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any()))
                .thenReturn(ResponseEntity.ok(createProofRequestResponse));

        String response = proofRestClient.createProofRequest(
                CreateProofRequest.builder().withSchemaName(schemaName).withProofComment(proofComment).withRequestJson(requestJson).withUser("abc").build());
        Assertions.assertEquals(createProofRequestResponse, response);
        URI restUri = uriCaptor.getValue();
        Assertions.assertTrue(restUri.getPath().contains("/proofs/" + schemaName));
        Assertions.assertTrue(restUri.getQuery().contains("comment=" + proofComment));
        Assertions.assertTrue(restUri.getQuery().contains("user=" + "abc"));
    }

    @Test
    void CreateMandateRequestNoContent() {
        String schemaName = TestDataGenerator.createSchemaName();
        String proofComment = TestDataGenerator.createProofCommentString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        String error = "A technical error has occurred, please try again later.";

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(ResponseEntity.ok().build());

        ApplicationRuntimeException exception = assertThrows(ApplicationRuntimeException.class, () -> proofRestClient.createProofRequest(
                CreateProofRequest.builder().withSchemaName(schemaName).withProofComment(proofComment).withRequestJson(requestJson).withUser("abc")
                        .build()));
        Assertions.assertEquals(error, exception.getMessage());

        URI restUri = uriCaptor.getValue();
        Assertions.assertTrue(restUri.getPath().contains("/proofs/" + schemaName));
        Assertions.assertTrue(restUri.getQuery().contains("comment=" + proofComment));
        Assertions.assertTrue(restUri.getQuery().contains("user=" + "abc"));
    }

    @Test
    void CreateMandateRequestClientErrorSpecific() {
        String schemaName = TestDataGenerator.createSchemaName();
        String proofComment = TestDataGenerator.createProofCommentString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        String error = "oops";

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any()))
                .thenReturn(ResponseEntity.badRequest().body(error));

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class,
                () -> proofRestClient.createProofRequest(CreateProofRequest.builder().withSchemaName(schemaName).withProofComment(proofComment)
                        .withRequestJson(requestJson).withUser("abc").build()));

        Assertions.assertEquals(error, exception.getMessage());

        URI restUri = uriCaptor.getValue();
        Assertions.assertTrue(restUri.getPath().

                contains("/proofs/" + schemaName));
        Assertions.assertTrue(restUri.getQuery().

                contains("comment=" + proofComment));
        Assertions.assertTrue(restUri.getQuery().

                contains("user=" + "abc"));
    }

    @Test
    void CreateMandateRequestClientErrorUnknown() {
        String schemaName = TestDataGenerator.createSchemaName();
        String proofComment = TestDataGenerator.createProofCommentString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any())).thenReturn(ResponseEntity.badRequest().build());

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class,
                () -> proofRestClient.createProofRequest(CreateProofRequest.builder().withSchemaName(schemaName).withProofComment(proofComment)
                        .withRequestJson(requestJson).withUser("abc").build()));
        URI restUri = uriCaptor.getValue();
        Assertions.assertTrue(restUri.getPath().contains("/proofs/" + schemaName));
        Assertions.assertTrue(restUri.getQuery().contains("comment=" + proofComment));
        Assertions.assertTrue(restUri.getQuery().contains("user=" + "abc"));
        Assertions.assertEquals("Request is invalid, please review your input and try again.", exception.getMessage());
    }

    @Test
    void CreateMandateRequestInternalServerError() {
        String schemaName = TestDataGenerator.createSchemaName();
        String proofComment = TestDataGenerator.createProofCommentString();
        String requestJson = TestDataGenerator.createMandateRequestJsonString();
        String error = "A technical error has occurred, please try again later.";

        when(restTemplate.postForEntity(uriCaptor.capture(), requestBodyCaptor.capture(), Mockito.any()))
                .thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());

        try {
            proofRestClient.createProofRequest(CreateProofRequest.builder().withSchemaName(schemaName).withProofComment(proofComment)
                    .withRequestJson(requestJson).withUser("abc").build());
            fail();
        } catch (ApplicationRuntimeException are) {
            Assertions.assertEquals(error, are.getMessage());
        }
        URI restUri = uriCaptor.getValue();
        Assertions.assertTrue(restUri.getPath().contains("/proofs/" + schemaName));
        Assertions.assertTrue(restUri.getQuery().contains("comment=" + proofComment));
        Assertions.assertTrue(restUri.getQuery().contains("user=" + "abc"));
    }

    @Test
    void testRetrieveProofRequest() {
        ResponseEntity<Object> restResponse = ResponseEntity.ok("eyJyZXF1ZXN0X3ByZXNlbnRhdGlvbnN%2BYXR0YWNoIjpbeyJAaWQiO");
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        String result = proofRestClient.retrieveProofRequest("abc");
        assertEquals("eyJyZXF1ZXN0X3ByZXNlbnRhdGlvbnN%2BYXR0YWNoIjpbeyJAaWQiO", result);
    }

    @Test
    void testRetrieveProofRequestNoContent() {
        ResponseEntity<Object> restResponse = ResponseEntity.ok().build();
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        ApplicationRuntimeException exception = assertThrows(ApplicationRuntimeException.class, () -> proofRestClient.retrieveProofRequest("abc"));
        assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
    }

    @Test
    void testRetrieveProofRequestClientError() {
        ResponseEntity<Object> restResponse = ResponseEntity.notFound().build();
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> proofRestClient.retrieveProofRequest("abc"));
        assertEquals("Request is invalid, please review your input and try again.", exception.getMessage());
    }

    @Test
    void testRetrieveProofRequestClientErrorWithBody() {
        ResponseEntity<Object> restResponse = ResponseEntity.badRequest().body("bad");
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> proofRestClient.retrieveProofRequest("abc"));
        assertEquals("bad", exception.getMessage());
    }

    @Test
    void testRetrieveProofRequestServerError() {
        ResponseEntity<Object> restResponse = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        ApplicationRuntimeException exception = assertThrows(ApplicationRuntimeException.class, () -> proofRestClient.retrieveProofRequest("abc"));
        assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
    }

    @Test
    void testRetrieveProofRequestLoginEmail() {
        ResponseEntity<Object> restResponse = ResponseEntity.ok("test@test.com");
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        String result = proofRestClient.retrieveProofRequestLoginEmail("someUuid");
        assertEquals("test@test.com", result);
    }

    @Test
    void testRetrieveProofRequestLoginEmailNoContent() {
        ResponseEntity<Object> restResponse = ResponseEntity.ok().build();
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        ApplicationRuntimeException exception =
                assertThrows(ApplicationRuntimeException.class, () -> proofRestClient.retrieveProofRequestLoginEmail("someUuid"));
        assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
    }

    @Test
    void testRetrieveProofRequestLoginEmailClientError() {
        ResponseEntity<Object> restResponse = ResponseEntity.notFound().build();
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> proofRestClient.retrieveProofRequestLoginEmail("someUuid"));
        assertEquals("Request is invalid, please review your input and try again.", exception.getMessage());
    }

    @Test
    void testRetrieveProofRequestLoginEmailClientErrorWithBody() {
        ResponseEntity<Object> restResponse = ResponseEntity.badRequest().body("bad");
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> proofRestClient.retrieveProofRequestLoginEmail("someUuid"));
        assertEquals("bad", exception.getMessage());
    }

    @Test
    void testRetrieveProofRequestLoginEmailServerError() {
        ResponseEntity<Object> restResponse = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        when(restTemplate.getForEntity(uriCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        ApplicationRuntimeException exception =
                assertThrows(ApplicationRuntimeException.class, () -> proofRestClient.retrieveProofRequestLoginEmail("someUuid"));
        assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
    }
}