/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.json.AvailableSchemasResponse;
import com.visma.connect.hyper42.mandate.api.service.SchemaService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SchemaApiControllerTest {
    @Mock
    private SchemaService schemaService;
    @InjectMocks
    private SchemaApiController schemaApiController;

    @Test
    void testGetAvailableSchemas() {
        AvailableSchemasResponse schema1 = new AvailableSchemasResponse().withSchemaId("schema01").withTitle("Schema 01").withSsicommsEnabled(true);
        AvailableSchemasResponse schema2 = new AvailableSchemasResponse().withSchemaId("schema02").withTitle("Schema 02").withSsicommsEnabled(false);

        when(schemaService.retrieveSchemas(false)).thenReturn(Arrays.asList(schema1, schema2));

        ResponseEntity<List<AvailableSchemasResponse>> schemaResponse = schemaApiController.getAvailableSchemas(false);

        assertEquals(200, schemaResponse.getStatusCodeValue());
        List<AvailableSchemasResponse> body = schemaResponse.getBody();
        assertEquals(2, body.size());
        assertTrue(body.contains(schema1));
        assertTrue(body.contains(schema2));
    }

    @Test
    void testGetAvailableSchemasNoSchemasFound() {
        when(schemaService.retrieveSchemas(false)).thenReturn(new ArrayList<>());

        ResponseEntity<List<AvailableSchemasResponse>> schemaResponse = schemaApiController.getAvailableSchemas(false);

        assertEquals(204, schemaResponse.getStatusCodeValue());
    }

    @Test
    void testGetAvailableSchemasException() {
        when(schemaService.retrieveSchemas(false)).thenThrow(new ApplicationRuntimeException());

        ResponseEntity<List<AvailableSchemasResponse>> schemaResponse = schemaApiController.getAvailableSchemas(false);

        assertEquals(500, schemaResponse.getStatusCodeValue());
    }

    @Test
    void testGetSchemaDefinition() {
        String schemaId = "someSchemaId";
        SchemaDefinitionResponse response = new SchemaDefinitionResponse()
                .withSchemaId("someSchemaId")
                .withTitle("someTitle")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withName("someName")
                .withVersion("someVersion")
                .withSsicommsEnabled(false);

        when(schemaService.retrieveSchemaDefinition(schemaId)).thenReturn(response);

        ResponseEntity<SchemaDefinitionResponse> schemaDefinitionResponse = schemaApiController.getSchemaDefinition(schemaId);

        assertEquals(200, schemaDefinitionResponse.getStatusCodeValue());
        SchemaDefinitionResponse body = schemaDefinitionResponse.getBody();
        assertNotNull(body);
        assertEquals("someSchemaId", body.getSchemaId());
        assertEquals("someName", body.getName());
        assertEquals("someTitle", body.getTitle());
        assertEquals("someVersion", body.getVersion());
        assertTrue(body.getAttributes().isEmpty());
        assertTrue(body.getPredicates().isEmpty());
        assertFalse(body.getSsicommsEnabled());
    }

    @Test
    void testGetSchemaDefinitionWithPolicy() {
        String schemaId = "someSchemaId";
        SchemaDefinitionResponse response = new SchemaDefinitionResponse()
                .withSchemaId("someSchemaId")
                .withTitle("someTitle")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withName("someName")
                .withVersion("someVersion")
                .withPolicy("policy with content");

        when(schemaService.retrieveSchemaDefinition(schemaId)).thenReturn(response);

        ResponseEntity<SchemaDefinitionResponse> schemaDefinitionResponse = schemaApiController.getSchemaDefinition(schemaId);

        assertEquals(200, schemaDefinitionResponse.getStatusCodeValue());
        SchemaDefinitionResponse body = schemaDefinitionResponse.getBody();
        assertEquals("someSchemaId", body.getSchemaId());
        assertEquals("someName", body.getName());
        assertEquals("someTitle", body.getTitle());
        assertEquals("someVersion", body.getVersion());
        assertTrue(body.getAttributes().isEmpty());
        assertTrue(body.getPredicates().isEmpty());
        assertEquals("policy with content", body.getPolicy());
    }

    @Test
    void testGetSchemaDefinitionNoSchemasFound() {
        when(schemaService.retrieveSchemaDefinition("someSchemaId")).thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));

        ResponseEntity<SchemaDefinitionResponse> schemaDefinitionResponse = schemaApiController.getSchemaDefinition("someSchemaId");

        assertEquals(404, schemaDefinitionResponse.getStatusCodeValue());
    }

    @Test
    void testGetSchemaDefinitionException() {
        when(schemaService.retrieveSchemaDefinition("someSchemaId")).thenThrow(new ApplicationRuntimeException());

        ResponseEntity<SchemaDefinitionResponse> schemaDefinitionResponse = schemaApiController.getSchemaDefinition("someSchemaId");

        assertEquals(500, schemaDefinitionResponse.getStatusCodeValue());
    }

    @Test
    void testAddSchemaSuccess() {
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withSsicommsEnabled(false);
        AddSchemaResponse addSchemaResponse = new AddSchemaResponse().withTitle("someTitle").withStatus("ok");

        when(schemaService.addSchema(addSchemaRequest)).thenReturn(addSchemaResponse);

        ResponseEntity<AddSchemaResponse> response = schemaApiController.addSchema(addSchemaRequest);

        assertEquals(201, response.getStatusCodeValue());
        assertNotNull(response.getBody());
        assertEquals("ok", response.getBody().getStatus());
        assertEquals("someTitle", response.getBody().getTitle());
    }

    @Test
    void testAddSchemaAlreadyExists() {
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withSsicommsEnabled(false);
        AddSchemaResponse addSchemaResponse = new AddSchemaResponse().withTitle("someTitle").withStatus("already_exists");

        when(schemaService.addSchema(addSchemaRequest)).thenReturn(addSchemaResponse);

        ResponseEntity<AddSchemaResponse> response = schemaApiController.addSchema(addSchemaRequest);

        assertEquals(200, response.getStatusCodeValue());
        assertNotNull(response.getBody());
        assertEquals("already_exists", response.getBody().getStatus());
        assertEquals("someTitle", response.getBody().getTitle());
    }

    @Test
    void testAddSchemaFailed() {
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withSsicommsEnabled(false);
        AddSchemaResponse addSchemaResponse = new AddSchemaResponse().withTitle("someTitle").withStatus("failed");

        when(schemaService.addSchema(addSchemaRequest)).thenReturn(addSchemaResponse);

        ResponseEntity<AddSchemaResponse> response = schemaApiController.addSchema(addSchemaRequest);

        assertEquals(500, response.getStatusCodeValue());
        assertNotNull(response.getBody());
        assertEquals("failed", response.getBody().getStatus());
        assertEquals("someTitle", response.getBody().getTitle());
    }

    @Test
    void testAddSchemaInternalServerError() {
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withSsicommsEnabled(false);

        when(schemaService.addSchema(addSchemaRequest)).thenThrow(new ApplicationRuntimeException());

        ResponseEntity<AddSchemaResponse> response = schemaApiController.addSchema(addSchemaRequest);

        assertEquals(500, response.getStatusCodeValue());
    }

}
