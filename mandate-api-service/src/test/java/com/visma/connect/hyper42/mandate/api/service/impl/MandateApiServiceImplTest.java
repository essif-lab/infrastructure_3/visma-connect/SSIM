/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.api.TestDataGenerator;
import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.json.IssuedMandateResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.json.MandateRequest;
import com.visma.connect.hyper42.mandate.api.rest.MandateServiceRestClient;
import com.visma.connect.hyper42.mandate.api.service.MandateApiService;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MandateApiServiceImplTest {
    @InjectMocks
    private final MandateApiService mandateApiService = new MandateApiServiceImpl();

    @Mock
    private static MandateServiceRestClient agentRestClient;

    @Mock
    ObjectMapper objectMapper;

    @Captor
    private ArgumentCaptor<String> aliasCaptor;

    @Test
    void createMandateRequestOK() {
        String schemaName = TestDataGenerator.createSchemaName();
        String alias = TestDataGenerator.createAliasString();
        String jsonRequest = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;
        String urlDecoded = TestDataGenerator.createInvitationUrlDecoded();
        String urlEncoded = TestDataGenerator.createInvitationUrlEncoded();

        when(agentRestClient.createMandateRequest(schemaName, alias, jsonRequest, ssicommsEnabled)).thenReturn(urlDecoded);

        String response = mandateApiService.createMandateRequest(schemaName, alias, jsonRequest, ssicommsEnabled);
        Assertions.assertEquals(urlEncoded, response);
    }

    @Test
    void createMandateRequestGeneratedAliasOK() {
        Instant before = Instant.now().minusSeconds(5);

        String schemaName = TestDataGenerator.createSchemaName();
        String jsonRequest = TestDataGenerator.createMandateRequestJsonString();
        boolean ssicommsEnabled = true;
        String urlDecoded = TestDataGenerator.createInvitationUrlDecoded();
        String urlEncoded = TestDataGenerator.createInvitationUrlEncoded();

        when(agentRestClient.createMandateRequest(anyString(), aliasCaptor.capture(), anyString(), anyBoolean())).thenReturn(urlDecoded);

        String response = mandateApiService.createMandateRequest(schemaName, null, jsonRequest, ssicommsEnabled);
        Instant after = Instant.now().plusSeconds(5);
        Assertions.assertEquals(urlEncoded, response);
        String alias = aliasCaptor.getValue();
        String[] schemaAndTimestamp = alias.split("_");
        Assertions.assertEquals(schemaName, schemaAndTimestamp[0]);
        assertTrue(Instant.parse(schemaAndTimestamp[1]).isAfter(before));
        assertTrue(Instant.parse(schemaAndTimestamp[1]).isBefore(after));
    }

    @Test
    void testRetrieveIssuedMandatesSuccess() throws JsonProcessingException {
        List<MandateRequest> mandateRequests = TestDataGenerator.formIssuedMandateRequests();
        MandateRequest request = mandateRequests.get(0);
        JsonNode jsonNode = formJsonNode(request.getRequest());

        when(agentRestClient.retrieveIssuedMandates()).thenReturn(mandateRequests);
        when(objectMapper.readTree(request.getRequest())).thenReturn(jsonNode);

        List<IssuedMandateResponse> result = mandateApiService.retrieveIssuedMandates();

        IssuedMandateResponse response = result.get(0);

        assertEquals(1, result.size());
        assertEquals(request.getConnectionId(), response.getConnectionId());
        assertEquals(request.getSchemaName(), response.getSchemaName());
        assertEquals(request.getCredRevId(), response.getCredRevId());
        assertEquals(request.getRevRegId(), response.getRevRegId());
        assertEquals("authorization", response.getValues().get(0).getCredName());
        assertEquals("Books", response.getValues().get(0).getCredValue());
        assertEquals("spending-limit", response.getValues().get(1).getCredName());
        assertEquals("9001", response.getValues().get(1).getCredValue());
    }

    @Test
    void testRetrieveIssuedMandatesJsonProcessingException() throws JsonProcessingException {
        List<MandateRequest> mandateRequests = TestDataGenerator.formIssuedMandateRequests();
        MandateRequest request = mandateRequests.get(0);

        when(agentRestClient.retrieveIssuedMandates()).thenReturn(mandateRequests);
        when(objectMapper.readTree(request.getRequest())).thenThrow(JsonProcessingException.class);

        ApplicationRuntimeException exception = assertThrows(ApplicationRuntimeException.class, mandateApiService::retrieveIssuedMandates);
        assertTrue(exception.getMessage().startsWith("A technical error has occurred, please try again later."));
    }

    @Test
    void testRetrieveIssuedMandatesEmpty() {
        when(agentRestClient.retrieveIssuedMandates()).thenReturn(new ArrayList<>());

        List<IssuedMandateResponse> result = mandateApiService.retrieveIssuedMandates();

        assertEquals(0, result.size());
    }

    @Test
    void testRevokeCredentialOK() {
        IssuedMandateResponse issuedMandateResponse = TestDataGenerator.formIssuedMandateResponses().get(0);

        when(agentRestClient.revokeCredential(issuedMandateResponse.getCredRevId(), issuedMandateResponse.getRevRegId())).thenReturn("success");

        String response = mandateApiService.revokeCredential(issuedMandateResponse);
        assertEquals("success", response);
    }

    private JsonNode formJsonNode(String jsonRequest) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readTree(jsonRequest);
        return jsonNode;
    }
}

