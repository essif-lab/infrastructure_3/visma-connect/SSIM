/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.visma.connect.hyper42.mandate.api.service.impl.PolicyServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PolicyControllerUnitTest {

    @InjectMocks
    private PolicyController policyController;

    @Mock
    private PolicyServiceImpl policyService;

    @Test
    void shouldCreateOrUpdatePolicySuccessful() {
        // Given
        when(policyService.createOrUpdatePolicy(anyString(), anyString())).thenReturn("returnId");

        // When
        ResponseEntity<String> result = policyController.createOrUpdatePolicy("id", "content");

        // Then
        assertEquals(HttpStatus.OK, result.getStatusCode());
        assertEquals("returnId", result.getBody());
    }
}
