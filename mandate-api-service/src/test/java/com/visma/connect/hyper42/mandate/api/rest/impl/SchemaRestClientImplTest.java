/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest.impl;

import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.json.AvailableSchemasResponse;
import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SchemaRestClientImplTest {
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private SchemaRestClientImpl schemaRestClientImpl;

    @Captor
    private ArgumentCaptor<URI> urlCaptor;

    @Captor
    private ArgumentCaptor<HttpMethod> httpMethodCaptor;

    @Captor
    private ArgumentCaptor<AddSchemaRequest> addSchemaRequestArgumentCaptor;

    @BeforeEach
    public void setup() {
        Field field = ReflectionUtils.findField(SchemaRestClientImpl.class, "agentUrl");
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, schemaRestClientImpl, "http://mandateservice:8080");
    }

    @Test
    void testRetrieveCreatedSchemas() {
        AvailableSchemasResponse schema1 = new AvailableSchemasResponse().withSchemaId("schema01").withTitle("Schema 01").withSsicommsEnabled(true);
        AvailableSchemasResponse schema2 = new AvailableSchemasResponse().withSchemaId("schema02").withTitle("Schema 02").withSsicommsEnabled(false);
        ResponseEntity<List<AvailableSchemasResponse>> responseEntity = new ResponseEntity<>(Arrays.asList(schema1, schema2), HttpStatus.OK);
        when(restTemplate.exchange(urlCaptor.capture(), httpMethodCaptor.capture(), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(responseEntity);

        List<AvailableSchemasResponse> result = schemaRestClientImpl.retrieveCreatedSchemas(false);

        assertEquals(2, result.size());
        assertTrue(result.contains(schema1));
        assertTrue(result.contains(schema2));

        assertEquals("http://mandateservice:8080/schemas/all/", urlCaptor.getValue().toASCIIString());
        assertEquals(HttpMethod.GET, httpMethodCaptor.getValue());
    }

    @Test
    void testRetrieveCreatedOnlyUserSchemas() {
        AvailableSchemasResponse schema1 = new AvailableSchemasResponse().withSchemaId("schema01").withTitle("Schema 01").withSsicommsEnabled(true);
        AvailableSchemasResponse schema2 = new AvailableSchemasResponse().withSchemaId("schema02").withTitle("Schema 02").withSsicommsEnabled(false);
        ResponseEntity<List<AvailableSchemasResponse>> responseEntity = new ResponseEntity<>(Arrays.asList(schema1, schema2), HttpStatus.OK);
        Mockito.when(restTemplate.exchange(urlCaptor.capture(), httpMethodCaptor.capture(), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(responseEntity);

        List<AvailableSchemasResponse> result = schemaRestClientImpl.retrieveCreatedSchemas(true);

        assertEquals(2, result.size());
        assertTrue(result.contains(schema1));
        assertTrue(result.contains(schema2));

        assertEquals("http://mandateservice:8080/schemas/", urlCaptor.getValue().toASCIIString());
        assertEquals(HttpMethod.GET, httpMethodCaptor.getValue());
    }

    @Test
    void testRetrieveCreatedSchemasNoOk() {
        AvailableSchemasResponse schema1 = new AvailableSchemasResponse().withSchemaId("schema01").withTitle("Schema 01").withSsicommsEnabled(true);
        AvailableSchemasResponse schema2 = new AvailableSchemasResponse().withSchemaId("schema02").withTitle("Schema 02").withSsicommsEnabled(false);
        ResponseEntity<List<AvailableSchemasResponse>> responseEntity = new ResponseEntity<>(Arrays.asList(schema1, schema2), HttpStatus.FORBIDDEN);
        Mockito.when(restTemplate.exchange(urlCaptor.capture(), httpMethodCaptor.capture(), any(), any(ParameterizedTypeReference.class)))
                .thenReturn(responseEntity);

        assertThrows(ApplicationRuntimeException.class, () -> schemaRestClientImpl.retrieveCreatedSchemas(false));

        assertEquals("http://mandateservice:8080/schemas/all/", urlCaptor.getValue().toASCIIString());
        assertEquals(HttpMethod.GET, httpMethodCaptor.getValue());
    }

    @Test
    void testRetrieveSchemaDefinition() {
        String schemaId = "someSchemaId";
        SchemaDefinitionResponse response = new SchemaDefinitionResponse()
                .withSchemaId("someSchemaId")
                .withTitle("someTitle")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withName("someName")
                .withVersion("someVersion")
                .withSsicommsEnabled(false);

        ResponseEntity<Object> restResponse = ResponseEntity.ok(response);
        when(restTemplate.getForEntity(urlCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        SchemaDefinitionResponse result = schemaRestClientImpl.retrieveSchemaDefinition(schemaId);
        assertEquals("http://mandateservice:8080/schemas/" + schemaId, urlCaptor.getValue().toASCIIString());
        assertEquals("someSchemaId", result.getSchemaId());
        assertEquals("someName", result.getName());
        assertEquals("someTitle", result.getTitle());
        assertEquals("someVersion", result.getVersion());
        assertTrue(result.getAttributes().isEmpty());
        assertTrue(result.getPredicates().isEmpty());
        assertFalse(result.getSsicommsEnabled());
    }

    @Test
    void testRetrieveSchemaDefinitionClientError() {
        String schemaId = "someSchemaId";
        ResponseEntity<Object> restResponse = ResponseEntity.notFound().build();
        when(restTemplate.getForEntity(urlCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> schemaRestClientImpl.retrieveSchemaDefinition(schemaId));
        assertEquals("http://mandateservice:8080/schemas/" + schemaId, urlCaptor.getValue().toASCIIString());
        assertEquals("Request is invalid, please review your input and try again.", exception.getMessage());
    }

    @Test
    void testRetrieveSchemaDefinitionClientErrorWithNullBody() {
        String schemaId = "someSchemaId";
        ResponseEntity<Object> restResponse = ResponseEntity.ok(null);
        when(restTemplate.getForEntity(urlCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> schemaRestClientImpl.retrieveSchemaDefinition(schemaId));
        assertEquals("http://mandateservice:8080/schemas/" + schemaId, urlCaptor.getValue().toASCIIString());
        assertEquals("Request is invalid, please review your input and try again.", exception.getMessage());
    }

    @Test
    void testRetrieveSchemaDefinitionServerError() {
        String schemaId = "someSchemaId";
        ResponseEntity<Object> restResponse = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        when(restTemplate.getForEntity(urlCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        ApplicationRuntimeException exception = assertThrows(ApplicationRuntimeException.class, () -> schemaRestClientImpl.retrieveSchemaDefinition(schemaId));

        assertEquals("http://mandateservice:8080/schemas/" + schemaId, urlCaptor.getValue().toASCIIString());
        assertEquals("An error has occurred, please try again later.", exception.getMessage());
    }

    @Test
    void testAddSchema() {
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withSsicommsEnabled(true);
        AddSchemaResponse response = new AddSchemaResponse().withTitle("someTitle").withStatus("ok");

        ResponseEntity<Object> restResponse = ResponseEntity.ok(response);
        when(restTemplate.postForEntity(urlCaptor.capture(), addSchemaRequestArgumentCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        AddSchemaResponse result = schemaRestClientImpl.addSchema(addSchemaRequest);

        assertEquals("http://mandateservice:8080/schemas", urlCaptor.getValue().toASCIIString());
        assertEquals("someTitle", result.getTitle());
        assertEquals("ok", result.getStatus());
    }

    @Test
    void testAddSchemaClientError() {
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withSsicommsEnabled(true);

        ResponseEntity<Object> restResponse = ResponseEntity.notFound().build();
        when(restTemplate.postForEntity(urlCaptor.capture(), addSchemaRequestArgumentCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> schemaRestClientImpl.addSchema(addSchemaRequest));
        assertEquals("http://mandateservice:8080/schemas", urlCaptor.getValue().toASCIIString());
        assertEquals("Request is invalid, please review your input and try again.", exception.getMessage());
    }

    @Test
    void testAddSchemaServerError() {
        AddSchemaRequest addSchemaRequest = new AddSchemaRequest()
                .withName("someName")
                .withTitle("someTitle")
                .withVersion("1.0")
                .withAttributes(new ArrayList<>())
                .withPredicates(new ArrayList<>())
                .withSsicommsEnabled(true);

        ResponseEntity<Object> restResponse = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        when(restTemplate.postForEntity(urlCaptor.capture(), addSchemaRequestArgumentCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        ApplicationRuntimeException exception = assertThrows(ApplicationRuntimeException.class, () -> schemaRestClientImpl.addSchema(addSchemaRequest));

        assertEquals("http://mandateservice:8080/schemas", urlCaptor.getValue().toASCIIString());
        assertEquals("A technical error has occurred, please try again later.", exception.getMessage());
    }

    @Test
    void testRetrieveSchemaJsonDefinition() {
        String schemaName = "someSchemaName";
        ResponseEntity<Object> restResponse = ResponseEntity.ok("someSchemaJsonDefinition");

        when(restTemplate.getForEntity(urlCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        String result = schemaRestClientImpl.retrieveSchemaJsonDefinition(schemaName);
        assertEquals("http://mandateservice:8080/schemas/definitions/" + schemaName, urlCaptor.getValue().toASCIIString());
        assertEquals("someSchemaJsonDefinition", result);
    }

    @Test
    void testRetrieveSchemaJsonDefinitionClientError() {
        String schemaName = "someSchemaName";
        ResponseEntity<Object> restResponse = ResponseEntity.notFound().build();

        when(restTemplate.getForEntity(urlCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> schemaRestClientImpl.retrieveSchemaJsonDefinition(schemaName));
        assertEquals("http://mandateservice:8080/schemas/definitions/" + schemaName, urlCaptor.getValue().toASCIIString());
        assertEquals("Request is invalid, please review your input and try again.", exception.getMessage());
    }

    @Test
    void testRetrieveSchemaJsonDefinitionClientErrorWithNullBody() {
        String schemaName = "someSchemaName";
        ResponseEntity<Object> restResponse = ResponseEntity.ok(null);
        when(restTemplate.getForEntity(urlCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        HttpClientErrorException exception = assertThrows(HttpClientErrorException.class, () -> schemaRestClientImpl.retrieveSchemaJsonDefinition(schemaName));
        assertEquals("http://mandateservice:8080/schemas/definitions/" + schemaName, urlCaptor.getValue().toASCIIString());
        assertEquals("Request is invalid, please review your input and try again.", exception.getMessage());
    }

    @Test
    void testRetrieveSchemaJsonDefinitionServerError() {
        String schemaName = "someSchemaName";
        ResponseEntity<Object> restResponse = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        when(restTemplate.getForEntity(urlCaptor.capture(), Mockito.any())).thenReturn(restResponse);

        ApplicationRuntimeException exception = assertThrows(ApplicationRuntimeException.class,
                () -> schemaRestClientImpl.retrieveSchemaJsonDefinition(schemaName));

        assertEquals("http://mandateservice:8080/schemas/definitions/" + schemaName, urlCaptor.getValue().toASCIIString());
        assertEquals("An error has occurred, please try again later.", exception.getMessage());
    }

}
