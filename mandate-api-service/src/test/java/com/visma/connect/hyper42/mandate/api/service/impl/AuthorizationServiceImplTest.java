/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service.impl;

import com.visma.connect.hyper42.mandate.api.model.generated.UserResponse;
import com.visma.connect.hyper42.mandate.api.rest.AuthorizationRestClient;
import com.visma.connect.hyper42.mandate.api.util.JwtTokenUtil;
import java.time.Clock;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AuthorizationServiceImplTest {
    @Mock
    private AuthorizationRestClient authorizationRestClient;

    @Mock
    private JwtTokenUtil jwtTokenUtil;

    @InjectMocks
    private AuthorizationServiceImpl authorizationServiceImpl;

    @Test
    void testCreateToken() {
        String email = "test@test.com";
        UserResponse userResponse = new UserResponse().withId("someId").withRole("someRole").withEmail(email);
        when(authorizationRestClient.getUser(email)).thenReturn(userResponse);
        when(jwtTokenUtil.generateToken(userResponse.getId(), Clock.systemDefaultZone())).thenReturn("someToken");

        String result = authorizationServiceImpl.createToken(email);

        assertEquals("someToken", result);
    }
}
