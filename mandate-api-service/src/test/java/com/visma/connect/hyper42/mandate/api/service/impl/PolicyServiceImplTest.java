/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service.impl;

import com.visma.connect.hyper42.mandate.api.model.generated.GetPolicyResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.PolicyValidationRequest;
import com.visma.connect.hyper42.mandate.api.rest.PolicyRestClient;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PolicyServiceImplTest {

    @InjectMocks
    private PolicyServiceImpl policyService;

    @Mock
    private PolicyRestClient policyRestClient;

    @Test()
    void whenDeletingAPolicyTemporaryFileFailsThrowAnException() {
        File f;
        try {
            f = File.createTempFile("rego", ".rego");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        // Setup Mockito to fail deleting a file
        try (var files = Mockito.mockStatic(Files.class)) {
            files.when(() -> Files.deleteIfExists(f.toPath())).thenThrow(new IOException());
            var deleted = PolicyServiceImpl.deleteTempFile(f);
            assertFalse(deleted);
        }
        try (var files = Mockito.mockStatic(Files.class)) {
            files.when(() -> Files.deleteIfExists(f.toPath())).thenReturn(false);
            var deleted = PolicyServiceImpl.deleteTempFile(f);
            assertFalse(deleted);
        }
    }

    @Test
    void createTempFileThrowsIoExceptionThusFailingValidatingThePolicy() {
        try (var file = Mockito.mockStatic(File.class)) {
            file.when(() -> File.createTempFile("rego", ".rego")).thenThrow(new IOException());
            var r = policyService.validatePolicy(new PolicyValidationRequest());
            assertNull(r);
        }
    }

    @Test
    void shouldCallCreateOrUpdateRestClientSuccessful() {
        // Given
        when(policyRestClient.createOrUpdatePolicy(anyString(), anyString())).thenReturn("someId");

        // When
        String result = policyService.createOrUpdatePolicy("id", "content");

        // Then
        assertEquals("id", result);
    }

    @Test
    void shouldCallGetPolicyRestClient() {
        // Given
        GetPolicyResponse response = new GetPolicyResponse();
        response.setId("responseId");
        response.setContent("my policy content");
        when(policyRestClient.getPolicy("policyId")).thenReturn(response);

        // When
        GetPolicyResponse result = policyService.getPolicy("policyId");

        // Then
        assertEquals("responseId", result.getId());
        assertEquals("my policy content", result.getContent());
    }

    @Test
    void shouldResultNullWhenPolicyNotFound() {
        // Given
        when(policyRestClient.getPolicy("policyId")).thenReturn(null);

        // When
        GetPolicyResponse result = policyService.getPolicy("policyId");

        // Then
        assertNull(result);
    }
}
