/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest.impl;

import com.visma.connect.hyper42.mandate.api.model.generated.GetPolicyResponse;
import java.lang.reflect.Field;
import java.net.URI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PolicyRestClientImplTest {

    @InjectMocks
    private PolicyRestClientImpl policyRestClient;

    @Mock
    private RestTemplate restTemplate;

    @Captor
    private ArgumentCaptor<URI> uriCaptor;

    @Captor
    private ArgumentCaptor<Object> requestBodyCaptor;

    @BeforeEach
    public void setup() {
        Field field = ReflectionUtils.findField(PolicyRestClientImpl.class, "mandateServiceUrl");
        ReflectionUtils.makeAccessible(field);
        ReflectionUtils.setField(field, policyRestClient, "http://mandate-service:8000");
    }

    @Test
    void shouldCallMandateServicePolicyControllerSuccessful() {
        // Given
        doNothing().when(restTemplate).put(uriCaptor.capture(), requestBodyCaptor.capture());

        // When
        String result = policyRestClient.createOrUpdatePolicy("123", "content");

        // Then
        assertEquals("123", result);
        assertEquals("http://mandate-service:8000/policies/123", uriCaptor.getValue().toString());
        assertEquals("content", requestBodyCaptor.getValue());
    }

    @Test
    void shouldGetPolicyFromMandateService() {
        // Given
        GetPolicyResponse policyResponse = new GetPolicyResponse();
        policyResponse.setId("policyId");
        policyResponse.setContent("policyContent");
        ResponseEntity<GetPolicyResponse> result = ResponseEntity.ok(policyResponse);
        when(restTemplate.getForEntity(uriCaptor.capture(), ArgumentMatchers.<Class<GetPolicyResponse>>any())).thenReturn(result);

        // When
        GetPolicyResponse response = policyRestClient.getPolicy("myPolicyId");

        // Then
        assertEquals("http://mandate-service:8000/policies/myPolicyId", uriCaptor.getValue().toString());
        assertEquals("policyId", response.getId());
        assertEquals("policyContent", response.getContent());
    }

    @Test
    void shouldResultNotFoundWhenGetPolicyFromMandateService() {
        // Given
        when(restTemplate.getForEntity(uriCaptor.capture(), ArgumentMatchers.<Class<GetPolicyResponse>>any()))
                .thenThrow(new HttpClientErrorException(HttpStatus.NOT_FOUND));

        // When
        GetPolicyResponse response = policyRestClient.getPolicy("myPolicyId");

        // Then
        assertEquals("http://mandate-service:8000/policies/myPolicyId", uriCaptor.getValue().toString());
        assertNull(response);
    }

    @Test
    void shouldThrowBadRequestWhenGetPolicyFromMandateServiceGivesBadRequest() {
        // Given
        when(restTemplate.getForEntity(uriCaptor.capture(), ArgumentMatchers.<Class<GetPolicyResponse>>any()))
                .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST));

        // When
        try {
            policyRestClient.getPolicy("myPolicyId");

            fail("Test should throw exception");
        } catch (Exception e) {
            // Then
            assertTrue(e instanceof HttpClientErrorException);
            assertEquals(HttpStatus.BAD_REQUEST, ((HttpClientErrorException) e).getStatusCode());
        }
    }
}
