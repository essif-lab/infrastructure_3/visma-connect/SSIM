/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.api.TestDataGenerator;
import com.visma.connect.hyper42.mandate.api.rest.SchemaRestClient;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class JsonProofValidatorTest {

    @Autowired
    private JsonProofValidator jsonProofValidator;

    @MockBean
    private ObjectMapper mapper;

    @MockBean
    private SchemaRestClient schemaRestClient;

    @Captor
    private ArgumentCaptor<String> fileName;

    @Captor
    private ArgumentCaptor<String> contentCaptor;

    @Test
    void testValidateProof() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"credential\":\"authorization\"},{\"credential\":\"spending-limit\",\"predicate\":\">=\",\"condition\":1000}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertTrue(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofEncoded() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic%3A1.0";
        String requestString = "[{\"credential\":\"authorization\"},{\"credential\":\"spending-limit\",\"predicate\":\">=\",\"condition\":1000}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertTrue(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkEmptySchema() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"credential\":\"authorization\"},{\"credential\":\"spending-limit\",\"predicate\":\">=\",\"condition\":1000}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(null);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkNoArray() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "{\"credential\":\"authorization\"},{\"credential\":\"spending-limit\",\"predicate\":\">=\",\"condition\":1000}";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkCredentialDoesNotExistInSchema() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"credential\":\"42\"},{\"credential\":\"spending-limit\",\"predicate\":\">=\",\"condition\":1000}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkCredentialIsMissing() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"credential\":\"spending-limit\"},{\"predicate\":\">=\",\"condition\":1000}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkRequestEmpty() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkCredentialIsNull() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"credential\":null,\"predicate\":\">=\",\"condition\":1000}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkCredentialIsNullAndPredicateIsNotSupported() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"credential\":null,\"predicate\":\"<=\",\"condition\":1000}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkCredentialNoString() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"credential\":123,\"predicate\":\">=\",\"condition\":1000}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkUnsupportedPredicate() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"credential\":\"spending-limit\",\"predicate\":\"==\",\"condition\":1000}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkMultiplePredicatesRightWrongNumber() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"credential\":\"spending-limit\",\"predicate\":\">=\",\"condition\":1000}," +
                "{\"credential\":\"spending-limit\",\"predicate\":9001,\"condition\":\"?\"}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkPredicateWithoutCondition() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"credential\":\"spending-limit\",\"predicate\":\">=\"}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkConditionWithoutPredicate() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"credential\":\"spending-limit\",\"condition\":1000}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);
        JsonNode dataNode = TestDataGenerator.createSchemaNode(requestString);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode, dataNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkPredicateNoString() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"credential\":\"spending-limit\"},\"predicate\":123,\"condition\":1000}]";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }

    @Test
    void testValidateProofNOkIncorrectJson() throws JsonProcessingException {
        String schemaName = "mandate-signing-basic:1.0";
        String requestString = "[{\"I-hate-it-when-people-dont-finish-their-jsons\":\"";
        String schemaDefinition = TestDataGenerator.createSigningBasicSchemaJson;
        JsonNode schemaNode = TestDataGenerator.createSchemaNode(TestDataGenerator.createSigningBasicSchemaJson);

        Mockito.when(schemaRestClient.retrieveSchemaJsonDefinition(fileName.capture())).thenReturn(schemaDefinition);
        Mockito.when(mapper.readTree(contentCaptor.capture())).thenReturn(schemaNode);

        assertFalse(jsonProofValidator.validate(schemaName, requestString));
        assertEquals("mandate-signing-basic:1.0", fileName.getValue());
    }
}