/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.api.model.generated.json.IssuedMandateResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.json.MandateRequest;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class TestDataGenerator {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static String createSchemaName() {
        return UUID.randomUUID().toString();
    }

    public static String createAliasString() {
        return UUID.randomUUID().toString();
    }

    public static String createProofCommentString() {
        return UUID.randomUUID().toString();
    }

    public static String createMandateRequestJsonString() {
        return "{\n" +
                "   \"mandate-schema-id\":\"MandateRequest\",\n" +
                "   \"requested-schema\":\"3ccxRR8oeQR2MKmVLDvHgL:2:mandate-signing-basic:1.0\",\n" +
                "   \"representative\":\"Kevin Employee\",\n" +
                "   \"dependant\":\"7617e320-0c64-4875-a17b-e51e9048e2a0\",\n" +
                "   \"authorizations\":[\n" +
                "      {\n" +
                "         \"key\":\"authorization\",\n" +
                "         \"value\":\"signing\"\n" +
                "      },\n" +
                "      {\n" +
                "         \"key\":\"spending-limit\",\n" +
                "         \"value\":\"9001\"\n" +
                "      }\n" +
                "   ]\n" +
                "}";
    }

    public static String createProofRequestJsonString() {
        return "[\n" +
                "   {\n" +
                "      \"credential\":\"authorization\"\n" +
                "   },\n" +
                "   {\n" +
                "      \"credential\":\"spending-limit\",\n" +
                "      \"predicate\":\">=\",\n" +
                "      \"condition\":\"1000\"\n" +
                "   }\n" +
                "]";
    }

    public static String createProofRequestString() {
        return "[\n" +
                "   {\n" +
                "      \"schemaName\":\"" + createSchemaName() + "\"\n" +
                "   },\n" +
                "   {\n" +
                "      \"jsonRequest\":\"" + createProofRequestJsonString() + "\",\n" +
                "   }\n" +
                "]";
    }

    public static String createInvitationUrlDecoded() {
        return "http://www.someurl.org";
    }

    public static String createInvitationUrlEncoded() {
        return Base64.getUrlEncoder().encodeToString(createInvitationUrlDecoded().getBytes(StandardCharsets.UTF_8));
    }

    public static String createSigningBasicSchemaJson = "{\n" +
            "   \"$schema\":\"http://json-schema.org/draft-07/schema#\",\n" +
            "   \"title\":\"mandate-signing-basic:1.0\",\n" +
            "   \"type\":\"object\",\n" +
            "   \"required\":[\n" +
            "      \"authorization\",\n" +
            "      \"spending-limit\"\n" +
            "   ],\n" +
            "   \"properties\":{\n" +
            "      \"authorization\":{\n" +
            "         \"title\":\"authorization\",\n" +
            "         \"type\":\"string\"\n" +
            "      },\n" +
            "      \"spending-limit\":{\n" +
            "         \"title\":\"spending-limit\",\n" +
            "         \"type\":\"string\"\n" +
            "      }\n" +
            "   }\n" +
            "}";

    public static String createKvkSchemaJson = "{\n" +
            "   \"$schema\":\"http://json-schema.org/draft-07/schema#\",\n" +
            "   \"type\":\"object\",\n" +
            "   \"required\":[\n" +
            "      \"chamber-of-commerce-reference\",\n" +
            "      \"director\"\n" +
            "   ],\n" +
            "   \"properties\":{\n" +
            "      \"chamber-of-commerce-reference\":{\n" +
            "         \"title\":\"Chamber-of-commerce-reference\",\n" +
            "         \"type\":\"string\"\n" +
            "      },\n" +
            "      \"director\":{\n" +
            "         \"title\":\"Director\",\n" +
            "         \"type\":\"string\"\n" +
            "      }\n" +
            "   }\n" +
            "}";

    public static final String FENEX_SCHEMA_JSON = "{\n"
            + "  \"$schema\": \"http://json-schema.org/draft-07/schema#\",\n"
            + "  \"title\": \"fenex-schema:1.0.json\",\n"
            + "  \"type\": \"object\",\n"
            + "  \"required\": [\n"
            + "    \"represented-company-name\",\n"
            + "    \"represented-address\",\n"
            + "    \"represented-postal-code\",\n"
            + "    \"represented-place\",\n"
            + "    \"represented-country\",\n"
            + "    \"represented-chamber-of-commerce-registration-no\",\n"
            + "    \"represented-vat-id-no\",\n"
            + "    \"represented-eori-number\",\n"
            + "    \"forwarder-company-name\",\n"
            + "    \"forwarder-address\",\n"
            + "    \"forwarder-postal-code\",\n"
            + "    \"forwarder-place\",\n"
            + "    \"forwarder-country\",\n"
            + "    \"valid-from\",\n"
            + "    \"valid-until\"\n"
            + "  ],\n"
            + "  \"properties\": {\n"
            + "    \"represented-company-name\": {\n"
            + "      \"title\": \"Represented-company-name\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"represented-address\": {\n"
            + "      \"title\": \"Represented-address\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"represented-postal-code\": {\n"
            + "      \"title\": \"Represented-postal-code\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"represented-place\": {\n"
            + "      \"title\": \"Represented-place\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"represented-country\": {\n"
            + "      \"title\": \"Represented-country\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"represented-chamber-of-commerce-registration-no\": {\n"
            + "      \"title\": \"Represented-chamber-of-commerce-registration-no\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"represented-vat-id-no\": {\n"
            + "      \"title\": \"Represented-vat-id-no\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"represented-eori-number\": {\n"
            + "      \"title\": \"Represented-eori-number\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"forwarder-company-name\": {\n"
            + "      \"title\": \"Forwarder-company-name\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"forwarder-address\": {\n"
            + "      \"title\": \"Forwarder-address\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"forwarder-postal-code\": {\n"
            + "      \"title\": \"Forwarder-postal-code\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"forwarder-place\": {\n"
            + "      \"title\": \"Forwarder-place\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"forwarder-country\": {\n"
            + "      \"title\": \"Forwarder-country\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"valid-from\": {\n"
            + "      \"title\": \"Valid-from\",\n"
            + "      \"type\": \"string\"\n"
            + "    },\n"
            + "    \"valid-until\": {\n"
            + "      \"title\": \"Valid-until\",\n"
            + "      \"type\": \"string\"\n"
            + "    }\n"
            + "  }\n"
            + "}\n";

    public static JsonNode createSchemaNode(String schema) throws JsonProcessingException {
        return MAPPER.readTree(schema);
    }

    public static String convertObjectToJsonString(Object object) throws JsonProcessingException {
        return MAPPER.writeValueAsString(object);
    }

    public static String convertStringToBase64Utf8(String string) {
        return Base64.getEncoder().encodeToString(string.getBytes(StandardCharsets.UTF_8));
    }

    public static String convertUrlToBase64Utf8(String string) {
        return Base64.getEncoder().encodeToString(string.getBytes(StandardCharsets.UTF_8));
    }

    public static Principal createPrinciple(String name) {
        return new Principal() {

            @Override
            public String getName() {
                return name;
            }
        };
    }

    public static List<IssuedMandateResponse> formIssuedMandateResponses() {
        return Collections.singletonList(
                new IssuedMandateResponse()
                        .withSchemaName("someSchemaName")
                        .withConnectionId("someConnectionId")
                        .withCredRevId("someCredRevId")
                        .withRevRegId("someRevRegId")
                        .withValues(new ArrayList<>()));
    }

    public static List<MandateRequest> formIssuedMandateRequests() {
        return Collections.singletonList(
                new MandateRequest()
                        .withSchemaName("someSchemaName")
                        .withConnectionId("someConnectionId")
                        .withCredRevId("someCredRevId")
                        .withRevRegId("someRevRegId")
                        .withRequest("{\"authorization\":\"Books\",\"spending-limit\":\"9001\"}"));
    }
}
