/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service;

/**
 * Service for handling API calls for proofs, namely requesting and responding
 *
 * @author Kevin Kerkhoven
 */
public interface ProofService {
    /**
     * Create a proof request in order to verify a credential from the client later on by querying it's UUID.
     *
     * @param schemaName Name of requested schema which holds the credential definition ID needed for proof
     * @param proofComment Optional comment which can be used to describe the request for the client
     * @param requestJson Validated schema to add to our MongoDB
     * @param user the unique name of the user.
     * @return Proof request UUID
     */
    String createProofRequest(String schemaName, String proofComment, String requestJson, String user);

    /**
     * Retrieve the proof request based on the UUID.
     *
     * @param uuid - String
     * @return the base64 encoded proof request.
     */
    String retrieveProofRequest(String uuid);

    /**
     * Update state of a proofrequest.
     *
     * @param proofId - String
     * @param state - String
     */
    void updateState(String proofId, String state);
}
