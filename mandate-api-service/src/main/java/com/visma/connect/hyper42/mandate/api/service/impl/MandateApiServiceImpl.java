/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.json.IssuedMandateResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.json.MandateRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.json.Value;
import com.visma.connect.hyper42.mandate.api.rest.MandateServiceRestClient;
import com.visma.connect.hyper42.mandate.api.service.MandateApiService;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of the mandate api service
 *
 * @author Kevin Kerkhoven, Lukas Nakas
 */
@Service
public class MandateApiServiceImpl implements MandateApiService {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(MandateApiService.class);

    /**
     * The mandate rest client
     */
    @Autowired
    private MandateServiceRestClient mandateRestClient;

    /**
     * Instantiates ObjectMapper
     */
    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public String createMandateRequest(String schemaName, String userAlias, String jsonRequest, boolean ssicommsEnabled) {
        // Generate alias if null
        String alias = Objects.requireNonNullElseGet(userAlias, () -> schemaName + "_" + Instant.now().toString());

        // Gather invitation url for mandate
        String url = mandateRestClient.createMandateRequest(schemaName, alias, jsonRequest, ssicommsEnabled);
        LOG.debug("Received invitation response for alias {}, ssicomsEnabled={} -> url: {}.", userAlias, ssicommsEnabled, url);

        // Convert mandate request url to base64
        return Base64.getEncoder().encodeToString(url.getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public List<IssuedMandateResponse> retrieveIssuedMandates() {
        LOG.debug("Get issued credentials with rest client");
        List<MandateRequest> issuedMandates = mandateRestClient.retrieveIssuedMandates();
        List<IssuedMandateResponse> issuedMandateResponses = formIssuedMandateResponses(issuedMandates);
        LOG.debug("Found issued credentials {}", issuedMandateResponses);
        return issuedMandateResponses;
    }

    @Override
    public String revokeCredential(IssuedMandateResponse issuedMandateResponse) {
        LOG.debug("Revoking credential with revocation ID {} and registry revocation ID {}",
                issuedMandateResponse.getCredRevId(), issuedMandateResponse.getRevRegId());
        String revocationStatus = mandateRestClient.revokeCredential(issuedMandateResponse.getCredRevId(), issuedMandateResponse.getRevRegId());
        LOG.debug("Revocation done with status {}", revocationStatus);
        return revocationStatus;
    }

    private List<IssuedMandateResponse> formIssuedMandateResponses(List<MandateRequest> issuedMandates) {
        return issuedMandates.stream()
                .map(this::formIssuedMandateResponse)
                .collect(Collectors.toList());
    }

    private IssuedMandateResponse formIssuedMandateResponse(MandateRequest mandateRequest) {
        try {
            return new IssuedMandateResponse()
                    .withSchemaName(mandateRequest.getSchemaName())
                    .withConnectionId(mandateRequest.getConnectionId())
                    .withCredRevId(mandateRequest.getCredRevId())
                    .withRevRegId(mandateRequest.getRevRegId())
                    .withValues(formValues(mandateRequest.getRequest()));
        } catch (JsonProcessingException e) {
            LOG.error("Internal server error occurred in API service when trying to form issued mandate responses.");
            throw new ApplicationRuntimeException("A technical error has occurred, please try again later.", e);
        }
    }

    private List<Value> formValues(String jsonRequest) throws JsonProcessingException {
        JsonNode jsonNode = objectMapper.readTree(jsonRequest);
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(jsonNode.fieldNames(), Spliterator.ORDERED), false)
                .map(key -> formValue(key, jsonNode))
                .collect(Collectors.toList());
    }

    private Value formValue(String key, JsonNode node) {
        return new Value().withCredName(key).withCredValue(node.get(key).asText());
    }
}
