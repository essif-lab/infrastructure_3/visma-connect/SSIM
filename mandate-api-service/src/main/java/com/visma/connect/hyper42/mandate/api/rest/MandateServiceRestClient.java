/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest;

import com.visma.connect.hyper42.mandate.api.model.generated.json.MandateRequest;
import java.util.List;

/**
 * Cloud Agent Rest Client Interface for invitations
 *
 * @author Kevin Kerkhoven, Lukas Nakas
 */
public interface MandateServiceRestClient {

    /**
     * Create an invitation via our agent for the user via the mandate service
     *
     * @param schemaName Name of chosen schema
     * @param alias Alias chosen by user
     * @param requestJson Request json based on schema
     * @param ssicommsEnabled value indicating if mandate is for SSIComms enabled schema or not
     * @return Unencoded Connection invitation URL
     */
    String createMandateRequest(String schemaName, String alias, String requestJson, boolean ssicommsEnabled);

    /**
     * Retrieve all issued mandates via the mandate service
     *
     * @return List of MandateRequest
     */
    List<MandateRequest> retrieveIssuedMandates();

    /**
     * Revoke credential by using credential revocation ID
     * and registry revocation ID via the mandate service
     *
     * @param credRevId Credential revocation ID
     * @param revRegId Revocation registry ID
     * @return Status of revocation
     */
    String revokeCredential(String credRevId, String revRegId);
}