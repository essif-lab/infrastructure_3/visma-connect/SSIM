/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest;

/**
 * The CreateProofRequest.
 *
 * @author micha
 */
public class CreateProofRequest {

    /**
     * Schema name.
     */
    private final String schemaName;

    /**
     * Proof comment.
     */
    private final String proofComment;

    /**
     * Request json.
     */
    private final String requestJson;

    /**
     * User.
     */
    private final String user;

    /**
     * Instantiates a new CreateProofRequest.
     *
     * @param builder - Builder
     */
    private CreateProofRequest(Builder builder) {
        this.schemaName = builder.schemaName;
        this.proofComment = builder.proofComment;
        this.requestJson = builder.requestJson;
        this.user = builder.user;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.api.rest.CreateProofRequest#schemaName}
     */
    public String getSchemaName() {
        return schemaName;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.api.rest.CreateProofRequest#proofComment}
     */
    public String getProofComment() {
        return proofComment;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.api.rest.CreateProofRequest#requestJson}
     */
    public String getRequestJson() {
        return requestJson;
    }

    /**
     * Getter.
     *
     * @return {@link com.visma.connect.hyper42.mandate.api.rest.CreateProofRequest#user}
     */
    public String getUser() {
        return user;
    }

    /**
     * Builder.
     *
     * @return the Builder
     */
    public static Builder builder() {
        return new Builder();
    }

    /**
     * The Class Builder.
     *
     * @author Micha Wensveen
     */
    public static class Builder {

        /**
         * Schema name.
         */
        private String schemaName;

        /**
         * Proof comment.
         */
        private String proofComment;

        /**
         * Request json.
         */
        private String requestJson;

        /**
         * User.
         */
        private String user;

        /**
         * With schema name.
         *
         * @param schemaName - String
         * @return the Builder
         */
        public Builder withSchemaName(String schemaName) {
            this.schemaName = schemaName;
            return this;
        }

        /**
         * With proof comment.
         *
         * @param proofComment - String
         * @return the Builder
         */
        public Builder withProofComment(String proofComment) {
            this.proofComment = proofComment;
            return this;
        }

        /**
         * With request json.
         *
         * @param requestJson - String
         * @return the Builder
         */
        public Builder withRequestJson(String requestJson) {
            this.requestJson = requestJson;
            return this;
        }

        /**
         * With user.
         *
         * @param user - String
         * @return the Builder
         */
        public Builder withUser(String user) {
            this.user = user;
            return this;
        }

        /**
         * Builds the.
         *
         * @return the CreateProofRequest
         */
        public CreateProofRequest build() {
            return new CreateProofRequest(this);
        }
    }
}