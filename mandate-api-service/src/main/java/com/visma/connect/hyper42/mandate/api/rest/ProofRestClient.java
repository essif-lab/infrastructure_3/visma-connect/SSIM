/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest;

/**
 * Cloud Agent Rest Client Interface for requesting for and responding to proofs
 *
 * @author Kevin Kerkhoven
 */
public interface ProofRestClient {

    /**
     * Create a proof request in order to verify a credential from the client later on by querying it's UUID.
     *
     * @param createProofRequest - CreateProofRequest
     * @return Proof request UUID
     */
    String createProofRequest(CreateProofRequest createProofRequest);

    /**
     * Retrieve proof request from the Mandate service that stored it.
     *
     * @param uuid - String
     * @return the String
     */
    String retrieveProofRequest(String uuid);

    /**
     * Retrieve proof request's login email from the Mandate service.
     *
     * @param uuid - String
     * @return the String
     */
    String retrieveProofRequestLoginEmail(String uuid);
}