/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.model;

import java.security.Principal;
import java.util.UUID;

/**
 * Principal that is used for anonymous users.
 * Name property of the Principle will be a random UUID.
 */
public class AnonymousPrincipal implements Principal {

    /**
     * The name of this Principal.
     * A random UUID.
     */
    private final String name;

    /**
     * Instantiates a new anonymous principal.
     */
    public AnonymousPrincipal() {
        name = UUID.randomUUID().toString();
    }

    @Override
    public String getName() {
        return name;
    }
}