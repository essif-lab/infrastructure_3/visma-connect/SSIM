/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersionDetector;
import com.networknt.schema.ValidationMessage;
import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.rest.SchemaRestClient;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The Util that will validate Json Mandate Schemas.
 *
 * @author Micha Wensveen
 */
@Component
public class JsonSchemaValidator {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(JsonSchemaValidator.class);

    /**
     * The objectMapper
     */
    @Autowired
    private ObjectMapper mapper;

    /**
     * Instantiates SchemaRestClient
     */
    @Autowired
    private SchemaRestClient schemaRestClient;

    /**
     * Validate.
     *
     *
     * @param schemaName - String
     * @param json - String
     * @return true if successful else false
     * @throws ApplicationRuntimeException if the jsonSchema cannot be read.
     */
    public boolean validate(String schemaName, String json) {
        boolean result = false;
        // schemaName contains a ":" and that char needs to be encoded if it is to be used in a path parameter.
        String unencodedSchemaName = URLDecoder.decode(schemaName, StandardCharsets.UTF_8);
        try {
            String schemaJsonDefinition = schemaRestClient.retrieveSchemaJsonDefinition(unencodedSchemaName);
            Optional<JsonNode> schemaNode = Optional.ofNullable(mapper.readTree(schemaJsonDefinition));
            if (schemaNode.isPresent()) {
                JsonSchema schema = getJsonSchemaFromJsonNodeAutomaticVersion(schemaNode.get());
                Optional<JsonNode> node = getJsonNodeFromStringContent(json);
                if (node.isPresent()) {
                    Set<ValidationMessage> errors = schema.validate(node.get());
                    result = errors.isEmpty();
                }
            }
        } catch (JsonProcessingException e) {
            throw new ApplicationRuntimeException("Cannot read mandate schema", e);
        }
        LOG.info("Validation {}", result ? "Success" : "Fail");
        return result;
    }

    // Automatically detect version for given JsonNode
    private JsonSchema getJsonSchemaFromJsonNodeAutomaticVersion(JsonNode jsonNode) {
        JsonSchemaFactory factory = JsonSchemaFactory.getInstance(SpecVersionDetector.detect(jsonNode));
        return factory.getSchema(jsonNode);
    }

    private Optional<JsonNode> getJsonNodeFromStringContent(String content) {
        Optional<JsonNode> result;
        try {
            result = Optional.ofNullable(mapper.readTree(content));
        } catch (JsonProcessingException e) {
            LOG.info("Cannot parse json", e);
            result = Optional.empty();
        }
        return result;
    }
}
