/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.util;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.nio.charset.StandardCharsets;
import java.time.Clock;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * The Util that will handle JWT token actions.
 *
 * @author Lukas Nakas
 */
@Component
public class JwtTokenUtil {

    /**
     * JWT validity duration (1 hour) in milliseconds
     */
    public static final int JWT_TOKEN_VALIDITY_IN_MILLISECONDS = 60 * 60 * 1000;

    /**
     * The Mandate API JWT Secret
     */
    @Value("${jwt.secret}")
    private String secret;

    /**
     * Generate token for given userId.
     *
     * @param userId - String
     * @return JWT token
     */
    public String generateToken(String userId, Clock currentClock) {
        Instant currentInstant = Instant.now(currentClock);
        Instant expirationInstant = currentInstant.plusMillis(JWT_TOKEN_VALIDITY_IN_MILLISECONDS);
        Map<String, Object> claims = new HashMap<>();
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(userId)
                .setIssuedAt(Date.from(currentInstant))
                .setExpiration(Date.from(expirationInstant))
                .signWith(SignatureAlgorithm.HS512, secret.getBytes(StandardCharsets.UTF_8))
                .compact();
    }
}
