/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.visma.connect.hyper42.mandate.api.model.generated.ProofResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.ProofResponse.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * The ExceptionHandler for the Websocket.
 *
 * @author Micha Wensveen
 */
@ControllerAdvice(assignableTypes = { ProofWebSocketController.class })
public class WebsocketExceptionHandler {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(WebsocketExceptionHandler.class);

    /**
     * Handle ApplicationRuntimeException by returning a ProofResponse with an error state.
     *
     * @param ex - ApplicationRuntimeException
     * @return the ProofResponse
     */
    @MessageExceptionHandler
    @SendToUser("/present-proof/response")
    public ProofResponse handleException(Exception ex) {
        LOG.error("Problem during processing the websocket", ex);
        return new ProofResponse().withState(State.ERROR).withResponse("Oeps an error occured");
    }
}
