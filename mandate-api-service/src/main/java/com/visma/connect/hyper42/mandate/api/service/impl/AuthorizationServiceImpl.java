/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service.impl;

import com.visma.connect.hyper42.mandate.api.model.generated.UserResponse;
import com.visma.connect.hyper42.mandate.api.rest.AuthorizationRestClient;
import com.visma.connect.hyper42.mandate.api.service.AuthorizationService;
import com.visma.connect.hyper42.mandate.api.util.JwtTokenUtil;
import java.time.Clock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of the authorization service
 *
 * @author Lukas Nakas
 */
@Service
public class AuthorizationServiceImpl implements AuthorizationService {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(AuthorizationServiceImpl.class);

    /**
     * The authorization rest client
     */
    @Autowired
    private AuthorizationRestClient authorizationRestClient;

    /**
     * The JWT token util for token generation
     */
    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    public String createToken(String email) {
        UserResponse userResponse = authorizationRestClient.getUser(email);
        String userId = userResponse.getId();
        LOG.debug("Creating JWT token for userId {}", userId);
        return jwtTokenUtil.generateToken(userId, Clock.systemDefaultZone());
    }
}
