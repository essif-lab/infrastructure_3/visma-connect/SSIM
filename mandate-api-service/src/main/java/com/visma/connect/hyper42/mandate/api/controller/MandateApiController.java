/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.json.IssuedMandateResponse;
import com.visma.connect.hyper42.mandate.api.service.MandateApiService;
import com.visma.connect.hyper42.mandate.api.util.JsonSchemaValidator;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller of Mandate Service
 *
 * @author Kevin Kerkhoven, Lukas Nakas
 */
@RestController
@RequestMapping("/mandates")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class MandateApiController {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(com.visma.connect.hyper42.mandate.api.controller.MandateApiController.class);

    /**
     * Instantiate Mandate Api Service
     */
    @Autowired
    private MandateApiService mandateApiService;

    /**
     * Validator to check a Json string against a JsonSchema.
     */
    @Autowired
    private JsonSchemaValidator jsonSchemaValidator;

    /**
     * Create a mandate invitation request
     *
     * @param schemaName Name of requested schema
     * @param alias The alias of the user for whom we create the invite
     * @param ssicommsEnabled value indicating is mandate is SSIComms enabled or not
     * @param jsonRequest contents of requested schema
     * @return Base64 UTF8 URL String for connection invitation
     */
    @Operation(summary = "Submit a new request for a mandate")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "A new request for the mandate is created.",
                    content = @Content(mediaType = "text/plain", schema = @Schema(type = "string"),
                            examples = { @ExampleObject(name = "Base64 encoded invitation to be used in displaying a QR-Code.",
                                    value = "aHR0cDovL2FyaWVzLWNsb3VkYWdlbnQtcnVubmVyOjgwMjA/Y19pPWV5SkFkSGx3WlNJNklDSmthV1E2YzI5Mk9rSjZRMkp6VGxsb1RYSnFTR2x4"
                                            + "V2tSVVZVRlRTR2M3YzNCbFl5OWpiMjV1WldOMGFXOXVjeTh4TGpBdmFXNTJhWFJoZEdsdmJpSXNJQ0pBYVdRaU9pQWlOR0kxTXpFM05qWXROV1"
                                            + "ExTmkwMFpUYzRMV0ZsWW1JdE1UVmpOemN4WXpJeFltWm1JaXdnSW5KbFkybHdhV1Z1ZEV0bGVYTWlPaUJiSWtjeFNtaGFja1paWldad1VuUTFl"
                                            + "akpxWlVwMFRIQjVjV1pyWldJeVVtRnBkRFZyUldkRlVGVlJOV1JYSWwwc0lDSnpaWEoyYVdObFJXNWtjRzlwYm5RaU9pQWlhSFIwY0RvdkwyRn"
                                            + "lhV1Z6TFdOc2IzVmtZV2RsYm5RdGNuVnVibVZ5T2pnd01qQWlMQ0FpYkdGaVpXd2lPaUFpYkc5allXeEJjbWxsYzBOc2IzVmtRV2RsYm5RaWZR"
                                            + "PT0=",
                                    summary = "Base64 encoded invitation"),
                                    @ExampleObject(name = "Base64 encoded invitation to be used in displaying a SSIComms URL.",
                                            value = "L2FwaS9tYW5kYXRlcy9mNDk5NjA2Ny0xYWVmLTRiODUtYTUyYS0yY2ZkMzNiMDdjMzE=",
                                            summary = "Base64 encoded SSIComms invitation") })),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @PostMapping
    public ResponseEntity<String> createNewMandateRequest(
            @Parameter(description = "The url-encoded id of the mandate-schema", example = "mandate-signing-basic%3A1.0") @RequestParam String schemaName,
            @Parameter(description = "Alias that can be used to identify the connection that will be created",
                    example = "Alice") @RequestParam(required = false) String alias,
            @Parameter(description = "Value indicating if schema is SSIComms enabled or not", example = "true") @RequestParam Boolean ssicommsEnabled,
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "The actual mandate request (must match the layout of the mandate-schema)",
                    required = true,
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            examples = @ExampleObject(description = "json",
                                    value = "{\"authorization\":\"Ralph\",\"spending-limit\":\"10\"}"))) @RequestBody String jsonRequest) {
        LOG.debug(
                "Received mandate request for alias {}, schema {} and SSIComms-enabled={} : {}",
                Optional.ofNullable(alias).map(a -> a.replaceAll("[\r\n\t]]", "")).orElse("NULL"), schemaName.replaceAll("[\r\n\t]]", ""),
                ssicommsEnabled, Optional.ofNullable(jsonRequest).map(j -> j.replaceAll("[\r\n\t]]", "")).orElse("NULL"));

        boolean valid = jsonSchemaValidator.validate(schemaName, jsonRequest);
        ResponseEntity<String> response;
        if (valid) {
            // Gather new invitation for mandate and store data in MongoDB
            String result = mandateApiService.createMandateRequest(schemaName, alias, jsonRequest, ssicommsEnabled);
            response = ResponseEntity.status(HttpStatus.CREATED).body(result);
        } else {
            response = ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return response;
    }

    /**
     * Revoke credential.
     *
     * @param issuedMandateResponse Issued credential details
     * @return String of revocation status
     */
    @Operation(summary = "Revoke credential")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Credential has been revoked",
                    content = @Content(mediaType = "text/plain", schema = @Schema(type = "string"),
                            examples = @ExampleObject(name = "Status of revocation",
                                    value = "success",
                                    summary = "Status of revocation"))),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content)
    })
    @PostMapping("/revoke")
    public ResponseEntity<String> revokeCredential(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "The issued mandate request",
            required = true, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
            examples = @ExampleObject(description = "json",
                    value = "{\n" +
                            "  \"schemaName\" : \"mandate-signing-basic:1.0\",\n" +
                            "  \"connectionId\" : \"446a0164-e59d-443f-9287-23554111f7a9\",\n" +
                            "  \"values\": [\n" +
                            "    {\n" +
                            "      \"credName\": \"authorization\",\n" +
                            "      \"credValue\": \"Books\"\n" +
                            "    }\n" +
                            "  ],\n" +
                            "  \"credRevId\": \"1\",\n" +
                            "  \"revRegId\": \"WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0\"\n" +
                            "}"))) @RequestBody IssuedMandateResponse issuedMandateResponse) {
        ResponseEntity<String> response;
        LOG.debug("Received credential revocation request for json {}", issuedMandateResponse.toString().replaceAll("[\r\n\t]]", ""));

        try {
            // Revoke existing credential by using its revocation ID and revocation registry ID
            String result = mandateApiService.revokeCredential(issuedMandateResponse);
            response = ResponseEntity.status(HttpStatus.OK).body(result);
            LOG.debug("Credential revoked for revocation details {}", issuedMandateResponse.toString().replaceAll("[\r\n\t]]", ""));
        } catch (ApplicationRuntimeException e) {
            LOG.error("Problem revoking selected credential", e);
            response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return response;
    }

    /**
     * Retrieve list of issued mandates
     *
     * @return List of IssuedMandateResponse
     */
    @Operation(summary = "Retrieve list of issued mandates")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "List of IssuedMandateResponse returned.",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = IssuedMandateResponse.class)),
                            examples = @ExampleObject(name = "IssuedMandateResponse",
                                    value = "[{\n" +
                                            "  \"schemaName\" : \"mandate-signing-basic:1.0\",\n" +
                                            "  \"connectionId\" : \"446a0164-e59d-443f-9287-23554111f7a9\",\n" +
                                            "  \"values\": [\n" +
                                            "    {\n" +
                                            "      \"credName\": \"authorization\",\n" +
                                            "      \"credValue\": \"Books\"\n" +
                                            "    }\n" +
                                            "  ],\n" +
                                            "  \"credRevId\": \"1\",\n" +
                                            "  \"revRegId\": \"WgWxqztrNooG92RXvxSTWv:4:WgWxqztrNooG92RXvxSTWv:3:CL:20:tag:CL_ACCUM:0\"\n" +
                                            "}]",
                                    summary = "IssuedMandateResponse"))),
            @ApiResponse(responseCode = "204", description = "No issued credentials are available"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content)
    })
    @GetMapping("/issued")
    public ResponseEntity<List<IssuedMandateResponse>> getIssuedMandates() {
        LOG.debug("Get all issued credentials");
        ResponseEntity<List<IssuedMandateResponse>> response;
        try {
            List<IssuedMandateResponse> issuedMandates = mandateApiService.retrieveIssuedMandates();
            if (issuedMandates.isEmpty()) {
                response = ResponseEntity.noContent().build();
            } else {
                response = ResponseEntity.ok(issuedMandates);
            }
        } catch (ApplicationRuntimeException e) {
            LOG.error("Problem retrieving issued credentials", e);
            response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }
}
