/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.config;

import org.apache.catalina.connector.Connector;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.servlet.TomcatServletWebServerFactoryCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;

/**
 * The TomcatServletWebServerFactoryCustomizer that will create a connector for Tomcat to listen to a port for internal use only.
 * Internal use means that request from outside the VPC should not be accepted.
 * This class has to be used in combination with {@link com.visma.connect.hyper42.mandate.api.config.InternalEndpointFilter}
 *
 * @author Micha Wensveen
 */
class InternalConnectorTomcatServletWebServerFactoryCustomizer extends TomcatServletWebServerFactoryCustomizer {

    /**
     * Port that tomcat should listen to.
     */
    private final int internalPort;

    /**
     * Instantiates a new InternalConnectorTomcatServletWebServerFactoryCustomizer.
     *
     * @param internalPort - int
     */
    /* default */ InternalConnectorTomcatServletWebServerFactoryCustomizer(int internalPort) {
        super(new ServerProperties());
        this.internalPort = internalPort;
    }

    @Override
    public void customize(TomcatServletWebServerFactory factory) {
        super.customize(factory);
        factory.addAdditionalTomcatConnectors(additionalConnector());
    }

    private Connector additionalConnector() {
        var connector = new Connector(TomcatServletWebServerFactory.DEFAULT_PROTOCOL);
        connector.setPort(internalPort);
        return connector;
    }
}