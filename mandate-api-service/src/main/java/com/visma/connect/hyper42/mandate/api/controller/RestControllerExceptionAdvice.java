/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.common.BadRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;

/**
 * This an exception controller advice class which handles the exception
 *
 * @author ragesh
 */
@RestControllerAdvice(assignableTypes = { MandateApiController.class, PresentProofController.class, PolicyController.class, SchemaApiController.class })
public class RestControllerExceptionAdvice {

    /**
     * For Application Runtime Exception
     *
     * @param ex exception
     * @return ExceptionResponse Object
     */
    @ExceptionHandler(ApplicationRuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ExceptionResponse> handleApplicationRuntimeException(ApplicationRuntimeException ex) {
        return new ResponseEntity<>(new ExceptionResponse(ex), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * For HttpClientErrorException
     *
     * @param ex exception
     * @return ExceptionResponse Object
     */
    @ExceptionHandler(HttpClientErrorException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ExceptionResponse> handleHttpClientErrorException(HttpClientErrorException ex) {
        return new ResponseEntity<>(new ExceptionResponse(ex), HttpStatus.BAD_REQUEST);
    }

    /**
     * For Bad Request Exception
     *
     * @param ex exception
     * @return ExceptionResponse Object
     */
    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<ExceptionResponse> handleBadRequestException(BadRequestException ex) {
        return new ResponseEntity<>(new ExceptionResponse(ex), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles the exception response message
     */
    public static class ExceptionResponse {
        /**
         * Holds the message
         */
        private final String message;

        /**
         * ExceptionResponse Constructor
         *
         * @param exception
         */
        public ExceptionResponse(Exception exception) {
            this.message = exception.getMessage();
        }

        public String getMessage() {
            return message;
        }
    }
}
