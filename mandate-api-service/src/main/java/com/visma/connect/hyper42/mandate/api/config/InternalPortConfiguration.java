/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring configuration for the InternalPort.
 * Configures the connecter that listens to the internal port and
 * configures the filter that will only allow access to the internal path on the internal port.
 *
 * @author Micha Wensveen
 */
@Configuration
public class InternalPortConfiguration {

    /**
     * The Internal port.
     */
    @Value("${server.internal.port}")
    private int internalPort;

    /**
     * The Internal path.
     */
    @Value("${server.internal.path}")
    private String internalPath;

    /**
     * SpringBean for the WebServerFactoryCustomizer that will add the internalPort to the webserver.
     *
     * @return the WebServerFactoryCustomizer
     */
    @Bean
    public WebServerFactoryCustomizer<TomcatServletWebServerFactory> webServerFactoryCustomizer() {
        return new InternalConnectorTomcatServletWebServerFactoryCustomizer(internalPort);
    }

    /**
     * SpringBean for the servletfilter for internalPath and internalPort validation.
     *
     * @return the FilterRegistrationBean
     */
    @Bean
    public FilterRegistrationBean<InternalEndpointFilter> trustedEndpointsFilter() {
        return new FilterRegistrationBean<>(new InternalEndpointFilter(internalPort, internalPath));
    }
}
