/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest;

import com.visma.connect.hyper42.mandate.api.model.generated.UserResponse;

/**
 * The RestClient that will call the Mandate service Rest API for processing authorization for users.
 *
 * @author Lukas Nakas
 */
public interface AuthorizationRestClient {

    /**
     * Retrieve user from the Mandate service.
     *
     * @param email - String
     * @return the String
     */
    UserResponse getUser(String email);

}
