/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.visma.connect.hyper42.mandate.api.model.generated.json.ProofState;
import com.visma.connect.hyper42.mandate.api.service.ProofService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller for requesting and responding to proof requests.
 * This controller also makes use of websockets for polling in order to to provide realtime responses for the proof.
 *
 * @author Kevin Kerkhoven
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/internal/proofs")
public class InternalProofController {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(InternalProofController.class);

    /**
     * Instantiate Mandate Api Service
     */
    @Autowired
    private ProofService proofService;

    /**
     * Receive an updated status for a given proof request.
     *
     * @param uuid - String
     * @param proofState - ProofState
     * @return the ResponseEntity
     */
    @Operation(summary = "Receive an updated status for a given proof request")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Proof request has been updated",
                    content = @Content(mediaType = "text/plain", schema = @Schema(type = "string"),
                            examples = @ExampleObject(name = "ok",
                                    summary = "ok message"))),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @PatchMapping(path = "{uuid}")
    public ResponseEntity<String> updateProofRequest(
            @Parameter(description = "proof-id of the proofrequest", example = "1234") @PathVariable("uuid") String uuid,
            @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "The information to use in the update", required = true,
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            examples = @ExampleObject(description = "json",
                                    value = "{\"id\" : \"abc-def\", \"state\" : \"succeeded\"}"))) @RequestBody ProofState proofState) {
        LOG.debug("Update for proofrequest received {}", proofState.toString().replaceAll("[\r\n\t]", ""));
        proofService.updateState(proofState.getId(), proofState.getState());
        return ResponseEntity.ok("{\"result\": \"ok\"}");
    }
}
