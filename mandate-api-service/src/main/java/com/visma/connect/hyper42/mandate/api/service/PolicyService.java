/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service;

import com.visma.connect.hyper42.mandate.api.model.generated.GetPolicyResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.PolicyValidationRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.PolicyValidationResponse;

/**
 * The Service for Policy related actions.
 *
 * @author Lukas Nakas
 */
public interface PolicyService {

    /**
     * Validate policy.
     *
     * @param policyValidationRequest The inbound request that contains the Rego file in its body
     * @return PolicyValidationResponse
     */
    PolicyValidationResponse validatePolicy(PolicyValidationRequest policyValidationRequest);

    /**
     * Create or update a policy.
     *
     * @param policyId id of the policy
     * @param policyContent policy content
     * @return response with id of the policy
     */
    String createOrUpdatePolicy(String policyId, String policyContent);

    /**
     * Get a policy from the mandate service.
     * 
     * @param policyId id of the policy
     * @return {@link GetPolicyResponse} object
     */
    GetPolicyResponse getPolicy(String policyId);
}
