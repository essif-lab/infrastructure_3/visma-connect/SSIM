/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest.impl;

import com.visma.connect.hyper42.mandate.api.model.generated.GetPolicyResponse;
import com.visma.connect.hyper42.mandate.api.rest.PolicyRestClient;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Implementation of interface {@link PolicyRestClient}.
 *
 * @author Rik Sonderkamp
 */
@Service
public class PolicyRestClientImpl implements PolicyRestClient {

    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(PolicyRestClientImpl.class);

    /**
     * Rest template
     */
    @Autowired
    private RestTemplate restTemplate;

    /**
     * The mandate service URL
     */
    @Value("${mandate.service.url}")
    private String mandateServiceUrl;

    @Override
    public String createOrUpdatePolicy(String policyId, String policyContent) {
        LOG.debug("create or update policy with policyId: {}", policyId);
        URI targetUrl = UriComponentsBuilder.fromUriString(mandateServiceUrl + "/policies/{id}")
                .build(policyId);
        restTemplate.put(targetUrl, policyContent);

        return policyId;
    }

    @Override
    public GetPolicyResponse getPolicy(String policyId) {
        LOG.debug("Get policy from the mandate service with policyId: {}", policyId);

        URI targetUrl = UriComponentsBuilder.fromUriString(mandateServiceUrl + "/policies/{id}")
                .build(policyId);
        try {
            ResponseEntity<GetPolicyResponse> response = restTemplate.getForEntity(targetUrl, GetPolicyResponse.class);

            return response.getBody();
        } catch (HttpClientErrorException e) {
            // custom catch 404 not found status
            if (HttpStatus.NOT_FOUND == e.getStatusCode()) {
                return null;
            }
            throw e;
        }

    }

}
