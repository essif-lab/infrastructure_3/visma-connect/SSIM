/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service;

import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.json.AvailableSchemasResponse;
import java.util.List;

/**
 * The Service for Schema related actions.
 *
 * @author Micha Wensveen, Lukas Nakas
 */
public interface SchemaService {

    /**
     * Retrieve schemas that can be used in the mandates.
     *
     * @param onlyUserSchemas - boolean
     * @return the List
     */
    List<AvailableSchemasResponse> retrieveSchemas(boolean onlyUserSchemas);

    /**
     * Retrieve schema definition for form generation.
     *
     * @param schemaId - String
     * @return SchemaDefinitionResponse
     */
    SchemaDefinitionResponse retrieveSchemaDefinition(String schemaId);

    /**
     * Add schema that will be used in the mandates
     *
     * @param addSchemaRequest new schema's details
     * @return AddSchemaResponse
     */
    AddSchemaResponse addSchema(AddSchemaRequest addSchemaRequest);

}
