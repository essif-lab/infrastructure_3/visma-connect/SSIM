/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.visma.connect.hyper42.mandate.api.model.generated.ProofRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.ProofResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.ProofResponse.State;
import com.visma.connect.hyper42.mandate.api.service.ProofService;
import com.visma.connect.hyper42.mandate.api.util.JsonProofValidator;
import java.security.Principal;
import java.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

/**
 * Rest controller for requesting and responding to proof requests.
 * This controller also makes use of websockets for polling in order to to provide realtime responses for the proof.
 *
 * @author Kevin Kerkhoven
 */
@Controller
public class ProofWebSocketController {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(ProofWebSocketController.class);

    /**
     * Instantiate Mandate Api Service
     */
    @Autowired
    private ProofService proofService;

    /**
     * Validator to check a Json string against a JsonSchema.
     */
    @Autowired
    private JsonProofValidator jsonProofValidator;

    /**
     * Create a proof request in order to verify a credential from the client later on by querying it's UUID.
     *
     * @param request - the requested ProofRequest
     * @param sessionId - id assigned by Spring
     * @param principal - Principal.
     * @return response to show as qr-code.
     */
    @MessageMapping("/request")
    @SendToUser("/present-proof/response")
    public ProofResponse createNewProofRequest(ProofRequest request, @Header("simpSessionId") String sessionId, Principal principal) {
        LOG.debug("Received proof request with json {} with session {} for user {}", request, sessionId, principal.getName());
        String jsonRequest = new String(Base64.getDecoder().decode(request.getJsonRequest()));
        LOG.debug("Proof request: {}", jsonRequest);

        boolean valid = jsonProofValidator.validate(request.getSchemaName(), jsonRequest);
        ProofResponse response;
        if (valid) {
            // Gather new QR url and presentation exchange URL for proof in Base64
            String responseData = proofService.createProofRequest(request.getSchemaName(), request.getComment(), jsonRequest, principal.getName());
            response = new ProofResponse().withState(State.QRCODE).withResponse(responseData);
        } else {
            response = new ProofResponse().withState(State.ERROR).withResponse("Invalid proof request json");
        }

        LOG.debug("Send response {}", response.getState());
        return response;
    }

}
