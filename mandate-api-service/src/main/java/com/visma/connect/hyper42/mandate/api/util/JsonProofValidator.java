/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.rest.SchemaRestClient;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The Util that will validate Json Proof Schemas.
 *
 * @author Kevin Kerkhoven
 */
@Component
public class JsonProofValidator {

    /**
     * The objectMapper
     */
    @Autowired
    private ObjectMapper mapper;

    /**
     * Instantiates SchemaRestClient
     */
    @Autowired
    private SchemaRestClient schemaRestClient;

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(JsonProofValidator.class);

    /**
     * Validate.
     *
     * @param schemaName - String
     * @param json - String
     * @return true if successful else false
     * @throws ApplicationRuntimeException if the jsonSchema cannot be read.
     */
    public boolean validate(String schemaName, String json) {
        boolean result = false;
        // schemaName contains a ":" and that char needs to be encoded if it is to be used in a path parameter.
        String unencodedSchemaName = URLDecoder.decode(schemaName, StandardCharsets.UTF_8);

        try {
            String schemaJsonDefinition = schemaRestClient.retrieveSchemaJsonDefinition(unencodedSchemaName);
            Optional<JsonNode> schemaNode = Optional.ofNullable(mapper.readTree(schemaJsonDefinition));
            // Check if selected schema exists and gather field properties
            if (schemaNode.isPresent()) {
                JsonNode schemaJsonNode = schemaNode.get();
                List<String> schemaCredentials = new ArrayList<>();
                schemaJsonNode.get("properties").fieldNames().forEachRemaining(schemaCredentials::add);

                JsonNode requestNode = mapper.readTree(json);

                // Check if request is an array
                if (!requestNode.isArray()) {
                    throw new IllegalArgumentException("Given json is not an array");
                }

                List<String> requestCredentials = new ArrayList<>();
                List<String> requestPredicates = new ArrayList<>();
                List<String> supportedPredicates = List.of(">=", ">", "<", "<=");

                // Iterate over all nodes
                for (JsonNode node : requestNode) {
                    gatherCredentialForNode(node).ifPresent(requestCredentials::add);
                    gatherPredicateAndCheckConditionForNode(node).ifPresent(requestPredicates::add);
                }
                // Check if the amount of conditions is as big as the amount of items, and if they all exist in the schema
                boolean credentialsOk = isCredentialsOk(schemaCredentials, requestNode, requestCredentials);
                // Check if the predicates given are supported.
                boolean conditionsOk = supportedPredicates.containsAll(requestPredicates);
                result = credentialsOk && conditionsOk;
                LOG.debug("Successfully validated proof request for schema name {} and json {}", unencodedSchemaName, json);
            } else {
                LOG.debug("No schema found {} during validation of proof", unencodedSchemaName);
            }
        } catch (JsonProcessingException | IllegalArgumentException e) {
            // When these errors occur, this is due to human error
            LOG.debug("Failed to validate proof request (reason: {}) for schema name {} and json {}", e.getMessage(), schemaName, json);
        }
        return result;
    }

    private boolean isCredentialsOk(List<String> schemaCredentials, JsonNode requestNode, List<String> requestCredentials) {
        return !requestCredentials.isEmpty() && requestCredentials.size() == requestNode.size() &&
                schemaCredentials.containsAll(requestCredentials);
    }

    private Optional<String> gatherCredentialForNode(JsonNode node) {
        Optional<String> result = Optional.empty();
        if (node.hasNonNull("credential")) {
            result = Optional.of(node.get("credential").asText());
        }
        return result;
    }

    private Optional<String> gatherPredicateAndCheckConditionForNode(JsonNode node) {
        Optional<String> result = Optional.empty();
        if (node.hasNonNull("predicate") && node.hasNonNull("condition")) {
            if (!node.get("condition").isNumber()) {
                // If condition is not a number, this is wrong.
                throw new IllegalArgumentException("Condition is not a number!");
            }
            result = Optional.of(node.get("predicate").asText());
        } else if (node.hasNonNull("predicate") || node.hasNonNull("condition")) {
            // If there is a predicate without a condition or vice-versa, this is wrong.
            throw new IllegalArgumentException("Predicate and condition are not a combination!");
        }
        return result;
    }
}