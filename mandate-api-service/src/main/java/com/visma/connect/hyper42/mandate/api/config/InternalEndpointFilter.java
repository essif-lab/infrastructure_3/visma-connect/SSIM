/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.config;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

/**
 * The InternalEndpointFilter will only allow request if:
 * - the request is for the internalPath on the internalPort or
 * - the request is not for the internalPath
 *
 * @author Micha Wensveen
 */
public class InternalEndpointFilter implements Filter {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(InternalEndpointFilter.class);

    /**
     * Internal port.
     */
    private final int internalPort;

    /**
     * Internal path.
     */
    private final String internalPath;

    /**
     * Instantiates a new InternalEndpointFilter.
     *
     * @param internalPort - int
     * @param internalPath - String
     */
    /* default */ InternalEndpointFilter(int internalPort, String internalPath) {
        this.internalPort = internalPort;
        this.internalPath = internalPath;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (!isRequestAllowed(request)) {
            LOG.warn("Request for internal endpoint on external port is not allowed");
            response.setStatus(HttpStatus.NOT_FOUND.value());
            response.getOutputStream().close();
            return;
        }
        // allowed so continue to next filter or controler.
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private boolean isRequestAllowed(HttpServletRequest request) {
        var allowed = true;
        if (request.getRequestURI().startsWith(internalPath)) {
            allowed = request.getLocalPort() == internalPort;
        }
        return allowed;
    }
}
