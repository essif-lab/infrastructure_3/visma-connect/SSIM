/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest.impl;

import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.UserResponse;
import com.visma.connect.hyper42.mandate.api.rest.AuthorizationRestClient;
import java.net.URI;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Service Implementation class for AuthorizationRestClient
 *
 * @author Lukas Nakas
 */
@Service
public class AuthorizationRestClientImpl implements AuthorizationRestClient {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(AuthorizationRestClientImpl.class);

    /**
     * The resttemplate
     */
    @Autowired
    private RestTemplate restTemplate;

    /**
     * The aries agent service URL
     */
    @Value("${mandate.service.url}")
    private String agentUrl;

    @Override
    public UserResponse getUser(String email) {
        URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/users/" + email).build().toUri();
        LOG.debug("Calling agent for retrieving user with URI {}", targetUrl);
        ResponseEntity<UserResponse> response = restTemplate.getForEntity(targetUrl, UserResponse.class);

        // Errors are handled by our advice class
        // Check whether the response is OK. If not, give detailed info on a client error if available, or return a generic error message
        var body = Optional.ofNullable(response.getBody());
        if (response.getStatusCode().is2xxSuccessful() && body.isPresent()) {
            LOG.debug("Retrieved user for email ({})  -> {}", email, body.get());
            return body.get();
        } else if (response.getStatusCode().is4xxClientError()) {
            LOG.debug("Client error occurred in API service when trying to retrieve user for email  {}", email);
            String clientError = body.isEmpty() ? "Request is invalid, please review your input and try again."
                    : body.get().toString();
            throw new HttpClientErrorException(clientError, response.getStatusCode(), response.getStatusCode().getReasonPhrase(), null, null, null);
        } else {
            LOG.error("Internal server error occurred in API service when trying to retrieve user for email {}", email);
            // We always throw a generic error when dealing with a server error!
            throw new ApplicationRuntimeException("A technical error has occurred, please try again later.");
        }
    }
}
