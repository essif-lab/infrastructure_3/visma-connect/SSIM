/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.visma.connect.hyper42.mandate.api.model.generated.PolicyValidationRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.PolicyValidationResponse;
import com.visma.connect.hyper42.mandate.api.service.PolicyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller to validate, create and retrieve policies.
 *
 * @author Lukas Nakas
 */
@RestController
@RequestMapping("/policies")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PolicyController {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(PolicyController.class);

    /**
     * Instantiate Policy Service
     */
    @Autowired
    private PolicyService policyService;

    /**
     * Validate policy.
     *
     * @param policyValidationRequest policy as string
     * @return policyValidationResponse
     */
    @Operation(summary = "Validate policy")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Policy validated",
                    content = @Content(mediaType = "application/json", schema = @Schema(type = "string"),
                            examples = @ExampleObject(name = "Policy validation response",
                                    value = "{\n"
                                            + "  \"valid\": true\n"
                                            + "}",
                                    summary = "Policy validation response"))),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content)
    })
    @PostMapping("/validate")
    public ResponseEntity<PolicyValidationResponse> validate(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Policy validation request",
            required = true, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    examples = @ExampleObject(description = "json",
                            value = "{\n"
                                    + "  \"policy\": \"package example import future.keywords  allow := true {     count(violation) == 0 }  "
                                    + "violation[server.id] {     some server in public_servers     \"http\" in server.protocols }  "
                                    + "violation[server.id] {     some server in input.servers     \"telnet\" in server.protocols }  "
                                    + "public_servers[server] {     some server in input.servers      "
                                    + "some port in server.ports     some input_port in input.ports     port == input_port.id      "
                                    + "some input_network in input.networks     input_port.network == input_network.id     input_network.public }\"\n"
                                    + "}"))) @RequestBody @Valid PolicyValidationRequest policyValidationRequest) {
        LOG.info("Validating policy");
        var policyValidationResponse = policyService.validatePolicy(policyValidationRequest);
        LOG.info("Policy validation result: valid = {}", policyValidationResponse.getValid());
        return ResponseEntity.ok(policyValidationResponse);
    }

    /**
     * Create or update a policy.
     * 
     * @param policyId id of the policy
     * @param policyContent content of the policy
     * @return policyId of the created/updated policy
     */
    @Operation(summary = "Create or update a policy")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The id of the created/updated policy is returned.", content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content)
    })
    @PutMapping("/{id}")
    public ResponseEntity<String> createOrUpdatePolicy(@PathVariable(name = "id") String policyId, @RequestBody String policyContent) {
        LOG.debug("Create or update policy");
        String result = policyService.createOrUpdatePolicy(policyId, policyContent);

        return ResponseEntity.ok(result);
    }
}
