/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service.impl;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import com.visma.connect.hyper42.mandate.api.model.generated.ProofResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.ProofResponse.State;
import com.visma.connect.hyper42.mandate.api.rest.CreateProofRequest;
import com.visma.connect.hyper42.mandate.api.rest.ProofRestClient;
import com.visma.connect.hyper42.mandate.api.service.AuthorizationService;
import com.visma.connect.hyper42.mandate.api.service.ProofService;

/**
 * Implementation of the proof service
 *
 * @author Kevin Kerkhoven
 */
@Service
public class ProofServiceImpl implements ProofService {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(ProofServiceImpl.class);

    /**
     * The proof rest client
     */
    @Autowired
    private ProofRestClient proofRestClient;

    /**
     * The proof rest client
     */
    @Autowired
    private AuthorizationService authorizationService;

    /**
     * The simp messaging template for websocket communication
     */
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    /**
     * The Mandate API Service host
     */
    @Value("${mandate.api.url}")
    private String apiUrl;

    @Override
    public String createProofRequest(String schemaName, String proofComment, String jsonRequest, String user) {
        // Generate comment if null
        String comment = Objects.requireNonNullElseGet(proofComment, () -> "Proof request for schema " + schemaName);

        // Gather UUID for proof request
        String proofRequestUuid = proofRestClient
                .createProofRequest(CreateProofRequest.builder().withSchemaName(schemaName).withProofComment(comment)
                        .withRequestJson(jsonRequest).withUser(user).build());

        // Create url for proof request
        String url = apiUrl + "/proofs/" + proofRequestUuid;

        LOG.debug("Created proof response for schemaName {} -> url: {}", schemaName, url);

        // Create base64 UTF-8 String from url
        return Base64.getUrlEncoder().encodeToString(url.getBytes(StandardCharsets.UTF_8));
    }

    @Override
    public String retrieveProofRequest(String uuid) {
        informProofRequestScanned(uuid);
        LOG.debug("Retrieve proof request on apiUrl {}", apiUrl);
        String proofRequest = proofRestClient.retrieveProofRequest(uuid);
        String urlEncodedProofRequest = URLEncoder.encode(proofRequest, Charset.defaultCharset());
        return apiUrl + "?d_m=" + urlEncodedProofRequest + "&orig=" + apiUrl + "/proofs/" + uuid;
    }

    @Override
    public void updateState(String proofId, String state) {
        LOG.debug("Proof request: {} has been send by the owner, with state state: {}", proofId, state);
        if ("succeeded".equals(state)) {
            // when we implement login using a credential then we need to get the user id.
            // But only for authentication schema.
            // String userEmail = proofRestClient.retrieveProofRequestLoginEmail(userId);
            // String jwt = authorizationService.createToken(userEmail);

            // If there is a policy attached to this schema, get the supplied values
            // and validate it first against the policy

            sendProofState(proofId, State.SUCCEEDED);
        }
        if ("failed".equals(state)) {
            sendProofState(proofId, State.FAILED);
        } else if ("timed-out".equals(state)) {
            informTimedOut(proofId);
        } else if ("policy validation failed".equals(state)) {
            LOG.warn("Policy validation failed");
            sendProofState(proofId, State.POLICY_FAILED);
        } else {
            LOG.debug("Ignoring because of state");
        }
    }

    private void informProofRequestScanned(String uuid) {
        LOG.debug("Proof request: {} has been scanned, returned state: {}", uuid, State.LOADING);
        sendProofState(uuid, State.LOADING);
    }

    private void informTimedOut(String uuid) {
        LOG.debug("Proof request has not been provided for {}. Operation timed out.", uuid);
        sendProofState(uuid, State.TIMED_OUT);
    }

    private void sendProofState(String uuid, State state) {
        ProofResponse proofResponse = new ProofResponse().withState(state);
        simpMessagingTemplate.convertAndSendToUser(uuid, "/present-proof/response", proofResponse);
    }

    private void sendProofState(String uuid, String responseValue) {
        ProofResponse proofResponse = new ProofResponse().withState(State.SUCCEEDED).withResponse(responseValue);
        simpMessagingTemplate.convertAndSendToUser(uuid, "/present-proof/response", proofResponse);
    }

}
