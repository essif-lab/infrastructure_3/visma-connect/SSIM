/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest.impl;

import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.json.AvailableSchemasResponse;
import com.visma.connect.hyper42.mandate.api.rest.SchemaRestClient;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Implementation of SchemaRestClient.
 *
 * @author Micha Wensveen, Lukas Nakas
 */
@Component
public class SchemaRestClientImpl implements SchemaRestClient {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(SchemaRestClientImpl.class);

    /**
     * The resttemplate
     */
    @Autowired
    private RestTemplate restTemplate;

    /**
     * The aries agent service URL
     */
    @Value("${mandate.service.url}")
    private String agentUrl;

    @Override
    public List<AvailableSchemasResponse> retrieveCreatedSchemas(boolean onlyUserSchemas) {
        String path = onlyUserSchemas ? "/schemas/" : "/schemas/all/";
        URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + path).build().toUri();
        LOG.debug("Calling agent for retrieve available schemas with URI {}", targetUrl);
        ResponseEntity<List<AvailableSchemasResponse>> response =
                restTemplate.exchange(targetUrl, HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        } else {
            LOG.warn("Error {} occurred in API service when trying to retrieve available schemas", response.getStatusCodeValue());
            throw new ApplicationRuntimeException("An error has occurred, please try again later.");
        }
    }

    @Override
    public SchemaDefinitionResponse retrieveSchemaDefinition(String schemaId) {
        URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/schemas/" + schemaId).build().toUri();
        LOG.debug("Calling agent for retrieve schema definition with URI {}", targetUrl);
        ResponseEntity<SchemaDefinitionResponse> response = restTemplate.getForEntity(targetUrl, SchemaDefinitionResponse.class);
        var body = Optional.ofNullable(response.getBody());
        if (response.getStatusCode().is2xxSuccessful()) {
            if (body.isEmpty()) {
                String clientError = "Request is invalid, please review your input and try again.";
                throw new HttpClientErrorException(clientError, HttpStatus.NOT_FOUND, "Schema Definition Not Found", null, null, null);
            }
            return body.get();
        } else if (response.getStatusCode().is4xxClientError()) {
            LOG.debug("Client error occurred in API service when trying to retrieve a schema definition for schemaId  {}", schemaId);
            String clientError = body.isEmpty() ? "Request is invalid, please review your input and try again."
                    : body.get().toString();
            throw new HttpClientErrorException(clientError, response.getStatusCode(), response.getStatusCode().getReasonPhrase(), null, null, null);
        } else {
            LOG.warn("Error {} occurred in API service when trying to retrieve a schema definition for schemaId  {}", response.getStatusCodeValue(), schemaId);
            throw new ApplicationRuntimeException("An error has occurred, please try again later.");
        }
    }

    @Override
    public AddSchemaResponse addSchema(AddSchemaRequest addSchemaRequest) {
        URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/schemas").build().toUri();
        LOG.debug("Calling agent for adding new schema with URI {}", targetUrl);
        ResponseEntity<AddSchemaResponse> response = restTemplate.postForEntity(targetUrl, addSchemaRequest, AddSchemaResponse.class);

        // Errors are handled by our advice class
        // Check whether the response is OK. If not, give detailed info on a client error if available, or return a generic error message
        var body = Optional.ofNullable(response.getBody());
        if (response.getStatusCode().is2xxSuccessful() && body.isPresent()) {
            LOG.debug("New schema addition responded with status: {}", body.map(AddSchemaResponse::getStatus).orElse(""));
            return body.get();
        } else if (response.getStatusCode().is4xxClientError()) {
            LOG.debug("Client error occurred in API service when trying to add new schema -> schemaName: {}", addSchemaRequest.getName());
            var clientError = body.map(Object::toString).orElse("Request is invalid, please review your input and try again.");
            throw new HttpClientErrorException(clientError, response.getStatusCode(), response.getStatusCode().getReasonPhrase(), null, null, null);
        } else {
            LOG.error("Internal server error occurred in API service when trying to add new schema -> schemaName: {}", addSchemaRequest.getName());
            // We always throw a generic error when dealing with a server error!
            throw new ApplicationRuntimeException("A technical error has occurred, please try again later.");
        }
    }

    @Override
    public String retrieveSchemaJsonDefinition(String name) {
        URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/schemas/definitions/" + name).build().toUri();
        LOG.debug("Calling agent for retrieve schema JSON definition with URI {}", targetUrl);
        ResponseEntity<String> response = restTemplate.getForEntity(targetUrl, String.class);
        if (response.getStatusCode().is2xxSuccessful()) {
            if (response.getBody() == null) {
                String clientError = "Request is invalid, please review your input and try again.";
                throw new HttpClientErrorException(clientError, HttpStatus.NOT_FOUND, "Schema JSON Definition Not Found", null, null, null);
            }
            return response.getBody();
        } else if (response.getStatusCode().is4xxClientError()) {
            LOG.debug("Client error occurred in API service when trying to retrieve a schema JSON definition for schemaName  {}", name);
            String clientError = response.getBody() == null ? "Request is invalid, please review your input and try again."
                    : response.getBody();
            throw new HttpClientErrorException(clientError, response.getStatusCode(), response.getStatusCode().getReasonPhrase(), null, null, null);
        } else {
            LOG.warn("Error {} occurred in API service when trying to retrieve a schema JSON definition for schemaName  {}", response.getStatusCodeValue(),
                    name);
            throw new ApplicationRuntimeException("An error has occurred, please try again later.");
        }
    }

}
