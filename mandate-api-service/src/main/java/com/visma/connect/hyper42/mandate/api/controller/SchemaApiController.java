/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.json.AvailableSchemasResponse;
import com.visma.connect.hyper42.mandate.api.service.SchemaService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Rest controller to retrieve and create Mandate schemas and credential definitions.
 *
 * @author Micha Wensveen, Lukas Nakas
 */
@RestController
@RequestMapping("/schemas")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SchemaApiController {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(SchemaApiController.class);

    /**
     * Instantiate Mandate Api Service
     */
    @Autowired
    private SchemaService schemaService;

    /**
     * Get a list of all available schemas.
     * Only schema's that the mandate service created are available.
     *
     * @return List of schemas;
     */
    @Operation(summary = "Get a list of available schemas")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The list with schemas is returned.",
                    content = @Content(array = @ArraySchema(schema = @Schema(implementation = AvailableSchemasResponse.class)), examples = @ExampleObject("[\n"
                            + "  {\n"
                            + "    \"schemaId\": \"WgWxqztrNooG92RXvxSTWv:2:schema_name:1.0\",\n"
                            + "    \"title\": \"Schema Name\",\n"
                            + "    \"name\": \"schema_name\",\n"
                            + "    \"ssicommsEnabled\": false\n"
                            + "  },\n"
                            + "  {\n"
                            + "    \"schemaId\": \"WgWxqztrNooG92RXvxSTWv: 2: another_schema_name: 2.0\",\n"
                            + "    \"title\": \"AnotherSchemaName\",\n"
                            + "    \"name\": \"another_schema_name\",\n"
                            + "    \"ssicommsEnabled\": true\n"
                            + "  }\n"
                            + "]"))),
            @ApiResponse(responseCode = "204", description = "No schemas are available"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content)
    })
    @GetMapping
    public ResponseEntity<List<AvailableSchemasResponse>> getAvailableSchemas(
            @Parameter(description = "Flag to decide if response should include all schemas or just user schemas",
                    example = "true") @RequestParam(required = false, defaultValue = "false") boolean onlyUserSchemas) {
        LOG.debug("Get all available {}schemas", onlyUserSchemas ? "user " : "");
        ResponseEntity<List<AvailableSchemasResponse>> response;
        try {
            List<AvailableSchemasResponse> schemas = schemaService.retrieveSchemas(onlyUserSchemas);
            if (schemas.isEmpty()) {
                response = ResponseEntity.noContent().build();
            } else {
                response = ResponseEntity.ok(schemas);
            }
        } catch (ApplicationRuntimeException e) {
            LOG.error("Problem retrieving available schemas", e);
            response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }

    /**
     * Get schema definition.
     *
     * @param schemaId - String
     * @return Schema definition;
     */
    @Operation(summary = "Get schema definition")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Schemas definition is returned.",
                    content = @Content(schema = @Schema(implementation = SchemaDefinitionResponse.class),
                            examples = @ExampleObject("{\n"
                                    + "  \"schemaId\": \"55MgvkQDB815vtbFky4kEt:2:mandate-schema:1.1\",\n"
                                    + "  \"name\": \"mandate-schema\",\n"
                                    + "  \"title\": \"Mandate Schema\",\n"
                                    + "  \"version\": \"1.1\",\n"
                                    + "  \"attributes\": [\n"
                                    + "    {\n"
                                    + "      \"name\": \"authorization\",\n"
                                    + "      \"title\": \"Authorization\",\n"
                                    + "      \"type\": \"text\"\n"
                                    + "    }\n"
                                    + "  ],\n"
                                    + "  \"predicates\": [\n"
                                    + "    {\n"
                                    + "      \"name\": \"spending-limit\",\n"
                                    + "      \"type\": \"number\",\n"
                                    + "      \"condition\": [\n"
                                    + "        {\n"
                                    + "          \"title\": \"Greater than\",\n"
                                    + "          \"value\": \">\"\n"
                                    + "        }\n"
                                    + "      ]\n"
                                    + "    }\n"
                                    + "  ],\n"
                                    + "  \"ssicommsEnabled\": false\n"
                                    + "  \"policy\": \"policy content\"" +
                                    "}"))),
            @ApiResponse(responseCode = "404", description = "Schema definition not found"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content)
    })
    @GetMapping("/{schemaId}")
    public ResponseEntity<SchemaDefinitionResponse> getSchemaDefinition(@PathVariable String schemaId) {
        LOG.debug("Get schema definition by schemaId: {}", schemaId);
        ResponseEntity<SchemaDefinitionResponse> response;
        try {
            SchemaDefinitionResponse schemaDefinitionResponse = schemaService.retrieveSchemaDefinition(schemaId);
            response = ResponseEntity.ok(schemaDefinitionResponse);
        } catch (HttpClientErrorException e) {
            LOG.error("Could not find schema definition by schemaId {}", schemaId, e);
            response = ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } catch (ApplicationRuntimeException e) {
            LOG.error("Problem retrieving schema definition by schemaId {}", schemaId, e);
            response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return response;
    }

    /**
     * Add schema.
     *
     * @param addSchemaRequest New schema's details
     * @return AddSchemaResponse
     */
    @Operation(summary = "Add schema")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Schema is already added",
                    content = @Content(mediaType = "application/json", schema = @Schema(type = "string"),
                            examples = @ExampleObject(name = "Status of existing schema",
                                    value = "{\n" +
                                            "  \"title\": \"Schema Title\",\n" +
                                            "  \"status\": \"already_exists\"\n" +
                                            "}",
                                    summary = "Status of existing schema"))),
            @ApiResponse(responseCode = "201", description = "Schema has been added",
                    content = @Content(mediaType = "application/json", schema = @Schema(type = "string"),
                            examples = @ExampleObject(name = "Status of schema addition",
                                    value = "{\n" +
                                            "  \"title\": \"Schema Title\",\n" +
                                            "  \"status\": \"ok\"\n" +
                                            "}",
                                    summary = "Status of schema addition"))),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error", content = @Content)
    })
    @PostMapping
    public ResponseEntity<AddSchemaResponse> addSchema(@io.swagger.v3.oas.annotations.parameters.RequestBody(description = "New schema details",
            required = true, content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                    examples = @ExampleObject(description = "json",
                            value = "{\n"
                                    + "  \"name\": \"schema-name\",\n"
                                    + "  \"title\": \"SchemaName\",\n"
                                    + "  \"version\": \"1.0\",\n"
                                    + "  \"attributes\": [\n"
                                    + "    {\n"
                                    + "      \"name\": \"attribute-name\",\n"
                                    + "      \"title\": \"AttributeName\",\n"
                                    + "      \"type\": \"number\",\n"
                                    + "      \"hasPredicates\": \"true\"\n"
                                    + "    }\n"
                                    + "  ],\n"
                                    + "  \"predicates\": [\n"
                                    + "    {\n"
                                    + "      \"name\": \"attribute-name\",\n"
                                    + "      \"type\": \"number\",\n"
                                    + "      \"conditions\": [\n"
                                    + "        {\n"
                                    + "          \"title\": \"ConditionTitle\",\n"
                                    + "          \"value\": \">\"\n"
                                    + "        }\n"
                                    + "      ]\n"
                                    + "    }\n"
                                    + "  ],\n"
                                    + "  \"ssicommsEnabled\": \"false\"\n"
                                    + "}"))) @RequestBody @Valid AddSchemaRequest addSchemaRequest) {
        LOG.debug("Adding schema with details: {}", addSchemaRequest);
        ResponseEntity<AddSchemaResponse> response;
        try {
            AddSchemaResponse addSchemaResponse = schemaService.addSchema(addSchemaRequest);
            if (addSchemaResponse.getStatus().equals("ok")) {
                LOG.debug("Schema '{}' added successfully", addSchemaResponse.getTitle());
                response = ResponseEntity.status(HttpStatus.CREATED).body(addSchemaResponse);
            } else if (addSchemaResponse.getStatus().equals("already_exists")) {
                LOG.debug("Schema '{}' already exists", addSchemaResponse.getTitle());
                response = ResponseEntity.ok(addSchemaResponse);
            } else {
                LOG.debug("Failed to add Schema '{}'", addSchemaResponse.getTitle());
                response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(addSchemaResponse);
            }
        } catch (ApplicationRuntimeException e) {
            LOG.error("Problem adding new schema", e);
            response = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return response;
    }
}
