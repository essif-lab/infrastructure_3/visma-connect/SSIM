/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest.impl;

import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.rest.CreateProofRequest;
import com.visma.connect.hyper42.mandate.api.rest.ProofRestClient;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Service Implementation class for ProofRestClient
 *
 * @author Kevin Kerkhoven
 */
@Service
public class ProofRestClientImpl implements ProofRestClient {

    /**
     * The resttemplate
     */
    @Autowired
    private RestTemplate restTemplate;

    /**
     * The aries agent service URL
     */
    @Value("${mandate.service.url}")
    private String agentUrl;

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(ProofRestClientImpl.class);

    @Override
    public String createProofRequest(CreateProofRequest createProofRequest) {
        URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/proofs/" + createProofRequest.getSchemaName())
                .queryParam("comment", createProofRequest.getProofComment())
                .queryParam("user", createProofRequest.getUser())
                .build().toUri();
        LOG.debug("Calling agent for create proof request with URI {}", targetUrl.toString());
        ResponseEntity<String> response = restTemplate.postForEntity(targetUrl, createProofRequest.getRequestJson(), String.class);

        // Errors are handled by our advice class
        // Check whether the response is OK. If not, give detailed info on a client error if available, or return a generic error message
        if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
            LOG.debug("Generated UUID for proof request: {} -> schemaName: {}, comment: {}, requestJson: {}",
                    response.getBody(), createProofRequest.getSchemaName(), createProofRequest.getProofComment(), createProofRequest.getRequestJson());
            return response.getBody();
        } else if (response.getStatusCode().is4xxClientError()) {
            LOG.debug("Client error occurred in API service when trying to create a proof request -> schemaName: {}, comment: {}, requestJson: {}",
                    createProofRequest.getSchemaName(), createProofRequest.getProofComment(), createProofRequest.getRequestJson());
            String clientError = response.getBody() == null ? "Request is invalid, please review your input and try again."
                    : response.getBody();
            throw new HttpClientErrorException(clientError, response.getStatusCode(), response.getStatusCode().getReasonPhrase(), null, null, null);
        } else {
            LOG.warn("Internal server error occurred in API service when trying to create a proof request -> schemaName: {}, comment: {}, requestJson: {}",
                    createProofRequest.getSchemaName(), createProofRequest.getProofComment(), createProofRequest.getRequestJson());
            // We always throw a generic error when dealing with a server error!
            throw new ApplicationRuntimeException("A technical error has occurred, please try again later.");
        }
    }

    @Override
    public String retrieveProofRequest(String uuid) {
        URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/proofs/" + uuid).build().toUri();
        LOG.debug("Calling agent for retrieve prood invitation with URI {}", targetUrl.toString());
        ResponseEntity<String> response = restTemplate.getForEntity(targetUrl, String.class);

        // Errors are handled by our advice class
        // Check whether the response is OK. If not, give detailed info on a client error if available, or return a generic error message
        if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
            LOG.debug("Retrieved proof request for uuid ({})  -> {}", uuid, response.getBody());
            return response.getBody();
        } else if (response.getStatusCode().is4xxClientError()) {
            LOG.debug("Client error occurred in API service when trying to retrieve a proof request for uuid  {}", uuid);
            String clientError = response.getBody() == null ? "Request is invalid, please review your input and try again."
                    : response.getBody();
            throw new HttpClientErrorException(clientError, response.getStatusCode(), response.getStatusCode().getReasonPhrase(), null, null, null);
        } else {
            LOG.error("Internal server error occurred in API service when trying to retrieve a proof request for uuid {}", uuid);
            // We always throw a generic error when dealing with a server error!
            throw new ApplicationRuntimeException("A technical error has occurred, please try again later.");
        }
    }

    @Override
    public String retrieveProofRequestLoginEmail(String uuid) {
        URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/proofs/login/" + uuid).build().toUri();
        LOG.debug("Calling agent for retrieve proof request's login email with URI {}", targetUrl);
        ResponseEntity<String> response = restTemplate.getForEntity(targetUrl, String.class);

        // Errors are handled by our advice class
        // Check whether the response is OK. If not, give detailed info on a client error if available, or return a generic error message
        if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
            LOG.debug("Retrieved proof request's login email for uuid ({})  -> {}", uuid, response.getBody());
            return response.getBody();
        } else if (response.getStatusCode().is4xxClientError()) {
            LOG.debug("Client error occurred in API service when trying to retrieve a proof request's login email for uuid  {}", uuid);
            String clientError = response.getBody() == null ? "Request is invalid, please review your input and try again."
                    : response.getBody();
            throw new HttpClientErrorException(clientError, response.getStatusCode(), response.getStatusCode().getReasonPhrase(), null, null, null);
        } else {
            LOG.error("Internal server error occurred in API service when trying to retrieve a proof request's login email for uuid {}", uuid);
            // We always throw a generic error when dealing with a server error!
            throw new ApplicationRuntimeException("A technical error has occurred, please try again later.");
        }
    }
}
