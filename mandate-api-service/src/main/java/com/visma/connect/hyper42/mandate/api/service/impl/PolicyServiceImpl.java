/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service.impl;

import com.visma.connect.hyper42.mandate.api.model.generated.GetPolicyResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.PolicyValidationRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.PolicyValidationResponse;
import com.visma.connect.hyper42.mandate.api.rest.PolicyRestClient;
import com.visma.connect.hyper42.mandate.api.service.PolicyService;
import com.visma.connect.hyper42.mandate.api.service.RegoValidationService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Implementation of PolicyService.
 *
 * @author Lukas Nakas
 */
@Service
public class PolicyServiceImpl implements PolicyService {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PolicyServiceImpl.class);

    /**
     * Policy rest client service
     */
    private final PolicyRestClient policyRestClientService;

    /**
     * Used to validate the Rego files
     */
    private final RegoValidationService regoValidationService;

    /**
     * Default constructor
     * 
     * @param policyRestClientService The policy rest client service
     * @param regoValidationService Rego file validation service
     */
    public PolicyServiceImpl(PolicyRestClient policyRestClientService, RegoValidationService regoValidationService) {
        this.policyRestClientService = policyRestClientService;
        this.regoValidationService = regoValidationService;
    }

    /**
     * Validate policy.
     * 
     * @param policyValidationRequest The inbound request that contains the Rego file in its body
     *
     * @return PolicyValidationResponse
     */
    @Override
    public PolicyValidationResponse validatePolicy(PolicyValidationRequest policyValidationRequest) {
        File f = null;
        try {
            f = File.createTempFile("rego", ".rego");
            try (var ous = new FileOutputStream(f)) {
                ous.write(policyValidationRequest.getPolicy().getBytes(StandardCharsets.UTF_8));
            }
            var errors = regoValidationService.checkRegoTextFileForErrors(f.getAbsolutePath());
            return new PolicyValidationResponse()
                    .withValid(errors.isEmpty())
                    .withErrors(errors);
        } catch (IOException e) {
            LOG.error("Error writing and validating policy", e);
            return null;
        } finally {
            deleteTempFile(f);
        }
    }

    static boolean deleteTempFile(File f) {
        if (f != null) {
            try {
                if (!Files.deleteIfExists(f.toPath())) {
                    LOG.warn("Could not delete temporary file {}", f.getAbsolutePath());
                } else {
                    return true;
                }
            } catch (IOException ex) {
                LOG.error("Could not delete temporary file", ex);
            }
        }
        return false;
    }

    @Override
    public String createOrUpdatePolicy(String id, String policyContent) {
        LOG.debug("Create or update policy with id: {}", id);

        policyRestClientService.createOrUpdatePolicy(id, policyContent);

        return id;
    }

    @Override
    public GetPolicyResponse getPolicy(String policyId) {
        LOG.debug("Get policy from mandate service with id: {}", policyId);

        return policyRestClientService.getPolicy(policyId);
    }

}
