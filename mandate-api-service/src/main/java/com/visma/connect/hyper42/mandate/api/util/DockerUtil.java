/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Helper functions in interacting with Docker
 */
public class DockerUtil {
    /**
     * Logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(DockerUtil.class);

    /**
     * Prepare a path to be included into a Docker file mount
     * 
     * @param path The source path of the file
     * @param mountPoint The mount point inside the container
     * @param isWindows Whether the source path should be transformed because of the code
     *            being run on Windows
     * @return The prepared mount point argument
     */
    public static String sanitizeDockerMountPath(String path, String mountPoint, boolean isWindows) {
        if (!isWindows) {
            return path + ":" + mountPoint;
        } else {
            LOG.info("Need to change path " + path + " to be mountable by Docker");
            var drivePosition = path.indexOf(":");
            if (drivePosition > 0) {
                var drive = path.substring(0, drivePosition).toLowerCase();
                var subPath = path.substring(drivePosition + 1).replaceAll("\\\\", "/");
                return "//" + drive + subPath + ":" + mountPoint;
            } else {
                LOG.warn("Could not get drive for " + path);
                return path.replaceAll("\\\\", "/") + ":" + mountPoint;
            }
        }
    }
}
