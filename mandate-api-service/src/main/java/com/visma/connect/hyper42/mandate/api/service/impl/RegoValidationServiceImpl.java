/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.PolicyValidationResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.ValidationError;
import com.visma.connect.hyper42.mandate.api.service.RegoValidationService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.ApplicationScope;

/**
 * Validate Rego files using the OPA CLI
 *
 * @author Michael van Niekerk
 */
@Service
@ApplicationScope
public class RegoValidationServiceImpl implements RegoValidationService {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(RegoValidationServiceImpl.class);

    /**
     * The Docker image to run OPA from if not found locally
     */
    private static final String OPA_DOCKER_IMAGE = "openpolicyagent/opa:edge-static";

    /**
     * The command to run OPA
     */
    private final List<String> opaCommand;

    /**
     * Whether the Docker runtime is used to run the OPA CLI application
     */
    private final boolean isDocker;

    /**
     * The JSON object mapper
     */
    private final ObjectMapper objectMapper;

    /**
     * Constructor
     *
     * @param objectMapper Jackson object mapper
     */
    public RegoValidationServiceImpl(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        // A stream with possible ways to run OPA
        Stream<ImmutablePair<Supplier<List<String>>, Boolean>> opaBins = Stream.of(
                // Run OPA as an executable on the $PATH
                ImmutablePair.of(() -> List.of("opa"), false),
                // Run OPA with the supplied executable saved in the Java resources
                ImmutablePair.of(this::setupOpaUsingJavaResource, false),
                // Run OPA inside of Docker
                ImmutablePair.of(() -> List.of("docker", "run", "--rm", OPA_DOCKER_IMAGE), true));

        // Get the first way of running the OPA CLI
        var opaBin = opaBins
                // Call supplier to get list of command arguments
                .map(b -> ImmutablePair.of(b.left.get(), b.right))
                .filter(b -> !b.left.isEmpty())
                .filter(b -> canRunOpaPath(b.left))
                .findFirst();

        if (opaBin.isPresent()) {
            var p = opaBin.get();
            this.isDocker = p.right;
            this.opaCommand = this.isDocker ? List.of("docker", "run", "--rm") : p.left;
        } else {
            throw new ApplicationRuntimeException("Can not run OPA CLI");
        }
    }

    List<String> setupOpaUsingJavaResource() {
        try {
            var tempFile = File.createTempFile("opa", "exe");
            tempFile.deleteOnExit();
            if (tempFile.setExecutable(true)) {
                var is = ClassLoader.getSystemClassLoader().getResourceAsStream("opa");
                if (is == null) {
                    var f = new File("/workspace/BOOT-INF/classes/opa");
                    if (f.exists()) {
                        is = new FileInputStream("/workspace/BOOT-INF/classes/opa");
                    } else {
                        throw new ApplicationRuntimeException("Could not get resource as stream");
                    }
                }
                Files.copy(is, Paths.get(tempFile.getAbsolutePath()), StandardCopyOption.REPLACE_EXISTING);
                var p = new ProcessBuilder("chmod", "+x", tempFile.getAbsolutePath());
                waitForProcessToEnd(p.start());
                return List.of(tempFile.getAbsolutePath());
            }
        } catch (IOException e) {
            LOG.warn("Could not create temp file", e);
        } catch (ApplicationRuntimeException e) {
            LOG.warn("Could not copy OPA resource executable", e);
        }
        return List.of();
    }

    /**
     * Check whether a text file parses correctly
     *
     * @param path The path of the file to check
     * @return The list of errors
     */
    @Override
    public List<ValidationError> checkRegoTextFileForErrors(String path) {
        try {
            var command = new ArrayList<>(opaCommand);
            if (isDocker) {
                command.addAll(List.of("-v", path + ":" + path, OPA_DOCKER_IMAGE));
            }
            command.addAll(List.of("check", "-f", "json", path));
            var p = new ProcessBuilder(command).start();
            waitForProcessToEnd(p);
            if (p.exitValue() > 0) {
                var json = new String(p.getErrorStream().readAllBytes(), StandardCharsets.UTF_8);
                var opaParseError = objectMapper.readValue(json, PolicyValidationResponse.class);
                return opaParseError.getErrors();
            }
        } catch (IOException e) {
            LOG.warn("IO Exception when trying to validate rego file", e);
            throw new ApplicationRuntimeException("Could not check file on " + path);
        }
        return List.of();
    }

    int waitForProcessToEnd(Process p) {
        try {
            return p.waitFor();
        } catch (InterruptedException e) {
            LOG.error("Interrupted when waiting for CLI process to end");
            Thread.currentThread().interrupt();
            return -1;
        }
    }

    /**
     * Check whether OPA can be run using provided path
     *
     * @param opaCommand The OPA command path and parameters needed
     * @return True if cant be run, false if can be run
     */
    private static boolean canRunOpaPath(List<String> opaCommand) {
        var canRun = false;
        try {
            var cmd = new ArrayList<>(opaCommand);
            cmd.add("version");
            var p = new ProcessBuilder().command(cmd).start();
            var ins = p.getInputStream().readAllBytes();
            var output = new String(ins, StandardCharsets.UTF_8);
            LOG.info("Output from OPA version: {}", output);
            canRun = !output.trim().isEmpty();
        } catch (IOException e) {
            LOG.warn("Could not run OPA on " + String.join(" ", opaCommand), e);
        }
        return canRun;
    }
}
