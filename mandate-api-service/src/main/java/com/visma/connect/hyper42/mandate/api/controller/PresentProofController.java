/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.controller;

import com.visma.connect.hyper42.mandate.api.service.ProofService;
import com.visma.connect.hyper42.mandate.api.util.JsonProofValidator;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller for requesting and responding to proof requests.
 * This controller also makes use of websockets for polling in order to to provide realtime responses for the proof.
 *
 * @author Kevin Kerkhoven
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/proofs")
public class PresentProofController {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(PresentProofController.class);

    /**
     * Instantiate Mandate Api Service
     */
    @Autowired
    private ProofService proofService;

    /**
     * Validator to check a Json string against a JsonSchema.
     */
    @Autowired
    private JsonProofValidator jsonProofValidator;

    /**
     * Create a proof request in order to verify a credential from the client later on by querying it's UUID.
     *
     * @deprecated RestApi call should be replaced with the websocked call.
     *
     * @param schemaName Name of used schema for verifying. Used to look up cred-def-id's in the mandate service
     * @param comment Optional comment which can be used to describe the request for the client
     * @param jsonRequest contents of requested proof
     * @return Proof request UUID
     */
    @Operation(summary = "Create a new Request for Proof")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "A new request for proof is created.",
                    content = @Content(mediaType = "text/plain", schema = @Schema(type = "string"),
                            examples = @ExampleObject(name = "Base64 encoded proof request to be used in displaying a QR-Code.",
                                    value = "aHR0cDovL2FyaWVzLWNsb3VkYWdlbnQtcnVubmVyOjgwMjA/Y19pPWV5SkFkSGx3WlNJNklDSmthV1E2YzI5Mk9rSjZRMkp6VGxsb1RYSnFTR2x4"
                                            + "V2tSVVZVRlRTR2M3YzNCbFl5OWpiMjV1WldOMGFXOXVjeTh4TGpBdmFXNTJhWFJoZEdsdmJpSXNJQ0pBYVdRaU9pQWlOR0kxTXpFM05qWXROV1"
                                            + "ExTmkwMFpUYzRMV0ZsWW1JdE1UVmpOemN4WXpJeFltWm1JaXdnSW5KbFkybHdhV1Z1ZEV0bGVYTWlPaUJiSWtjeFNtaGFja1paWldad1VuUTFl"
                                            + "akpxWlVwMFRIQjVjV1pyWldJeVVtRnBkRFZyUldkRlVGVlJOV1JYSWwwc0lDSnpaWEoyYVdObFJXNWtjRzlwYm5RaU9pQWlhSFIwY0RvdkwyRn"
                                            + "lhV1Z6TFdOc2IzVmtZV2RsYm5RdGNuVnVibVZ5T2pnd01qQWlMQ0FpYkdGaVpXd2lPaUFpYkc5allXeEJjbWxsYzBOc2IzVmtRV2RsYm5RaWZR"
                                            + "PT0=",
                                    summary = "Base64 encoded proof request"))),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @PostMapping
    @Deprecated(forRemoval = true)
    public ResponseEntity<String> createNewProofRequest(
            @Parameter(description = "The url-encoded id of the mandate-schema which needs to be proofed",
                    example = "mandate-signing-basic%3A1.0") @RequestParam String schemaName,
            @Parameter(description = "Comment to be displayed in the proof request",
                    example = "Proof required") @RequestParam(required = false) String comment,
            // @Parameter(description = "The attributes for which proof is required",
            // example = "{\"credential\":\"authorization\"},{\"credential\":\"spending-limit\",\"predicate\":\">=\",\"condition\":\"10\"}")
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "The actual mandate request (must match the layout of the mandate-schema)",
                    required = true,
                    content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            examples = @ExampleObject(description = "json",
                                    value = "[{\"credential\":\"authorization\"},{\"credential\":\"spending-limit\",\"predicate\":\">=\",\"condition\":\"10\"}]"))) @RequestBody String jsonRequest) {
        LOG.debug("Received proof request for schemaName {}, comment {} and json {}", schemaName, comment, jsonRequest);

        boolean valid = jsonProofValidator.validate(schemaName, jsonRequest);

        ResponseEntity<String> response;
        if (valid) {
            // Gather new QR url and presentation exchange URL for proof in Base64
            String responseData = proofService.createProofRequest(schemaName, comment, jsonRequest, UUID.randomUUID().toString());
            response = ResponseEntity.status(HttpStatus.CREATED).body(responseData);
        } else {
            response = ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return response;
    }

    /**
     * Retrieve the stored presentation request base on the short-url.
     *
     * @param uuid - String
     * @return the ResponseEntity
     */
    @Operation(summary = "Retrieve a stored Request for Proof")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "301", description = "A proof request if found and returned.",
                    content = @Content(mediaType = "text/plain", schema = @Schema(type = "string"),
                            examples = @ExampleObject(name = "url with the request",
                                    value = "\"http://localhost:8443?d_m=aHR0cDovL2FyaWVzLWNsb3VkYWdlbnQtcnVubmVyOjgwMjA/Y19pPWV5SkFkSGx3WlNJNklDSmthV1E2YzI5"
                                            + "Mk9rSjZRMkp6VGxsb1RYSnFTR2x4V2tSVVZVRlRTR2M3YzNCbFl5OWpiMjV1WldOMGFXOXVjeTh4TGpBdmFXNTJhWFJoZEdsdmJpSXNJQ0pBYV"
                                            + "dRaU9pQWlOR0kxTXpFM05qWXROV1ExTmkwMFpUYzRMV0ZsWW1JdE1UVmpOemN4WXpJeFltWm1JaXdnSW5KbFkybHdhV1Z1ZEV0bGVYTWlPaUJi"
                                            + "SWtjeFNtaGFja1paWldad1VuUTFlakpxWlVwMFRIQjVjV1pyWldJeVVtRnBkRFZyUldkRlVGVlJOV1JYSWwwc0lDSnpaWEoyYVdObFJXNWtjRz"
                                            + "lwYm5RaU9pQWlhSFIwY0RvdkwyRnlhV1Z6TFdOc2IzVmtZV2RsYm5RdGNuVnVibVZ5T2pnd01qQWlMQ0FpYkdGaVpXd2lPaUFpYkc5allXeEJj"
                                            + "bWxsYzBOc2IzVmtRV2RsYm5RaWZRPT0=&orig=http://localhost:8443/proofs/abcd",
                                    summary = "UrlEncode Base64 encoded proof request"))),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content)
    })
    @GetMapping("/{uuid}")
    public ResponseEntity<String> retrievePresentationRequest(@PathVariable("uuid") String uuid) {
        LOG.info("Retrieving proofrequest for {}", uuid);
        String proofRequest = proofService.retrieveProofRequest(uuid);
        LOG.debug("Proof request {}", proofRequest);
        return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY).header(HttpHeaders.LOCATION, proofRequest).build();
    }
}
