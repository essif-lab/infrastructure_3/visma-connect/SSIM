/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service.impl;

import com.visma.connect.hyper42.mandate.api.common.BadRequestException;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.AddSchemaResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.Attribute;
import com.visma.connect.hyper42.mandate.api.model.generated.Predicate;
import com.visma.connect.hyper42.mandate.api.model.generated.GetPolicyResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.SchemaDefinitionResponse;
import com.visma.connect.hyper42.mandate.api.model.generated.json.AvailableSchemasResponse;
import com.visma.connect.hyper42.mandate.api.rest.SchemaRestClient;
import com.visma.connect.hyper42.mandate.api.service.PolicyService;
import com.visma.connect.hyper42.mandate.api.service.SchemaService;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Implementation of SchemaService.
 *
 * @author Micha Wensveen, Lukas Nakas
 */
@Service
public class SchemaServiceImpl implements SchemaService {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(SchemaServiceImpl.class);

    /**
     * Mandatory SSIComms attributes.
     */
    private static final List<String> MANDATORY_SSI_COM_ATTRIBUTES = List.of("sipid-authorized-representative", "sipid-mandate-provider");

    /**
     * Client to call the rest-api for schemas.
     */
    @Autowired
    private SchemaRestClient schemaRestClient;

    /**
     * Policy service to store a policy with a schema
     */
    @Autowired
    private PolicyService policyService;

    @Override
    public List<AvailableSchemasResponse> retrieveSchemas(boolean onlyUserSchemas) {
        LOG.debug("Get schemas with rest client");
        List<AvailableSchemasResponse> schemas = schemaRestClient.retrieveCreatedSchemas(onlyUserSchemas);
        LOG.debug("Found schemas {}", schemas);
        return schemas;
    }

    @Override
    public SchemaDefinitionResponse retrieveSchemaDefinition(String schemaId) {
        LOG.debug("Get schema definition with rest client");
        SchemaDefinitionResponse schema = schemaRestClient.retrieveSchemaDefinition(schemaId);

        // get policy and add to result when found
        String policyId = schema.getName() + ':' + schema.getVersion();
        GetPolicyResponse policy = policyService.getPolicy(policyId);
        if (policy != null) {
            schema.setPolicy(policy.getContent());
        }

        return schema;
    }

    @Override
    public AddSchemaResponse addSchema(AddSchemaRequest addSchemaRequest) {
        if (Boolean.TRUE.equals(addSchemaRequest.getSsicommsEnabled())) {
            Pair<Boolean, String> validationResult = validateSSICommsSchemaRequest(addSchemaRequest);
            if (Boolean.FALSE.equals(validationResult.getLeft())) {
                // Build error response with details on what is incorrect
                throw new BadRequestException(validationResult.getRight());
            }
        }

        LOG.debug("Send new schema's details via rest client");
        AddSchemaResponse schemaResponse = schemaRestClient.addSchema(addSchemaRequest);

        if (null != addSchemaRequest.getPolicy() && !addSchemaRequest.getPolicy().isEmpty() &&
                schemaResponse.getStatus() != null && "ok".equals(schemaResponse.getStatus())) {
            SchemaDefinitionResponse schemaDefinition = retrieveSchemaDefinition(schemaResponse.getSchemaId());

            String policyId = schemaDefinition.getName() + ':' + schemaDefinition.getVersion();
            LOG.debug("Save policy accompanying schema with id: {}", policyId);
            policyService.createOrUpdatePolicy(policyId, addSchemaRequest.getPolicy());
        }

        return schemaResponse;
    }

    private Pair<Boolean, String> validateSSICommsSchemaRequest(AddSchemaRequest addSchemaRequest) {
        // Validate SSIComms enabled schema addition request for mandatory fields
        String missingAttributesErrors = getMissingAttributesErrors(addSchemaRequest.getAttributes());
        String predicatesNotEmptyErrors = getPredicatesNotEmptyErrors(addSchemaRequest.getPredicates());
        String invalidAttributesTypeErrors = getInvalidAttributesTypeErrors(addSchemaRequest.getAttributes());
        String duplicateAttributesErrors = getDuplicateAttributesErrors(addSchemaRequest.getAttributes());

        String errors = Stream.of(missingAttributesErrors, predicatesNotEmptyErrors, invalidAttributesTypeErrors, duplicateAttributesErrors)
                .filter(errorMsg -> !errorMsg.isBlank())
                .collect(Collectors.joining("; "));
        return errors.isBlank() ? Pair.of(true, "") : Pair.of(false, errors);
    }

    private static String getMissingAttributesErrors(List<Attribute> attributes) {
        return MANDATORY_SSI_COM_ATTRIBUTES.stream()
                .map(getMissingAttributeError(attributes))
                .filter(errorMsg -> !errorMsg.isBlank())
                .collect(Collectors.joining(", "));
    }

    private static Function<String, String> getMissingAttributeError(List<Attribute> attributes) {
        return ssiCommsAttribute -> attributes.stream()
                .noneMatch(attribute -> ssiCommsAttribute.equals(attribute.getName()))
                        ? String.format("missing attribute [%s]", ssiCommsAttribute)
                        : "";
    }

    private static String getPredicatesNotEmptyErrors(List<Predicate> predicates) {
        return MANDATORY_SSI_COM_ATTRIBUTES.stream()
                .map(getPredicateNotEmptyError(predicates))
                .filter(errorMsg -> !errorMsg.isBlank())
                .collect(Collectors.joining(", "));
    }

    private static Function<String, String> getPredicateNotEmptyError(List<Predicate> predicates) {
        return ssiCommsAttribute -> predicates.stream()
                .anyMatch(predicate -> ssiCommsAttribute.equals(predicate.getName()))
                        ? String.format("predicates for attribute [%s] not allowed", ssiCommsAttribute)
                        : "";
    }

    private static String getInvalidAttributesTypeErrors(List<Attribute> attributes) {
        return MANDATORY_SSI_COM_ATTRIBUTES.stream()
                .filter(ssiCommsAttribute -> attributes.stream().anyMatch(attribute -> ssiCommsAttribute.equals(attribute.getName())))
                .map(getInvalidAttributeTypeError(attributes))
                .filter(errorMsg -> !errorMsg.isBlank())
                .collect(Collectors.joining(", "));
    }

    private static Function<String, String> getInvalidAttributeTypeError(List<Attribute> attributes) {
        return ssiCommsAttribute -> attributes.stream()
                .filter(attribute -> ssiCommsAttribute.equals(attribute.getName()))
                .map(Attribute::getType)
                .noneMatch("text"::equals)
                        ? String.format("incorrect attribute [%s] type - must be type [text]", ssiCommsAttribute)
                        : "";
    }

    private static String getDuplicateAttributesErrors(List<Attribute> attributes) {
        return MANDATORY_SSI_COM_ATTRIBUTES.stream()
                .map(getDuplicateAttributeError(attributes))
                .filter(errorMsg -> !errorMsg.isBlank())
                .collect(Collectors.joining(", "));
    }

    private static Function<String, String> getDuplicateAttributeError(List<Attribute> attributes) {
        return ssiCommsAttribute -> attributes.stream()
                .filter(attribute -> ssiCommsAttribute.equals(attribute.getName()))
                .count() > 1L
                        ? String.format("attribute [%s] cannot have duplicates", ssiCommsAttribute)
                        : "";
    }

}
