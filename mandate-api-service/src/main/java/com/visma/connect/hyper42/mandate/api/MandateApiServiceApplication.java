/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main Application
 *
 * @author Kevin Kerkhoven
 */
@SuppressWarnings("PMD") // Ignore PMDs' utility class message
@SpringBootApplication
public class MandateApiServiceApplication {

    /**
     * Main method
     *
     * @param args args
     */
    public static void main(String[] args) {
        SpringApplication.run(MandateApiServiceApplication.class, args);
    }
}
