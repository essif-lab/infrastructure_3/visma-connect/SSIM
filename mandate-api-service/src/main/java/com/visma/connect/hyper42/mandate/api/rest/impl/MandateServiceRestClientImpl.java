/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.rest.impl;

import com.visma.connect.hyper42.mandate.api.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.api.model.generated.json.MandateRequest;
import com.visma.connect.hyper42.mandate.api.model.generated.json.RevokeCredentialRequest;
import com.visma.connect.hyper42.mandate.api.rest.MandateServiceRestClient;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Service Implementation class for MandateServiceRestClient
 *
 * @author Kevin Kerkhoven, Lukas Nakas
 */
@Service
public class MandateServiceRestClientImpl implements MandateServiceRestClient {

    /**
     * The resttemplate
     */
    @Autowired
    private RestTemplate restTemplate;

    /**
     * The aries agent service URL
     */
    @Value("${mandate.service.url}")
    private String agentUrl;

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(com.visma.connect.hyper42.mandate.api.rest.impl.MandateServiceRestClientImpl.class);

    /**
     * Invalid request explanation
     */
    private static final String INVALID_REQUEST = "Request is invalid, please review your input and try again.";

    /**
     * Technical error explanation
     */
    private static final String TECHNICAL_ERROR = "A technical error has occurred, please try again later.";

    @Override
    public String createMandateRequest(String schemaName, String alias, String requestJson, boolean ssicommsEnabled) {
        URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/mandates/" + schemaName)
                .query("alias={alias}")
                .query("ssicommsEnabled={ssicommsEnabled}")
                .build(alias, ssicommsEnabled);
        LOG.debug("Calling agent for create invitation with URI {}", targetUrl);
        ResponseEntity<String> response = restTemplate.postForEntity(targetUrl, requestJson, String.class);

        // Errors are handled by our advice class
        // Check whether the response is OK. If not, give detailed info on a client error if available, or return a generic error message
        if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
            LOG.debug("Generated URL for mandate request: {} -> schemaName: {}, alias: {}, requestJson: {}, ssicommsEnabled: {}", response.getBody(),
                    schemaName,
                    alias, requestJson, ssicommsEnabled);
            return response.getBody();
        } else if (response.getStatusCode().is4xxClientError()) {
            LOG.debug("Client error occurred in API service when trying to create a mandate request -> schemaName: {}, alias: {}, requestJson: {}, "
                    + "ssicommsEnabled: {}", schemaName, alias, requestJson, ssicommsEnabled);
            String clientError = response.getBody() == null ? INVALID_REQUEST : response.getBody();
            throw new HttpClientErrorException(clientError, response.getStatusCode(), response.getStatusCode().getReasonPhrase(), null, null, null);
        } else {
            LOG.error("Internal server error occurred in API service when trying to create a mandate request -> schemaName: {}, alias: {}, requestJson: {}, "
                    + "ssicommsEnabled: {}", schemaName, alias, requestJson, ssicommsEnabled);
            // We always throw a generic error when dealing with a server error!
            throw new ApplicationRuntimeException(TECHNICAL_ERROR);
        }
    }

    @Override
    public List<MandateRequest> retrieveIssuedMandates() {
        URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/mandates/issued").build().toUri();
        LOG.debug("Calling agent for retrieve issued mandates with URI {}", targetUrl);
        ResponseEntity<List<MandateRequest>> response = restTemplate.exchange(targetUrl, HttpMethod.GET, null, new ParameterizedTypeReference<>() {
        });
        // Errors are handled by our advice class
        // Check whether the response is OK. If not, give detailed info on a client error if available, or return a generic error message
        var body = Optional.ofNullable(response.getBody());
        if (response.getStatusCode().is2xxSuccessful() && body.isPresent()) {
            LOG.debug("Retrieved issued mandates -> {}", body.get());
            return body.get();
        } else if (response.getStatusCode().is4xxClientError()) {
            LOG.debug("Client error occurred in API service when trying to retrieve issued mandates");
            String clientError = body.isEmpty() ? INVALID_REQUEST : body.get().toString();
            throw new HttpClientErrorException(clientError, response.getStatusCode(), response.getStatusCode().getReasonPhrase(), null, null, null);
        } else {
            LOG.error("Internal server error occurred in API service when trying to retrieve issued mandates");
            // We always throw a generic error when dealing with a server error!
            throw new ApplicationRuntimeException(TECHNICAL_ERROR);
        }
    }

    @Override
    public String revokeCredential(String credRevId, String revRegId) {
        URI targetUrl = UriComponentsBuilder.fromUriString(agentUrl + "/mandates/revoke").build().toUri();
        LOG.debug("Calling agent for credential revocation with URI {}", targetUrl);
        RevokeCredentialRequest request = new RevokeCredentialRequest().withCredRevId(credRevId).withRevRegId(revRegId);
        ResponseEntity<String> response = restTemplate.postForEntity(targetUrl, request, String.class);

        // Errors are handled by our advice class
        // Check whether the response is OK. If not, give detailed info on a client error if available, or return a generic error message
        if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
            LOG.debug("Revoked credential with status {} for credential revocation ID: {}, revocation registry ID: {}", response.getBody(), credRevId,
                    revRegId);
            return response.getBody();
        } else if (response.getStatusCode().is4xxClientError()) {
            LOG.debug("Client error occurred in API service when trying to revoke credential -> credRevId: {}, revRegId: {}", credRevId, revRegId);
            String clientError = response.getBody() == null ? INVALID_REQUEST : response.getBody();
            throw new HttpClientErrorException(clientError, response.getStatusCode(), response.getStatusCode().getReasonPhrase(), null, null, null);
        } else {
            LOG.error("Internal server error occurred in API service when trying to revoke credential -> credRevId: {}, revRegId: {}", credRevId, revRegId);
            // We always throw a generic error when dealing with a server error!
            throw new ApplicationRuntimeException(TECHNICAL_ERROR);
        }
    }
}
