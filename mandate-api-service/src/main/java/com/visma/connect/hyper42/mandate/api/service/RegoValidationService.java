/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.api.service;

import com.visma.connect.hyper42.mandate.api.model.generated.ValidationError;

import java.util.List;

/**
 * A service that validates Rego files
 */
public interface RegoValidationService {
    /**
     * Get the list of errors of a Rego file
     * 
     * @param path Path of the rego file that needs to be validated
     *
     * @return The list of errors
     */
    List<ValidationError> checkRegoTextFileForErrors(String path);
}
