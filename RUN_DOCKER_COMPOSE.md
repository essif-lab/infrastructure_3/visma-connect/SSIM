# Run HTTPS instance on local

## Prerequisites

### JQ
For Ubuntu: ```sudo apt install jq```
For macOS: ```brew install jq```
For Windows: Install [chocolatey](https://chocolatey.org) first. Then in a terminal, run ```choco install jq```

### Netcat
For Ubuntu, macOS: Comes with the operating system
For Windows: Install [netmap](https://nmap.org/dist/nmap-7.92-setup.exe), be sure to select the option for ncat.

### NGROK Token
Create an .env file in the root folder of the project (an example .env-sample is provided). Set the NGROK_AUTH
variable in the .env file to the token you've got from your own [NGROK account](https://www.ngrok.com).

## Local start
Run ```./run_local_docker_compose.sh```.

Remember to set your user agent in your browser to be something
that's not a browser's, like "curl" for instance.
[User agent instructions for MS Edge](https://docs.microsoft.com/en-us/microsoft-edge/devtools-guide-chromium/device-mode/override-user-agent).
[User agent instructions for Google Chrome, Mozilla Firefox and Apple Safari](https://geekflare.com/change-user-agent-in-browser/).

When the script is done, it'll print out the NGROK tunnel endpoints and open a new browser window 
with the web application running on it.

When you want to stop and remove everything, run ```docker-compose down; docker-compose rm -f```.

The Spring Boot services (mandate-service, mandate-api-service, mandate-webhook) and the React application
(mandate-web-dynamic) all are served as-is from local context, which means that by changing the source
of these will cause them to be live reloaded.

## What this does

There's an [NGROK](https://ngrok.com) instance started up that tunnels an HTTPS connections to
a [Traefik](https://traefik.io) reverse proxy. 

This reverse proxy listens on the Docker daemon's events, which in turn updates its internal routing
to containers running on Docker based on Docker labels.

The docker-compose file is environmental variable heavy. These variables do get set when running the different
deployment scripts in ```./docker/scripts/```.

## Start services individually

| Services            | Command                                       | Comment                                 |
|---------------------|-----------------------------------------------|-----------------------------------------|
| Everything          | ./run_local_docker_compose.sh                 | All the services below                  |
| traefik + ngrok     | ./docker/scripts/start-traefik.sh             | Reverse proxy and HTTPS tunnel          |
| tails               | ./docker/scripts/start-tails.sh               | Tails server                            |
| acapy               | ./docker/scripts/start-acapy.sh               | Aries cloud runner. Also starts Webhook |
| mandate             | ./docker/scripts/start-mandate-service.sh     | Mandate service                         |
| mandate-api         | ./docker/scripts/start-mandate-api-service.sh | Mandate API service                     |
| mandate-web-dynamic | ./docker/scripts/start-web-app.sh             | React Web application                   |

