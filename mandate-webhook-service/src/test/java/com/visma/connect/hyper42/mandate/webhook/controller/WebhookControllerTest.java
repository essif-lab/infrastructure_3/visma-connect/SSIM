/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.webhook.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import com.visma.connect.hyper42.mandate.webhook.model.generated.Connection;
import com.visma.connect.hyper42.mandate.webhook.model.generated.IssueCredential;
import com.visma.connect.hyper42.mandate.webhook.model.generated.IssuerCredentialRevocation;
import com.visma.connect.hyper42.mandate.webhook.model.generated.V10PresentationExchange;
import com.visma.connect.hyper42.mandate.webhook.model.generated.V10PresentationExchange.Verified;
import com.visma.connect.hyper42.mandate.webhook.service.WebhookService;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WebhookControllerTest {

    @InjectMocks
    private WebhookController webhookController;

    @Mock
    private ObjectMapper mapper;

    @Mock
    private WebhookService webhookService;

    @Captor
    private ArgumentCaptor<String> connectionIdCaptor;

    @Captor
    private ArgumentCaptor<String> stateCapture;

    @Captor
    private ArgumentCaptor<String> verifiedCapture;

    @Captor
    private ArgumentCaptor<Map> proofCapture;

    @Captor
    private ArgumentCaptor<String> presentationExchangeIdCaptor;

    @Captor
    private ArgumentCaptor<IssueCredential> issueCredentialArgumentCaptor;

    @Test
    public void testConnections() {
        Connection connection = new Connection().withConnectionId("34f840fe-40a9-420b-8b0a-2fce9482ccca").withState("active").withInitiator("self")
                .withTheirRole("invitee");

        when(webhookService.processConnectionRequest(connectionIdCaptor.capture(), stateCapture.capture())).thenReturn("ok");
        ResponseEntity<String> responseEntity = webhookController.connections(connection);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals("34f840fe-40a9-420b-8b0a-2fce9482ccca", connectionIdCaptor.getValue());
        assertEquals("active", stateCapture.getValue());
    }

    @Test
    public void testConnections2() {
        Connection connection = new Connection().withConnectionId("34f840fe-40a9-420b-8b0a-2fce9482ccca").withState("active").withInitiator("self");

        when(webhookService.processConnectionRequest(connectionIdCaptor.capture(), stateCapture.capture())).thenReturn("ok");
        ResponseEntity<String> responseEntity = webhookController.connections(connection);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals("34f840fe-40a9-420b-8b0a-2fce9482ccca", connectionIdCaptor.getValue());
        assertEquals("active", stateCapture.getValue());
    }

    @Test
    public void testConnectionsNotFromSelf() {
        Connection connection =
                new Connection().withConnectionId("34f840fe-40a9-420b-8b0a-2fce9482ccca").withState("active").withInitiator("Other").withTheirRole("some");

        ResponseEntity<String> responseEntity = webhookController.connections(connection);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        verifyNoInteractions(webhookService);
    }

    @Test
    public void testConnectionsStateRequest() {
        Connection connection = new Connection().withConnectionId("34f840fe-40a9-420b-8b0a-2fce9482ccca").withState("request").withTheirRole("invitee");
        when(webhookService.processConnectionRequest(connectionIdCaptor.capture(), stateCapture.capture())).thenReturn("ok");

        ResponseEntity<String> responseEntity = webhookController.connections(connection);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals("34f840fe-40a9-420b-8b0a-2fce9482ccca", connectionIdCaptor.getValue());
        assertEquals("request", stateCapture.getValue());
    }

    @Test
    public void testConnectionsStateRequestThereRoleNotInvitee() {
        Connection connection = new Connection().withConnectionId("34f840fe-40a9-420b-8b0a-2fce9482ccca").withState("request").withTheirRole("something");

        ResponseEntity<String> responseEntity = webhookController.connections(connection);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        verifyNoInteractions(webhookService);
    }

    @Test
    public void testConnectionsStateResponse() {
        Connection connection = new Connection().withConnectionId("34f840fe-40a9-420b-8b0a-2fce9482ccca").withState("response").withInitiator("self");
        when(webhookService.processConnectionRequest(connectionIdCaptor.capture(), stateCapture.capture())).thenReturn("ok");

        ResponseEntity<String> responseEntity = webhookController.connections(connection);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertEquals("34f840fe-40a9-420b-8b0a-2fce9482ccca", connectionIdCaptor.getValue());
        assertEquals("response", stateCapture.getValue());
    }

    @Test
    public void testConnectionsStateResponseInitiathorNotSelft() {
        Connection connection = new Connection().withConnectionId("34f840fe-40a9-420b-8b0a-2fce9482ccca").withState("response").withInitiator("them");

        ResponseEntity<String> responseEntity = webhookController.connections(connection);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        verifyNoInteractions(webhookService);
    }

    @Test
    public void testConnectionsNotHandledState() {
        Connection connection = new Connection().withConnectionId("34f840fe-40a9-420b-8b0a-2fce9482ccca").withState("something").withInitiator("self");

        ResponseEntity<String> responseEntity = webhookController.connections(connection);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        verifyNoInteractions(webhookService);
    }

    @Test
    public void testIssueCredential() {
        doNothing().when(webhookService).processIssueCredential(issueCredentialArgumentCaptor.capture());

        IssueCredential issueCredential = new IssueCredential()
                .withConnectionId("someConnection")
                .withState("credential_issued")
                .withRevocRegId("revoc_reg_id123")
                .withRevocationId("revocation_id123");

        ResponseEntity<String> responseEntity = webhookController.issueCredential(issueCredential);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals("revocation_id123", issueCredentialArgumentCaptor.getValue().getRevocationId());
        assertEquals("revoc_reg_id123", issueCredentialArgumentCaptor.getValue().getRevocRegId());
        assertEquals("someConnection", issueCredentialArgumentCaptor.getValue().getConnectionId());
        assertEquals("credential_issued", issueCredentialArgumentCaptor.getValue().getState());
    }

    @Test
    public void testIssueCredentialNoProcessing() {
        ResponseEntity<String> responseEntity = webhookController.issueCredential(new IssueCredential()
                .withConnectionId("someConnection")
                .withState("credential_acked")
                .withRevocRegId("revoc_reg_id123")
                .withRevocationId("revocation_id123"));

        assertEquals(200, responseEntity.getStatusCodeValue());
        verifyNoMoreInteractions(webhookService);
    }

    @Test
    public void testReceiveMessage() {
        ResponseEntity<String> responseEntity = webhookController.receiveMessage("somemessage");
        assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testPresentProofNotVerified() {
        Mockito.verifyNoInteractions(webhookService);

        V10PresentationExchange v10PresentationExchange = getPresentation();
        v10PresentationExchange.withState("presentation_received").withPresentationExchangeId("id00001").withVerified(Verified.FALSE);

        ResponseEntity<String> responseEntity = webhookController.presentProof(v10PresentationExchange);

        assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testPresentProofVerified() {
        doNothing().when(webhookService).processProofPresentation(presentationExchangeIdCaptor.capture(), stateCapture.capture(), verifiedCapture.capture(),
                proofCapture.capture());

        V10PresentationExchange v10PresentationExchange = getPresentation();
        v10PresentationExchange.withState("verified").withThreadId("id00001").withVerified(Verified.TRUE);

        ResponseEntity<String> responseEntity = webhookController.presentProof(v10PresentationExchange);

        assertEquals(200, responseEntity.getStatusCodeValue());
        assertEquals("id00001", presentationExchangeIdCaptor.getValue());
        assertEquals("verified", stateCapture.getValue());
        assertEquals("true", verifiedCapture.getValue());
        Map<String, Object> proof = (Map<String, Object>) proofCapture.getValue().get("revealed_attr_groups");
        assertEquals("291325fa-d2b8-4788-a998-5dc024ef5905", proof.keySet().iterator().next());
    }

    @Test
    public void testPresentProofNoProcessing() {
        V10PresentationExchange v10PresentationExchange = getPresentation();
        v10PresentationExchange.setState("presentation_sent");
        v10PresentationExchange.setPresentationExchangeId("id00001");
        ResponseEntity<String> responseEntity = webhookController.presentProof(v10PresentationExchange);

        assertEquals(200, responseEntity.getStatusCodeValue());
        verifyNoMoreInteractions(webhookService);
    }

    @Test
    public void testReceiveIssueCredentialRevocation() {
        ResponseEntity<String> responseEntity =
                webhookController.receiveIssueCredentialRevocation(
                        new IssuerCredentialRevocation().withCredDefId("creddef").withCredExId("credexid").withCredRevId("credrevid").withState("issued"));

        assertEquals(200, responseEntity.getStatusCodeValue());
        verifyNoMoreInteractions(webhookService);
    }

    @Test
    public void testReceiveProblemReport() {
        ResponseEntity<String> responseEntity = webhookController.receiveProblemReport("something");
        assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testReceiveRevocationRegistry() {
        ResponseEntity<String> responseEntity = webhookController.receiveRevocationRegistry("something");
        assertEquals(200, responseEntity.getStatusCodeValue());
    }

    private V10PresentationExchange getPresentation() {
        ObjectMapper realMapper = new ObjectMapper();
        try {
            String v10PresentationExchange =
                    Resources.asCharSource(Resources.getResource("Json/V10PresentationExchange.json"), Charset.defaultCharset()).read();
            V10PresentationExchange value = realMapper.readValue(v10PresentationExchange, V10PresentationExchange.class);
            return value;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
