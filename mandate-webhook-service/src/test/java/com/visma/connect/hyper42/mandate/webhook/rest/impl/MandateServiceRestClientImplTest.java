/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.webhook.rest.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import com.visma.connect.hyper42.mandate.webhook.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.webhook.model.generated.ProofReceivedStatus;
import com.visma.connect.hyper42.mandate.webhook.model.generated.UpdateCredentialDetailsRequest;
import com.visma.connect.hyper42.mandate.webhook.model.generated.V10PresentationExchange;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
public class MandateServiceRestClientImplTest {

    private final static String URL = "http://someUrl/";

    @InjectMocks
    private MandateServiceRestClientImpl mandateServiceRestClient;

    @Mock
    private static RestTemplate restTemplate;

    @Mock
    private ObjectMapper mapper;

    @Captor
    private ArgumentCaptor<URI> uriCaptor;

    @Captor
    private ArgumentCaptor<Object> bodyCaptor;

    @Captor
    private ArgumentCaptor<HttpMethod> httpMethodArgumentCaptor;

    @Captor
    private ArgumentCaptor<Class<String>> classArgumentCaptor;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(mandateServiceRestClient, "mandateServiceUrl", URL);
    }

    @Test
    public void findMandateRequestOk() {
        String connectionId = "someConnectionId";
        String state = "accept";
        String responseJson = "{\"some\":\"json\"}";
        when(restTemplate.getForEntity(uriCaptor.capture(), any())).thenReturn(ResponseEntity.ok(responseJson));

        String response = mandateServiceRestClient.processConnection(connectionId, state);
        assertEquals(responseJson, response);

        URI restUri = uriCaptor.getValue();
        assertEquals("/mandates/" + connectionId, restUri.getPath());
        assertEquals("state=" + state, restUri.getQuery());
    }

    @Test
    public void findMandateRequestClientError() {
        String connectionId = "someConnectionId";
        String state = "accept";
        String error = "someClientError";
        when(restTemplate.getForEntity(uriCaptor.capture(), any())).thenReturn(ResponseEntity.badRequest().body(error));

        try {
            mandateServiceRestClient.processConnection(connectionId, state);
        } catch (HttpClientErrorException hcee) {
            assertEquals(error, hcee.getMessage());
        }

        URI restUri = uriCaptor.getValue();
        assertTrue(restUri.getPath().contains("/mandates/" + connectionId));
    }

    @Test
    public void findMandateRequestClientErrorUnKnown() {
        String connectionId = "someConnectionId";
        String state = "accept";
        String error = "Request is invalid, please review your input and try again.";
        when(restTemplate.getForEntity(uriCaptor.capture(), any())).thenReturn(ResponseEntity.badRequest().build());

        try {
            mandateServiceRestClient.processConnection(connectionId, state);
        } catch (HttpClientErrorException hcee) {
            assertEquals(error, hcee.getMessage());
        }

        URI restUri = uriCaptor.getValue();
        assertTrue(restUri.getPath().contains("/mandates/" + connectionId));
    }

    @Test
    public void findMandateRequestServerError() {
        String connectionId = "someConnectionId";
        String state = "accept";
        String error = "A technical error has occurred, please try again later.";
        when(restTemplate.getForEntity(uriCaptor.capture(), any())).thenReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());

        try {
            mandateServiceRestClient.processConnection(connectionId, state);
        } catch (ApplicationRuntimeException hcee) {
            assertEquals(error, hcee.getMessage());
        }

        URI restUri = uriCaptor.getValue();
        assertTrue(restUri.getPath().contains("/mandates/" + connectionId));
    }

    @Test
    public void findMandateRequestOkNoBody() {
        String connectionId = "someConnectionId";
        String state = "accept";
        String error = "A technical error has occurred, please try again later.";
        when(restTemplate.getForEntity(uriCaptor.capture(), any())).thenReturn(ResponseEntity.ok(null));

        try {
            mandateServiceRestClient.processConnection(connectionId, state);
        } catch (ApplicationRuntimeException hcee) {
            assertEquals(error, hcee.getMessage());
        }

        URI restUri = uriCaptor.getValue();
        assertEquals("/mandates/" + connectionId, restUri.getPath());
        assertEquals("state=" + state, restUri.getQuery());
    }

    @Test
    public void testProcessProofPresentation() {
        when(restTemplate.patchForObject(uriCaptor.capture(), bodyCaptor.capture(), any())).thenReturn(new ProofReceivedStatus().withStatus("ok"));

        Map<String, Object> proof = getProof();
        String result = mandateServiceRestClient.processProofPresentation("abc", "changed", "yes", proof);

        assertEquals("ok", result);
        assertEquals("http://someUrl/proofs/abc?state=changed&verified=yes", uriCaptor.getValue().toString());
        assertEquals(proof, bodyCaptor.getValue());
    }

    @Test
    public void testProcessProofPresentationReturnNoBody() {
        when(restTemplate.patchForObject(uriCaptor.capture(), bodyCaptor.capture(), any())).thenReturn(null);

        Map<String, Object> proof = getProof();
        ApplicationRuntimeException applicationRuntimeException =
                assertThrows(ApplicationRuntimeException.class, () -> mandateServiceRestClient.processProofPresentation("abc", "changed", "yes", proof));
        assertEquals("A technical error has occurred, please try again later.", applicationRuntimeException.getMessage());

        assertEquals("http://someUrl/proofs/abc?state=changed&verified=yes", uriCaptor.getValue().toString());
        assertEquals(proof, bodyCaptor.getValue());
    }

    @Test
    public void testProcessIssueCredential() {
        when(restTemplate.exchange(
                uriCaptor.capture(),
                httpMethodArgumentCaptor.capture(),
                (HttpEntity<?>) bodyCaptor.capture(),
                classArgumentCaptor.capture()))
                        .thenReturn(ResponseEntity.ok("ok"));

        String result = mandateServiceRestClient.processIssueCredential("abc", "revog_reg_id", "revocation_id", "credential_issued");
        UpdateCredentialDetailsRequest updateCredentialDetailsRequest = ((HttpEntity<UpdateCredentialDetailsRequest>) bodyCaptor.getValue()).getBody();

        assertEquals("ok", result);
        assertEquals("http://someUrl/mandates/abc", uriCaptor.getValue().toString());
        assertEquals(HttpMethod.PUT, httpMethodArgumentCaptor.getValue());
        assertEquals("abc", updateCredentialDetailsRequest.getConnectionId());
        assertEquals("credential_issued", updateCredentialDetailsRequest.getState());
        assertEquals("revog_reg_id", updateCredentialDetailsRequest.getRevocRegId());
        assertEquals("revocation_id", updateCredentialDetailsRequest.getRevocationId());
    }

    @Test
    public void testProcessIssueCredentialReturnNoBody() {
        when(restTemplate.exchange(
                uriCaptor.capture(),
                httpMethodArgumentCaptor.capture(),
                (HttpEntity<?>) bodyCaptor.capture(),
                classArgumentCaptor.capture()))
                        .thenReturn(ResponseEntity.ok().build());

        ApplicationRuntimeException applicationRuntimeException =
                assertThrows(ApplicationRuntimeException.class,
                        () -> mandateServiceRestClient.processIssueCredential("abc", "revog_reg_id", "revocation_id", "credential_issued"));
        UpdateCredentialDetailsRequest updateCredentialDetailsRequest = ((HttpEntity<UpdateCredentialDetailsRequest>) bodyCaptor.getValue()).getBody();

        assertEquals("A technical error has occurred, please try again later.", applicationRuntimeException.getMessage());
        assertEquals("http://someUrl/mandates/abc", uriCaptor.getValue().toString());
        assertEquals(HttpMethod.PUT, httpMethodArgumentCaptor.getValue());
        assertEquals("abc", updateCredentialDetailsRequest.getConnectionId());
        assertEquals("credential_issued", updateCredentialDetailsRequest.getState());
        assertEquals("revog_reg_id", updateCredentialDetailsRequest.getRevocRegId());
        assertEquals("revocation_id", updateCredentialDetailsRequest.getRevocationId());
    }

    @Test
    public void testProcessIssueCredentialClientError() {
        String error = "oeps";
        when(restTemplate.exchange(
                uriCaptor.capture(),
                httpMethodArgumentCaptor.capture(),
                (HttpEntity<?>) bodyCaptor.capture(),
                classArgumentCaptor.capture()))
                        .thenReturn(ResponseEntity.badRequest().body(error));

        HttpClientErrorException httpClientErrorException =
                assertThrows(HttpClientErrorException.class,
                        () -> mandateServiceRestClient.processIssueCredential("abc", "revog_reg_id", "revocation_id", "credential_issued"));
        UpdateCredentialDetailsRequest updateCredentialDetailsRequest = ((HttpEntity<UpdateCredentialDetailsRequest>) bodyCaptor.getValue()).getBody();

        assertEquals(error, httpClientErrorException.getMessage());
        assertEquals("http://someUrl/mandates/abc", uriCaptor.getValue().toString());
        assertEquals(HttpMethod.PUT, httpMethodArgumentCaptor.getValue());
        assertEquals("abc", updateCredentialDetailsRequest.getConnectionId());
        assertEquals("credential_issued", updateCredentialDetailsRequest.getState());
        assertEquals("revog_reg_id", updateCredentialDetailsRequest.getRevocRegId());
        assertEquals("revocation_id", updateCredentialDetailsRequest.getRevocationId());
    }

    @Test
    public void testProcessIssueCredentialClientErrorWithoutMessage() {
        when(restTemplate.exchange(
                uriCaptor.capture(),
                httpMethodArgumentCaptor.capture(),
                (HttpEntity<?>) bodyCaptor.capture(),
                classArgumentCaptor.capture()))
                        .thenReturn(ResponseEntity.badRequest().build());

        HttpClientErrorException httpClientErrorException =
                assertThrows(HttpClientErrorException.class,
                        () -> mandateServiceRestClient.processIssueCredential("abc", "revog_reg_id", "revocation_id", "credential_issued"));
        UpdateCredentialDetailsRequest updateCredentialDetailsRequest = ((HttpEntity<UpdateCredentialDetailsRequest>) bodyCaptor.getValue()).getBody();

        assertEquals("Request is invalid, please review your input and try again.", httpClientErrorException.getMessage());
        assertEquals("http://someUrl/mandates/abc", uriCaptor.getValue().toString());
        assertEquals(HttpMethod.PUT, httpMethodArgumentCaptor.getValue());
        assertEquals("abc", updateCredentialDetailsRequest.getConnectionId());
        assertEquals("credential_issued", updateCredentialDetailsRequest.getState());
        assertEquals("revog_reg_id", updateCredentialDetailsRequest.getRevocRegId());
        assertEquals("revocation_id", updateCredentialDetailsRequest.getRevocationId());
    }

    @Test
    public void testProcessIssueCredentialServerError() {
        String error = "oeps";
        when(restTemplate.exchange(
                uriCaptor.capture(),
                httpMethodArgumentCaptor.capture(),
                (HttpEntity<?>) bodyCaptor.capture(),
                classArgumentCaptor.capture()))
                        .thenReturn(ResponseEntity.status(500).body(error));

        ApplicationRuntimeException applicationRuntimeException =
                assertThrows(ApplicationRuntimeException.class,
                        () -> mandateServiceRestClient.processIssueCredential("abc", "revog_reg_id", "revocation_id", "credential_issued"));
        UpdateCredentialDetailsRequest updateCredentialDetailsRequest = ((HttpEntity<UpdateCredentialDetailsRequest>) bodyCaptor.getValue()).getBody();

        assertEquals("A technical error has occurred, please try again later.", applicationRuntimeException.getMessage());
        assertEquals("http://someUrl/mandates/abc", uriCaptor.getValue().toString());
        assertEquals(HttpMethod.PUT, httpMethodArgumentCaptor.getValue());
        assertEquals("abc", updateCredentialDetailsRequest.getConnectionId());
        assertEquals("credential_issued", updateCredentialDetailsRequest.getState());
        assertEquals("revog_reg_id", updateCredentialDetailsRequest.getRevocRegId());
        assertEquals("revocation_id", updateCredentialDetailsRequest.getRevocationId());
    }

    private Map<String, Object> getProof() {
        ObjectMapper realMapper = new ObjectMapper();
        try {
            String v10PresentationExchange =
                    Resources.asCharSource(Resources.getResource("Json/V10PresentationExchange.json"), Charset.defaultCharset()).read();
            V10PresentationExchange value = realMapper.readValue(v10PresentationExchange, V10PresentationExchange.class);
            Map<String, Object> proof = (Map<String, Object>) value.getPresentation().getAdditionalProperties().get("requested_proof");
            return proof;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
