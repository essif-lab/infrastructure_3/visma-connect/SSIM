/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.webhook.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import com.visma.connect.hyper42.mandate.webhook.model.AuthorizationSchemaResponse;
import com.visma.connect.hyper42.mandate.webhook.model.generated.AuthorizationSchema;
import com.visma.connect.hyper42.mandate.webhook.model.generated.CredentialDefinitionRequest;
import com.visma.connect.hyper42.mandate.webhook.model.generated.CredentialDefinitionResponse;
import com.visma.connect.hyper42.mandate.webhook.model.generated.IssueCredential;
import com.visma.connect.hyper42.mandate.webhook.model.generated.Schema;
import com.visma.connect.hyper42.mandate.webhook.model.generated.SchemaResponse;
import com.visma.connect.hyper42.mandate.webhook.rest.MandateAgentRestClient;
import com.visma.connect.hyper42.mandate.webhook.rest.MandateServiceRestClient;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class WebhookServiceImplTest {

    @InjectMocks
    private WebhookServiceImpl webhookService;

    @Mock
    private MandateAgentRestClient mandateAgentRestClient;

    @Mock
    private MandateServiceRestClient mandateServiceRestClient;

    @Captor
    private ArgumentCaptor<String> connectionIdCaptor;

    @Captor
    private ArgumentCaptor<AuthorizationSchema> authorizationSchemaArgumentCaptor;

    @Captor
    private ArgumentCaptor<CredentialDefinitionRequest> definitionRequestArgumentCaptor;

    @Captor
    private ArgumentCaptor<String> stateCaptor;

    @Captor
    private ArgumentCaptor<String> verifiedCaptor;

    @Captor
    private ArgumentCaptor<Map> proofCaptor;

    @Captor
    private ArgumentCaptor<String> presentationExchangeIdCaptor;

    @Captor
    private ArgumentCaptor<String> revRegIdArgumentCaptor;

    @Captor
    private ArgumentCaptor<String> credRevIdArgumentCaptor;

    @Test
    public void testCreateSchemaAndDefinition() {
        when(mandateAgentRestClient.createSchema(authorizationSchemaArgumentCaptor.capture())).thenReturn(createSchemaResponse());
        when(mandateAgentRestClient.createCredentialDefinition(definitionRequestArgumentCaptor.capture())).thenReturn(createCredentialDefinitionResponse());

        AuthorizationSchemaResponse response = webhookService.createSchemaAndDefinition(createauthorizationSchema());

        assertNotNull(response);
        assertEquals("somecredentialid", response.getCredentialDefinitionId());
        assertEquals("someSchemaId", response.getSchemaId());

        AuthorizationSchema authorizationSchema = authorizationSchemaArgumentCaptor.getValue();
        assertEquals("someschema", authorizationSchema.getSchemaName());
        assertEquals("1.0", authorizationSchema.getSchemaVersion());
        assertEquals(2, authorizationSchema.getAttributes().size());
        assertTrue(authorizationSchema.getAttributes().contains("signature"));
        assertTrue(authorizationSchema.getAttributes().contains("representativeIdentifier"));

        CredentialDefinitionRequest credentialDefinitionRequest = definitionRequestArgumentCaptor.getValue();
        assertEquals("someSchemaId", credentialDefinitionRequest.getSchemaId());
        assertEquals("default", credentialDefinitionRequest.getTag());
        assertEquals(1000, credentialDefinitionRequest.getRevocationRegistrySize());
        assertTrue(credentialDefinitionRequest.getSupportRevocation());
    }

    @Test
    public void testProcessConnectionRequest() {
        String result = "success";
        String connectionId = "someConnectionId";
        String state = "accept";
        when(mandateServiceRestClient.processConnection(connectionIdCaptor.capture(), stateCaptor.capture())).thenReturn(result);

        String response = webhookService.processConnectionRequest(connectionId, state);

        assertNotNull(response);
        assertEquals(result, response);
        assertEquals(connectionId, connectionIdCaptor.getValue());
        assertEquals(state, stateCaptor.getValue());
    }

    private SchemaResponse createSchemaResponse() {
        SchemaResponse schemaResponse = new SchemaResponse();
        Schema schema = new Schema();
        schema.setId("someId");
        schema.setName("someName");
        schema.setSeqNo(10L);
        schema.setVer("1.0");
        schema.setVersion("1.0");
        schema.setAttrNames(Arrays.asList("signature", "representativeIdentifier"));
        schemaResponse.setSchema(schema);
        schemaResponse.setSchemaId("someSchemaId");
        return schemaResponse;
    }

    private AuthorizationSchema createauthorizationSchema() {
        List<String> attrList = Arrays.asList("signature", "representativeIdentifier");
        AuthorizationSchema authorizationSchema = new AuthorizationSchema();
        authorizationSchema.setAttributes(attrList);
        authorizationSchema.setSchemaName("someschema");
        authorizationSchema.setSchemaVersion("1.0");
        return authorizationSchema;
    }

    private CredentialDefinitionResponse createCredentialDefinitionResponse() {
        CredentialDefinitionResponse credentialDefinitionResponse = new CredentialDefinitionResponse();
        credentialDefinitionResponse.setCredentialDefinitionId("somecredentialid");
        return credentialDefinitionResponse;
    }

    @Test
    public void testProcessProofPresentation() {
        when(mandateServiceRestClient.processProofPresentation(presentationExchangeIdCaptor.capture(), stateCaptor.capture(), verifiedCaptor.capture(),
                proofCaptor.capture())).thenReturn("ok");

        Map<String, Object> proof = new HashMap<>();
        webhookService.processProofPresentation("abc", "verified", "true", proof);

        assertEquals("abc", presentationExchangeIdCaptor.getValue());
        assertEquals("verified", stateCaptor.getValue());
        assertEquals("true", verifiedCaptor.getValue());
        assertEquals(proof, proofCaptor.getValue());
    }

    @Test
    public void testProcessIssueCredential() {
        when(mandateServiceRestClient.processIssueCredential(connectionIdCaptor.capture(), revRegIdArgumentCaptor.capture(), credRevIdArgumentCaptor.capture(),
                stateCaptor.capture())).thenReturn("ok");

        IssueCredential issueCredential = new IssueCredential()
                .withConnectionId("someConnection")
                .withState("credential_issued")
                .withRevocRegId("revoc_reg_id123")
                .withRevocationId("revocation_id123");

        webhookService.processIssueCredential(issueCredential);

        assertEquals("someConnection", connectionIdCaptor.getValue());
        assertEquals("credential_issued", stateCaptor.getValue());
        assertEquals("revoc_reg_id123", revRegIdArgumentCaptor.getValue());
        assertEquals("revocation_id123", credRevIdArgumentCaptor.getValue());
    }
}
