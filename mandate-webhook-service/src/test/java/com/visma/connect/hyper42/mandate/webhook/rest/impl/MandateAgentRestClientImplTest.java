/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.webhook.rest.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

import com.visma.connect.hyper42.mandate.webhook.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.webhook.model.generated.AuthorizationSchema;
import com.visma.connect.hyper42.mandate.webhook.model.generated.CredentialDefinitionRequest;
import com.visma.connect.hyper42.mandate.webhook.model.generated.CredentialDefinitionResponse;
import com.visma.connect.hyper42.mandate.webhook.model.generated.Schema;
import com.visma.connect.hyper42.mandate.webhook.model.generated.SchemaResponse;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class MandateAgentRestClientImplTest {

    private final static String URL = "http://someUrl/";

    @InjectMocks
    private MandateAgentRestClientImpl mandateAgentRestClient;

    @Mock
    private RestTemplate restTemplate;

    @Captor
    private ArgumentCaptor<AuthorizationSchema> authorizationSchemaArgumentCaptor;

    @Captor
    private ArgumentCaptor<CredentialDefinitionRequest> credentialDefinitionRequestArgumentCaptor;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(mandateAgentRestClient, "agentUrl", URL);
    }

    @Test
    public void createSchemaTest() {
        Mockito.when(restTemplate.postForEntity(anyString(), authorizationSchemaArgumentCaptor.capture(), any()))
                .thenReturn(ResponseEntity.ok().body(createSchemaResponse()));
        SchemaResponse response = mandateAgentRestClient.createSchema(createAuthorizationSchema());
        assertEquals("someSchemaId", response.getSchemaId());
        assertEquals("someId", response.getSchema().getId());
        assertEquals("someName", response.getSchema().getName());
        assertEquals("1.0", response.getSchema().getVer());
        assertEquals("1.0", response.getSchema().getVersion());
        assertEquals(10L, response.getSchema().getSeqNo());
        assertEquals("signature", response.getSchema().getAttrNames().get(0));
    }

    @Test
    public void testCreateSchemaNotOk() {
        try {
            Mockito.when(restTemplate.postForEntity(anyString(), authorizationSchemaArgumentCaptor.capture(), any()))
                    .thenReturn(ResponseEntity.badRequest().build());
            SchemaResponse response = mandateAgentRestClient.createSchema(createAuthorizationSchema());
        } catch (Exception e) {
            assertTrue(e instanceof ApplicationRuntimeException);
        }
    }

    @Test
    public void testCreateSchemaHttpClientErrorException() {
        try {
            Mockito.when(restTemplate.postForEntity(anyString(), authorizationSchemaArgumentCaptor.capture(), any()))
                    .thenThrow(new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Schema already exists on ledger, but attributes do not match"));
            SchemaResponse response = mandateAgentRestClient.createSchema(createAuthorizationSchema());
            fail();
        } catch (Exception e) {
            assertTrue(e instanceof HttpClientErrorException);
        }
    }

    @Test
    public void testCreateSchemaRestClientException() {
        try {
            Mockito.when(restTemplate.postForEntity(anyString(), authorizationSchemaArgumentCaptor.capture(), any()))
                    .thenThrow(new RestClientException("Creating schema finished unexpected"));
            SchemaResponse response = mandateAgentRestClient.createSchema(createAuthorizationSchema());
            fail();
        } catch (Exception e) {
            assertTrue(e instanceof ApplicationRuntimeException);
        }
    }

    @Test
    public void testCreateCredentialDefinition() {
        Mockito.when(restTemplate.postForEntity(anyString(), credentialDefinitionRequestArgumentCaptor.capture(), any()))
                .thenReturn(ResponseEntity.ok().body(credentialDefinitionResponse()));
        CredentialDefinitionResponse response = mandateAgentRestClient.createCredentialDefinition(credentialDefinitionRequest());
        assertEquals("someDefId", response.getCredentialDefinitionId());
    }

    @Test
    public void testCreateCredDefNotOk() {
        try {
            Mockito.when(restTemplate.postForEntity(anyString(), credentialDefinitionRequestArgumentCaptor.capture(), any()))
                    .thenReturn(ResponseEntity.badRequest().build());
            CredentialDefinitionResponse response = mandateAgentRestClient.createCredentialDefinition(credentialDefinitionRequest());
        } catch (Exception e) {
            assertTrue(e instanceof ApplicationRuntimeException);
        }
    }

    @Test
    public void testCreateDefRestClientException() {
        try {
            Mockito.when(restTemplate.postForEntity(anyString(), credentialDefinitionRequestArgumentCaptor.capture(), any()))
                    .thenThrow(new RestClientException("Creating credential definition finished unexpected"));
            CredentialDefinitionResponse response = mandateAgentRestClient.createCredentialDefinition(credentialDefinitionRequest());
            fail();
        } catch (Exception e) {
            assertTrue(e instanceof ApplicationRuntimeException);
        }
    }

    private SchemaResponse createSchemaResponse() {
        SchemaResponse schemaResponse = new SchemaResponse();
        Schema schema = new Schema();
        schema.setId("someId");
        schema.setName("someName");
        schema.setSeqNo(10L);
        schema.setVer("1.0");
        schema.setVersion("1.0");
        schema.setAttrNames(Arrays.asList("signature", "representativeIdentifier"));
        schemaResponse.setSchema(schema);
        schemaResponse.setSchemaId("someSchemaId");
        return schemaResponse;
    }

    private AuthorizationSchema createAuthorizationSchema() {
        List<String> attrList = Arrays.asList("signature", "representativeIdentifier");
        AuthorizationSchema authorizationSchema = new AuthorizationSchema();
        authorizationSchema.setAttributes(attrList);
        authorizationSchema.setSchemaName("someschema");
        authorizationSchema.setSchemaVersion("1.0");
        return authorizationSchema;
    }

    private CredentialDefinitionRequest credentialDefinitionRequest() {
        CredentialDefinitionRequest credentialDefinitionRequest = new CredentialDefinitionRequest();
        credentialDefinitionRequest.setTag("default");
        credentialDefinitionRequest.setSchemaId("someSchemaId");
        credentialDefinitionRequest.setSupportRevocation(false);
        credentialDefinitionRequest.setRevocationRegistrySize(1000L);
        return credentialDefinitionRequest;
    }

    private CredentialDefinitionResponse credentialDefinitionResponse() {
        CredentialDefinitionResponse credentialDefinitionResponse = new CredentialDefinitionResponse();
        credentialDefinitionResponse.setCredentialDefinitionId("someDefId");
        return credentialDefinitionResponse;
    }
}
