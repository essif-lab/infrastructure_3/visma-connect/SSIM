/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.webhook.common;

/**
 * Class which holds constant values
 *
 * @author ragesh
 */
public class CommonConstants {

    /**
     * Field SUPPORT_REVOCATION
     */
    public static final Long REVOCATION_REGISTRY_SIZE = 1000L;

    /**
     * Field TAG
     */
    public static final String TAG = "default";

    /**
     * Field SUPPORT_REVOCATION
     */
    public static final boolean SUPPORT_REVOCATION = true;

}
