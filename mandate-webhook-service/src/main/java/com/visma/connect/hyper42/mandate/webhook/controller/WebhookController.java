/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.webhook.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.webhook.model.generated.Connection;
import com.visma.connect.hyper42.mandate.webhook.model.generated.IssueCredential;
import com.visma.connect.hyper42.mandate.webhook.model.generated.IssuerCredentialRevocation;
import com.visma.connect.hyper42.mandate.webhook.model.generated.V10PresentationExchange;
import com.visma.connect.hyper42.mandate.webhook.service.WebhookService;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Rest controller of Mandate Webhook Service
 *
 * @author ragesh, Lukas Nakas
 */
@RestController
@RequestMapping("/webhooks/topic")
public class WebhookController {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(WebhookController.class);

    /**
     * The Constant ALLOWED_CONNECTION_STATES indicates which states need to be processed when connecting a new client.
     */
    private static final List<String> ALLOWED_CONNECTION_STATES = Arrays.asList("request", "active", "response");

    /**
     * The Constant ALLOWED_PROOF_STATES indicates which states need to be processed after verifying a proofrequest.
     */
    private static final List<String> ALLOWED_PROOF_STATES = Arrays.asList("verified");

    /**
     * The Constant ALLOWED_ISSUANCE_STATES indicates which states need to be processed after issuing a credential.
     */
    private static final List<String> ALLOWED_ISSUANCE_STATES = Arrays.asList("credential_issued");

    /**
     * ObjectMapper instantiation
     */
    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Instantiate Mandate Service
     */
    @Autowired
    private WebhookService webhookService;

    /**
     * Dummy webhook for /connections in order to remove the 404 in logging
     *
     * @param connection message
     * @return ok
     */
    @PostMapping("/connections")
    public ResponseEntity<String> connections(@RequestBody Connection connection) {
        LOG.debug("/connections: {}", connection);
        String state = connection.getState();
        String theirRole = connection.getTheirRole();
        String initiator = connection.getInitiator();
        LOG.info("Connections called for id {} with state {}, initiator {} and their role {}", connection.getConnectionId(), state, initiator, theirRole);
        if (needsProcessing(state, theirRole, initiator)) {
            webhookService.processConnectionRequest(connection.getConnectionId(), state);
        }
        return ResponseEntity.ok().build();
    }

    private boolean needsProcessing(String state, String theirRole, String initiator) {
        boolean result = false;
        if (ALLOWED_CONNECTION_STATES.contains(state)) {
            if ("self".equals(initiator)) {
                result = true;
            }
            if ("invitee".equals(theirRole)) {
                result = true;
            }
        }
        return result;
    }

    /**
     * webhook to handle connection events on the agent.
     *
     * @param issueCredential message
     * @return ok
     */
    @PostMapping("/issue_credential")
    public ResponseEntity<String> issueCredential(@RequestBody IssueCredential issueCredential) {
        LOG.debug("/issue_credential: {}", issueCredential);
        LOG.info("Issue credential called for connection {}, schema {} and credential exchange {} with state {}", issueCredential.getConnectionId(),
                issueCredential.getSchemaId(), issueCredential.getCredentialExchangeId(), issueCredential.getState());

        if (issueCredentialNeedsProcessing(issueCredential.getState())) {
            webhookService.processIssueCredential(issueCredential);
        }

        return ResponseEntity.ok().build();
    }

    private boolean issueCredentialNeedsProcessing(String state) {
        return ALLOWED_ISSUANCE_STATES.contains(state);
    }

    /**
     * webhook to handle PresentProof events on the agent.
     *
     * @param presentProof - String
     * @return the ResponseEntity
     */
    @PostMapping("/present_proof")
    public ResponseEntity<String> presentProof(@RequestBody V10PresentationExchange presentProof) {
        LOG.debug("/present_proof: ");
        try {
            LOG.debug(objectMapper.writeValueAsString(presentProof));
        } catch (JsonProcessingException e) {
            LOG.debug("parsing problem {}", e.getMessage());
        }
        String threadId = presentProof.getThreadId();
        String state = presentProof.getState();
        LOG.info("Present proof called for id {} with state {}", threadId, state);

        if (presentationProofNeedsProcessing(state)) {
            String verified = presentProof.getVerified().value();
            Map<String, Object> proof = (Map<String, Object>) presentProof.getPresentation().getAdditionalProperties().get("requested_proof");
            webhookService.processProofPresentation(threadId, state, verified, proof);
        }

        return ResponseEntity.ok().build();
    }

    private boolean presentationProofNeedsProcessing(String state) {
        return ALLOWED_PROOF_STATES.contains(state);
    }

    /**
     * Dummy webhook to handle Message events of the agent.
     *
     * @param message - String
     * @return the ResponseEntity
     */
    @PostMapping("/basicmessages")
    public ResponseEntity<String> receiveMessage(@RequestBody String message) {
        LOG.trace(message);
        return ResponseEntity.ok().build();
    }

    /**
     * Dummy webhook to handle Problem reports of the agent.
     *
     * @param message - String
     * @return the ResponseEntity
     */
    @PostMapping("/problem_report")
    public ResponseEntity<String> receiveProblemReport(@RequestBody String message) {
        LOG.warn(message);
        return ResponseEntity.ok().build();
    }

    /**
     * Dummy webhook to receive revocation_registry calls from aries-cloud-agent.
     *
     * @param message - String
     * @return the ResponseEntity
     */
    @PostMapping("/revocation_registry")
    public ResponseEntity<String> receiveRevocationRegistry(@RequestBody String message) {
        LOG.trace(message);
        return ResponseEntity.ok().build();
    }

    /**
     * Dummy webhook to receive issuer_cred_rev calls from aries-cloud-agent.
     *
     * @param message - String
     * @return the ResponseEntity
     */
    @PostMapping("/issuer_cred_rev")
    public ResponseEntity<String> receiveIssueCredentialRevocation(@RequestBody IssuerCredentialRevocation message) {
        LOG.info(message.toString());
        return ResponseEntity.ok().build();
    }
}
