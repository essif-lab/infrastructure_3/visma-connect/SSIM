/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.webhook.service.impl;

import com.visma.connect.hyper42.mandate.webhook.common.CommonConstants;
import com.visma.connect.hyper42.mandate.webhook.model.AuthorizationSchemaResponse;
import com.visma.connect.hyper42.mandate.webhook.model.generated.AuthorizationSchema;
import com.visma.connect.hyper42.mandate.webhook.model.generated.CredentialDefinitionRequest;
import com.visma.connect.hyper42.mandate.webhook.model.generated.CredentialDefinitionResponse;
import com.visma.connect.hyper42.mandate.webhook.model.generated.IssueCredential;
import com.visma.connect.hyper42.mandate.webhook.model.generated.SchemaResponse;
import com.visma.connect.hyper42.mandate.webhook.rest.MandateAgentRestClient;
import com.visma.connect.hyper42.mandate.webhook.rest.MandateServiceRestClient;
import com.visma.connect.hyper42.mandate.webhook.service.WebhookService;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service Implementation of Mandate Webhook Service
 *
 * @author ragesh, Lukas Nakas
 */
@Service
public class WebhookServiceImpl implements WebhookService {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(WebhookServiceImpl.class);

    /**
     * Instantiatie mandate agent rest client
     */
    @Autowired
    private MandateAgentRestClient mandateAgentRestClient;

    /**
     * Instantiatie mandate service rest client
     */
    @Autowired
    private MandateServiceRestClient mandateServiceRestClient;

    @Override
    public AuthorizationSchemaResponse createSchemaAndDefinition(AuthorizationSchema authorizationSchema) {
        AuthorizationSchemaResponse authorizationSchemaResponse = new AuthorizationSchemaResponse();
        SchemaResponse schemaResponse = mandateAgentRestClient.createSchema(authorizationSchema);
        CredentialDefinitionRequest credentialDefinitionRequest = formCredentialDefinitionRequest(schemaResponse.getSchemaId());
        CredentialDefinitionResponse credentialDefinitionResponse = mandateAgentRestClient.createCredentialDefinition(credentialDefinitionRequest);
        authorizationSchemaResponse.setCredentialDefinitionId(credentialDefinitionResponse.getCredentialDefinitionId());
        authorizationSchemaResponse.setSchemaId(schemaResponse.getSchemaId());
        return authorizationSchemaResponse;
    }

    @Override
    public String processConnectionRequest(String connectionId, String connectionState) {
        LOG.info("handle connection {} with state {}", connectionId, connectionState);
        String result = mandateServiceRestClient.processConnection(connectionId, connectionState);
        LOG.info("handled connection: {}", result);
        return result;
    }

    private CredentialDefinitionRequest formCredentialDefinitionRequest(String schemaId) {
        CredentialDefinitionRequest credentialDefinitionRequest = new CredentialDefinitionRequest();
        credentialDefinitionRequest.setRevocationRegistrySize(CommonConstants.REVOCATION_REGISTRY_SIZE);
        credentialDefinitionRequest.setSupportRevocation(CommonConstants.SUPPORT_REVOCATION);
        credentialDefinitionRequest.setSchemaId(schemaId);
        credentialDefinitionRequest.setTag(CommonConstants.TAG);
        return credentialDefinitionRequest;
    }

    @Override
    public void processProofPresentation(String threadId, String state, String verified, Map<String, Object> proof) {
        LOG.info("handle Proof Presentation {} with state {} with verified {} amd proofs provided", threadId, state, verified);
        String result = mandateServiceRestClient.processProofPresentation(threadId, state, verified, proof);
        LOG.info("handled Proof Presentation with result {}", result);
    }

    @Override
    public void processIssueCredential(IssueCredential issueCredential) {
        LOG.info("Handle Issue Credential {} with state {}", issueCredential.getCredentialExchangeId(), issueCredential.getState());
        String result = mandateServiceRestClient.processIssueCredential(issueCredential.getConnectionId(),
                issueCredential.getRevocRegId(), issueCredential.getRevocationId(), issueCredential.getState());
        LOG.info("Handled Issue Credential with result {}", result);
    }
}
