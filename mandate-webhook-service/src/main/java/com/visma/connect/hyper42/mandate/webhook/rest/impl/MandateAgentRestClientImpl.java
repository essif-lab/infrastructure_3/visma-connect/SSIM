/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.webhook.rest.impl;

import com.visma.connect.hyper42.mandate.webhook.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.webhook.model.generated.AuthorizationSchema;
import com.visma.connect.hyper42.mandate.webhook.model.generated.CredentialDefinitionRequest;
import com.visma.connect.hyper42.mandate.webhook.model.generated.CredentialDefinitionResponse;
import com.visma.connect.hyper42.mandate.webhook.model.generated.SchemaResponse;
import com.visma.connect.hyper42.mandate.webhook.rest.MandateAgentRestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * Service Implementation class for MandateAgentRestClient
 *
 * @author ragesh
 */
@Service
@SuppressWarnings("PMD.PreserveStackTrace")
public class MandateAgentRestClientImpl implements MandateAgentRestClient {

    /**
     * Instantiates logging for this class
     */
    private static final Logger LOG = LoggerFactory.getLogger(MandateAgentRestClientImpl.class);

    /**
     * The resttemplate
     */
    @Autowired
    private RestTemplate restTemplate;

    /**
     * The mandate aries agent service URL
     */
    @Value("${mandate.aries.agent.url}")
    private String agentUrl;

    @Override
    public SchemaResponse createSchema(AuthorizationSchema authorizationSchema) {
        try {
            ResponseEntity<SchemaResponse> restTemplateForEntity = restTemplate.postForEntity(agentUrl + "schemas", authorizationSchema,
                    SchemaResponse.class);
            if (!restTemplateForEntity.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Something went wrong while creating the schema");
            }
            return restTemplateForEntity.getBody();
        } catch (HttpClientErrorException e) {
            LOG.warn("Exception occurred while creating schema ", e);
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "Schema already exists on ledger, but attributes do not match");
        } catch (RestClientException e) {
            LOG.warn("Exception occurred while creating schema ", e);
            throw new ApplicationRuntimeException("Something went wrong while creating the schema");
        }
    }

    @Override
    public CredentialDefinitionResponse createCredentialDefinition(CredentialDefinitionRequest credentialDefinitionRequest) {
        try {
            ResponseEntity<CredentialDefinitionResponse> restTemplateForEntity = restTemplate.postForEntity(agentUrl + "credential-definitions",
                    credentialDefinitionRequest, CredentialDefinitionResponse.class);
            if (!restTemplateForEntity.getStatusCode().is2xxSuccessful()) {
                throw new ApplicationRuntimeException("Creating credential definition finished unexpected");
            }
            return restTemplateForEntity.getBody();
        } catch (RestClientException e) {
            LOG.error("Exception occurred while creating schema ", e);
            throw new ApplicationRuntimeException("Something went wrong while creating the credential definition.", e);
        }
    }

}
