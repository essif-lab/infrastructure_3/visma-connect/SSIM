/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.webhook.rest;

import java.util.Map;

/**
 * Mandate Service Rest Client Interface
 *
 * @author ragesh, Lukas Nakas
 */
public interface MandateServiceRestClient {

    /**
     * Retrieve requestJson from MandateRequest for given connectionId
     *
     * @param connectionId as string
     * @return responseJson as string
     */
    String processConnection(String connectionId, String connectionState);

    /**
     * Process proof presentation.
     *
     * @param presentationExchangeId - String
     * @param state - String
     * @param verified - String
     * @param proof - Proof as Map<String, Object>
     * @return the String
     */
    String processProofPresentation(String presentationExchangeId, String state, String verified, Map<String, Object> proof);

    /**
     * Process issue credential.
     *
     * @param connectionId - String
     * @param revRegId - String
     * @param credRevId - String
     * @param state - String
     * @return the String
     */
    String processIssueCredential(String connectionId, String revRegId, String credRevId, String state);
}
