/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.webhook.service;

import com.visma.connect.hyper42.mandate.webhook.model.AuthorizationSchemaResponse;
import com.visma.connect.hyper42.mandate.webhook.model.generated.AuthorizationSchema;
import com.visma.connect.hyper42.mandate.webhook.model.generated.IssueCredential;
import java.util.Map;

/**
 * Mandate Webhook Service Interface
 *
 * @author ragesh, Lukas Nakas
 */
public interface WebhookService {
    /**
     * Create Schema to store into ledger
     *
     * @param authorizationSchema object
     * @return SchemaResponse object
     */
    AuthorizationSchemaResponse createSchemaAndDefinition(AuthorizationSchema authorizationSchema);

    /**
     * Find temporary stored information for mandate
     *
     * @param connectionId the id to retrieve the information
     * @return the stored json
     */
    String processConnectionRequest(String connectionId, String connectionState);

    /**
     * Process proof presentation.
     *
     * @param threadId - String
     * @param state - String
     * @param verified - String
     * @param proof - Map containing the Proof.
     */
    void processProofPresentation(String threadId, String state, String verified, Map<String, Object> proof);

    /**
     * Process issue credential.
     *
     * @param issueCredential - Object
     */
    void processIssueCredential(IssueCredential issueCredential);

}
