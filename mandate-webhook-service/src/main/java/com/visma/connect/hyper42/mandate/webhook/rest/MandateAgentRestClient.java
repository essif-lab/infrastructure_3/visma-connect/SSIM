/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.webhook.rest;

import com.visma.connect.hyper42.mandate.webhook.model.generated.AuthorizationSchema;
import com.visma.connect.hyper42.mandate.webhook.model.generated.CredentialDefinitionRequest;
import com.visma.connect.hyper42.mandate.webhook.model.generated.CredentialDefinitionResponse;
import com.visma.connect.hyper42.mandate.webhook.model.generated.SchemaResponse;

/**
 * Mandate Agent Rest Client Interface
 *
 * @author ragesh
 */
public interface MandateAgentRestClient {

    /**
     * Create Schema to the ledger
     *
     * @param authorizationSchema object
     * @return SchemaResponse
     */
    SchemaResponse createSchema(AuthorizationSchema authorizationSchema);

    /**
     * Create Credential Definition to the ledger
     *
     * @param credentialDefinitionRequest object
     * @return SchemaResponse
     */
    CredentialDefinitionResponse createCredentialDefinition(CredentialDefinitionRequest credentialDefinitionRequest);

}
