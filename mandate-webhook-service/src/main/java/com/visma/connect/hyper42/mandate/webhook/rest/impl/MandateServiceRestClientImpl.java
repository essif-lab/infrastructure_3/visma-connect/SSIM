/*
 * Copyright © 2022 Visma Connect (vco.hyper42@visma.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.visma.connect.hyper42.mandate.webhook.rest.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.visma.connect.hyper42.mandate.webhook.common.ApplicationRuntimeException;
import com.visma.connect.hyper42.mandate.webhook.model.generated.ProofReceivedStatus;
import com.visma.connect.hyper42.mandate.webhook.model.generated.UpdateCredentialDetailsRequest;
import com.visma.connect.hyper42.mandate.webhook.rest.MandateServiceRestClient;
import java.net.URI;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Service Implementation class for MandateServiceRestClient
 *
 * @author Ragesh Shunmugam, Lukas Nakas
 */
@Service
public class MandateServiceRestClientImpl implements MandateServiceRestClient {

    /**
     * The logger
     */
    private static final Logger LOG = LoggerFactory.getLogger(MandateServiceRestClientImpl.class);
    /**
     * The resttemplate
     */
    @Autowired
    private RestTemplate restTemplate;
    /**
     * The objectmapper
     */
    @Autowired
    private ObjectMapper objectMapper;
    /**
     * The Mandate Service URL
     */
    @Value("${mandate.service.url}")
    private String mandateServiceUrl;

    @Override
    public String processConnection(String connectionId, String connectionState) {
        URI targetUrl = UriComponentsBuilder.fromUriString(mandateServiceUrl + "/mandates/{connectionId}?state={connectionState}").build(connectionId,
                connectionState);
        LOG.debug("Calling mandate service with getForEntity with URI {}", targetUrl.toString());
        ResponseEntity<String> response = restTemplate.getForEntity(targetUrl, String.class);

        // Errors are handled by our advice class
        // Check whether the response is OK. If not, give detailed info on a client error if available, or return a generic error message
        if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
            LOG.debug("Result json for processed connection: {} -> connectionId: {}", response.getBody(), connectionId);
            return response.getBody();
        } else if (response.getStatusCode().is4xxClientError()) {
            LOG.debug("Client error occurred in API service when trying to retrieve a mandate request -> connectionId: {}", connectionId);
            String clientError = response.getBody() == null ? "Request is invalid, please review your input and try again." : response.getBody();
            throw new HttpClientErrorException(clientError, response.getStatusCode(), response.getStatusCode().getReasonPhrase(), null, null, null);
        } else {
            LOG.error("Internal server error occurred in API service when trying to retrieve a mandate request -> connectionId: {}", connectionId);
            // We always throw a generic error when dealing with a server error!
            throw new ApplicationRuntimeException("A technical error has occurred, please try again later.");
        }
    }

    @Override
    public String processProofPresentation(String threadId, String state, String verified, Map<String, Object> proof) {
        URI targetUrl = UriComponentsBuilder.fromUriString(mandateServiceUrl + "/proofs/{threadId}")
                .queryParam("state", state)
                .queryParam("verified", verified)
                .build(threadId);
        try {
            LOG.debug("Calling mandate service with postForEn with URI {} and proof {}", targetUrl, objectMapper.writeValueAsString(proof));
        } catch (JsonProcessingException e) {
            LOG.debug("parsing problem {}", e.getMessage());
        }
        ProofReceivedStatus response = restTemplate.patchForObject(targetUrl, proof, ProofReceivedStatus.class);

        // Errors are handled by our advice class
        // Check whether the response is OK. If not, return a generic error message
        if (response != null) {
            LOG.debug("Result json for processed proof presentation: {} -> threadId: {}", response, threadId);
            return response.getStatus();
        } else {
            LOG.error("Internal server error occurred in mandate service when trying to process a proof presentation -> threadId: {}",
                    threadId);
            // We always throw a generic error when dealing with a server error!
            throw new ApplicationRuntimeException("A technical error has occurred, please try again later.");
        }
    }

    @Override
    public String processIssueCredential(String connectionId, String revRegId, String credRevId, String state) {
        URI targetUrl = UriComponentsBuilder.fromUriString(mandateServiceUrl + "/mandates/{connectionId}").build(connectionId);
        LOG.debug("Calling mandate service with exchange method PUT with URI {}", targetUrl);
        UpdateCredentialDetailsRequest updateCredentialDetailsRequest = formUpdateCredentialDetailsRequest(connectionId, revRegId, credRevId, state);
        ResponseEntity<String> response = restTemplate.exchange(targetUrl, HttpMethod.PUT, new HttpEntity<>(updateCredentialDetailsRequest), String.class);

        // Errors are handled by our advice class
        // Check whether the response is OK. If not, give detailed info on a client error if available, or return a generic error message
        if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
            LOG.debug("Result json for processed credential issuance: {} -> connectionId: {}", response.getBody(), connectionId);
            return response.getBody();
        } else if (response.getStatusCode().is4xxClientError()) {
            LOG.debug("Client error occurred in API service when trying to process a credential issuance -> connectionId: {}", connectionId);
            String clientError = response.getBody() == null ? "Request is invalid, please review your input and try again." : response.getBody();
            throw new HttpClientErrorException(clientError, response.getStatusCode(), response.getStatusCode().getReasonPhrase(), null, null, null);
        } else {
            LOG.error("Internal server error occurred in mandate service when trying to process a credential issuance -> connectionId: {}",
                    connectionId);
            // We always throw a generic error when dealing with a server error!
            throw new ApplicationRuntimeException("A technical error has occurred, please try again later.");
        }
    }

    private UpdateCredentialDetailsRequest formUpdateCredentialDetailsRequest(String connectionId, String revRegId, String credRevId, String state) {
        UpdateCredentialDetailsRequest updateCredentialDetailsRequest = new UpdateCredentialDetailsRequest();
        updateCredentialDetailsRequest.setConnectionId(connectionId);
        updateCredentialDetailsRequest.setRevocRegId(revRegId);
        updateCredentialDetailsRequest.setRevocationId(credRevId);
        updateCredentialDetailsRequest.setState(state);
        return updateCredentialDetailsRequest;
    }
}
