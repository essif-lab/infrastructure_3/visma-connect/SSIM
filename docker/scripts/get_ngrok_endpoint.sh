#!/bin/bash

name=$1
port=$2
url=$(curl --silent localhost:$port/api/tunnels |  jq -r -c '.tunnels | .[] | select ( .proto == "https" ) | .public_url ' )
if [ -z "$url" ] || [ "$url" = "null" ]; then
  echo "Waiting 5s for getting endpoint for $name to start up http://localhost:$port" >&2
  sleep 5
  url=$(curl --silent localhost:$port/api/tunnels |  jq -r -c '.tunnels | .[] | select ( .proto == "https" ) | .public_url' )
  if [ -z "$url" ] || [ "$url" = "null" ]; then
    echo "Could not get $name endpoint in 5s" >&2
    echo "Waiting 5s for getting endpoint for $name to start up http://localhost:$port" >&2
    sleep 5
    url=$(curl --silent localhost:$port/api/tunnels |  jq -r -c '.tunnels | .[] | select ( .proto == "https" ) | .public_url' )
    if [ -z "$url" ] || [ "$url" = "null" ]; then
      echo "Could not get $name endpoint in 10s" >&2
      if [ -z "$url" ] || [ "$url" = "null" ]; then
        echo "Could not get $name endpoint in 5s" >&2
        echo "Waiting 5s for getting endpoint for $name to start up http://localhost:$port" >&2
        sleep 5
        url=$(curl --silent localhost:$port/api/tunnels |  jq -r -c '.tunnels | .[] | select ( .proto == "https" ) | .public_url' )
        if [ -z "$url" ] || [ "$url" = "null" ]; then
          echo "Could not get $name endpoint in 15s" >&2
          exit 1
        fi
      fi
    fi
  fi
fi
echo "$url"