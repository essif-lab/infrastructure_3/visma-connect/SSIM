#!/bin/bash

CUR_DIR=$(pwd)
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR/../../

. .env

echo "#######################" >&2
echo "# Serenity testing    #" >&2
echo "#######################" >&2

printf '\n\e[31m%s\e[0m' "#######################" >&2
printf '\n\e[31m%s\e[0m\n' "IMPORTANT!" >&2
printf '\n\e[31m%s\e[0m' "1. PostMan tests needs to be run before this otherwise the schemas won't be available" >&2
printf '\n\e[31m%s\e[0m\n\n' "2. Login credentials needs to be in .env file (login_username & login_password)" >&2
printf '\e[31m%s\e[0m\n\n' "#######################"

export HTTPS_TRAEFIK=$(./docker/scripts/get_ngrok_endpoint.sh "Traefik" 4045)
export HTTPS_ALICE=$(./docker/scripts/get_ngrok_endpoint.sh "Alice" 4043)
export HTTPS_TRAEFIK_HOST=$(echo $HTTPS_TRAEFIK | awk '{print substr($0,9)}')

export HTTPS_MANDATE_API="${HTTPS_TRAEFIK}/api/mandate"

cd test/serenity

CMD="login_username=$login_username login_password='$login_password' mvn verify -Dwebdriver.base.url=http://localhost:3000"
CMD="$CMD -Dbackend.api.url=$HTTPS_MANDATE_API -Dalice.api.url=${HTTPS_ALICE} -Denvironment=docker_compose"

echo $CMD
eval $CMD

cd ../../

cd $CUR_DIR
echo $HTTPS_MANDATE_API