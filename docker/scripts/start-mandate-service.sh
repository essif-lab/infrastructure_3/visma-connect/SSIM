#!/bin/bash

CUR_DIR=$(pwd)
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR/../../


echo "#######################" >&2
echo "# Mandate Service     #" >&2
echo "#######################" >&2
export HTTPS_TRAEFIK=$(./docker/scripts/get_ngrok_endpoint.sh "Traefik" 4045)
export HTTPS_TRAEFIK_HOST=$(echo $HTTPS_TRAEFIK | awk '{print substr($0,9)}')
echo "Tunneling to $HTTPS_TRAEFIK_HOST" >&2
export HTTPS_ACAPY="${HTTPS_TRAEFIK}/api/acapy"
export HTTPS_TAILS="${HTTPS_TRAEFIK}/api/tails"

docker-compose up -d mandate-db

NC=${NC:-`which nc`}
if [ -x "$NC" ]; then
  echo "nc: $NC"
else
  echo "nc not found, trying ncat"
  NC=${NC:-`which ncat`}
  if [ -x "$NC" ]; then
    echo "ncat: $NC"
  else
    echo "ncat not found"
    exit 1
  fi
  exit 1
fi

until "$NC" -z localhost 27017
do
    sleep 1
done

docker-compose exec -i mandate-db sh -c 'mongoimport -d mandatedb -c schema --drop --jsonArray' < ./mandate-service/src/test/resources/mongo-seed/init.json
docker-compose exec -i mandate-db sh -c 'mongoimport -d mandatedb -c schema_definition --drop --jsonArray' < ./mandate-service/src/test/resources/mongo-seed/schemaDefs.json
docker-compose exec -i mandate-db sh -c 'mongoimport -d mandatedb -c user --drop --jsonArray' < ./mandate-service/src/test/resources/mongo-seed/users.json

docker-compose up -d mandate-service

cd $CUR_DIR