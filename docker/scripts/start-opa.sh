#!/bin/bash

CUR_DIR=$(pwd)
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR/../../

echo "#######################" >&2
echo "# OPA                 #" >&2
echo "#######################" >&2
docker-compose up -d open-policy-agent

cd $CUR_DIR