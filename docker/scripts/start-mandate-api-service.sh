#!/bin/bash

CUR_DIR=$(pwd)
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR/../../

echo "#######################" >&2
echo "# Mandate API Service #" >&2
echo "#######################" >&2
export HTTPS_TRAEFIK=$(./docker/scripts/get_ngrok_endpoint.sh "Traefik" 4045)
export HTTPS_TRAEFIK_HOST=$(echo $HTTPS_TRAEFIK | awk '{print substr($0,9)}')

export HTTPS_MANDATE_API="${HTTPS_TRAEFIK}/api/mandate"

docker-compose up -d mandate-api-service

cd $CUR_DIR
echo $HTTPS_MANDATE_API