#!/bin/bash


CUR_DIR=$(pwd)
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR/../../

echo "#######################" >&2
echo "# Acapy               #" >&2
echo "#######################" >&2
export HTTPS_TRAEFIK=$(./docker/scripts/get_ngrok_endpoint.sh "Traefik" 4045)
export HTTPS_TRAEFIK_HOST=$(echo $HTTPS_TRAEFIK | awk '{print substr($0,9)}')
export HTTPS_TAILS="${HTTPS_TRAEFIK}/api/tails"
echo "Tunneling to $HTTPS_TRAEFIK_HOST" >&2
export HTTPS_ACAPY="${HTTPS_TRAEFIK}/api/acapy"

docker-compose up -d aries-cloudagent-runner

cd $CUR_DIR
echo $HTTPS_ACAPY