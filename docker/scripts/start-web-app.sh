#!/bin/bash

CUR_DIR=$(pwd)
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR/../../

echo "#######################" >&2
echo "# Mandate Web Dynamic #" >&2
echo "#######################" >&2
export HTTPS_TRAEFIK=$(./docker/scripts/get_ngrok_endpoint.sh "Traefik" 4045)
export HTTPS_TRAEFIK_HOST=$(echo $HTTPS_TRAEFIK | awk '{print substr($0,9)}')
echo "Tunneling to $HTTPS_TRAEFIK_HOST" >&2
export HTTPS_MANDATE_API="${HTTPS_TRAEFIK}/api/mandate"

cat mandate-web-dynamic/.env.development | egrep -v "REACT_APP_DOMAIN" > mandate-web-dynamic/.env
echo "REACT_APP_DOMAIN=${HTTPS_MANDATE_API}" >> mandate-web-dynamic/.env
docker-compose up -d mandate-web-dynamic

cd $CUR_DIR
echo "http://localhost:3000"