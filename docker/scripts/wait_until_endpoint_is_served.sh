#!/bin/bash

name=$1
port=$2
echo "Waiting for $name"
WEB_APP_RUNNING=$(curl -L --silent http://localhost:$2)
until [ -n "$WEB_APP_RUNNING" ]
do
  sleep 1
  printf "."
  WEB_APP_RUNNING=$(curl -L --silent http://localhost:$2)
done
echo "$name Done"