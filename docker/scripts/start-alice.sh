#!/bin/bash

CUR_DIR=$(pwd)
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR/../../

echo "#######################" >&2
echo "# Alice               #" >&2
echo "#######################" >&2
docker-compose up -d https-alice
export HTTPS_ALICE=$(./docker/scripts/get_ngrok_endpoint.sh "Alice" 4043)
docker-compose up -d alice

cd $CUR_DIR
echo $HTTPS_ALICE