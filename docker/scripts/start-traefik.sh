#!/bin/bash


CUR_DIR=$(pwd)
SCRIPT_DIR=$(dirname $0)
cd $SCRIPT_DIR/../../
. .env

echo "#######################" >&2
echo "# Traefik             #" >&2
echo "#######################" >&2
docker-compose up -d https-traefik-proxy
export HTTPS_TRAEFIK=$(./docker/scripts/get_ngrok_endpoint.sh "Traefik" 4045)
docker-compose up -d traefik-proxy

cd $CUR_DIR
echo $HTTPS_TRAEFIK