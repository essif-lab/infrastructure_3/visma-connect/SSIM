#!/bin/bash
VON_WEB_SERVER_HOST_PORT=8000
AGENT_SEED=vonlocalseed_abcdefghijklmnioprs

echo "wait for webserver to initialize"
until $(curl --output /dev/null --silent --head --fail http://von-webserver:$VON_WEB_SERVER_HOST_PORT/genesis); do
  printf '.'
  sleep 1
done
echo -e "\nVON Web Server started"

# Register a DID based on the seed
sleep 5
echo "Register the public DID"
VON_WEBSERVER_URL="http://von-webserver:$VON_WEB_SERVER_HOST_PORT/register"
echo "calling $VON_WEBSERVER_URL with {\"alias\":\"VismaMandateService\", \"seed\":\"$AGENT_SEED\", \"role\": \"TRUST_ANCHOR\"}"
# echo curl "$VON_WEBSERVER_URL" --header "Content-Type: application/json" --request POST --data "{\"alias\":\"VismaMandateService\", \"seed\":\"$AGENT_SEED\", \"role\": \"TRUST_ANCHOR\"}"
response=""
until [[ ${response} == *"200" ]]; do
  echo "."
  response=$(curl -v "$VON_WEBSERVER_URL" --write-out "%{http_code}" --header "Content-Type: application/json" --request POST --data "{\"alias\":\"VismaMandateService\", \"seed\":\"$AGENT_SEED\", \"role\": \"TRUST_ANCHOR\"}")
  sleep 1
done
echo -e "\n${response}"