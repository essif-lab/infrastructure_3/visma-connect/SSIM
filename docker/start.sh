#!/bin/bash
echo "SDKMan init"
source /root/.sdkman/bin/sdkman-init.sh
cd /app/app
find /app
echo "mvn spring-boot:run"
mvn spring-boot:run -P autoInstallPackage -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -DdontCopyOpa=true