#!/bin/bash

START_DIR=$PWD
cd ..
BASE_DIR=$PWD

VON_WEB_SERVER_HOST_PORT=8081
VON_WEB_SERVER_HOST_PORT_INTERNAL=8000
ACA_ADMIN_PORT=8021
ACA_INBOUND_PORT=8020
WEBHOOK_PORT=8022
WEBHOOK_PORT_ALICE=9031
AGENT_SEED=vonlocalseed_abcdefghijklmnioprs
MANDATE_PORT=8030
API_PORT=8443
API_PORT_2=8444
REACT_PORT=3000
MONGO_PORT=27017
TAILS_SERVER_PORT=6543
NGROK_SERVER_PORT=4044
VON_WEB_SERVER_HOST=von_webserver_1
OPA_SERVER_PORT=8181

function startMainAgent() {
  startVonTailsServer
  startVonNetwork
  startAriesCloudAgent
}

function startVonNetwork() {
  echo ""
  echo "******************************************"
  echo " Starting von network"
  echo "******************************************"
  echo ""

  # start local von network
  cd "$BASE_DIR/von-network"
  ./manage build
  ./manage start WEB_SERVER_HOST_PORT=$VON_WEB_SERVER_HOST_PORT "LEDGER_INSTANCE_NAME=My Ledger" &

  echo "wait for webserver to initialize"
  sleep 10
  until $(curl --output /dev/null --silent --head --fail http://localhost:$VON_WEB_SERVER_HOST_PORT/genesis); do
    printf '.'
    sleep 1
  done
  echo -e "\nVON Web Server started"

  if [ -z $(docker ps --filter="name=von_webserver_1" -q | xargs) ]
  then
     echo "changing VON_WEB_SERVER_HOST"
     sleep 5
      VON_WEB_SERVER_HOST=von-webserver-1
  fi


  # Register a DID based on the seed
  sleep 5
  echo "Register the public DID"
  VON_WEBSERVER_URL="http://localhost:$VON_WEB_SERVER_HOST_PORT/register"
  echo "calling $VON_WEBSERVER_URL with {\"alias\":\"VismaMandateService\", \"seed\":\"$AGENT_SEED\", \"role\": \"TRUST_ANCHOR\"}"
  # echo curl "$VON_WEBSERVER_URL" --header "Content-Type: application/json" --request POST --data "{\"alias\":\"VismaMandateService\", \"seed\":\"$AGENT_SEED\", \"role\": \"TRUST_ANCHOR\"}"
  response=""
  until [[ ${response} == *"200" ]]; do
    echo "."
    response=$(curl "$VON_WEBSERVER_URL" --silent --fail --write-out "%{http_code}" --header "Content-Type: application/json" --request POST --data "{\"alias\":\"VismaMandateService\", \"seed\":\"$AGENT_SEED\", \"role\": \"TRUST_ANCHOR\"}")
    sleep 1
  done
  echo -e "\n${response}"

  # open webserver page
  if [[ -n "$IS_WSL" || -n "$WSL_DISTRO_NAME" ]]; then
    cmd.exe /C start http://localhost:$VON_WEB_SERVER_HOST_PORT
  elif command -v xdg-open >/dev/null; then
    xdg-open http://localhost:$VON_WEB_SERVER_HOST_PORT
  else
    start http://localhost:$VON_WEB_SERVER_HOST_PORT
  fi
}

function startAriesCloudAgent() {
  echo ""
  echo "******************************************"
  echo " Starting von Aries Cloud Agent"
  echo "******************************************"
  echo ""

  if [ -z $(docker ps --filter="name=von_webserver_1" -q | xargs) ]
  then
     echo "changing VON_WEB_SERVER_HOST"
     sleep 5
     VON_WEB_SERVER_HOST=von-webserver-1
  fi

  docker stop aries-cloudagent-runner
  docker rm aries-cloudagent-runner
  # start Aries Cloud Agent
  cd "$BASE_DIR/aries-cloudagent-python/scripts"
  PORTS="$ACA_ADMIN_PORT $ACA_INBOUND_PORT" \
    ./run_docker start \
    --endpoint http://aries-cloudagent-runner:$ACA_INBOUND_PORT \
    --label localAriesCloudAgent \
    --auto-ping-connection \
    --auto-respond-messages \
    --auto-accept-invites \
    --inbound-transport http aries-cloudagent-runner $ACA_INBOUND_PORT \
    --outbound-transport http \
    --admin aries-cloudagent-runner $ACA_ADMIN_PORT \
    --admin-insecure-mode \
    --auto-provision \
    --wallet-type indy \
    --wallet-name vonlocal \
    --wallet-key vonlocal \
    --seed $AGENT_SEED \
    --preserve-exchange-records \
    --genesis-url http://$VON_WEB_SERVER_HOST:$VON_WEB_SERVER_HOST_PORT_INTERNAL/genesis \
    --webhook-url http://mandate-webhook-service:$WEBHOOK_PORT/webhooks \
	  --tails-server-base-url http://docker_tails-server:$TAILS_SERVER_PORT \
    --trace-target log \
    --trace-tag acapy.events \
    --trace-label localAriesCloudAgent.trace &

  echo "wait for ACA admin swagger to initialize"
  sleep 10
  until $(curl --output /dev/null --silent --fail http://localhost:$ACA_ADMIN_PORT/api/doc); do
    printf '.'
    sleep 1
  done
  echo -e "\nACA admin swagger started"

  # open aries swagger
  if [[ -n "$IS_WSL" || -n "$WSL_DISTRO_NAME" ]]; then
    cmd.exe /C start http://localhost:$ACA_ADMIN_PORT
  elif command -v xdg-open >/dev/null; then
    xdg-open http://localhost:$ACA_ADMIN_PORT
  else
    start http://localhost:$ACA_ADMIN_PORT
  fi
}

function startVonTailsServer() {
  echo ""
  echo "******************************************"
  echo " Starting VON Tails server"
  echo "******************************************"
  echo ""

  docker rm docker_tails-server

  # Create von_von network if it does not exist yet
  docker network create von_von || true

  # start local von network
  cd "$BASE_DIR/indy-tails-server"
  echo ""
  echo "******************************************"
  echo " Building VON Tails server image"
  echo "******************************************"
  echo ""

  docker build -f ./docker/Dockerfile.tails-server . -t indy-tails-server

  echo ""
  echo "******************************************"
  echo " Deploying VON Tails server"
  echo "******************************************"
  echo ""
  docker run -d \
    --name docker_tails-server \
    -p $TAILS_SERVER_PORT:$TAILS_SERVER_PORT \
    --network von_von \
    indy-tails-server:latest \
    tails-server --host 0.0.0.0 \
    --port $TAILS_SERVER_PORT \
    --storage-path ledger \
    --log-level INFO

  echo -e "\nVON Tails Server started"

}

function buildAndStartAll() {
  startMainAgent
  buildAndStartMandate
  buildAndStartWebhook
  buildAndStartApi
  buildAndStartReactDynamic
}

function buildAndStartMandate() {
  echo ""
  echo "******************************************"
  echo " Building Mandate Service"
  echo "******************************************"
  echo ""
  cd "$START_DIR/mandate-service"
  mvn clean install -Dmaven.test.skip=true

  startMandate
}

function startMandate() {
  echo ""
  echo "******************************************"
  echo " (Re)Starting Mandate Service"
  echo "******************************************"
  echo ""
  docker stop mandate-service
  docker stop mandate-db
  docker rm mandate-service
  docker rm mandate-db
  docker run -d --network von_von -p $MONGO_PORT:$MONGO_PORT --name "mandate-db" mongo:latest
  docker run -d --network von_von -p $MANDATE_PORT:$MANDATE_PORT --name "mandate-service" mandate-service:0.0.1-SNAPSHOT &
  # insert schema data in to mongodb
  docker exec -i mandate-db sh -c 'mongoimport -d mandatedb -c schema --drop --jsonArray' < ./src/test/resources/mongo-seed/init.json
  docker exec -i mandate-db sh -c 'mongoimport -d mandatedb -c schema_definition --drop --jsonArray' < ./src/test/resources/mongo-seed/schemaDefs.json
  docker exec -i mandate-db sh -c 'mongoimport -d mandatedb -c user --drop --jsonArray' < ./src/test/resources/mongo-seed/users.json
  sleep 5
}

function startOpa() {
  echo ""
  echo "******************************************"
  echo " (Re)Starting Open Policy Agent (OPA)"
  echo "******************************************"
  echo ""
  docker stop open-policy-agent
  docker rm open-policy-agent
  docker run -d --network von_von -p $OPA_SERVER_PORT:$OPA_SERVER_PORT --name "open-policy-agent" openpolicyagent/opa run -s &
  sleep 5
}

function buildAndStartWebhook() {
  echo ""
  echo "******************************************"
  echo " Building Mandate Webhook Service"
  echo "******************************************"
  echo ""
  cd "$START_DIR/mandate-webhook-service"
  mvn clean install -Dmaven.test.skip=true
  startWebhook
}

function startWebhook() {
  echo ""
  echo "******************************************"
  echo " (Re)Starting Mandate Webhook Service"
  echo "******************************************"
  echo ""
  docker stop mandate-webhook-service
  docker rm mandate-webhook-service
  docker run -d --network von_von -p $WEBHOOK_PORT:$WEBHOOK_PORT --name "mandate-webhook-service" mandate-webhook-service:0.0.1-SNAPSHOT &
  sleep 5
}

function buildAndStartApi() {
  echo ""
  echo "******************************************"
  echo " Building Mandate API Service"
  echo "******************************************"
  echo ""
  cd "$START_DIR/mandate-api-service"
  mvn clean install -Dmaven.test.skip=true

  startApi
}

function startApi() {
  echo ""
  echo "******************************************"
  echo " (Re)Starting Mandate API Service"
  echo "******************************************"
  echo ""
  docker stop mandate-api-service
  docker rm mandate-api-service
  docker run -d --network von_von -p $API_PORT-$API_PORT_2:$API_PORT-$API_PORT_2 --name "mandate-api-service" mandate-api-service:0.0.1-SNAPSHOT &
  sleep 5
}

function buildAndStartReactDynamic() {
  echo ""
  echo "******************************************"
  echo " Building Mandate React Web Dynamic"
  echo "******************************************"
  echo ""
  cd "$START_DIR/mandate-web-dynamic"

  # We need the package-lock file for building the docker, so we have to run an npm install first
  npm run build-docker:dev

  startReactDynamic
}

function startReactDynamic() {
  echo ""
  echo "******************************************"
  echo " (Re)Starting Mandate React Web Dynamic"
  echo "******************************************"
  echo ""
  docker stop mandate-web-dynamic
  docker rm mandate-web-dynamic
  docker run -d --network von_von -p $REACT_PORT:$REACT_PORT --name "mandate-web-dynamic" mandate-web-dynamic:dev &
  sleep 5

  # open mandate web
  if [[ -n "$IS_WSL" || -n "$WSL_DISTRO_NAME" ]]; then
    cmd.exe /C start http://localhost:$REACT_PORT
  elif command -v xdg-open >/dev/null; then
    xdg-open http://localhost:$REACT_PORT
  else
    start http://localhost:$REACT_PORT
  fi
}

function startAliceAgent() {
  echo ""
  echo "******************************************"
  echo " Starting Alice agent"
  echo "******************************************"
  echo ""

  if [ -z $(docker ps --filter="name=von-webserver-1" -q | xargs) ]
  then
     VON_WEB_SERVER_HOST=von-webserver-1
  fi

  docker stop alice
  cd "$BASE_DIR/aries-cloudagent-python/demo"
  LEDGER_URL=http://$VON_WEB_SERVER_HOST:$VON_WEB_SERVER_HOST_PORT_INTERNAL DOCKER_NET=von_von ./run_demo alice --events --bg

  # wait for ACA admin swagger to initialize
  sleep 10
  until $(curl --output /dev/null --silent --fail http://localhost:$WEBHOOK_PORT_ALICE/api/doc); do
    printf '.'
    sleep 1
  done
  echo "Alice agent started"

  # open aries swagger
  if [[ -n "$IS_WSL" || -n "$WSL_DISTRO_NAME" ]]; then
    cmd.exe /C start http://localhost:$WEBHOOK_PORT_ALICE
  elif command -v xdg-open >/dev/null; then
    xdg-open http://localhost:$WEBHOOK_PORT_ALICE
  else
    start http://localhost:$WEBHOOK_PORT_ALICE
  fi
}

function stopAll() {
  echo ""
  echo "******************************************"
  echo " Stopping all dockers"
  echo "******************************************"
  echo ""
  cd "$BASE_DIR"
  ./von-network/manage stop
  docker stop docker_tails-server
  docker stop aries-cloudagent-runner
  docker stop mandate-service
  docker stop mandate-db
  docker stop mandate-webhook-service
  docker stop mandate-api-service
  docker stop mandate-web-dynamic
  docker stop alice
  docker stop open-policy-agent
}
function removeAll() {
  stopAll

  echo ""
  echo "******************************************"
  echo " Removing all docker container and Volumes"
  echo "******************************************"
  echo ""

  cd "$BASE_DIR"
  docker rm von_node1_1
  docker rm von_node2_1
  docker rm von_node3_1
  docker rm von_node4_1
  docker rm von_webserver_1
  docker rm docker_tails-server
  docker rm aries-cloudagent-runner
  docker rm mandate-service
  docker rm mandate-db
  docker rm mandate-webhook-service
  docker rm mandate-api-service
  docker rm mandate-web-dynamic
  docker rm open-policy-agent
  docker volume rm von_client-data von_node1-data von_node2-data von_node3-data von_node4-data von_nodes-data von_webserver-cli von_webserver-ledger
}

echo "******************************************"
echo " RUN_LOCAL SCRIPT for SSI Mandate Service"
echo "******************************************"

while true
do
    PS3='Select an option and press Enter: '
    options=("Start all services and main agent" "Start VON main agent and tails server only" "Start VON tails server only" "Build and (re)start mandate service" "Build and (re)start mandate webhook service" "Build and (re)start mandate api service" "Build and (re)start Mandate React Web-Dynamic" "Stop all services and main agent" "Remove all containers" "Extra: Start Agent for Alice" "Extra: Start agents incl. Alice" "startAriesCloudAgent" "Start Open Policy agent (OPA)" "exit")
	select opt in "${options[@]}"; do
	  case $opt in
	  "Start all services and main agent")
	    stopAll
	    buildAndStartAll
	    ;;
	  "Start VON main agent and tails server only")
	    startMainAgent
	    ;;
	  "Start VON tails server only")
	    startVonTailsServer
	    ;;
	  "Build and (re)start mandate service")
	    buildAndStartMandate
	    ;;
	  "Build and (re)start mandate webhook service")
	    buildAndStartWebhook
	    ;;
	  "Build and (re)start mandate api service")
	    buildAndStartApi
	    ;;
	  "Stop all services and main agent")
	    stopAll
	    ;;
	  "Remove all containers")
	    removeAll
	    ;;
	  "Build and (re)start Mandate React Web-Dynamic")
	    buildAndStartReactDynamic
	    ;;
	  "Extra: Start Agent for Alice")
	    startAliceAgent
	    ;;
	  "Extra: Start agents incl. Alice")
	    startMainAgent
	    startAliceAgent
	    ;;
      "startAriesCloudAgent")
        startAriesCloudAgent
        ;;
      "Start Open Policy agent (OPA)")
        startOpa
        ;;
      "exit")
	    exit
	    ;;
	  *) echo "invalid option" ;;
	  esac
      echo "Done"
      echo ""
      REPLY=
	  PS3='Select another option and press Enter: '
  done
  exit
done
