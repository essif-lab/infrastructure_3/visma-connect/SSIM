# Run Local

To run the Mandate service locally, you'll need to run a local network (the von-network), local tails-server and a local Aries-Cloud-Agent Python (ACA-PY).

With the run_local.sh script you can start these.

## Setup
Before you can use these scripts you need to take make some preparation.

1. Checkout the von-network [github](https://github.com/bcgov/von-network.git) in the same folder as this GIT project.
2. Checkout aries-cloudagent-python [github](https://github.com/hyperledger/aries-cloudagent-python) also in the same folder as this GIT project.
   A while ago, the main branch had breaking changes, but now things seem to work fine. You can either use the 0.7.0 tag (checkout tags/0.7.0, recommended) or use the main branch.
3. Change the aries-cloudagent-python/scripts/run_docker
   - Last rows should be replaced by  

> $CONTAINER_RUNTIME run --network von_von -d --name "aries-cloudagent-runner" $ARGS aries-cloudagent-run "$@"

   - For windows users change row 28 (prior, this was row 24) to use the absolute path (C:\Source\aries-cloudagent-python\ example)
    

> ARGS="${ARGS} -v C:\Source\aries-cloudagent-python\logs:/home/indy/logs"

4. Checkout von tails-server [github](https://github.com/bcgov/indy-tails-server) also in the same folder as this GIT project.

## Extra note for Linux
When you use the linux image from VITC, you have to disable the firewall.
Else containers cannot connect to eachother.

> sudo ufw disable


## Start
Now just run 
> ./run_local.sh

and select the correct option.

## Result
The result of running this script:
-  docker network von-von
-  three docker containers for von-network nodes (von_node?_1)
-  one docker container for von tails-server (docker_tails-server_1)
-  one docker container for ngrok server for tails-server (docker_ngrok-tails-server_1)
-  one docker container for von network webserver (von_webserver_1)
-  one docker container for aries cloud agent (aries-cloudagent-runner)
-  one docker container for the mandate webhook service (mandate-webhook-service)
-  one docker container for the mandate agent service (mandate-service)
-  one docker container for the mandate api service (mandate-api-service)
-  one docker container for the mandate web dynamic React app (mandate-web-dynamic)
-  one docker container for the POC web React app (poc-web)

## Alice
If you want to run the Alice agent for testing please see [Alice](./Alice.md)

